# NSW Electronics - VRS Supervisory Board - VMM

# Contents

* [Introduction]
* [Setup Overview]
* [Deployment Information]
* [Status]
* [Pictures]
* [Contact]

## Introduction
This repository is to be used as a base for the firmware which will supervise VMM/FPGA-bearing boards. It can issue test-pulses to the boards, distriute a common reference clock and a BUSY signal, and fan-out trigger signals either by generating them internally, or by accepting an external input.

## Setup Overview
The firmware is based on a Xilinx VC709 board. It features a UDP interface for communicating with a control software, and differential interfacing with the VMM-bearing front-end boards. A dedicated module of the design accepts external triggers and can fine-select which triggers to propagate to the front-end nodes based on the time-of-arrival of the trigger. This logic can be used to emulate a synchronous LHC-like DAQ system. The firmware also includes a trigger processing core, capable of reading out ART data from the VMMs, and issuing a trigger signal accordingly to read-out these events. 

## Deployment Information
In order to deploy the project, one has to source the .tcl script included using Vivado.

## Status
This project has successfully been implemented in a VC709 and tested in test-beam and test-bench scenarios.


## Pictures
![alt text](https://i.imgur.com/EpoNkGc.png)

## Contact

Questions, comments, suggestions, or help?

**Christos Bakalis**: <christos.bakalis@cern.ch>
