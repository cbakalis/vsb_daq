# Set the reference directory for source file relative paths (by default the value is script directory path)
set origin_dir [file dirname [info script]] 

# Use origin directory path location variable, if specified in the tcl shell
if { [info exists ::origin_dir_loc] } {
  set origin_dir $::origin_dir_loc
}

# Set the project name
set project_name "VSB_VMM"

# Use project name variable, if specified in the tcl shell
if { [info exists ::user_project_name] } {
  set project_name $::user_project_name
}

variable script_file
set script_file "build_VSB.tcl"

# Help information for this script
proc help {} {
  variable script_file
  puts "\nDescription:"
  puts "Recreate a Vivado project from this script. The created project will be"
  puts "functionally equivalent to the original project for which this script was"
  puts "generated. The script contains commands for creating a project, filesets,"
  puts "runs, adding/importing sources and setting properties on various objects.\n"
  puts "Syntax:"
  puts "$script_file"
  puts "$script_file -tclargs \[--origin_dir <path>\]"
  puts "$script_file -tclargs \[--project_name <name>\]"
  puts "$script_file -tclargs \[--help\]\n"
  puts "Usage:"
  puts "Name                   Description"
  puts "-------------------------------------------------------------------------"
  puts "\[--origin_dir <path>\]  Determine source file paths wrt this path. Default"
  puts "                       origin_dir path value is \".\", otherwise, the value"
  puts "                       that was set with the \"-paths_relative_to\" switch"
  puts "                       when this script was generated.\n"
  puts "\[--project_name <name>\] Create project with the specified name. Default"
  puts "                       name is the name of the project from where this"
  puts "                       script was generated.\n"
  puts "\[--help\]               Print help information for this script"
  puts "-------------------------------------------------------------------------\n"
  exit 0
}

if { $::argc > 0 } {
  for {set i 0} {$i < [llength $::argc]} {incr i} {
    set option [string trim [lindex $::argv $i]]
    switch -regexp -- $option {
      "--origin_dir"   { incr i; set origin_dir [lindex $::argv $i] }
      "--project_name" { incr i; set project_name [lindex $::argv $i] }
      "--help"         { help }
      default {
        if { [regexp {^-} $option] } {
          puts "ERROR: Unknown option '$option' specified, please type '$script_file -tclargs --help' for usage info.\n"
          return 1
        }
      }
    }
  }
}

# Create project
create_project $project_name $origin_dir/$project_name -part xc7vx690tffg1761-2

# Set the directory path for the new project
set proj_dir [get_property directory [current_project]]

# Reconstruct message rules
# None

# Set project properties
set obj [current_project]
set_property -name "board_connections" -value "" -objects $obj
set_property -name "board_part" -value "xilinx.com:vc709:part0:1.8" -objects $obj
set_property -name "compxlib.activehdl_compiled_library_dir" -value "$proj_dir/${project_name}.cache/compile_simlib/activehdl" -objects $obj
set_property -name "compxlib.funcsim" -value "1" -objects $obj
set_property -name "compxlib.ies_compiled_library_dir" -value "$proj_dir/${project_name}.cache/compile_simlib/ies" -objects $obj
set_property -name "compxlib.modelsim_compiled_library_dir" -value "$proj_dir/${project_name}.cache/compile_simlib/modelsim" -objects $obj
set_property -name "compxlib.overwrite_libs" -value "0" -objects $obj
set_property -name "compxlib.questa_compiled_library_dir" -value "$proj_dir/${project_name}.cache/compile_simlib/questa" -objects $obj
set_property -name "compxlib.riviera_compiled_library_dir" -value "$proj_dir/${project_name}.cache/compile_simlib/riviera" -objects $obj
set_property -name "compxlib.timesim" -value "1" -objects $obj
set_property -name "compxlib.vcs_compiled_library_dir" -value "$proj_dir/${project_name}.cache/compile_simlib/vcs" -objects $obj
set_property -name "compxlib.xsim_compiled_library_dir" -value "" -objects $obj
set_property -name "corecontainer.enable" -value "1" -objects $obj
set_property -name "default_lib" -value "xil_defaultlib" -objects $obj
set_property -name "dsa.num_compute_units" -value "16" -objects $obj
set_property -name "dsa.rom.debug_type" -value "0" -objects $obj
set_property -name "dsa.rom.prom_type" -value "0" -objects $obj
set_property -name "enable_optional_runs_sta" -value "0" -objects $obj
set_property -name "generate_ip_upgrade_log" -value "1" -objects $obj
set_property -name "ip_cache_permissions" -value "read write" -objects $obj
set_property -name "ip_interface_inference_priority" -value "" -objects $obj
set_property -name "ip_output_repo" -value "$proj_dir/${project_name}.cache/ip" -objects $obj
set_property -name "managed_ip" -value "0" -objects $obj
set_property -name "pr_flow" -value "0" -objects $obj
set_property -name "sim.ip.auto_export_scripts" -value "1" -objects $obj
set_property -name "sim.use_ip_compiled_libs" -value "1" -objects $obj
set_property -name "simulator_language" -value "Mixed" -objects $obj
set_property -name "source_mgmt_mode" -value "All" -objects $obj
set_property -name "target_language" -value "VHDL" -objects $obj
set_property -name "target_simulator" -value "XSim" -objects $obj
set_property -name "xpm_libraries" -value "XPM_CDC XPM_MEMORY" -objects $obj
set_property -name "xsim.array_display_limit" -value "1024" -objects $obj
set_property -name "xsim.radix" -value "hex" -objects $obj
set_property -name "xsim.time_unit" -value "ns" -objects $obj
set_property -name "xsim.trace_limit" -value "65536" -objects $obj

# Create 'sources_1' fileset (if not found)
if {[string equal [get_filesets -quiet sources_1] ""]} {
  create_fileset -srcset sources_1
}

# top
read_vhdl -library work $origin_dir/sources/src/VSB_VMM_top.vhd

# elinks
read_vhdl -library work $origin_dir/sources/src/elinks/8b10_dec_wrap.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/8b10_dec.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/centralRouter_package.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/Elink2FIFO.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/elink2udp_manager.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/elink2udp.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/elink2UDP_wrapper.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/elink_feeder.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/elinkRXfifo_wrap.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/elink_TxRx.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/elink_wrapper.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/enc_8b10.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/enc8b10_wrap.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/EPROC_IN16_ALIGN_BLOCK.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/EPROC_IN16_DEC8b10b.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/EPROC_IN16_direct.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/EPROC_IN16.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/EPROC_IN2_DEC8b10b.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/EPROC_IN2_HDLC.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/EPROC_IN2.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/EPROC_IN4_DEC8b10b.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/EPROC_IN4.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/EPROC_IN8_DEC8b10b.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/EPROC_IN8.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/EPROC_OUT2_direct.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/EPROC_OUT2_ENC8b10b.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/EPROC_OUT2_HDLC.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/EPROC_OUT2.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/EPROC_OUT4_direct.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/EPROC_OUT4_ENC8b10b.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/EPROC_OUT4.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/EPROC_OUT8_ENC8b10b.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/EPROC_OUT8.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/FIFO2Elink.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/KcharTest.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/MUX2_Nbit.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/MUX4_Nbit.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/MUX4.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/MUX8_Nbit.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/pulse_fall_pw01.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/pulse_pdxx_pwxx.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/roc2udp.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/simple_mode.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/elink_inhibitor.vhd
# elinks/ttc
read_vhdl -library work $origin_dir/sources/src/elinks/ttc/ttc_controller.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/ttc/ttc_gen_wrapper.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/ttc/ttc_generator.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/ttc/microTTC.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/ttc/VSB_fineTrg.vhd
read_vhdl -library work $origin_dir/sources/src/elinks/ttc/microTTC_perSR.vhd

# ethernet
read_vhdl -library work $origin_dir/sources/src/ethernet/vc709_eth_wrapper.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/arp_REQ.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/arp_RX.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/arp_STORE_br.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/arp_SYNC.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/arp_TX.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/arp_types.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/arpv2.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/arp.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/axi.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/Code8b10bPkg.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/ICMP_RX.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/ICMP_TX.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/icmp_udp_mux.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/IP_complete_nomac.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/IPv4_RX.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/IPv4_TX.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/ipv4_types.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/IPv4.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/ping_reply_processor.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/StdRtlPkg.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/temac_10_100_1000_block.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/temac_10_100_1000_config_vector_sm.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/temac_10_100_1000_fifo_block.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/temac_10_100_1000_ipif_pkg.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/temac_10_100_1000_reset_sync.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/temac_10_100_1000_rx_client_fifo.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/temac_10_100_1000_sync_block.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/temac_10_100_1000_ten_100_1g_eth_fifo.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/temac_10_100_1000_tx_client_fifo.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/tx_arbitrator.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/UDP_ICMP_Complete_nomac.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/UDP_RX.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/UDP_TX.vhd
# ethernet/userLogic
read_vhdl -library work $origin_dir/sources/src/ethernet/userLogic/sca_config_block.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/userLogic/udp_din_handler.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/userLogic/fpga_config_block.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/userLogic/fpga_config_router.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/userLogic/VSB2UDP.vhd
read_vhdl -library work $origin_dir/sources/src/ethernet/userLogic/ip_switcher.vhd

# trigger processing core
read_vhdl -library work $origin_dir/sources/src/trigger/trigger_processing_core.vhd
read_vhdl -library work $origin_dir/sources/src/trigger/art_ramLUT_wrapper.vhd
read_vhdl -library work $origin_dir/sources/src/trigger/art_des.vhd
read_vhdl -library work $origin_dir/sources/src/trigger/art_fifos.vhd
read_vhdl -library work $origin_dir/sources/src/trigger/art_package.vhd
read_vhdl -library work $origin_dir/sources/src/trigger/art_sregs.vhd
read_vhdl -library work $origin_dir/sources/src/trigger/l1a_buff_wrapper.vhd
read_vhdl -library work $origin_dir/sources/src/trigger/tp_supervisor.vhd
read_vhdl -library work $origin_dir/sources/src/trigger/tp_ramFiller.vhd

# i2c
read_vhdl -library work $origin_dir/sources/src/i2c/i2c_wrapper.vhd
read_vhdl -library work $origin_dir/sources/src/i2c/I2C_controller.vhd
read_vhdl -library work $origin_dir/sources/src/i2c/i2c_programClk_FSM.vhd

# other
read_vhdl -library work $origin_dir/sources/src/other/CDCC.vhd
read_vhdl -library work $origin_dir/sources/src/other/startup_logic.vhd
read_vhdl -library work $origin_dir/sources/src/other/clk_oddr_wrapper.vhd
read_vhdl -library work $origin_dir/sources/src/other/VSB_cktp.vhd

# IP cores
set obj [get_filesets sources_1]   
set files [list \
 "[file normalize "$origin_dir/sources/ip/gig_ethernet_pcs_pma_0.xcix"]"\
 "[file normalize "$origin_dir/sources/ip/mmcm_main.xcix"]"\
 "[file normalize "$origin_dir/sources/ip/mmcm_vmm.xcix"]"\
 "[file normalize "$origin_dir/sources/ip/mmcm_ttc.xcix"]"\
 "[file normalize "$origin_dir/sources/ip/icmp_payload_buffer.xcix"]"\
 "[file normalize "$origin_dir/sources/ip/temac_10_100_1000.xcix"]"\
 "[file normalize "$origin_dir/sources/ip/fifo_ramLUT.xcix"]"\
 "[file normalize "$origin_dir/sources/ip/fifo_l1a.xcix"]"\
 "[file normalize "$origin_dir/sources/ip/fifo_main.xcix"]"\
 "[file normalize "$origin_dir/sources/ip/fifo_prioritizer.xcix"]"\
 "[file normalize "$origin_dir/sources/ip/fpga_reg_buffer.xcix"]"\
 "[file normalize "$origin_dir/sources/ip/ramLUT.xcix"]"\
 "[file normalize "$origin_dir/sources/ip/ila_tp.xcix"]"\
 "[file normalize "$origin_dir/sources/ip/elink2UDP_daq.xcix"]"\
 "[file normalize "$origin_dir/sources/ip/elink2UDP_len.xcix"]"\
 "[file normalize "$origin_dir/sources/ip/hdlc_bist_fifo.xcix"]"\
 "[file normalize "$origin_dir/sources/ip/elinkFeeder_fifo.xcix"]"\
 "[file normalize "$origin_dir/sources/ip/upstreamFIFO.xcix"]"\
 "[file normalize "$origin_dir/sources/ip/sca_conf_buffer.xcix"]"\
 "[file normalize "$origin_dir/sources/ip/vio_ttc.xcix"]"\
 "[file normalize "$origin_dir/sources/ip/vio_elink.xcix"]"\
 "[file normalize "$origin_dir/sources/ip/ila_overview.xcix"]"\
]

add_files -norecurse -fileset $obj $files

# constraints
read_xdc -verbose $origin_dir/sources/xdc/VSB.xdc
read_xdc -verbose $origin_dir/sources/xdc/vc709.xdc

# Set 'sources_1' fileset properties
set obj [get_filesets sources_1]
set_property "top" "VSB_VMM_top" $obj

puts "############################################################################"
puts "Copyright Notice/Copying Permission:
    Copyright 2017 Christos Bakalis (christos.bakalis@cern.ch)\n

    This file is part of VSB_firmware.\n

    VSB_firmware is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VSB_firmware is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VSB_firmware.  If not, see <http://www.gnu.org/licenses/>.\n"
puts "############################################################################"

puts "###################################"
puts "INFO: Project created:$project_name"
puts "###################################"
puts "Build Succesful. Enjoy :)"
