----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2017 Christos Bakalis
--
--    This file is part of NTUA_BNL_VSB_firmware.
--
--    NTUA_BNL_VSB_firmware is free software: you can redistribute it 
--    and/or modify it under the terms of the GNU General Public License 
--    as published by the Free Software Foundation, either version 3 of
--    the License, or (at your option) any later version.
--
--    NTUA_BNL_VSB_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA_BNL_VSB_firmware. If not, see <http://www.gnu.org/licenses/>.
-- 
-- Create Date: 22.11.2017 16:22:35
-- Design Name: VC709 VSB top
-- Module Name: VSB_VMM_top - RTL
-- Project Name: VSB DAQ - NTUA/BNL
-- Target Devices: Virtex7 xc7vx690tffg1761-2
-- Tool Versions: Vivado 2017.3
-- Description: Top module of VSB for SCA/ROC/VMM configuration, VMM readout
-- and ART data processing.
--
-- Dependencies: NTUA_BNL_VSB_firmware
-- 
-- Changelog:
-- 06.02.2018 Added trigger processing core. (Christos Bakalis)
-- 14.05.2018 Added support for BB5/testbeam implementations. (Christos Bakalis)
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;
use work.axi.all;
use work.ipv4_types.all;
use work.arp_types.all;


entity VSB_VMM_top is
    port(
        ---------------------------------
        ------ General Interface --------
        SYSCLK_P        : IN    STD_LOGIC;
        SYSCLK_N        : IN    STD_LOGIC;
        USER_CLK_P      : IN    STD_LOGIC;
        USER_CLK_N      : IN    STD_LOGIC;
        GPIO_SW         : IN    STD_LOGIC_VECTOR(4 DOWNTO 0);
        GPIO_DIP        : IN    STD_LOGIC_VECTOR(7 DOWNTO 0);
        GPIO_LED        : OUT   STD_LOGIC_VECTOR(7 DOWNTO 0);   
        ---------------------------------
        ----- Transceiver Interface -----
        GTREFCLK_P      : IN    STD_LOGIC;    -- Differential +ve of reference clock for transceiver: 125MHz, very high quality
        GTREFCLK_N      : IN    STD_LOGIC;    -- Differential -ve of reference clock for transceiver: 125MHz, very high quality
        TXP             : OUT   STD_LOGIC;    -- Differential +ve of serial transmission from PMA to PMD.
        TXN             : OUT   STD_LOGIC;    -- Differential -ve of serial transmission from PMA to PMD.
        RXP             : IN    STD_LOGIC;    -- Differential +ve for serial reception from PMD to PMA.
        RXN             : IN    STD_LOGIC;    -- Differential -ve for serial reception from PMD to PMA.
        SI5324_REFCLK_P : OUT   STD_LOGIC;    -- Reference clock for the Si5324 Jitter cleaner.
        SI5324_REFCLK_N : OUT   STD_LOGIC;    -- Reference clock for the Si5324 Jitter cleaner.
        SI5324_INTRPT   : IN    STD_LOGIC;    -- Interrupt signal from Si5324 (high -> loss of CKIN1)
        SI5324_RESETB   : OUT   STD_LOGIC;    -- ResetB signal for the Si5324 (active-low)
        SFP_TX_DISABLE  : OUT   STD_LOGIC;    -- TX disable
        ---------------------------------
        ------- CTF Interface -----------
        CTF_CLK_P       : IN    STD_LOGIC;
        CTF_CLK_N       : IN    STD_LOGIC;
        CTF_RST_P       : IN    STD_LOGIC;
        CTF_RST_N       : IN    STD_LOGIC;
        CTF_TRG_P       : IN    STD_LOGIC;
        CTF_TRG_N       : IN    STD_LOGIC;
        ---------------------------------
        -------- FE Interface -----------
        FE_REFCLK_P     : OUT   STD_LOGIC_VECTOR(7 DOWNTO 0);
        FE_REFCLK_N     : OUT   STD_LOGIC_VECTOR(7 DOWNTO 0);
        FE_INHIBIT_P    : OUT   STD_LOGIC_VECTOR(7 DOWNTO 0);
        FE_INHIBIT_N    : OUT   STD_LOGIC_VECTOR(7 DOWNTO 0);
        FE_TTC_P        : OUT   STD_LOGIC_VECTOR(7 DOWNTO 0);
        FE_TTC_N        : OUT   STD_LOGIC_VECTOR(7 DOWNTO 0);
        FE_CKTP_P       : OUT   STD_LOGIC_VECTOR(7 DOWNTO 0);
        FE_CKTP_N       : OUT   STD_LOGIC_VECTOR(7 DOWNTO 0);
        FE_BUSY_P       : IN    STD_LOGIC_VECTOR(7 DOWNTO 0);
        FE_BUSY_N       : IN    STD_LOGIC_VECTOR(7 DOWNTO 0);
        FE_ART_P        : IN    STD_LOGIC_VECTOR(2 DOWNTO 0);
        FE_ART_N        : IN    STD_LOGIC_VECTOR(2 DOWNTO 0);
        ---------------------------------
        -------- I2C Interface ----------
        I2C_MUX_RESETB  : OUT   STD_LOGIC;    -- ResetB signal for the PCA9548A (active-low)
        SCL             : OUT   STD_LOGIC;
        SDA             : INOUT STD_LOGIC
    );
end VSB_VMM_top;

architecture RTL of VSB_VMM_top is



    -------------------------------------------------------------------
    -- Global Settings
    ------------------------------------------------------------------- 

    -- Default IP and MAC address of the board
    signal default_IP           : std_logic_vector(31 downto 0) := x"c0a8000a";     -- to UDP/ICMP block
    signal default_MAC          : std_logic_vector(47 downto 0) := x"001220109823"; -- to UDP/ICMP block
    signal default_destIP       : std_logic_vector(31 downto 0) := x"c0a80010";     -- to elink2udp
    signal vc709_IP             : std_logic_vector(31 downto 0) := x"c0a8000a";     -- to UDP/ICMP block
    signal vc709_MAC            : std_logic_vector(47 downto 0) := x"001220109823"; -- to UDP/ICMP block
    -------------------------------------------------------------------
    signal srcPort_conf         : std_logic_vector(15 downto 0) := x"19CB";         -- to elink2udp  
    signal dstPort_conf         : std_logic_vector(15 downto 0) := x"1778";         -- to elink2udp
    signal srcPort_daq          : std_logic_vector(15 downto 0) := x"0666";         -- to elink2udp       
    signal dstPort_daq          : std_logic_vector(15 downto 0) := x"1990";         -- to elink2udp

    -- generate trigger processing core
    constant generate_trigProc  : std_logic := '1';
    -- debug the trigger processing core
    constant debug_trigProc     : std_logic := '0';
    -- bypass IP switcher
    constant bypass_switcher    : std_logic := '1';
    -- use fine trigger
    constant fine_trigger       : std_logic := '1';
    -- use periodic soft-reset and set the frequency
    constant ena_perSR          : std_logic := '0';
    constant sr_period          : std_logic_vector(15 downto 0) := x"0190"; -- 0x190=400*25ns->100kHz

--------------------------
--  Component Declaration 
--------------------------

component mmcm_main
    port(
        -- Clock in ports
        clk_in1_p   : in std_logic;
        clk_in1_n   : in std_logic;
        -- Clock out ports
        clk_out_50  : out std_logic;
        clk_out_200 : out std_logic;
        clk_out_125 : out std_logic;
        -- Status and control signals
        reset       : in  std_logic;
        mmcm_locked : out std_logic
    );
end component;

component mmcm_vmm
    port(
        -- Clock in ports
        clk_in1_p    : in std_logic;
        clk_in1_n    : in std_logic;
        -- Clock out ports
        clk_out_40  : out std_logic;
        clk_out_80  : out std_logic;
        clk_out_160 : out std_logic;
        clk_out_320 : out std_logic;
        -- Status and control signals
        reset       : in  std_logic;
        mmcm_locked : out std_logic
    );
end component;

component mmcm_ttc
    port(
        -- Clock in ports
        clk_in1_p   : in std_logic;
        clk_in1_n   : in std_logic;
        -- Clock out ports
        clk_out_40  : out std_logic;
        clk_out_80  : out std_logic;
        clk_out_160 : out std_logic;
        clk_out_320 : out std_logic;
        -- Status and control signals
        reset       : in  std_logic;
        mmcm_locked : out std_logic
    );
end component;

-- ODDR for clock forwarding
component clk_oddr_wrapper
    Port(
        -----------------------------
        ---- Clock Input Interface --
        clk_40          : in  std_logic;
        clk_50          : in  std_logic;
        clk_80          : in  std_logic;
        clk_125_dirty   : in  std_logic;
        clk_125_clean   : in  std_logic;
        clk_160         : in  std_logic;
        clk_200         : in  std_logic;
        clk_320         : in  std_logic;
        rst_oddr        : in  std_logic_vector(7 downto 0);
        -----------------------------
        --- Clock Output Interface --
        clk_40_oddr     : out  std_logic_vector(7 downto 0);
        clk_50_oddr     : out  std_logic_vector(0 downto 0);
        clk_80_oddr     : out  std_logic_vector(0 downto 0);
        clk_125cl_oddr  : out  std_logic_vector(0 downto 0);
        clk_125dir_oddr : out  std_logic_vector(1 downto 0);
        clk_160_oddr    : out  std_logic_vector(0 downto 0);
        clk_200_oddr    : out  std_logic_vector(0 downto 0);
        clk_320_oddr    : out  std_logic_vector(0 downto 0)
    );
end component;

-- startup pulses (resets)
component startup_logic
    generic( cnt_1ms  : natural := 40_000;  -- 25ns*40_000 = 1ms
             cnt_10ms : natural := 400_000); --25ns*400_000 = 10ms  
    port(
        clk_in       : in  std_logic; -- clk40
        mmcm_locked  : in  std_logic_vector(1 downto 0);
        fifo_init    : out std_logic;
        phy_rstn_out : out std_logic
    );
end component;

-- I2C controller and programming logic
component i2c_wrapper
    port( 
        clk_50  : in    std_logic;
        resetB  : out   std_logic;
        sda     : inout std_logic;
        scl     : out   std_logic
     );
end component;

-- Ethernet Interface wrapper
component vc709_eth_wrapper
    port(
        ---------------------------------
        ------ General Interface --------
        clk_200         : in  std_logic;    -- Independent clock (from BUFG)
        clk_50          : in  std_logic;    -- For CDCC
        fifo_init       : in  std_logic;    -- Initialize the ICMP FIFO
        rst_eth         : in  std_logic;    -- Reset the ethernet cores
        FPGA_IP         : in  std_logic_vector(31 downto 0);
        FPGA_MAC        : in  std_logic_vector(47 downto 0);
        userclk2        : out std_logic;    -- 125 Mhz for user logic
        TXRXmmcm_locked : out std_logic;    -- Transceiver MMCM lock signal
        ---------------------------------
        ---- UDP/ICMP Block Interface ---
        udp_tx_start    : in  std_logic;
        udp_txi         : in  udp_tx_type;
        udp_rxo         : out udp_rx_type;
        udp_tx_dout_rdy : out std_logic;
        ---------------------------------
        ----- Transceiver Interface -----
        gtrefclk_p      : in  std_logic;    -- Differential +ve of reference clock for transceiver: 125MHz, very high quality
        gtrefclk_n      : in  std_logic;    -- Differential -ve of reference clock for transceiver: 125MHz, very high quality
        rxp             : in  std_logic;    -- Differential +ve for serial reception from PMD to PMA.
        rxn             : in  std_logic;    -- Differential -ve for serial reception from PMD to PMA.
        txp             : out std_logic;    -- Differential +ve of serial transmission from PMA to PMD.
        txn             : out std_logic     -- Differential -ve of serial transmission from PMA to PMD.
    );
end component;

-- dynamic IP switcher based on the DIP switch state
component ip_switcher
    generic(bypass_switcher : std_logic);
    port(
        ------------------------------------
        ------- General Interface ----------
        clk_eth         : in  std_logic;
        mmcm_locked     : in  std_logic;
        gpio_dip        : in  std_logic_vector(7 downto 0);
        default_IP      : in  std_logic_vector(31 downto 0);
        default_MAC     : in  std_logic_vector(47 downto 0);
        ------------------------------------
        --------- UDP Interface ------------
        vc709_IP        : out std_logic_vector(31 downto 0);
        vc709_MAC       : out std_logic_vector(47 downto 0)
    );
end component;

-- UDP Tx module
component VSB2UDP
    port(
        ---------------------------
        ---- General Interface ----
        clk_ckbc        : in  std_logic;
        clk_eth         : in  std_logic;
        fifo_init       : in  std_logic;
        glbl_bcid       : in  std_logic_vector(11 downto 0);
        art_addr_udp    : in  std_logic_vector(17 downto 0);
        art_valid_udp   : in  std_logic;
        art_inhibit     : in  std_logic;
        ---------------------------
        ---- Trigger Interface ----
        trigger_latched : in  std_logic;
        fe_busy         : in  std_logic;
        ---------------------------
        ---- UDP Interface --------
        default_destIP  : in  std_logic_vector(31 downto 0);
        dstPort         : in  std_logic_vector(15 downto 0);
        srcPort         : in  std_logic_vector(15 downto 0);
        udp_tx_dout_rdy : in  std_logic;
        udp_tx_start    : out std_logic;
        udp_txi         : out udp_tx_type
    );
end component;

-- UDP DataIn Handler
component udp_din_handler
    port(
        ------------------------------------
        ------- General Interface ----------
        clk_eth         : in  std_logic; -- userclk2 from transceiver
        clk_ckbc        : in  std_logic; -- BC clock (40)
        clk_art         : in  std_logic; -- ART clock (160)
        clk_art2        : in  std_logic; -- ART clock x2 (320)
        rst             : in  std_logic; -- global reset
        fifo_init       : in  std_logic; -- initializing reset for fifos
        handler_busy    : out std_logic; -- busy signal
        serial_number   : out std_logic_vector(31 downto 0);
        frontEnd_bmsk   : out std_logic_vector(7 downto 0);
        ------------------------------------
        -------- UDP/IPv4 Interface --------
        udp_rx          : in  udp_rx_type;
        -------------------------------------
        -------- TPC RAM Interface ----------
        wrEn_LUTRAM      : out std_logic; -- wr_en to RAM-LUT
        din_LUTRAM       : out std_logic; -- din to RAM-LUT
        wrAddr_LUTRAM    : out std_logic_vector(17 downto 0); -- write-address
        -------------------------------------
        ------ SCA Config Interface ---------
        hdlc_dout       : out std_logic_vector(17 downto 0);
        hdlc_dvalid     : out std_logic;
        -------------------------------------
        ------ FPGA Config Interface --------
        fpga_rst        : out std_logic;
        latency         : out std_logic_vector(15 downto 0);
        acq_on          : out std_logic;
        extTrg_ena      : out std_logic;
        trigProc_ena    : out std_logic
    );
end component;

-- processor for ART data
component trigger_processing_core
    generic(debug_trigProc : std_logic := '0');
    port(
        ------------------------------------
        ------- General Interface ----------
        clk_ckbc        : in  std_logic; -- BC clock (40)
        clk_art         : in  std_logic; -- ART clock (160)
        clk_art2        : in  std_logic; -- ART clock x2 (320)
        clk_eth         : in  std_logic; -- userclk2 from transceiver
        rst             : in  std_logic; -- global reset
        fifo_init       : in  std_logic; -- initializing reset for fifos
        bcid_glbl       : in  std_logic_vector(11 downto 0); -- global BCID (must be synced with front-ends)
        bcid_offset     : in  std_logic_vector(11 downto 0); -- when to send L1A?
        inhibit_des     : in  std_logic; -- inhibit deserialization
        trg_cktp        : in  std_logic; -- debugging
        alarm           : out std_logic; -- processor too slow
        buff_overflow   : out std_logic; -- throughput too big
        art_addr_udp    : out std_logic_vector(17 downto 0); -- to be sent through UDP
        art_valid_udp   : out std_logic; -- found valid addresses
        -------------------------------------
        ---------- UDP Interface ------------
        wrEn_LUTRAM      : in  std_logic; -- wr_en to RAM-LUT
        din_LUTRAM       : in  std_logic; -- din to RAM-LUT
        wrAddr_LUTRAM    : in  std_logic_vector(17 downto 0); -- write-address
        ------------------------------------
        ---------- VMM Interface -----------
        art_in0         : in  std_logic_vector(7 downto 0); -- ART board 0
        art_in1         : in  std_logic_vector(7 downto 0); -- ART board 1
        art_in2         : in  std_logic_vector(7 downto 0); -- ART board 2
        l1a_vmm_vec     : out std_logic_vector(3 downto 0); -- L1A signal
        l1a_vmm_all     : out std_logic                     -- L1A signal
    );
end component;

-- VSB TTC
component microTTC
    generic(fine_trigger : std_logic;
            ena_perSR    : std_logic;
            sr_period    : std_logic_vector(15 downto 0));
    port(
        ------------------------------------
        ------- General Interface ----------
        clk_ckbc        : in  std_logic;
        clk_art         : in  std_logic;
        clk_art2        : in  std_logic;
        rst_oddr        : in  std_logic_vector(7 downto 0);
        acq_on          : in  std_logic;
        mmcm_locked     : in  std_logic;
        l0_latency      : in  std_logic_vector(15 downto 0);
        cktp_trg        : in  std_logic;
        ext_trg_enable  : in  std_logic;
        trigProc_enable : in  std_logic;
        trigProc_l0     : in  std_logic;
        trg_latched     : out std_logic;
        rst_latched     : out std_logic;
        trg_debug       : out std_logic;
        -------------------------------------
        ----------- CTF Interface -----------
        ctf_softReset   : in  std_logic;
        ctf_trigger     : in  std_logic;
        -------------------------------------
        ----------- FE Interface ------------
        fe_busy         : in  std_logic;
        fe_ttc          : out std_logic_vector(7 downto 0)
    );
end component;

-- CKTP asserter
component VSB_cktp
    port(
        ------------------------------------
        ------- General Interface ----------
        clk_ckbc    : in  std_logic;
        enable      : in  std_logic;
        rst_oddr    : in  std_logic_vector(7 downto 0);
        cktp_trg    : out std_logic;
        ------------------------------------
        --------- FE Interface -------------
        fe_busy     : in  std_logic;
        fe_cktp     : out std_logic_vector(7 downto 0)
     );
end component;

-- RAM filler
component tp_ramFiller
    generic(addr_offset : std_logic_vector(2 downto 0));
    port(
        ------------------------------------
        ------- General Interface ----------
        clk_eth         : in  std_logic;
        fifo_init       : in  std_logic;
        ------------------------------------
        ------- TP-RAM Interface -----------
        wrEn_LUTRAM      : out std_logic; -- wr_en to RAM-LUT
        din_LUTRAM       : out std_logic; -- din to RAM-LUT
        wrAddr_LUTRAM    : out std_logic_vector(17 downto 0) -- write-address
    );
end component;

    -------------------------------------------------------------------
    -- MMCM/ODDR signals
    ------------------------------------------------------------------- 
    signal clk_40               : std_logic := '0';
    signal clk_50               : std_logic := '0';
    signal clk_80               : std_logic := '0';
    signal clk_160              : std_logic := '0';
    signal clk_200              : std_logic := '0';
    signal clk_320              : std_logic := '0';
    signal clk_125_noClean      : std_logic := '0';
    signal masterMMCM_locked_all: std_logic := '0';
    signal masterMMCM_locked    : std_logic_vector(1 downto 0) := (others => '0');

    signal clk_40_oddr          : std_logic_vector(7 downto 0) := (others => '0');
    signal clk_40_oddr_sca      : std_logic_vector(3 downto 0) := (others => '0');
    signal clk_40_oddr_ttc      : std_logic_vector(3 downto 0) := (others => '0');
    signal clk_50_oddr          : std_logic_vector(0 downto 0) := (others => '0');
    signal clk_80_oddr          : std_logic_vector(0 downto 0) := (others => '0');
    signal clk_125dir_oddr      : std_logic_vector(1 downto 0) := (others => '0');
    signal clk_125cl_oddr       : std_logic_vector(0 downto 0) := (others => '0');
    signal clk_160_oddr         : std_logic_vector(0 downto 0) := (others => '0');
    signal clk_200_oddr         : std_logic_vector(0 downto 0) := (others => '0');
    signal clk_320_oddr         : std_logic_vector(0 downto 0) := (others => '0');

    -------------------------------------------------------------------
    -- Trigger Processing Core signals
    ------------------------------------------------------------------- 
    signal alarm                : std_logic := '0';
    signal buff_overflow        : std_logic := '0';
    signal wrEn_LUTRAM           : std_logic := '0';
    signal wrAddr_LUTRAM         : std_logic_vector(17 downto 0) := (others => '0');
    signal bcid_offset          : std_logic_vector(11 downto 0) := (others => '0');
    signal din_LUTRAM            : std_logic := '0';
    signal art_valid_udp        : std_logic := '0';    
    signal art_vec_buf          : std_logic_vector(2 downto 0)  := (others => '0');
    signal art_in0_vec          : std_logic_vector(7 downto 0)  := (others => '0');
    signal art_in1_vec          : std_logic_vector(7 downto 0)  := (others => '0');
    signal art_in2_vec          : std_logic_vector(7 downto 0)  := (others => '0');
    signal art_addr_udp         : std_logic_vector(17 downto 0) := (others => '0');
    signal l1a_vmm              : std_logic_vector(3 downto 0)  := (others => '0');
    signal l1a_vmm_trigProc     : std_logic := '0';

    -------------------------------------------------------------------
    -- UDP Data Handler signals
    ------------------------------------------------------------------- 
    signal udp_tx_start         : std_logic := '0';
    signal udp_tx_dout_rdy      : std_logic := '0';
    signal clk_eth              : std_logic := '0';
    signal fpga_glbl_rst_i      : std_logic := '0';
    signal extTrg_ena           : std_logic := '0';
    signal trigProc_ena         : std_logic := '0';
    signal acq_on               : std_logic := '0';
    signal udp_rx               : udp_rx_type;
    signal udp_tx               : udp_tx_type;

    -------------------------------------------------------------------
    -- BB5/Testbeam - microTTC
    -------------------------------------------------------------------
    signal rst_bcid             : std_logic := '0';
    signal bcid_glbl_cnt        : unsigned(11 downto 0)         := (others => '0');
    signal glbl_bcid            : std_logic_vector(11 downto 0) := (others => '0');
    signal rst_oddr             : std_logic_vector(7 downto 0)  := (others => '0');
    signal busy_vec_buf         : std_logic_vector(7 downto 0)  := (others => '0');
    signal inhibit_vec_buf      : std_logic_vector(7 downto 0)  := (others => '0');
    signal ttcFE_vec_buf        : std_logic_vector(7 downto 0)  := (others => '0');
    signal inhibit_out_i        : std_logic := '0';
    signal busy_vec_i0          : std_logic_vector(7 downto 0) := (others => '0');
    signal busy_vec_i1          : std_logic_vector(7 downto 0) := (others => '0');
    signal busy_latched         : std_logic := '0';
    signal busy_cnt             : unsigned(22 downto 0) := (others => '0');
    signal ctf_rst_buf          : std_logic := '0';
    signal ctf_trg_buf          : std_logic := '0';
    signal l0_latency           : std_logic_vector(15 downto 0) := (others => '0');
    signal trg_latched          : std_logic := '0';

    -------------------------------------------------------------------
    -- Other
    -------------------------------------------------------------------
    signal gpio_led_i           : std_logic_vector(7 downto 0) := (others => '0');
    signal gpio_sw_i            : std_logic_vector(4 downto 0) := (others => '0');
    signal gpio_dip_i           : std_logic_vector(7 downto 0) := (others => '0');
    signal feCKTP_vec_buf       : std_logic_vector(7 downto 0) := (others => '0');
    signal cnt_wait             : unsigned(5 downto 0) := (others => '0');
    signal cnt_write            : unsigned(5 downto 0) := (others => '0');
    signal flag_sent            : std_logic := '0';
    signal ena_cktp             : std_logic := '0';
    signal trg_cktp             : std_logic := '0';
    signal pulse_cnt            : unsigned(22 downto 0) := (others => '0');
    signal daq_on_ack           : std_logic := '0';
    signal glbl_fifo_init       : std_logic := '0';
    signal glbl_rst_i           : std_logic := '0';
    signal phy_rstn_i           : std_logic := '0';
    signal TXRXmmcm_locked      : std_logic := '0';
    signal si5324_int_i         : std_logic := '0';
    signal resetB               : std_logic := '0';
    signal rst_phy              : std_logic := '0';
    signal trg_debug            : std_logic := '0';
    signal inhibit_artDes       : std_logic := '0';

begin

-- main MMCM (1)
mmcm_master_inst: mmcm_main
    port map (  
        -- Clock out ports  
        clk_out_50      => clk_50,
        clk_out_200     => clk_200,
        clk_out_125     => clk_125_noClean,
        -- Status and control signals                
        reset           => '0',
        mmcm_locked     => masterMMCM_locked(0),
        -- Clock in ports
        clk_in1_p       => SYSCLK_P,
        clk_in1_n       => SYSCLK_N
    );

-- main MMCM (2) (for use with local clock)
--mmcm_vmm_inst: mmcm_vmm
--    port map ( 
--        -- Clock out ports  
--        clk_out_40   => clk_40,
--        clk_out_80   => clk_80,
--        clk_out_160  => clk_160,
--        clk_out_320  => clk_320,
--        -- Status and control signals                
--        reset        => '0',
--        mmcm_locked  => masterMMCM_locked(1),
--        -- Clock in ports
--        clk_in1_p    => USER_CLK_P,
--        clk_in1_n    => USER_CLK_N
--    );

-- main MMCM (for use with external clock)
mmcm_ttc_inst: mmcm_ttc
    port map ( 
        -- Clock out ports  
        clk_out_40   => clk_40,
        clk_out_80   => clk_80,
        clk_out_160  => clk_160,
        clk_out_320  => clk_320,
        -- Status and control signals                
        reset        => '0',
        mmcm_locked  => masterMMCM_locked(1),
        -- Clock in ports
        clk_in1_p    => CTF_CLK_P,
        clk_in1_n    => CTF_CLK_N
    );

-- ODDR clock forwarder
clk_oddr_wrapper_inst: clk_oddr_wrapper
    port map(
        -----------------------------
        ---- Clock Input Interface --
        clk_40          => clk_40,
        clk_50          => clk_50,
        clk_80          => clk_80,
        clk_125_dirty   => clk_125_noClean,
        clk_125_clean   => '0',
        clk_160         => clk_160,
        clk_200         => clk_200,
        clk_320         => clk_320,
        rst_oddr        => rst_oddr,
        -----------------------------
        --- Clock Output Interface --
        clk_40_oddr     => clk_40_oddr,
        clk_50_oddr     => clk_50_oddr,
        clk_80_oddr     => clk_80_oddr,
        clk_125dir_oddr => clk_125dir_oddr,
        clk_125cl_oddr  => clk_125cl_oddr,
        clk_160_oddr    => clk_160_oddr,
        clk_200_oddr    => clk_200_oddr,
        clk_320_oddr    => clk_320_oddr
    );
 
-- Startup pulse creator
startup_inst: startup_logic
    generic map(cnt_1ms  => 40_000,
                cnt_10ms => 400_000)     
    port map(
        clk_in          => clk_50, -- clk_40
        mmcm_locked(0)  => masterMMCM_locked_all,
        mmcm_locked(1)  => TXRXmmcm_locked,
        fifo_init       => glbl_fifo_init,
        phy_rstn_out    => phy_rstn_i
    );

-- I2C controller
i2c_inst: i2c_wrapper
    port map( 
        clk_50  => clk_50,
        resetB  => resetB,
        sda     => SDA,
        scl     => SCL
    );       
         
-- Ethernet wrapper
vc709_eth_wrapper_inst: vc709_eth_wrapper
    port map(
        ---------------------------------
        ------ General Interface --------
        clk_200         => clk_200,
        clk_50          => clk_50,
        fifo_init       => glbl_fifo_init,
        rst_eth         => fpga_glbl_rst_i, -- glbl_fifo_init
        FPGA_IP         => vc709_IP,
        FPGA_MAC        => vc709_MAC,
        userclk2        => clk_eth,
        TXRXmmcm_locked => TXRXmmcm_locked,
        ---------------------------------
        ---- UDP/ICMP Block Interface ---
        udp_tx_start    => udp_tx_start,
        udp_txi         => udp_tx,
        udp_rxo         => udp_rx,
        udp_tx_dout_rdy => udp_tx_dout_rdy,
        ---------------------------------
        ----- Transceiver Interface -----
        gtrefclk_p      => GTREFCLK_P, 
        gtrefclk_n      => GTREFCLK_N, 
        rxp             => RXP,
        rxn             => RXN,
        txp             => TXP,
        txn             => TXN
    );

-- dynamic IP switcher instantiation
ip_switcher_inst: ip_switcher
    generic map(bypass_switcher => bypass_switcher)
    port map(
        ------------------------------------
        ------- General Interface ----------
        clk_eth         => clk_eth,
        mmcm_locked     => TXRXmmcm_locked,
        gpio_dip        => gpio_dip_i,
        default_IP      => default_IP,
        default_MAC     => default_MAC,
        ------------------------------------
        --------- UDP Interface ------------
        vc709_IP        => vc709_IP,
        vc709_MAC       => vc709_MAC
    );

-- UDP din handling instantiation
udp_din_handler_inst: udp_din_handler
    port map(
        ------------------------------------
        ------- General Interface ----------
        clk_eth         => clk_eth,
        clk_ckbc        => clk_40,
        clk_art         => clk_160,
        clk_art2        => clk_320,
        rst             => fpga_glbl_rst_i,
        fifo_init       => glbl_fifo_init,
        handler_busy    => open,
        serial_number   => open,
        frontEnd_bmsk   => open,
        ------------------------------------
        -------- UDP/IPv4 Interface --------
        udp_rx          => udp_rx,
        -------------------------------------
        -------- TPC RAM Interface ----------
        wrEn_LUTRAM      => open,
        din_LUTRAM       => open,
        wrAddr_LUTRAM    => open,
        -------------------------------------
        ------ SCA Config Interface ---------
        hdlc_dout       => open,
        hdlc_dvalid     => open,
        -------------------------------------
        ------ FPGA Config Interface --------
        fpga_rst        => fpga_glbl_rst_i,
        latency         => l0_latency,
        acq_on          => acq_on,
        extTrg_ena      => extTrg_ena,
        trigProc_ena    => trigProc_ena
    );

-- UDP TX
VSB2UDP_inst: VSB2UDP
    port map(
        ---------------------------
        ---- General Interface ----
        clk_ckbc        => clk_40,
        clk_eth         => clk_eth,
        fifo_init       => glbl_fifo_init,
        glbl_bcid       => glbl_bcid,
        art_addr_udp    => art_addr_udp,
        art_valid_udp   => art_valid_udp,
        art_inhibit     => inhibit_artDes,
        ---------------------------
        ---- Trigger Interface ----
        trigger_latched => trg_latched,
        fe_busy         => inhibit_out_i,
        ---------------------------
        ---- UDP Interface --------
        default_destIP  => default_destIP,
        dstPort         => dstPort_daq,
        srcPort         => srcPort_daq,
        udp_tx_dout_rdy => udp_tx_dout_rdy,
        udp_tx_start    => udp_tx_start,
        udp_txi         => udp_tx
    );

-- VSB trigger processor
trigProc_generate : if (generate_trigProc = '1') generate
trigger_processing_core_inst: trigger_processing_core
    generic map(debug_trigProc => debug_trigProc)
    port map(
        ------------------------------------
        ------- General Interface ----------
        clk_ckbc        => clk_40,
        clk_art         => clk_160,
        clk_art2        => clk_320,
        clk_eth         => clk_eth,
        rst             => fpga_glbl_rst_i,
        fifo_init       => glbl_fifo_init,
        bcid_glbl       => glbl_bcid,
        bcid_offset     => bcid_offset,
        inhibit_des     => inhibit_artDes,
        trg_cktp        => trg_cktp,
        alarm           => alarm,
        buff_overflow   => buff_overflow,
        art_addr_udp    => art_addr_udp,
        art_valid_udp   => art_valid_udp,
        -------------------------------------
        ---------- UDP Interface ------------
        wrEn_LUTRAM      => wrEn_LUTRAM,
        din_LUTRAM       => din_LUTRAM,
        wrAddr_LUTRAM    => wrAddr_LUTRAM,
        ------------------------------------
        ---------- VMM Interface -----------
        art_in0         => art_in0_vec,
        art_in1         => art_in1_vec,
        art_in2         => art_in2_vec,
        l1a_vmm_vec     => l1a_vmm,
        l1a_vmm_all     => l1a_vmm_trigProc
    );
end generate trigProc_generate;

-- microTTC
microTTC_inst: microTTC
    generic map(fine_trigger    => fine_trigger,
                ena_perSR       => ena_perSR,
                sr_period       => sr_period)
    port map(
        ------------------------------------
        ------- General Interface ----------
        clk_ckbc        => clk_40,
        clk_art         => clk_160,
        clk_art2        => clk_320,
        rst_oddr        => rst_oddr,
        acq_on          => acq_on,
        mmcm_locked     => masterMMCM_locked(1),
        l0_latency      => l0_latency,
        cktp_trg        => trg_cktp,
        ext_trg_enable  => extTrg_ena,
        trigProc_enable => trigProc_ena,
        trigProc_l0     => l1a_vmm_trigProc,
        trg_latched     => trg_latched,
        rst_latched     => rst_bcid,
        trg_debug       => trg_debug,
        -------------------------------------
        ----------- CTF Interface -----------
        ctf_softReset   => ctf_rst_buf,
        ctf_trigger     => ctf_trg_buf,
        -------------------------------------
        ----------- FE Interface ------------
        fe_busy         => inhibit_out_i,
        fe_ttc          => ttcFE_vec_buf
    );

-- CKTP asserter
VSB_cktp_inst: VSB_cktp
    port map(
        ------------------------------------
        ------- General Interface ----------
        clk_ckbc    => clk_40,
        enable      => ena_cktp,
        rst_oddr    => rst_oddr,
        cktp_trg    => trg_cktp,
        ------------------------------------
        --------- FE Interface -------------
        fe_busy     => inhibit_out_i,
        fe_cktp     => feCKTP_vec_buf
    );

-- RAM filler instantiation
tp_ramFiller_inst: tp_ramFiller
    generic map(addr_offset => "011")
    port map(
        ------------------------------------
        ------- General Interface ----------
        clk_eth         => clk_eth,
        fifo_init       => glbl_fifo_init,
        ------------------------------------
        ------- TP-RAM Interface -----------
        wrEn_LUTRAM      => wrEn_LUTRAM,
        din_LUTRAM       => din_LUTRAM,
        wrAddr_LUTRAM    => wrAddr_LUTRAM
    );
    
-------------------------------------------------------------------
-- I/O Buffers
-------------------------------------------------------------------

resetn_obuf_0:  OBUF  port map (O => I2C_MUX_RESETB, I => resetB);
resetn_obuf_1:  OBUF  port map (O => SI5324_RESETB,  I => resetB);

gen_buffers_led: for I in 0 to 7 generate
led_obuf:       OBUF  port map (O => GPIO_LED(I),    I => gpio_led_i(I));
end generate gen_buffers_led;

gen_buffers_sw: for I in 0 to 4 generate
sw_ibuf:       IBUF  port map (O => gpio_sw_i(I), I => GPIO_SW(I));
end generate gen_buffers_sw;

gen_buffers_dip: for I in 0 to 7 generate
dip_ibuf:      IBUF  port map (O => gpio_dip_i(I), I => GPIO_DIP(I));
end generate gen_buffers_dip;

tx_dis_obuf:    OBUF  port map (O => SFP_TX_DISABLE, I => rst_phy);

refClk_obufds: OBUFDS generic map (IOSTANDARD => "DEFAULT" , SLEW => "FAST") port map (O => SI5324_REFCLK_P, OB => SI5324_REFCLK_N, I => clk_125dir_oddr(0));

interrupt_ibuf: IBUF  port map (O => si5324_int_i,  I => SI5324_INTRPT);
                       
gen_buffersFE: for I in 0 to 7 generate 
inhibit_obufds: OBUFDS generic map (IOSTANDARD => "DEFAULT" , SLEW => "FAST") port map (O => FE_INHIBIT_P(I), OB => FE_INHIBIT_N(I), I => inhibit_vec_buf(I));
refclk_obufds:  OBUFDS generic map (IOSTANDARD => "DEFAULT" , SLEW => "FAST") port map (O => FE_REFCLK_P(I), OB => FE_REFCLK_N(I),   I => clk_40_oddr(I));
rstTrg_obufds:  OBUFDS generic map (IOSTANDARD => "DEFAULT" , SLEW => "FAST") port map (O => FE_TTC_P(I), OB => FE_TTC_N(I), I => ttcFE_vec_buf(I));
cktp_obufds:    OBUFDS generic map (IOSTANDARD => "DEFAULT" , SLEW => "FAST") port map (O => FE_CKTP_P(I), OB => FE_CKTP_N(I), I => feCKTP_vec_buf(I));
busy_ibufds:    IBUFDS generic map (DIFF_TERM => TRUE, IBUF_LOW_PWR => FALSE) port map (O => busy_vec_buf(I), I => FE_BUSY_P(I), IB => FE_BUSY_N(I));
end generate gen_buffersFE;

gen_buffersART: for I in 0 to 2 generate
art_ibufds:    IBUFDS generic map (DIFF_TERM => TRUE, IBUF_LOW_PWR => FALSE) port map (O => art_vec_buf(I), I => FE_ART_P(I), IB => FE_ART_N(I));
end generate gen_buffersART; 

rst_ibufds:    IBUFDS generic map (DIFF_TERM => TRUE, IBUF_LOW_PWR => FALSE) port map (O => ctf_rst_buf, I => CTF_RST_P, IB => CTF_RST_N);
trg_ibufds:    IBUFDS generic map (DIFF_TERM => TRUE, IBUF_LOW_PWR => FALSE) port map (O => ctf_trg_buf, I => CTF_TRG_P, IB => CTF_TRG_N);

-------------------------------------------------------------------
-- Busy/Inhibit Logic
-------------------------------------------------------------------

gen_iddrOddr: for I in 0 to 7 generate

-- INHIBIT SIGNAL DISTRIBUTION
ODDR_INHIBIT: ODDR
    generic map(
        DDR_CLK_EDGE => "SAME_EDGE",
        INIT         => '0',
        SRTYPE       => "SYNC")
    port map(
        Q   => inhibit_vec_buf(I),
        C   => clk_40,
        CE  => '1',
        D1  => inhibit_out_i,
        D2  => inhibit_out_i,
        R   => '0',
        S   => '0' 
    );

-- BUSY SIGNAL INPUT
IDDR_BUSY: IDDR
    generic map (
        DDR_CLK_EDGE => "SAME_EDGE",
        INIT_Q1      => '0',
        INIT_Q2      => '0',
        SRTYPE       => "SYNC")
    port map (
        Q1  => busy_vec_i0(I),
        Q2  => busy_vec_i1(I),
        C   => clk_40,
        CE  => '1',
        D   => busy_vec_buf(I),
        R   => rst_oddr(I),
        S   => '0'
    );

    rst_oddr(I) <= not gpio_dip_i(I);

end generate gen_iddrOddr;

-- busy/inhibit logic
busy_inhib_proc: process(clk_40)
begin
    if(rising_edge(clk_40))then
        inhibit_out_i <= busy_vec_i0(0) or busy_vec_i0(1) or busy_vec_i0(2) or busy_vec_i0(3)
                      or busy_vec_i0(4) or busy_vec_i0(5) or busy_vec_i0(6) or busy_vec_i0(7);
    end if;
end process;

-- pulse the gpio led when in busy mode
busy_pulse_proc: process(clk_40)
begin
    if(rising_edge(clk_40))then
        if(busy_latched = '1')then
            busy_cnt    <= busy_cnt + 1;
            if(busy_cnt = "1111111111111111111111")then
                busy_latched <= '0';
            else
                busy_latched <= '1';
            end if;
        elsif(inhibit_out_i = '1')then
            busy_latched    <= '1';
            busy_cnt        <= (others => '0');
        else
            busy_latched    <= '0';
            busy_cnt        <= (others => '0');
        end if;
    end if;
end process;

-- pulse LED process when in ACQ_ON
daq_pulse_proc: process(clk_40)
begin
    if(rising_edge(clk_40))then
        if(acq_on = '1')then
            pulse_cnt <= pulse_cnt + 1;
            if(pulse_cnt = "1111111111111111111111")then
                daq_on_ack <= not daq_on_ack;    
            else
                daq_on_ack <= daq_on_ack;
            end if;
        else
            pulse_cnt   <= (others => '0');
            daq_on_ack  <= '0';
        end if;
    end if;
end process;

-- process that counts BCIDs
bcid_cnt_proc: process(clk_40)
begin
    if(rising_edge(clk_40))then
        if(glbl_fifo_init = '1' or rst_bcid = '1')then
            bcid_glbl_cnt   <= (others => '0');
        else
            bcid_glbl_cnt   <= bcid_glbl_cnt + 1;
        end if;
    end if;
end process;

    gpio_led_i(0)               <= masterMMCM_locked_all;
    gpio_led_i(1)               <= TXRXmmcm_locked;
    gpio_led_i(2)               <= busy_latched;
    gpio_led_i(3)               <= '0';
    gpio_led_i(4)               <= alarm;
    gpio_led_i(5)               <= buff_overflow;
    gpio_led_i(6)               <= '0';
    gpio_led_i(7)               <= daq_on_ack;
    ena_cktp                    <= (acq_on and (not extTrg_ena));
    inhibit_artDes              <= (not acq_on or (not trigProc_ena));             

    bcid_offset                 <= l0_latency(11 downto 0);
    masterMMCM_locked_all       <= masterMMCM_locked(0) and masterMMCM_locked(1);
    rst_phy                     <= glbl_fifo_init;
    glbl_bcid                   <= std_logic_vector(bcid_glbl_cnt);
    art_in0_vec(0)              <= art_vec_buf(0);
    art_in1_vec(0)              <= art_vec_buf(1);
    art_in2_vec(0)              <= art_vec_buf(2);
    art_in0_vec(7 downto 1)     <= (others => '0');
    art_in1_vec(7 downto 1)     <= (others => '0');
    art_in2_vec(7 downto 1)     <= (others => '0');

end RTL;
