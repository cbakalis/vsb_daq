----------------------------------------------------------------------------------
--! Company:  EDAQ WIS.  
--! Engineer: juna
--! 
--! Create Date:    17/08/2015 
--! Module Name:    Elink2FIFO
--! Project Name:   FELIX
----------------------------------------------------------------------------------
--! Use standard library
library ieee, work, unisim;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.all;
use unisim.VComponents.all;

--! consists of 1 E-path
entity Elink2FIFO is
generic (
    InputDataRate       : integer := 80; -- 80 / 160 / 320 / 640 MHz
    elinkEncoding       : std_logic_vector (1 downto 0); -- 00-direct data / 01-8b10b encoding / 10-HDLC encoding 
    serialized_input    : boolean := true
    );
port ( 
    clk40       : in  std_logic;
    clk80       : in  std_logic;
    clk160      : in  std_logic;    
    clk320      : in  std_logic;
    rst         : in  std_logic;
    fifo_flush  : in  std_logic;
    swap_input  : in  std_logic_vector(1 downto 0);
    ena_roc2udp : in  std_logic;
    inhibitRX   : in  std_logic;
    negateInput : in  std_logic;
    bit_reverse : in  std_logic;
    ------
    DATA1bitIN  : in std_logic := '0';
    elink2bit   : in std_logic_vector (1 downto 0) := (others=>'0'); -- 2 bits @ clk40, can interface 2-bit of GBT frame
    elink4bit   : in std_logic_vector (3 downto 0) := (others=>'0'); -- 4 bits @ clk40, can interface 4-bit of GBT frame
    elink8bit   : in std_logic_vector (7 downto 0) := (others=>'0'); -- 8 bits @ clk40, can interface 8-bit of GBT frame
    -- 640 Mbps e-link can't come in as a serial input yet (additional clock is needed)
    elink16bit  : in std_logic_vector (15 downto 0) := (others=>'0'); -- 16 bits @ clk40, can interface 16-bit of GBT frame
    ------
    efifoRclk   : in  std_logic;
    efifoRe     : in  std_logic; 
    efifoHF     : out std_logic; -- half-full flag: 1 KByte block is ready to be read
    efifoEmpty  : out std_logic;
    fifo_full   : out std_logic;
    data2bitOut : out std_logic_vector (1 downto 0);
    efifoDout   : out std_logic_vector (9 downto 0)
    ------
    );
end Elink2FIFO;

architecture Behavioral of Elink2FIFO is

component elinkRXfifo_wrap
    generic(ENCODING : std_logic_vector(1 downto 0));
    port(
        -----------------------------
        ------ General Interface ---- 
        clk_160         : in  std_logic;
        rst             : in  std_logic;
        inhibit         : in  std_logic;
        flush_fifo      : in  std_logic;
        enable_roc2udp  : in  std_logic;
        -----------------------------
        ---- EPROC_IN2 Interface ----
        din             : in  std_logic_vector(9 downto 0);
        din_rdy         : in  std_logic;
        ----------------------------
        --- User Logic Interface ---
        rd_en_elink     : in  std_logic;
        empty_elink     : out std_logic;
        din_elink       : out std_logic_vector(9 downto 0)
    );
end component;

--
constant maxClen    : std_logic_vector (11 downto 0) := (others => '0'); -- no limit on packet size here
signal DATA2bitIN, shreg2bit : std_logic_vector (1 downto 0) := (others => '0');
signal DATA4bitIN, shreg4bit : std_logic_vector (3 downto 0) := (others => '0');
signal DATA8bitIN, shreg8bit : std_logic_vector (7 downto 0) := (others => '0');
signal DATA_OUT     : std_logic_vector(9 downto 0); 
signal DATA_RDY, FIFO_RESET_STATE, almost_full, BWORD_RDY  : std_logic;
signal BWORD        : std_logic_vector(15 downto 0);
signal data_pos, data_neg : std_logic := '0';
----
signal efifoEmpty_i : std_logic := '0';
signal efifoDout_i  : std_logic_vector(9 downto 0) := (others => '0');
----

-- debugging
component ila_overview
    port(
       clk     : in std_logic;
       probe0  : in std_logic_vector(63 downto 0)
    );
end component;

attribute mark_debug                    : string;
attribute mark_debug of DATA_OUT        : signal is "true";
attribute mark_debug of DATA_RDY        : signal is "true";
attribute mark_debug of efifoEmpty_i    : signal is "true";
attribute mark_debug of efifoDout_i     : signal is "true";
--

begin


------------------------------------------------------------
-- E-PATH case 80 MHz
------------------------------------------------------------
InputDataRate80: if InputDataRate = 80 generate
--
actual_elink_case: if serialized_input = true generate

IDDR_inst_data: IDDR
    generic map (
        DDR_CLK_EDGE => "SAME_EDGE_PIPELINED",
        INIT_Q1      => '0',
        INIT_Q2      => '0',
        SRTYPE       => "SYNC")
    port map (
        Q1  => data_pos,
        Q2  => data_neg,
        C   => clk40,
        CE  => '1',
        D   => DATA1bitIN,
        R   => '0',
        S   => '0'
    );
--
inv_input_proc: process(data_neg, data_pos, swap_input)
begin
    case swap_input(0) is
    when '0'    => shreg2bit <= data_neg & data_pos;
    when '1'    => shreg2bit <= data_pos & data_neg;
    when others => shreg2bit <= (others => '0');
    end case;
end process;
--
process(clk40)
begin
    if rising_edge(clk40) then
        if(negateInput = '0')then
            DATA2bitIN  <= shreg2bit;
    	    data2bitOut <= shreg2bit;
        else
            DATA2bitIN  <= not shreg2bit;
            data2bitOut <= not shreg2bit;
        end if;
    end if;
end process;

--------------------------------------    
--process(clk80)
--begin
--    if rising_edge(clk80) then
--        shreg2bit <= DATA1bitIN & shreg2bit(1);
--    end if;
--end process;
----
--process(clk40)
--begin
--    if rising_edge(clk40) then
--        DATA2bitIN <= shreg2bit;
--    end if;
--end process;
end generate actual_elink_case;
--
--
GBT_frame_case: if serialized_input = false generate
process(clk40)
begin
    if rising_edge(clk40) then
        DATA2bitIN <= elink2bit;
	end if;
end process;
end generate GBT_frame_case; 
--

EPROC_IN2bit: entity work.EPROC_IN2 
generic map (
            ENCODING                => elinkEncoding, -- 00-direct data / 01-8b10b encoding / 10-HDLC encoding
    		do_generate             => true,
    		includeNoEncodingCase   => true
    		)
port map( 
			bitCLK     => clk40,
			bitCLKx2   => clk80,
			bitCLKx4   => clk160,
			rst        => rst,
			ENA        => '1', -- always enabled here
			swap_inputbits   => swap_input(1), -- when '1', the input bits will be swapped
            		bit_reverse      => bit_reverse, -- read bits from MSB to LSB or vice-versa?
			EDATA_IN   => DATA2bitIN, -- @ 40MHz
			DATA_OUT   => DATA_OUT,  -- 10-bit data out
			DATA_RDY   => DATA_RDY
		);

end generate InputDataRate80; 



------------------------------------------------------------
-- E-PATH case 160 MHz
------------------------------------------------------------
InputDataRate160: if InputDataRate = 160 generate

actual_elink_case: if serialized_input = true generate

IDDR_inst_data: IDDR
    generic map (
        DDR_CLK_EDGE => "SAME_EDGE_PIPELINED",
        INIT_Q1      => '0',
        INIT_Q2      => '0',
        SRTYPE       => "SYNC")
    port map (
        Q1  => data_pos,
        Q2  => data_neg,
        C   => clk80,
        CE  => '1',
        D   => DATA1bitIN,
        R   => '0',
        S   => '0'
    );

inv_input_proc: process(data_neg, data_pos, swap_input)
begin
    case swap_input(0) is
    when '0'    => shreg2bit <= data_neg & data_pos;
    when '1'    => shreg2bit <= data_pos & data_neg;
    when others => shreg2bit <= (others => '0');
    end case;
end process;
--

process(clk80)
begin
    if rising_edge(clk80) then
        shreg4bit <= shreg2bit & shreg4bit(3 downto 2);
	end if;
end process;
--
process(clk40)
begin
    if rising_edge(clk40) then
        if(negateInput = '0')then
            DATA4bitIN <= shreg4bit;
        else
            DATA4bitIN <= not shreg4bit;
        end if;
end if;
end process;
end generate actual_elink_case;
--
--
GBT_frame_case: if serialized_input = false generate
process(clk40)
begin
    if rising_edge(clk40) then
        DATA4bitIN <= elink4bit;
	end if;
end process;
end generate GBT_frame_case; 
--

EPROC_IN4bit: entity work.EPROC_IN4 
generic map (
    		do_generate             => true,
    		includeNoEncodingCase   => true
    		)
port map( 
			bitCLK     => clk40,
			rst        => rst,
			ENA        => '1', -- always enabled here
			swap_inputbits   => swap_input(1), -- when '1', the input bits will be swapped
            		bit_reverse      => bit_reverse, -- read bits from MSB to LSB or vice-versa?
			ENCODING   => elinkEncoding,  -- 00-direct data / 01-8b10b encoding / 10-HDLC encoding 
			EDATA_IN   => DATA4bitIN, -- @ 40MHz
			DATA_OUT   => DATA_OUT,  -- 10-bit data out
			DATA_RDY   => DATA_RDY
);

end generate InputDataRate160; 





------------------------------------------------------------
-- E-PATH case 320 MHz
------------------------------------------------------------
InputDataRate320: if InputDataRate = 320 generate

--
actual_elink_case: if serialized_input = true generate

IDDR_inst_data: IDDR
    generic map (
        DDR_CLK_EDGE => "SAME_EDGE_PIPELINED",
        INIT_Q1      => '0',
        INIT_Q2      => '0',
        SRTYPE       => "SYNC")
    port map (
        Q1  => data_pos,
        Q2  => data_neg,
        C   => clk160,
        CE  => '1',
        D   => DATA1bitIN,
        R   => '0',
        S   => '0'
    );

inv_input_proc: process(data_neg, data_pos, swap_input)
begin
    case swap_input(0) is
    when '0'    => shreg2bit <= data_neg & data_pos;
    when '1'    => shreg2bit <= data_pos & data_neg;
    when others => shreg2bit <= (others => '0');
    end case;
end process;

process(clk160)
begin
    if rising_edge(clk160) then
        shreg8bit <= shreg2bit & shreg8bit(7 downto 2);
	end if;
end process;
--
process(clk40)
begin
    if rising_edge(clk40) then
        if(negateInput = '0')then
            DATA8bitIN <= shreg8bit;
        else
            DATA8bitIN <= not shreg8bit;
        end if;
end if;
end process;
end generate actual_elink_case;
--
--
GBT_frame_case: if serialized_input = false generate
process(clk40)
begin
    if rising_edge(clk40) then
        DATA8bitIN <= elink8bit;
	end if;
end process;
end generate GBT_frame_case; 
--

EPROC_IN8bit: entity work.EPROC_IN8 
generic map (
    		do_generate             => true,
    		includeNoEncodingCase   => true
    		)
port map( 
			bitCLK     => clk40,
			rst        => rst,
			ENA        => '1', -- always enabled here
			ENCODING   => elinkEncoding,  -- 00-direct data / 01-8b10b encoding / 10-HDLC encoding 
			swap_inputbits   => swap_input(1), -- when '1', the input bits will be swapped
            		bit_reverse      => bit_reverse, -- read bits from MSB to LSB or vice-versa?
			EDATA_IN   => DATA8bitIN, -- @ 40MHz
			DATA_OUT   => DATA_OUT,  -- 10-bit data out
			DATA_RDY   => DATA_RDY
		);

end generate InputDataRate320; 



------------------------------------------------------------
-- E-PATH case 640 MHz
------------------------------------------------------------
InputDataRate640: if InputDataRate = 640 generate
--
EPROC_IN16bit: entity work.EPROC_IN16 
generic map (
    		do_generate             => true,
    		includeNoEncodingCase   => true
    		)
port map(
			bitCLK     => clk40,
			bitCLKx2   => clk80,
			bitCLKx4   => clk160,
			rst        => rst,
			ENA        => '1', -- always enabled here
			ENCODING   => elinkEncoding,  -- 00-direct data / 01-8b10b encoding / 10-HDLC encoding 
			EDATA_IN   => elink16bit, -- @ 40MHz
			DATA_OUT   => DATA_OUT,  -- 10-bit data out
			DATA_RDY   => DATA_RDY
		);
--
end generate InputDataRate640; 




------------------------------------------------------------
-- EPATH FIFO DRIVER
------------------------------------------------------------
--efd: entity work.EPROC_FIFO_DRIVER 
--generic map(
--    GBTid       		=> 0, -- no use
--    egroupID    		=> 0, -- no use
--    epathID     		=> 0,  -- no use
--    toHostTimeoutBitn   => 8
--    )
--port map (
--    clk40           => clk40,
--    clk160          => clk160,
--    rst             => rst,
--    encoding        => elinkEncoding,--IG "10", -- 00-direct data / 01-8b10b encoding / 10-HDLC encoding 
--    maxCLEN         => "000", -- 000-not limit on packet length
--    raw_DIN         => DATA_OUT,  -- 10-bit data in
--    raw_DIN_RDY     => DATA_RDY,
--    xoff            => almost_full,
--    timeCntIn       => x"00", -- not in use
--    TimeoutEnaIn    => '0',  -- not in use
--    instTimeoutEnaIn=> '0',
--    wordOUT         => BWORD, -- 16-bit block word
--    wordOUT_RDY     => BWORD_RDY,
--    busyOut         => open -- not in use here 
--       );


------------------------------------------------------------
-- EPATH FIFOs
------------------------------------------------------------
--efw: entity work.EPATH_FIFO_WRAP
--port map (
--    rst         => rst,
--    fifoFlush   => fifo_flush,
--    wr_clk 	    => clk160,
--    rd_clk 	    => efifoRclk,
--    din         => BWORD,
--    wr_en       => BWORD_RDY,
--    rd_en       => efifoRe,
--    dout        => efifoDout,
--    almost_full => almost_full,
--    fifo_empty  => efifoEmpty,
--    fifo_full   => fifo_full,
--    prog_full   => efifoHF -- Half-Full - output: 1Kbyte block is ready
--    );

efw: elinkRXfifo_wrap
    generic map(ENCODING => elinkEncoding)
    port map(
        -----------------------------
        ------ General Interface ---- 
        clk_160         => clk160,
        rst             => rst,
        inhibit         => inhibitRX,
        flush_fifo      => fifo_flush,
        enable_roc2udp  => ena_roc2udp,
        -----------------------------
        ---- EPROC_IN2 Interface ----
        din             => DATA_OUT,
        din_rdy         => DATA_RDY,
        ----------------------------
        --- User Logic Interface ---
        rd_en_elink     => efifoRe,
        empty_elink     => efifoEmpty_i,
        din_elink       => efifoDout_i
    );

    efifoDout   <= efifoDout_i;
    efifoEmpty  <= efifoEmpty_i;
    
-- debugging
---
ila_RX: ila_overview
    port map(
        clk                  => clk160,
        probe0(9 downto 0)   => DATA_OUT,
        probe0(10)           => DATA_RDY,
        probe0(11)           => efifoRe,
        probe0(12)           => efifoEmpty_i,
        probe0(22 downto 13) => efifoDout_i,
        probe0(63 downto 23) => (others => '0')
    );
--


end Behavioral;

