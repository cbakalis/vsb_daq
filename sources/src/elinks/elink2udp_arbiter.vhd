----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2017 Christos Bakalis
--
--    This file is part of NTUA-BNL_VMM_firmware.
--
--    NTUA-BNL_VMM_firmware is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    NTUA-BNL_VMM_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA-BNL_VMM_firmware.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 14.11.2017 11:16:15
-- Design Name: UDP Arbiter Logic
-- Module Name: elink2udp_arbiter - RTL
-- Project Name: MMFE8 - NTUA
-- Target Devices: Artix7 xc7a200t-2fbg484 and xc7a200t-3fbg484
-- Tool Versions: Vivado 2017.3
-- Description: Module that enables the two different elink2udp modules, either
-- conf or DAQ
--
-- Dependencies: MMFE8 NTUA Project
-- 
-- Changelog:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use work.axi.all;
use work.ipv4_types.all;
use work.arp_types.all;

entity elink2udp_arbiter is
    generic(daq_bias : std_logic); -- give precedence to DAQ data
    port(
    ---------------------------------
    ------- General Interface -------
    clk_udp             : in  std_logic;
    rst                 : in  std_logic;
    ---------------------------------
    ----- elink2udp Interface -------
    data_rdy_daq        : in  std_logic;
    udp_tx_busy_daq     : in  std_logic;
    udp_tx_start_daq    : in  std_logic;
    udp_txi_daq         : in  udp_tx_type;
    enable_daq2udp      : out std_logic;
    ---------------------------------
    data_rdy_conf       : in  std_logic;
    udp_tx_busy_conf    : in  std_logic;
    udp_tx_start_conf   : in  std_logic;
    udp_txi_conf        : in  udp_tx_type;
    enable_conf2udp     : out std_logic;
    ---------------------------------
    -------- UDP Interface ----------
    udp_tx_start        : out std_logic;
    udp_txi             : out udp_tx_type
    );
end elink2udp_arbiter;

architecture RTL of elink2udp_arbiter is

    signal sel_daq  : std_logic := '0';
    signal wait_cnt : unsigned(1 downto 0) := (others => '0');

    type stateType is (ST_IDLE, ST_SEND_DAQ_0, ST_SEND_DAQ_1, ST_SEND_CONF_0, ST_SEND_CONF_1);
    signal state    : stateType := ST_IDLE;

    attribute FSM_ENCODING              : string;
    attribute FSM_ENCODING of state     : signal is "ONE_HOT";

begin

UDP_arbiter_FSM_proc: process(clk_udp)
begin
    if(rising_edge(clk_udp))then
        if(rst = '1')then
            sel_daq         <= '1';
            enable_daq2udp  <= '0';
            enable_conf2udp <= '0';
            wait_cnt        <= (others => '0');
            state           <= ST_IDLE;
        else
            case state is

            -- check the state of the elink2udp modules
            when ST_IDLE =>
                sel_daq         <= '1';
                enable_daq2udp  <= '0';
                enable_conf2udp <= '0';
                wait_cnt        <= (others => '0');

                if(daq_bias = '1')then -- first check if DAQ has data
                    if(data_rdy_daq = '1')then
                        state <= ST_SEND_DAQ_0;
                    elsif(data_rdy_conf = '1')then
                        state <= ST_SEND_CONF_0;
                    else
                        state <= ST_IDLE;
                    end if;
                else                   -- first check if CONF has data
                    if(data_rdy_conf = '1')then 
                        state <= ST_SEND_CONF_0;
                    elsif(data_rdy_daq = '1')then
                        state <= ST_SEND_DAQ_0;
                    else
                        state <= ST_IDLE;
                    end if;
                end if;

            -- select the DAQ path in the MUX
            when ST_SEND_DAQ_0 =>
                sel_daq         <= '1';
                enable_daq2udp  <= '0';
                enable_conf2udp <= '0';
                wait_cnt        <= wait_cnt + 1;

                if(wait_cnt = "11")then
                    state <= ST_SEND_DAQ_1;
                else
                    state <= ST_SEND_DAQ_0;
                end if;

            -- start sending...
            when ST_SEND_DAQ_1 =>
                sel_daq         <= '1';
                enable_daq2udp  <= '1';
                enable_conf2udp <= '0';
                wait_cnt        <= (others => '0');

                if(data_rdy_daq = '0' and udp_tx_busy_daq = '0')then
                    state <= ST_IDLE;
                else
                    state <= ST_SEND_DAQ_1;
                end if;

            -- select the CONF path in the MUX
            when ST_SEND_CONF_0 =>
                sel_daq         <= '0';
                enable_daq2udp  <= '0';
                enable_conf2udp <= '0';
                wait_cnt        <= wait_cnt + 1;

                if(wait_cnt = "11")then
                    state <= ST_SEND_CONF_1;
                else
                    state <= ST_SEND_CONF_0;
                end if;

            -- start sending...
            when ST_SEND_CONF_1 =>
                sel_daq         <= '0';
                enable_daq2udp  <= '0';
                enable_conf2udp <= '1';
                wait_cnt        <= (others => '0');

                if(data_rdy_conf = '0' and udp_tx_busy_conf = '0')then
                    state <= ST_IDLE;
                else
                    state <= ST_SEND_CONF_1;
                end if;


            when others => state <= ST_IDLE;

            end case;
        end if;
    end if;
end process;

-- which path to select?
MUX2UDP_proc: process(sel_daq, udp_tx_start_conf, udp_txi_conf, udp_txi_daq, udp_tx_start_daq)
begin
    case sel_daq is
    when '0'    =>
        udp_tx_start    <= udp_tx_start_conf;
        udp_txi         <= udp_txi_conf;
    when '1'    =>
        udp_tx_start    <= udp_tx_start_daq;
        udp_txi         <= udp_txi_daq;
    when others =>
        udp_tx_start    <= '0';
        udp_txi         <= udp_txi_daq;
    end case;
end process;


end RTL;
