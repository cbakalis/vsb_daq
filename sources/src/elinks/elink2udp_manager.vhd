-----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2017 Christos Bakalis
--
--    This file is part of NTUA_BNL_VSB_firmware.
--
--    NTUA_BNL_VSB_firmware is free software: you can redistribute it 
--    and/or modify it under the terms of the GNU General Public License 
--    as published by the Free Software Foundation, either version 3 of
--    the License, or (at your option) any later version.
--
--    NTUA_BNL_VSB_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA_BNL_VSB_firmware. If not, see <http://www.gnu.org/licenses/>.
-- 
-- Create Date: 14.02.2018 12:57:11
-- Design Name: Elink2UDP Manager
-- Module Name: elink2udp_manager - RTL
-- Project Name: VSB DAQ - NTUA/BNL
-- Target Devices: Virtex7 xc7vx690tffg1761-2
-- Tool Versions: Vivado 2017.3
-- Description: Component that receives requests for UDP utilization from the elink
-- submodules and gives access to the bus to one submodule at a time.
--
-- Dependencies: NTUA_BNL_VSB_firmware
-- 
-- Changelog: 
--
-----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.axi.all;
use work.ipv4_types.all;
use work.arp_types.all;

entity elink2udp_manager is
    generic(N : integer := 2);
    port(
        ---------------------------------
        ----- General/VIO Interface -----
        clk_eth             : in  std_logic;
        rst                 : in  std_logic;
        ---------------------------------
        ---- elink2UDP Interface --------
        udp_tx_start_i      : in  std_logic_vector(N-1 downto 0);
        udp_txi_i           : in  udp_tx_array;
        udp_tx_dout_rdy_i   : out std_logic_vector(N-1 downto 0);
        ---------------------------------
        ----- VSB2UDP Interface ----
        udp_tx_dout_rdy     : in  std_logic;
        udp_tx_start        : out std_logic;
        udp_txi             : out udp_tx_type
    );
end elink2udp_manager;

architecture RTL of elink2udp_manager is

    signal index    : unsigned(4 downto 0) := (others => '0');
    signal cnt_wait : unsigned(4 downto 0) := (others => '0');
    signal sel_all  : std_logic := '0';
    signal sel_txi  : std_logic := '0';

    type managerFSM is  (ST_SCAN, ST_ASSIGN_UDP_TXI, ST_WAIT, ST_CHK_INDEX);
    signal state        : managerFSM := ST_SCAN;

    attribute FSM_ENCODING          : string;
    attribute FSM_ENCODING of state : signal is "ONE_HOT";

begin

-- managing FSM process
elink2udp_manager_FSM_proc: process(clk_eth)
begin
    if(rising_edge(clk_eth))then
        if(rst = '1')then
            index       <= (others => '0');
            cnt_wait    <= (others => '0');
            sel_txi     <= '0';
            sel_all     <= '0';
            state       <= ST_SCAN;
        else
            case state is

            -- state that scans the udp_tx_start vector of all submodules
            when ST_SCAN =>
                sel_txi <= '0';
                sel_all <= '0';

                if(udp_tx_start_i(to_integer(index)) = '1')then
                    index <= index;
                    state <= ST_ASSIGN_UDP_TXI;
                elsif(udp_tx_start_i(to_integer(index)) = '0' and to_integer(index) /= N-1)then
                    index <= index + 1;
                    state <= ST_SCAN;
                elsif(udp_tx_start_i(to_integer(index)) = '0' and to_integer(index) = N-1)then
                    index <= (others => '0');
                    state <= ST_SCAN;
                else
                    index <= (others => '0');
                    state <= ST_SCAN;
                end if;

            -- request acknowledged
            when ST_ASSIGN_UDP_TXI =>
                sel_txi <= '1';

                -- postpone the selection of the start/rdy signals
                if(cnt_wait = "11111")then
                    sel_all     <= '1';
                    cnt_wait    <= cnt_wait;
                else
                    sel_all     <= '0';
                    cnt_wait    <= cnt_wait + 1;
                end if;

                if(udp_txi_i(to_integer(index)).data.data_out_last = '1')then
                    state <= ST_WAIT;
                else
                    state <= ST_ASSIGN_UDP_TXI;
                end if;

            -- one wait statement
            when ST_WAIT =>
                sel_all <= '0';
                if(cnt_wait = "11110")then -- the counter was already in 11111
                    cnt_wait    <= (others => '0');
                    state       <= ST_CHK_INDEX;
                else
                    cnt_wait    <= cnt_wait + 1;
                    state       <= ST_WAIT;
                end if;

            -- reset the index if it maxed out
            when ST_CHK_INDEX =>
                sel_txi <= '0';
                if(to_integer(index) /= N-1)then
                    index <= index + 1;
                else
                    index <= (others => '0');
                end if;

                state <= ST_SCAN;

            when others => state <= ST_SCAN;
            end case;
        end if;
    end if;
end process;

-- mux that selects the UDP bus. first assigns the udp_txi and then the start/rdy signals
mux_proc: process(sel_txi, sel_all, udp_tx_start_i, udp_txi_i, udp_tx_dout_rdy, index)
begin
    case sel_txi is
    when '0'    =>
        udp_tx_start                <= '0';
        udp_txi.data.data_out_last  <= '0';
        udp_txi.data.data_out_valid <= '0';
        udp_tx_dout_rdy_i           <= (others => '0');
    when '1'    =>
        udp_txi         <= udp_txi_i(to_integer(index));
        if(sel_all = '1')then
            udp_tx_start    <= udp_tx_start_i(to_integer(index));
            udp_tx_dout_rdy_i(to_integer(index)) <= udp_tx_dout_rdy;
        else
            udp_tx_start        <= '0';
            udp_tx_dout_rdy_i   <= (others => '0');
        end if;
    when others =>
        udp_tx_start                <= '0';
        udp_txi.data.data_out_last  <= '0';
        udp_txi.data.data_out_valid <= '0';
        udp_tx_dout_rdy_i           <= (others => '0');
    end case;
end process;

end RTL;
