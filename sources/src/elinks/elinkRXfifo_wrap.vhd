----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2017 Christos Bakalis
--
--    This file is part of NTUA-BNL_VMM_firmware.
--
--    NTUA-BNL_VMM_firmware is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    NTUA-BNL_VMM_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA-BNL_VMM_firmware.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 14.12.2017 19:01:08
-- Design Name: Elink RX-side FIFO Wrapper
-- Module Name: elinkRXfifo_wrap - RTL
-- Project Name: MMFE8 - NTUA
-- Target Devices: Artix7 xc7a200t-2fbg484 and xc7a200t-3fbg484
-- Tool Versions: Vivado 2017.3
-- Description: Wrapper for the upstream FIFO. Only writes non-comma characters
-- for the 8b10b case, and SCA packets in the HDLC case. For the time being only
-- works for 80 Mbps
--
-- Dependencies: MMFE8 NTUA Project
-- 
-- Changelog:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity elinkRXfifo_wrap is
    generic(ENCODING : std_logic_vector(1 downto 0));
    port(
        -----------------------------
        ------ General Interface ---- 
        clk_160         : in  std_logic;
        rst             : in  std_logic;
        inhibit         : in  std_logic;
        flush_fifo      : in  std_logic;
        enable_roc2udp  : in  std_logic;
        -----------------------------
        ---- EPROC_IN2 Interface ----
        din             : in  std_logic_vector(9 downto 0);
        din_rdy         : in  std_logic;
        ----------------------------
        --- User Logic Interface ---
        rd_en_elink     : in  std_logic;
        empty_elink     : out std_logic;
        din_elink       : out std_logic_vector(9 downto 0)
    );
end elinkRXfifo_wrap;

architecture RTL of elinkRXfifo_wrap is

component upstreamFIFO
  port (
    clk     : in  std_logic;
    srst    : in  std_logic;
    din     : in  std_logic_vector(9 downto 0);
    wr_en   : in  std_logic;
    rd_en   : in  std_logic;
    dout    : out std_logic_vector(9 downto 0);
    full    : out std_logic;
    empty   : out std_logic
  );
end component;

    signal wr_en        : std_logic := '0';
    signal flag_pack    : std_logic := '0';
    signal full_elink   : std_logic := '0';

    -- constant that determines how the data will be fed into the FIFO
    --constant delimiter : std_logic := '1'; -- now controlled by enable_roc2udp

begin

-- what to write into the FIFO?
wrFIFO_proc: process(clk_160)
begin
    if(rising_edge(clk_160))then
        if(rst = '1' or inhibit = '1')then
            wr_en       <= '0';
            flag_pack   <= '0';
        else
            case ENCODING is
            when "01" =>
            
                -- 8b10b
                case enable_roc2udp is

                when '1' =>
                -- NOT SIMPLE DELIMITER BEGIN
                    if(wr_en = '1')then -- always make sure you write only once
                        wr_en       <= '0';
                        flag_pack   <= flag_pack;
                    elsif(flag_pack = '1' and din_rdy = '1' and din(9 downto 8) = "11")then -- ignore commas
                        wr_en       <= '0';
                        flag_pack   <= flag_pack;
                    elsif(flag_pack = '1' and din_rdy = '1' and din /= ("01" & "11011100"))then -- 0xdc = 1101 1100 ROC_EOP
                        wr_en       <= '1';
                        flag_pack   <= '1';
                    elsif(flag_pack = '1' and din_rdy = '1' and din = ("01" & "11011100"))then  -- 0xdc = 1101 1100
                        wr_en       <= '1';
                        flag_pack   <= '0'; -- EOP detected. drop the flag
                    elsif(flag_pack = '0' and din_rdy = '1' and din = ("10" & "00111100"))then --0x3c = 0011 1100 ROC_SOP
                        wr_en       <= '1';
                        flag_pack   <= '1'; -- SOP detected. raise the flag, write everything
                    else
                        wr_en       <= '0';
                        flag_pack   <= flag_pack;
                    end if;

                when '0' =>
                -- SIMPLE DELIMITER BEGIN
                    if(wr_en = '1')then -- always make sure you write only once
                        wr_en <= '0';
                    elsif(din_rdy = '1' and din /= "1110111100")then
                        wr_en <= '1';
                    else
                        wr_en <= '0';
                    end if;

                when others => wr_en <= '0'; flag_pack <= '0';
                end case;

            -- HDLC
            when "10" =>
                if(wr_en = '1')then -- always make sure you write only once
                    wr_en       <= '0';
                    flag_pack   <= flag_pack;
                elsif(flag_pack = '1' and din_rdy = '1' and din /= ("01" & "01111110"))then
                    wr_en       <= '1';
                    flag_pack   <= '1';
                elsif(flag_pack = '1' and din_rdy = '1' and din = ("01" & "01111110"))then
                    wr_en       <= '1';
                    flag_pack   <= '0'; -- EOP detected. drop the flag
                elsif(flag_pack = '0' and din_rdy = '1' and din = ("10" & "01111110"))then --0x7e = 0111 1110
                    wr_en       <= '1';
                    flag_pack   <= '1'; -- SOP detected. raise the flag, write everything
                else
                    wr_en       <= '0';
                    flag_pack   <= flag_pack;
                end if;
            when others =>
                wr_en       <= '0';
                flag_pack   <= '0';
            end case;
        end if;
    end if;
end process;

upstreamFIFO_inst: upstreamFIFO
  port map(
    clk     => clk_160,
    srst    => flush_fifo,
    din     => din,
    wr_en   => wr_en,
    rd_en   => rd_en_elink,
    dout    => din_elink,
    full    => full_elink,
    empty   => empty_elink
  );

end RTL;
