-----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2017 Christos Bakalis
--
--    This file is part of NTUA_BNL_VSB_firmware.
--
--    NTUA_BNL_VSB_firmware is free software: you can redistribute it 
--    and/or modify it under the terms of the GNU General Public License 
--    as published by the Free Software Foundation, either version 3 of
--    the License, or (at your option) any later version.
--
--    NTUA_BNL_VSB_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA_BNL_VSB_firmware. If not, see <http://www.gnu.org/licenses/>.
-- 
-- Create Date: 27.10.2016 15:35:29
-- Design Name: Elink TX/RX Wrapper
-- Module Name: elink_TxRx - RTL
-- Project Name: VSB DAQ - NTUA/BNL
-- Target Devices: Virtex7 xc7vx690tffg1761-2
-- Tool Versions: Vivado 2017.3
-- Description: Wrapper for the Elink modules
--
-- Dependencies: NTUA_BNL_VSB_firmware
-- 
-- Changelog: 
-- 17.11.2016 Added the DAQ to elink connection functionality which allows DAQ data
-- to be sent over the e-link transmission line. (Christos Bakalis) 
-- 23.11.2016 Added an extra MMCM status monitoring port. (Christos Bakalis)
-- 29.11.2016 Added the Elink2FIFO rx-module which receives data from the e-link
-- transmission line. Minor changes to other tx-related user logic components.
-- Removed BUFGs from MMCM and placed phsically the whole wrapper near the MMCM via
-- Vivado's floorplanning. (Christos Bakalis)
-- 03.12.2016 Added a loopback functionality, and an enable DAQ input. Removed the
-- internal ILAs and merged them into one at the top of the wrapper. The clocking 
-- of the auxilliary logic is now provided by the e-link MMCM. Added BUFGs to the
-- MMCM clocks and removed floorplanning. (Christos Bakalis)
-- 13.12.2016 Added an option to swap the incoming or outcoming bits of the RX or
-- TX side respectively. (Christos Bakalis)
-- 05.07.2017 Updated the module. E-link MMCM is now external and link speed can 
-- go up to 320 Mbps. Also added a TTC auxiliary module. (Christos Bakalis)
-- 24.08.2017 Updated the module to comply with the ROC e-link format. (Christos Bakalis)
-- 03.09.2017 Added elink2udp_wrapper which receives data from the e-link and forwards 
-- them via UDP. This module supports ROC format. (Christos Bakalis)
-- 14.11.2017 Added HDLC TX/RX modules for SCA support. (Christos Bakalis)
-- 12.02.2018 Migrated the design from the MMFE8 readout firmware to the VSB
-- VC709 firmware. (Christos Bakalis)
--
-----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.axi.all;
use work.ipv4_types.all;
use work.arp_types.all;

entity elink_TxRx is
    generic(DataRate    : integer;      -- 80 / 160 / 320 MHz
            isDAQ       : std_logic     -- if '1' ROC DAQ readout, if '0' SCA conf readout and sending   
    );
    port(
        ---------------------------------
        ----- General/VIO Interface -----
        clk_eth         : in  std_logic;  -- user logic clocking
        glbl_rst        : in  std_logic;  -- reset the entire component
        rst_tx          : in  std_logic;  -- reset the e-link tx sub-component
        rst_rx          : in  std_logic;  -- reset the e-link rx sub-component
        swap_CONFtx     : in  std_logic_vector(1 downto 0);  -- swap the tx-side bits
        swap_CONFrx     : in  std_logic_vector(1 downto 0);  -- swap the rx-side bits
        swap_DAQrx      : in  std_logic_vector(1 downto 0);
        flush_elink2udp : in  std_logic;  -- flush the elink2UDP FIFOs
        enable_roc2udp  : in  std_logic;  -- enable the module that recognizes ROC packets
        udp_packet_size : in  std_logic_vector(15 downto 0); -- maximum UDP packet size, if ROC module is disabled
        inhibitRX       : in  std_logic;  -- inhibit the module's RX
        negateInput     : in  std_logic;  -- negate the DAQ path input
        bit_reverse     : in  std_logic;  -- bit-reverse the 8b10b alignment shift-register
        error_led       : out std_logic;  -- rx data flow is too high
        data2bitOut     : out std_logic_vector (1 downto 0); -- to FELIX
        ---------------------------------
        -------- E-link clocking --------
        clk_40          : in  std_logic;
        clk_80          : in  std_logic;
        clk_160         : in  std_logic;
        clk_320         : in  std_logic;
        elink_locked    : in  std_logic;
        ---------------------------------
        ---- E-link Serial Interface ----
        elink_CONF_tx   : out std_logic;    -- elink CONF tx bus
        elink_CONF_rx   : in  std_logic;    -- elink CONF rx bus
        elink_DAQ_rx    : in  std_logic;    -- elink DAQ rx bus
        ------------------------------------
        ------ SCA Config Interface --------
        hdlc_busy       : out std_logic;    -- unused 
        hdlc_din        : in  std_logic_vector(17 downto 0);
        hdlc_dvalid     : in  std_logic;
        ---------------------------------
        -------- UDP Interface ----------
        default_destIP  : in  std_logic_vector(31 downto 0);
        dstPort_conf    : in  std_logic_vector(15 downto 0);
        srcPort_conf    : in  std_logic_vector(15 downto 0);             
        dstPort_daq     : in  std_logic_vector(15 downto 0);
        srcPort_daq     : in  std_logic_vector(15 downto 0);
        udp_tx_dout_rdy : in  std_logic;
        udp_tx_start    : out std_logic;
        udp_txi         : out udp_tx_type
    );
end elink_TxRx;

architecture RTL of elink_TxRx is

component FIFO2Elink
    generic (
        OutputDataRate  : integer := 80; -- 80 / 160 / 320 MHz
        elinkEncoding   : std_logic_vector (1 downto 0) -- 00-direct data / 01-8b10b encoding / 10-HDLC encoding 
        );
    port ( 
        clk40       : in  std_logic;
        clk80       : in  std_logic;
        clk160      : in  std_logic;
        clk320      : in  std_logic;
        userclk     : in  std_logic;
        rst         : in  std_logic;
        fifo_flush  : in  std_logic;
        swap_output : in  std_logic_vector(1 downto 0);
        ------
        efifoDin    : in  std_logic_vector (17 downto 0);   -- [data_code,2bit][data,16bit]
        efifoWe     : in  std_logic;
        efifoPfull  : out std_logic;
        efifoEmpty  : out std_logic;
        efifoBusy   : out std_logic;
        efifoWclk   : in  std_logic; 
        ------
        DATA1bitOUT : out std_logic; -- serialized output
        elink2bit   : out std_logic_vector (1 downto 0); -- 2 bits @ clk40, can interface 2-bit of GBT frame
        elink4bit   : out std_logic_vector (3 downto 0); -- 4 bits @ clk40, can interface 4-bit of GBT frame
        elink8bit   : out std_logic_vector (7 downto 0)  -- 8 bits @ clk40, can interface 8-bit of GBT frame
        ------
    );
end component;

component Elink2FIFO
    generic (
        InputDataRate       : integer := 80; -- 80 / 160 / 320 / 640 MHz
        elinkEncoding       : std_logic_vector (1 downto 0); -- 00-direct data / 01-8b10b encoding / 10-HDLC encoding
        serialized_input    : boolean := true
        );
    port ( 
        clk40       : in  std_logic;
        clk80       : in  std_logic;
        clk160      : in  std_logic;    
        clk320      : in  std_logic;
        rst         : in  std_logic;
        fifo_flush  : in  std_logic;
        swap_input  : in  std_logic_vector(1 downto 0);
        ena_roc2udp : in  std_logic;
        inhibitRX   : in  std_logic;
        negateInput : in  std_logic;
        bit_reverse : in  std_logic;
        ------
        DATA1bitIN  : in std_logic := '0';
        elink2bit   : in std_logic_vector (1 downto 0) := (others=>'0'); -- 2 bits @ clk40, can interface 2-bit of GBT frame
        elink4bit   : in std_logic_vector (3 downto 0) := (others=>'0'); -- 4 bits @ clk40, can interface 4-bit of GBT frame
        elink8bit   : in std_logic_vector (7 downto 0) := (others=>'0'); -- 8 bits @ clk40, can interface 8-bit of GBT frame
        -- 640 Mbps e-link can't come in as a serial input yet (additional clock is needed)
        elink16bit  : in std_logic_vector (15 downto 0) := (others=>'0'); -- 16 bits @ clk40, can interface 16-bit of GBT frame
        ------
        efifoRclk   : in  std_logic;
        efifoRe     : in  std_logic; 
        efifoHF     : out std_logic; -- half-full flag: 1 KByte block is ready to be read
        efifoEmpty  : out std_logic; -- new
        fifo_full   : out std_logic;
        data2bitOut : out std_logic_vector (1 downto 0); -- to FELIX
        efifoDout   : out std_logic_vector (9 downto 0)
        ------
    );
end component;

component elink2UDP_wrapper
    generic(sca_enable  : std_logic);
    port(
        ---------------------------
        ---- General Interface ----
        clk_elink       : in  std_logic;
        clk_udp         : in  std_logic;
        rst_rx          : in  std_logic;
        flush_rx        : in  std_logic;
        enable_filter   : in  std_logic;
        enable_roc2udp  : in  std_logic;
        packSize_thresh : in  std_logic_vector(15 downto 0); -- how big the SCA packet must be to be sent out via UDP
        error_led       : out std_logic; -- indicating the data flow is too high
        data_ready      : out std_logic; -- ready to transmit UDP data
        udp_tx_busy     : out std_logic; -- busy sending UDP data
        ---------------------------
        ---- Elink Interface ------
        empty_elink     : in  std_logic;
        half_full_elink : in  std_logic;
        full_elink      : in  std_logic;
        rd_en_elink     : out std_logic;
        din_elink       : in  std_logic_vector(9 downto 0);
        ---------------------------
        ---- UDP Interface --------
        default_destIP  : in  std_logic_vector(31 downto 0);
        dstPort         : in  std_logic_vector(15 downto 0);
        srcPort         : in  std_logic_vector(15 downto 0);
        enable_udp_tx   : in  std_logic;
        udp_tx_dout_rdy : in  std_logic;
        udp_tx_start    : out std_logic;
        udp_txi         : out udp_tx_type
        );
end component;

-- function to convert std_logic to std_logic_vector for generic
function isDAQtoEnc (x: std_logic) return std_logic_vector is
begin
    if(x='1')then 
        return "01"; -- 8b10b
    else 
        return "10"; -- HDLC
    end if;
end;

    -- FIFO signals
    signal full_elink_tx        : std_logic := '0';
    signal empty_elink_rx_conf  : std_logic := '0';
    signal empty_elink_rx_daq   : std_logic := '0';
    signal dout_elink2fifo_conf : std_logic_vector(9 downto 0) := (others => '0');
    signal dout_elink2fifo_daq  : std_logic_vector(9 downto 0) := (others => '0');
    signal half_full_rx_conf    : std_logic := '0';
    signal half_full_rx_daq     : std_logic := '0';
    signal rd_ena_conf          : std_logic := '0';
    signal rd_ena_daq           : std_logic := '0'; 
    signal fifo_full_rx_daq     : std_logic := '0';
    signal fifo_full_rx_conf    : std_logic := '0';

    -- elink2UDP signals
    signal udp_tx_start_conf    : std_logic := '0';
    signal udp_tx_start_daq     : std_logic := '0';
    signal udp_txi_conf         : udp_tx_type;
    signal udp_txi_daq          : udp_tx_type;
    signal udp_tx_enable_daq    : std_logic := '0';
    signal udp_tx_enable_conf   : std_logic := '0';
    signal data_ready_daq       : std_logic := '0';
    signal data_ready_conf      : std_logic := '0';
    signal udp_tx_busy_daq      : std_logic := '0';
    signal udp_tx_busy_conf     : std_logic := '0';
    signal udp_tx_dout_rdy_daq  : std_logic := '0';
    signal udp_tx_dout_rdy_conf : std_logic := '0';
    
    signal error_led_conf       : std_logic := '0';
    signal error_led_daq        : std_logic := '0';

    -- resets
    signal rst_i_tx             : std_logic := '1';
    signal rst_i_tx_s0          : std_logic := '1';
    signal rst_i_tx_s1          : std_logic := '1'; -- sync to 80 Mhz
    signal rst_i_rx_s0          : std_logic := '1';
    signal rst_i_rx_s1          : std_logic := '1'; -- sync to 80 Mhz
    signal rst_i_rx             : std_logic := '1';
    signal flush_rx             : std_logic := '1';
    signal flush_tx             : std_logic := '1';
    signal flush_rx_s0          : std_logic := '1';
    signal flush_rx_s1          : std_logic := '1';
    signal flush_tx_s0          : std_logic := '1';
    signal flush_tx_s1          : std_logic := '1';
    signal cnt_init_rx          : unsigned(5 downto 0) := (others => '0');
    signal cnt_init_tx          : unsigned(5 downto 0) := (others => '0');
    signal flush_elink2udp_i    : std_logic := '1';
    signal inhibit_DAQrx        : std_logic := '0';
    signal inhibit_CONFrx       : std_logic := '0';
    
    attribute mark_debug        : string;
    attribute dont_touch        : string;
    attribute ASYNC_REG         : string;

    attribute ASYNC_REG of rst_i_tx_s0    : signal is "true";
    attribute ASYNC_REG of rst_i_tx_s1    : signal is "true";
    attribute ASYNC_REG of rst_i_rx_s0    : signal is "true";
    attribute ASYNC_REG of rst_i_rx_s1    : signal is "true";
    attribute ASYNC_REG of flush_rx_s0    : signal is "true";
    attribute ASYNC_REG of flush_rx_s1    : signal is "true";
    attribute ASYNC_REG of flush_tx_s0    : signal is "true";
    attribute ASYNC_REG of flush_tx_s1    : signal is "true";

begin

CONF_gen: if isDAQ = '0' generate

----------------------------------------------------
-- ============= CONF TX/RX ===================== --
----------------------------------------------------
    
elink_CONF_tx_instance: FIFO2Elink
generic map(OutputDataRate => DataRate, -- 80 / 160 / 320 MHz
            elinkEncoding  => isDAQtoEnc(isDAQ)) -- 00-direct data / 01-8b10b encoding / 10-HDLC encoding
port map(  
    clk40           => clk_40,
    clk80           => clk_80,
    clk160          => clk_160,
    clk320          => clk_320,
    userclk         => clk_eth,
    rst             => rst_i_tx_s1,
    fifo_flush      => flush_tx_s1,
    swap_output     => swap_CONFtx,
    ------
    efifoDin        => hdlc_din,
    efifoWe         => hdlc_dvalid,
    efifoPfull      => full_elink_tx,
    efifoEmpty      => open,
    efifoBusy       => hdlc_busy,
    efifoWclk       => clk_eth, -- must be the same with write clock from udp_din_handler
    ------
    DATA1bitOUT     => elink_CONF_tx,
    elink2bit       => open,
    elink4bit       => open,
    elink8bit       => open
    );

elink_CONF_rx_instance: Elink2FIFO
generic map(InputDataRate  => DataRate, -- 80 / 160 / 320 MHz
            elinkEncoding  => isDAQtoEnc(isDAQ)) -- 00-direct data / 01-8b10b encoding / 10-HDLC encoding
port map( 
    clk40       => clk_40,
    clk80       => clk_80,
    clk160      => clk_160,
    clk320      => clk_320,
    rst         => rst_i_rx_s1,
    fifo_flush  => flush_rx_s1,
    swap_input  => swap_CONFrx,
    ena_roc2udp => enable_roc2udp,
    inhibitRX   => inhibit_CONFrx,
    negateInput => '0', -- not needed in CONF path
    bit_reverse => '0', -- not needed in CONF path
    ------
    DATA1bitIN  => elink_CONF_rx,
    elink2bit   => (others => '0'),
    elink4bit   => (others => '0'),
    elink8bit   => (others => '0'),
    elink16bit  => (others => '0'),
    ------
    efifoRclk   => clk_160,
    efifoRe     => rd_ena_conf, 
    efifoHF     => half_full_rx_conf,
    efifoEmpty  => empty_elink_rx_conf,
    fifo_full   => fifo_full_rx_conf,
    data2bitOut => open,
    efifoDout   => dout_elink2fifo_conf
    ------
    );

elink2UDP_wrapper_CONF: elink2UDP_wrapper
generic map(sca_enable => '1')
port map(
    ---------------------------
    ---- General Interface ----
    clk_elink       => clk_160,
    clk_udp         => clk_eth,
    rst_rx          => rst_i_rx_s1,
    flush_rx        => flush_elink2udp_i,
    enable_filter   => '0', -- decomissioned
    enable_roc2udp  => '0', -- should not be used for the conf path
    packSize_thresh => udp_packet_size,
    error_led       => error_led_conf,
    data_ready      => data_ready_conf,
    udp_tx_busy     => udp_tx_busy_conf,
    ---------------------------
    ---- Elink Interface ------
    empty_elink     => empty_elink_rx_conf,
    half_full_elink => half_full_rx_conf,
    full_elink      => fifo_full_rx_conf,
    rd_en_elink     => rd_ena_conf,
    din_elink       => dout_elink2fifo_conf,
    ---------------------------
    ---- UDP Interface --------
    default_destIP  => default_destIP,
    dstPort         => dstPort_conf,
    srcPort         => srcPort_conf,
    enable_udp_tx   => '1',
    udp_tx_dout_rdy => udp_tx_dout_rdy_conf,
    udp_tx_start    => udp_tx_start_conf,
    udp_txi         => udp_txi_conf
    );

end generate CONF_gen;

----------------------------------------------------
----------------------------------------------------
----------------------------------------------------

DAQ_gen: if isDAQ = '1' generate

----------------------------------------------------
-- ================= DAQ RX ===================== --
----------------------------------------------------

elink_DAQ_rx_instance: Elink2FIFO
generic map(InputDataRate  => DataRate, -- 80 / 160 / 320 MHz
            elinkEncoding  => isDAQtoEnc(isDAQ)) -- 00-direct data / 01-8b10b encoding / 10-HDLC encoding
port map( 
    clk40       => clk_40,
    clk80       => clk_80,
    clk160      => clk_160,
    clk320      => clk_320,
    rst         => rst_i_rx_s1,
    fifo_flush  => flush_rx_s1,
    swap_input  => swap_DAQrx,
    ena_roc2udp => enable_roc2udp,
    inhibitRX   => inhibit_DAQrx,
    negateInput => negateInput,
    bit_reverse => bit_reverse,
    ------
    DATA1bitIN  => elink_DAQ_rx,
    elink2bit   => (others => '0'),
    elink4bit   => (others => '0'),
    elink8bit   => (others => '0'),
    elink16bit  => (others => '0'),
    ------
    efifoRclk   => clk_160,
    efifoRe     => rd_ena_daq, 
    efifoHF     => half_full_rx_daq,
    efifoEmpty  => empty_elink_rx_daq,
    fifo_full   => fifo_full_rx_daq,
    data2bitOut => data2bitOut,
    efifoDout   => dout_elink2fifo_daq
    ------
    );

elink2UDP_wrapper_DAQ: elink2UDP_wrapper
generic map(sca_enable => '0')
port map(
    ---------------------------
    ---- General Interface ----
    clk_elink       => clk_160,
    clk_udp         => clk_eth,
    rst_rx          => rst_i_rx_s1,
    flush_rx        => flush_elink2udp_i,
    enable_filter   => '0', -- decomissioned
    enable_roc2udp  => enable_roc2udp,
    packSize_thresh => udp_packet_size,
    error_led       => error_led_daq,
    data_ready      => data_ready_daq,
    udp_tx_busy     => udp_tx_busy_daq,
    ---------------------------
    ---- Elink Interface ------
    empty_elink     => empty_elink_rx_daq,
    half_full_elink => half_full_rx_daq,
    full_elink      => fifo_full_rx_daq,
    rd_en_elink     => rd_ena_daq,
    din_elink       => dout_elink2fifo_daq,
    ---------------------------
    ---- UDP Interface --------
    default_destIP  => default_destIP,
    dstPort         => dstPort_daq,
    srcPort         => srcPort_daq,
    enable_udp_tx   => '1',
    udp_tx_dout_rdy => udp_tx_dout_rdy_daq,
    udp_tx_start    => udp_tx_start_daq,
    udp_txi         => udp_txi_daq
    );

end generate DAQ_gen;

----------------------------------------------------
----------------------------------------------------
----------------------------------------------------

muxDAQ_CONF_proc: process(udp_tx_dout_rdy, udp_tx_start_daq, 
                          udp_txi_daq, udp_txi_conf, udp_tx_start_conf,
                          error_led_conf, error_led_daq, inhibitRX)
begin
    case isDAQ is
    when '0'    =>
        udp_txi <= udp_txi_conf; udp_tx_start <= udp_tx_start_conf; 
        udp_tx_dout_rdy_conf <= udp_tx_dout_rdy; udp_tx_dout_rdy_daq <= '0';
        inhibit_DAQrx <= '1'; inhibit_CONFrx <= inhibitRX;
        error_led <= error_led_conf;
    when '1'    =>
        udp_txi <= udp_txi_daq; udp_tx_start <= udp_tx_start_daq; 
        udp_tx_dout_rdy_conf <= '0'; udp_tx_dout_rdy_daq <= udp_tx_dout_rdy;
        inhibit_DAQrx <= inhibitRX; inhibit_CONFrx <= '1';
        error_led <= error_led_daq;
    when others =>
        udp_txi <= udp_txi_daq; udp_tx_start <= '0'; 
        udp_tx_dout_rdy_conf <= '0'; udp_tx_dout_rdy_daq <= '0';
        inhibit_DAQrx <= '1'; inhibit_CONFrx <= '1';
        error_led <= '0';
    end case;
end process;

----------------------------------------------------
----------------------------------------------------
----------------------------------------------------


-- creates an internal reset pulse for FIFO2Elink initialization/reset
initRstProc_tx: process(clk_eth)
begin
    if(rising_edge(clk_eth))then
        if(glbl_rst = '1' or rst_tx = '1')then
            cnt_init_tx <= (others => '0');
            rst_i_tx    <= '1';
            flush_tx    <= '1';
        else
            if(elink_locked = '1')then
                if(cnt_init_tx < "101000")then
                    cnt_init_tx <= cnt_init_tx + 1;
                    rst_i_tx    <= '1';
                    flush_tx    <= '1';
                elsif(cnt_init_tx >= "101000" and cnt_init_tx < "111111")then -- first release the flush signal
                    cnt_init_tx <= cnt_init_tx + 1; 
                    rst_i_tx    <= '1';
                    flush_tx    <= '0';
                elsif(cnt_init_tx = "111111")then         -- remain in this state until reset by top
                    cnt_init_tx <= "111111";
                    rst_i_tx    <= '0';
                    flush_tx    <= '0';
                else
                    cnt_init_tx <= (others => '0');
                    rst_i_tx    <= '1';
                    flush_tx    <= '1';
                end if;
            else
                cnt_init_tx <= (others => '0');
                rst_i_tx    <= '1';
                flush_tx    <= '1';
            end if;
        end if; 
    end if;
end process;

-- creates an internal reset pulse for Elink2FIFO initialization/reset
initRstProc_rx: process(clk_eth)
begin
    if(rising_edge(clk_eth))then
        if(glbl_rst = '1' or rst_rx = '1')then
            cnt_init_rx <= (others => '0');
            rst_i_rx    <= '1';
            flush_rx    <= '1';
        else
            if(elink_locked = '1')then
                if(cnt_init_rx < "101000")then
                    cnt_init_rx <= cnt_init_rx + 1;
                    rst_i_rx    <= '1';
                    flush_rx    <= '1';
                elsif(cnt_init_rx >= "101000" and cnt_init_rx < "111111")then -- first release the flush signal
                    cnt_init_rx <= cnt_init_rx + 1;
                    rst_i_rx    <= '1';
                    flush_rx    <= '0';
                elsif(cnt_init_rx = "111111")then         -- remain in this state until reset by top
                    cnt_init_rx <= "111111";
                    rst_i_rx    <= '0';
                    flush_rx    <= '0';
                else
                    cnt_init_rx <= (others => '0');
                    rst_i_rx    <= '1';
                    flush_rx    <= '1';
                end if;
            else
                cnt_init_rx <= (others => '0');
                rst_i_rx    <= '1';
                flush_rx    <= '1';
            end if;
        end if; 
    end if;
end process;

-- reset synchronizer
syncModuleSigs_proc: process(clk_80)
begin
    if(rising_edge(clk_80))then
        rst_i_tx_s0     <= rst_i_tx;
        rst_i_tx_s1     <= rst_i_tx_s0;
        rst_i_rx_s0     <= rst_i_rx;
        rst_i_rx_s1     <= rst_i_rx_s0;
        flush_rx_s0     <= flush_rx;
        flush_rx_s1     <= flush_rx_s0;
        flush_tx_s0     <= flush_tx;
        flush_tx_s1     <= flush_tx_s0;
    end if;
end process;


  flush_elink2udp_i <= flush_rx_s1 or flush_elink2udp; 
    
end RTL;