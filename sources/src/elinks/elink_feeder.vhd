----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2017 Christos Bakalis
--
--    This file is part of NTUA-BNL_VMM_firmware.
--
--    NTUA-BNL_VMM_firmware is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    NTUA-BNL_VMM_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA-BNL_VMM_firmware.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 12.12.2017 18:34:54
-- Design Name: Elink packet feeding block
-- Module Name: elink_feeder - RTL
-- Project Name: MMFE8 - NTUA
-- Target Devices: Artix7 xc7a200t-2fbg484 and xc7a200t-3fbg484
-- Tool Versions: Vivado 2017.3
-- Description: Module that sends a packets to the elink TX module
--
-- Dependencies: MMFE8 NTUA Project
-- 
-- Changelog:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity elink_feeder is
    port(
        -----------------------------
        ------ General Interface ----
        clk_125         : in  std_logic;
        clk_160         : in  std_logic;
        rst             : in  std_logic;
        -----------------------------
        ---- UDP Block Interface ----
        elink_din       : in  std_logic_vector(17 downto 0);
        elink_dvalid    : in  std_logic;
        feeder_busy     : out std_logic;
        -----------------------------
        ------ E-link Interface -----
        dout_rdy        : out std_logic;
        dout            : out std_logic_vector(9 downto 0)
    );
end elink_feeder;

architecture RTL of elink_feeder is

component elinkFeeder_fifo
  port (
    rst         : in  std_logic;
    wr_clk      : in  std_logic;
    rd_clk      : in  std_logic;
    din         : in  std_logic_vector(17 downto 0);
    wr_en       : in  std_logic;
    rd_en       : in  std_logic;
    dout        : out std_logic_vector(17 downto 0);
    full        : out std_logic;
    empty       : out std_logic;
    wr_rst_busy : out std_logic;
    rd_rst_busy : out std_logic
  );
end component;

    signal cnt_rdy          : unsigned(3 downto 0) := (others => '0');
    signal busy             : std_logic := '0';
    signal busy_i           : std_logic := '0';
    signal busy_s125        : std_logic := '0';

    signal FSM_start        : std_logic := '0';
    signal FSM_start_i      : std_logic := '0';
    signal FSM_start_s160   : std_logic := '0';

    signal rd_en            : std_logic := '0';
    signal fifo_empty       : std_logic := '0';
    signal fifo_full        : std_logic := '0';
    signal wr_rst_busy      : std_logic := '0';
    signal rd_rst_busy      : std_logic := '0';
    signal fifo_dout        : std_logic_vector(17 downto 0)  := (others => '0');
    signal dout_pipe        : std_logic_vector(9 downto 0) := (others => '0');

    attribute ASYNC_REG                     : string;
    attribute ASYNC_REG of busy_i           : signal is "TRUE";
    attribute ASYNC_REG of busy_s125        : signal is "TRUE";
    attribute ASYNC_REG of FSM_start_i      : signal is "TRUE";
    attribute ASYNC_REG of FSM_start_s160   : signal is "TRUE";

    type stateType is (ST_IDLE, ST_READ, ST_DRIVE_0, ST_DRIVE_1, ST_DONE); 
    signal state      : stateType := ST_IDLE;

    attribute FSM_ENCODING              : string;
    attribute FSM_ENCODING of state     : signal is "ONE_HOT";

begin

-- EOP flag detector. Wait for FSM to start sending
EOPdet_proc: process(clk_125)
begin
    if(rising_edge(clk_125))then
        -- synchronizer
        busy_i      <= busy;
        busy_s125   <= busy_i;

        if(FSM_start = '1')then
            if(busy_s125 = '1')then
                FSM_start <= '0';
            else
                FSM_start <= '1';
            end if;
        else
            if(elink_dvalid = '1' and elink_din(17 downto 16) = "01")then
                FSM_start <= '1';
            else
                FSM_start <= '0';
            end if;
        end if;
    end if;
end process;

sync_160_proc: process(clk_160)
begin
    if(rising_edge(clk_160))then
        -- synchronizer
        FSM_start_i     <= FSM_start;
        FSM_start_s160  <= FSM_start_i;
    end if;
end process;


-- FSM that reads out the buffer and drives the data to the HDLC core
FSM_send_proc: process(clk_160)
begin
    if(rising_edge(clk_160))then
        if(rst = '1')then
            rd_en       <= '0';
            dout_pipe   <= (others => '0');
            busy        <= '0';
            state       <= ST_IDLE;
        else
            case state is

            -- wait to receive the EOP packet
            when ST_IDLE =>
                if(FSM_start_s160 = '1' and cnt_rdy = "1110")then -- synchronize the two counters
                    state <= ST_READ;
                else
                    state <= ST_IDLE;
                end if;

            -- read out the buffer
            when ST_READ =>
                busy <= '1';
                if(fifo_empty = '0' and cnt_rdy = "0000")then
                    rd_en <= '1';
                    state <= ST_DRIVE_0;
                elsif(fifo_empty = '1' and cnt_rdy = "0000")then
                    state <= ST_DONE;
                else
                    state <= ST_READ;
                end if;

            -- drive the first byte, but keep the flag
            when ST_DRIVE_0 =>
                rd_en <= '0';
                if(cnt_rdy = "1000" and fifo_dout(17 downto 16) = "00")then
                    dout_pipe   <= fifo_dout(17 downto 16) & fifo_dout(15 downto 8);
                    state       <= ST_DRIVE_1;
                elsif(cnt_rdy = "1000" and fifo_dout(17 downto 16) /= "00")then -- SOP/EOP? drive only once
                    dout_pipe   <= fifo_dout(17 downto 16) & fifo_dout(15 downto 8);
                    state       <= ST_READ;
                else
                    state       <= ST_DRIVE_0;
                end if;

            -- drive the second byte, but keep the flag
            when ST_DRIVE_1  =>
                if(cnt_rdy = "1000")then
                    dout_pipe   <= fifo_dout(17 downto 16) & fifo_dout(7 downto 0);
                    state       <= ST_READ;
                else
                    state       <= ST_DRIVE_1;
                end if;

            -- done, wait here until safe to switch the data
            when ST_DONE =>
                if(cnt_rdy = "1000")then
                    busy  <= '0';
                    state   <= ST_IDLE;
                else
                    busy  <= '1';
                    state   <= ST_DONE;
                end if;

            end case;
        end if;
    end if;
end process;

-- dout_rdy asserting process
doutRdy_proc: process(clk_160)
begin
    if(rising_edge(clk_160))then
        if(rst = '1')then
            cnt_rdy     <= (others => '0');
            dout_rdy    <= '0';
        else
            cnt_rdy <= cnt_rdy + 1;
            if(cnt_rdy = "1111")then
                dout_rdy <= '1';
            else
                dout_rdy <= '0';
            end if;
        end if;
    end if;
end process;

-- packet selecting process
packSel_proc: process(clk_160)
begin
    if(rising_edge(clk_160))then
        if(rst = '1')then
            dout <= "11" & x"00"; -- idles
        else
            if(busy = '0')then
                dout <= "11" & x"00"; -- idles
            else
                if(cnt_rdy = "1111")then
                    dout <= dout_pipe; -- pass the word to the HDLC core     
                else null;  
                end if;
            end if;
        end if;
    end if;
end process;

elinkFeeder_buffer: elinkFeeder_fifo
  PORT MAP (
    rst         => rst,
    wr_clk      => clk_125,
    rd_clk      => clk_160,
    din         => elink_din,
    wr_en       => elink_dvalid,
    rd_en       => rd_en,
    dout        => fifo_dout,
    full        => fifo_full,
    empty       => fifo_empty,
    wr_rst_busy => wr_rst_busy,
    rd_rst_busy => rd_rst_busy
  );

  feeder_busy <= busy_s125;

end RTL;
