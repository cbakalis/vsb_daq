----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch)
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2017 Christos Bakalis
--
--    This file is part of NTUA-BNL_VMM_firmware.
--
--    NTUA-BNL_VMM_firmware is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    NTUA-BNL_VMM_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA-BNL_VMM_firmware.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 25.02.2018 18:24:07
-- Design Name: E-link Inhibitor
-- Module Name: elink_inhibitor - RTL
-- Project Name: 
-- Target Devices: Artix-7 xc7a200tfbg484-2 and xc7a200tfbg484-3
-- Tool Versions: Vivado 2017.3
-- Description: Module that opens readout windows in the elink RX modules for ROC and
-- SCA upon the transmission of a L1A or an HDLC frame respectively.
-- 
-- Dependencies: 
-- 
-- Changelog: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity elink_inhibitor is
    port(
        ----------------------------
        ----- General Interface ----
        clk_40          : in  std_logic; -- CKBC
        clk_160         : in  std_logic; -- CKBCx4
        clk_eth         : in  std_logic; -- ethernet 
        rst             : in  std_logic;
        ttc_bits        : in  std_logic_vector(7 downto 0); -- TTC broadcasted bits (with AND of ttc_enable)
        sca_alarm       : out std_logic; -- missed a packet
        roc_alarm       : out std_logic; -- missed a packet
        ttc_enable      : out std_logic; -- only allow TTC commands when ready
        ----------------------------
        ---- InhibFSM Interface ----
        l1a_bitmask     : in  std_logic_vector(7 downto 0); -- which is the L1A?
        hdlc_out_valid  : in  std_logic;    -- are we sending something to the SCA?
        scaReply_valid  : in  std_logic;    -- are we receiving a reply from the SCA?
        rocReply_last   : in  std_logic;    -- are we receiving a packet from the ROC?
        ----------------------------
        ----- E-link Interface -----
        inhibit_roc     : out std_logic;
        inhibit_sca     : out std_logic
    );
end elink_inhibitor;

architecture Behavioral of elink_inhibitor is

component CDCC
    generic(
        NUMBER_OF_BITS : integer := 8); -- number of signals to be synced
    port(
        clk_src     : in  std_logic;                                        -- input clk (source clock)
        clk_dst     : in  std_logic;                                        -- input clk (dest clock)
        data_in     : in  std_logic_vector(NUMBER_OF_BITS - 1 downto 0);    -- data to be synced
        data_out_s  : out std_logic_vector(NUMBER_OF_BITS - 1 downto 0)     -- synced data to clk_dst
        );
end component;

    -- SCA path
    signal inhibit_sca125       : std_logic := '1';
    signal inhibit_sca40        : std_logic := '1';
    signal inhibit_sca160       : std_logic := '1';
    signal sca_timeout40        : std_logic := '0';
    signal sca_timeout125       : std_logic := '0';

    signal SCAtimeout_cnt       : unsigned(15 downto 0) := (others => '0');
    constant SCAtimeout_limit   : unsigned(15 downto 0) := to_unsigned(8191, 16); -- left index * 25ns = limit

    type stateType_sca is (ST_IDLE, ST_WAIT_REPLY);
    signal state_sca : stateType_sca := ST_IDLE;

    -- ROC path
    signal roc_replying125      : std_logic := '0';
    signal roc_replying40       : std_logic := '0';
    signal clear_roc_in125      : std_logic := '0';
    signal clear_roc_in40       : std_logic := '0';
    signal inhibit_roc40        : std_logic := '0';
    signal inhibit_roc160       : std_logic := '0';
    signal l1a_sent             : std_logic := '0';
    signal roc_timeout          : std_logic := '0';
    signal rst125               : std_logic := '0';
    signal cnt_l1a              : unsigned(7 downto 0) := (others => '0');
    
    signal ROCtimeout_cnt       : unsigned(15 downto 0) := (others => '0');
    constant ROCtimeout_limit   : unsigned(15 downto 0) := to_unsigned(8191, 16); -- left index * 25ns = limit

    type stateType_roc is (ST_IDLE, ST_CNT_L1A, ST_WAIT_PACK, ST_CHK_CNT);
    signal state_roc : stateType_roc := ST_IDLE;

    -- alarm LED counters and signals
    signal sca_alarm_i          : std_logic := '0';
    signal roc_alarm_i          : std_logic := '0';
    signal sca_alarm_cnt        : unsigned(23 downto 0) := (others => '0');
    signal roc_alarm_cnt        : unsigned(23 downto 0) := (others => '0');
    constant alarm_thresh       : unsigned(23 downto 0) := x"FFFFFF";

    -- misc
    attribute FSM_ENCODING : string;
    attribute FSM_ENCODING of state_sca : signal is "ONE_HOT";
    attribute FSM_ENCODING of state_roc : signal is "ONE_HOT";

begin

----------------------------------
-- ROC Path
----------------------------------

-- ROC path inhibitor FSM
ROC_inhib_FSM_proc: process(clk_40)
begin
    if(rising_edge(clk_40))then
        if(rst = '1')then
            inhibit_roc40   <= '0';
            ttc_enable      <= '1';
            clear_roc_in40  <= '1';
            cnt_l1a         <= (others => '0');
            state_roc       <= ST_IDLE;
        else
            case state_roc is

            -- has a L1A being sent?
            when ST_IDLE =>
                inhibit_roc40   <= '1';
                ttc_enable      <= '1';
                clear_roc_in40  <= '1';

                if(l1a_sent = '1')then
                    cnt_l1a     <= cnt_l1a + 1;
                    state_roc   <= ST_CNT_L1A;
                else
                    cnt_l1a     <= (others => '0');
                    state_roc   <= ST_IDLE;
                end if;

            -- how many of them actually?
            when ST_CNT_L1A =>
                inhibit_roc40   <= '0';

                if(l1a_sent = '1')then
                    cnt_l1a     <= cnt_l1a + 1;
                    state_roc   <= ST_CNT_L1A;
                else
                    cnt_l1a     <= cnt_l1a;
                    state_roc   <= ST_WAIT_PACK;
                end if;

            -- decrement the L1A counter for each 'last' pulse detected
            -- inhibit any transmission over TTC
            when ST_WAIT_PACK =>
                ttc_enable      <= '0';

                if(roc_replying40 = '1')then
                    clear_roc_in40  <= '1';
                    cnt_l1a         <= cnt_l1a - 1;
                    state_roc       <= ST_CHK_CNT;
                elsif(roc_timeout = '1')then
                     clear_roc_in40  <= '1';
                     cnt_l1a         <= (others => '0');
                    state_roc        <= ST_IDLE;
                else
                    clear_roc_in40  <= '0';
                    cnt_l1a         <= cnt_l1a;
                    state_roc       <= ST_WAIT_PACK;
                end if;    

            -- check the counter state
            when ST_CHK_CNT =>
                if(cnt_l1a = x"00" and roc_replying40 = '0')then -- done!
                    clear_roc_in40  <= '0';
                    state_roc       <= ST_IDLE;
                elsif(cnt_l1a > x"00" and roc_replying40 = '0')then -- more to come
                    clear_roc_in40  <= '0';
                    state_roc       <= ST_WAIT_PACK;
                else
                    clear_roc_in40  <= '1'; -- internal reg not cleared yet
                    state_roc       <= ST_CHK_CNT;
                end if;
                
            when others => state_roc <= ST_IDLE;
            end case;
        end if;
    end if;
end process;

-- internal register for the 'last' pulse of the ethernet interface
last_reg_proc: process(clk_eth)
begin
    if(rising_edge(clk_eth))then
        if(roc_replying125 = '1')then
            if(clear_roc_in125 = '1')then
                roc_replying125 <= '0';
            else
                roc_replying125 <= '1';
            end if;
        elsif(rocReply_last = '1')then
            roc_replying125 <= '1';
        else
            roc_replying125 <= '0';
        end if;
    end if;
end process;


-- synchronous OR gate for the ttc bitmask
or_bitmask_proc: process(clk_40)
begin
    if(rising_edge(clk_40))then
        l1a_sent <= (ttc_bits(7) and l1a_bitmask(7)) or
                    (ttc_bits(6) and l1a_bitmask(6)) or
                    (ttc_bits(5) and l1a_bitmask(5)) or
                    (ttc_bits(4) and l1a_bitmask(4)) or
                    (ttc_bits(3) and l1a_bitmask(3)) or
                    (ttc_bits(2) and l1a_bitmask(2)) or
                    (ttc_bits(1) and l1a_bitmask(1)) or
                    (ttc_bits(0) and l1a_bitmask(0));
    end if;
end process;

-- timeout counter ROC
roc_timeout_proc: process(clk_40)
begin
    if(rising_edge(clk_40))then
        if(rst = '1')then
            roc_timeout     <= '0';
            ROCtimeout_cnt  <= (others => '0');
        else
            if(inhibit_roc40 = '0')then
                if(ROCtimeout_cnt = ROCtimeout_limit)then
                    roc_timeout     <= '1';
                    ROCtimeout_cnt  <= ROCtimeout_cnt;    
                else
                    roc_timeout     <= '0';
                    ROCtimeout_cnt  <= ROCtimeout_cnt + 1;
                end if;
            else
                roc_timeout     <= '0';
                ROCtimeout_cnt  <= (others => '0');
            end if;
        end if;
    end if;
end process;


----------------------------------
----------------------------------

----------------------------------
-- SCA Path
----------------------------------

-- SCA path inhibitor FSM
SCA_inhib_FSM_proc: process(clk_eth)
begin
    if(rising_edge(clk_eth))then
        if(rst125 = '1')then
            inhibit_sca125  <= '0';
            state_sca       <= ST_IDLE;
        else
            case state_sca is

            -- has an sca packet got sent?
            when ST_IDLE =>
                inhibit_sca125 <= '1';

                if(hdlc_out_valid = '1')then
                    state_sca <= ST_WAIT_REPLY;
                else
                    state_sca <= ST_IDLE;
                end if;

            -- wait for reply, or timeout
            when ST_WAIT_REPLY =>
                inhibit_sca125 <= '0';

                if(scaReply_valid = '1')then
                    state_sca <= ST_IDLE;
                elsif(sca_timeout125 = '1')then
                    state_sca <= ST_IDLE;
                else
                    state_sca <= ST_WAIT_REPLY;
                end if;

            when others => state_sca <= ST_IDLE;
            end case;
        end if;
    end if;
end process;

-- timeout counter SCA
sca_timeout_proc: process(clk_40)
begin
    if(rising_edge(clk_40))then
        if(rst = '1')then
            sca_timeout40   <= '0';
            SCAtimeout_cnt  <= (others => '0');
        else
            if(inhibit_sca40 = '0')then
                if(SCAtimeout_cnt = SCAtimeout_limit)then
                    sca_timeout40   <= '1';
                    SCAtimeout_cnt  <= SCAtimeout_cnt;    
                else
                    sca_timeout40   <= '0';
                    SCAtimeout_cnt  <= SCAtimeout_cnt + 1;
                end if;
            else
                sca_timeout40   <= '0';
                SCAtimeout_cnt  <= (others => '0');
            end if;
        end if;
    end if;
end process;

----------------------------------
----------------------------------



----------------------------------
-- CDCC circuitry
----------------------------------

CDCC_125to40_inst: CDCC
    generic map(
        NUMBER_OF_BITS => 2)
    port map(
        clk_src         => clk_eth,
        clk_dst         => clk_40,
        data_in(0)      => inhibit_sca125,
        data_in(1)      => roc_replying125,
        data_out_s(0)   => inhibit_sca40,  -- to sca counter
        data_out_s(1)   => roc_replying40  -- to roc fsm
    );

CDCC_125to160_inst: CDCC
    generic map(
        NUMBER_OF_BITS => 1)
    port map(
        clk_src         => clk_eth,
        clk_dst         => clk_160,
        data_in(0)      => inhibit_sca125,
        data_out_s(0)   => inhibit_sca160 -- to sca elink
    );

CDCC_40to125_inst: CDCC
    generic map(
        NUMBER_OF_BITS => 3)
    port map(
        clk_src         => clk_40,
        clk_dst         => clk_eth,
        data_in(0)      => sca_timeout40,
        data_in(1)      => clear_roc_in40,
        data_in(2)      => rst,
        data_out_s(0)   => sca_timeout125, -- to sca fsm
        data_out_s(1)   => clear_roc_in125, -- to roc int reg
        data_out_s(2)   => rst125
    );

CDCC_40to160_inst: CDCC
    generic map(
        NUMBER_OF_BITS => 1)
    port map(
        clk_src         => clk_40,
        clk_dst         => clk_160,
        data_in(0)      => inhibit_roc40,
        data_out_s(0)   => inhibit_roc160 -- to roc elink
    );

----------------------------------
----------------------------------
        
    inhibit_sca <= inhibit_sca160;
    inhibit_roc <= inhibit_roc160;
    sca_alarm   <= sca_alarm_i;
    roc_alarm   <= roc_alarm_i;

-- alarm LED counters
alarm_cnt_proc: process(clk_40)
begin
    if(rising_edge(clk_40))then

        -- for SCA
        if(sca_alarm_i = '1')then
            if(sca_alarm_cnt = alarm_thresh)then
                sca_alarm_i     <= '0';
                sca_alarm_cnt   <= (others => '0');
            else
                sca_alarm_i     <= '1';
                sca_alarm_cnt   <= sca_alarm_cnt + 1;
            end if;
        elsif(sca_timeout40 = '1')then
            sca_alarm_i     <= '1';
            sca_alarm_cnt   <= sca_alarm_cnt + 1;
        else
            sca_alarm_i     <= '0';
            sca_alarm_cnt   <= (others => '0');
        end if;

        -- for ROC
        if(roc_alarm_i = '1')then
            if(roc_alarm_cnt = alarm_thresh)then
                roc_alarm_i     <= '0';
                roc_alarm_cnt   <= (others => '0');
            else
                roc_alarm_i     <= '1';
                roc_alarm_cnt   <= roc_alarm_cnt + 1;
            end if;
        elsif(roc_timeout = '1')then
            roc_alarm_i     <= '1';
            roc_alarm_cnt   <= roc_alarm_cnt + 1;
        else
            roc_alarm_i     <= '0';
            roc_alarm_cnt   <= (others => '0');
        end if;
    end if;
end process;


end Behavioral;
