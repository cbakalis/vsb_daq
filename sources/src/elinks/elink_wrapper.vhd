-----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2017 Christos Bakalis
--
--    This file is part of NTUA_BNL_VSB_firmware.
--
--    NTUA_BNL_VSB_firmware is free software: you can redistribute it 
--    and/or modify it under the terms of the GNU General Public License 
--    as published by the Free Software Foundation, either version 3 of
--    the License, or (at your option) any later version.
--
--    NTUA_BNL_VSB_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA_BNL_VSB_firmware. If not, see <http://www.gnu.org/licenses/>.
-- 
-- Create Date: 12.02.2018 13:34:21
-- Design Name: Elink General Wrapper
-- Module Name: elink_wrapper - RTL
-- Project Name: VSB DAQ - NTUA/BNL
-- Target Devices: Virtex7 xc7vx690tffg1761-2
-- Tool Versions: Vivado 2017.3
-- Description: Master wrapper for the Elink modules
--
-- Dependencies: NTUA_BNL_VSB_firmware
-- 
-- Changelog: 
-- 26.02.2018 Added TTC generator wrapper. (Christos Bakalis)
--
-----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.axi.all;
use work.ipv4_types.all;
use work.arp_types.all;

entity elink_wrapper is
    port(
        ---------------------------------
        ----- General/VIO Interface -----
        clk_eth             : in  std_logic;  -- ethernet interface clocking
        glbl_rst            : in  std_logic;  -- reset the entire component
        rst_tx              : in  std_logic;  -- reset the e-link tx sub-component
        rst_rx              : in  std_logic;  -- reset the e-link rx sub-component
        swap_CONFtx         : in  std_logic_vector(1 downto 0);  -- swap the tx-side bits
        swap_CONFrx         : in  std_logic_vector(1 downto 0);  -- swap the rx-side bits
        swap_DAQrx          : in  std_logic_vector(1 downto 0);
        flush_elink2udp     : in  std_logic;  -- flush the elink2UDP FIFOs
        enable_roc2udp      : in  std_logic;  -- enable the module that recognizes ROC packets
        udp_packet_size     : in  std_logic_vector(15 downto 0); -- maximum UDP packet size, if ROC module is disabled
        inhibitRX           : in  std_logic_vector(19 downto 0); -- inhibit the RX-side of specific elink modules
        negateInput         : in  std_logic_vector(19 downto 0); -- negate the DAQ path input 
        bit_reverse         : in  std_logic_vector(19 downto 0); -- bit-reverse the 8b10b alignment shift-register
        error_led           : out std_logic;  -- rx data flow is too high
        sca_alarm           : out std_logic;
        roc_alarm           : out std_logic;
        data2bitOut         : out std_logic_vector (1 downto 0); -- to FELIX
        ---------------------------------
        -------- E-link clocking --------
        clk_40              : in  std_logic;
        clk_80              : in  std_logic;
        clk_160             : in  std_logic;
        clk_320             : in  std_logic;
        elink_locked        : in  std_logic;
        ---------------------------------
        ---- E-link Serial Interface ----
        elink_CONF_tx       : out std_logic_vector(3 downto 0); -- elink CONF tx bus
        elink_CONF_rx       : in  std_logic_vector(3 downto 0); -- elink CONF rx bus
        elink_DAQ0_rx       : in  std_logic_vector(3 downto 0); -- elink DAQ rx bus (multiple sROC)
        elink_DAQ1_rx       : in  std_logic_vector(3 downto 0); -- elink DAQ rx bus (multiple sROC)
        elink_DAQ2_rx       : in  std_logic_vector(3 downto 0); -- elink DAQ rx bus (multiple sROC)
        elink_DAQ3_rx       : in  std_logic_vector(3 downto 0); -- elink DAQ rx bus (multiple sROC)
        elink_TTC_tx        : out std_logic_vector(3 downto 0); -- elink TTC tx bus
        ---------------------------------
        ------ SCA Config Interface -----
        frontEnd_bmsk       : in  std_logic_vector(3 downto 0); -- bitmask, where to send conf?
        hdlc_din            : in  std_logic_vector(17 downto 0);
        hdlc_dvalid         : in  std_logic;
        ---------------------------------
        --- TTC Generator Interface -----
        ttc_bits_vio        : in  std_logic_vector(7 downto 0);
        ttc_cnt_limit_vio   : in  std_logic_vector(7 downto 0);
        ttc_bits_shift_vio  : in  std_logic_vector(2 downto 0);
        ttc_enable_vio      : in  std_logic;
        ---------------------------------
        ------- TTC FSM Interface -------
        enable_ctrl         : in  std_logic;
        enable_inhib        : in  std_logic;
        rst_cnt             : in  std_logic;
        l0l1_together       : in  std_logic;
        scan_enable         : in  std_logic;
        SrBcr_off           : in  std_logic_vector(7 downto 0);
        BcrTp_off           : in  std_logic_vector(7 downto 0);
        TpL0_off            : in  std_logic_vector(7 downto 0);
        L0L1_off            : in  std_logic_vector(7 downto 0);
        cktp_prio           : in  std_logic_vector(23 downto 0);
        cktp_limit          : in  std_logic_vector(11 downto 0);
        --
        sr_bitmask          : in  std_logic_vector(7 downto 0);
        bcr_bitmask         : in  std_logic_vector(7 downto 0);
        tp_bitmask          : in  std_logic_vector(7 downto 0);
        l0a_bitmask         : in  std_logic_vector(7 downto 0);
        l1a_bitmask         : in  std_logic_vector(7 downto 0);
        l1a_cnt_limit       : in  std_logic_vector(7 downto 0);
        ---------------------------------
        -------- UDP Interface ----------
        default_destIP      : in  std_logic_vector(31 downto 0);
        dstPort_conf        : in  std_logic_vector(15 downto 0); -- start value
        srcPort_conf        : in  std_logic_vector(15 downto 0); -- start value            
        dstPort_daq         : in  std_logic_vector(15 downto 0); -- start value
        srcPort_daq         : in  std_logic_vector(15 downto 0); -- start value
        udp_tx_dout_rdy     : in  std_logic;
        udp_tx_start        : out std_logic;
        udp_txi             : out udp_tx_type
    );
end elink_wrapper;

architecture RTL of elink_wrapper is

component elink_TxRx
    generic(DataRate    : integer;      -- 80 / 160 / 320 MHz
            isDAQ       : std_logic    -- if '1' ROC DAQ readout, if '0' SCA conf readout and sending
    );   
    port(
        ---------------------------------
        ----- General/VIO Interface -----
        clk_eth         : in  std_logic;  -- user logic clocking
        glbl_rst        : in  std_logic;  -- reset the entire component
        rst_tx          : in  std_logic;  -- reset the e-link tx sub-component
        rst_rx          : in  std_logic;  -- reset the e-link rx sub-component
        swap_CONFtx     : in  std_logic_vector(1 downto 0);  -- swap the tx-side bits
        swap_CONFrx     : in  std_logic_vector(1 downto 0);  -- swap the rx-side bits
        swap_DAQrx      : in  std_logic_vector(1 downto 0);
        flush_elink2udp : in  std_logic;  -- flush the elink2UDP FIFOs
        enable_roc2udp  : in  std_logic;  -- enable the module that recognizes ROC packets
        udp_packet_size : in  std_logic_vector(15 downto 0); -- maximum UDP packet size, if ROC module is disabled
        inhibitRX       : in  std_logic;  -- inhibit the module's RX
        negateInput     : in  std_logic;  -- negate the DAQ path input
        bit_reverse     : in  std_logic;  -- bit-reverse the 8b10b alignment shift-register
        error_led       : out std_logic;  -- rx data flow is too high
        data2bitOut     : out std_logic_vector (1 downto 0); -- to FELIX
        ---------------------------------
        -------- E-link clocking --------
        clk_40          : in  std_logic;
        clk_80          : in  std_logic;
        clk_160         : in  std_logic;
        clk_320         : in  std_logic;
        elink_locked    : in  std_logic;
        ---------------------------------
        ---- E-link Serial Interface ----
        elink_CONF_tx   : out std_logic;    -- elink CONF tx bus
        elink_CONF_rx   : in  std_logic;    -- elink CONF rx bus
        elink_DAQ_rx    : in  std_logic;    -- elink DAQ rx bus
        ------------------------------------
        ------ SCA Config Interface --------
        hdlc_busy       : out std_logic;    -- unused 
        hdlc_din        : in  std_logic_vector(17 downto 0);
        hdlc_dvalid     : in  std_logic;
        ---------------------------------
        -------- UDP Interface ----------
        default_destIP  : in  std_logic_vector(31 downto 0);
        dstPort_conf    : in  std_logic_vector(15 downto 0);
        srcPort_conf    : in  std_logic_vector(15 downto 0);             
        dstPort_daq     : in  std_logic_vector(15 downto 0);
        srcPort_daq     : in  std_logic_vector(15 downto 0);
        udp_tx_dout_rdy : in  std_logic;
        udp_tx_start    : out std_logic;
        udp_txi         : out udp_tx_type
    );
end component;

component ttc_gen_wrapper
    port(
        ----------------------------
        ----- General Interface ----
        clk_40              : in  std_logic;
        clk_160             : in  std_logic;
        clk_320             : in  std_logic;
        clk_80              : in  std_logic;
        clk_eth             : in  std_logic;
        mmcm_locked         : in  std_logic;
        rst                 : in  std_logic;
        ----------------------------
        ----- TTC Gen Interface ----
        ttc_bits_vio        : in  std_logic_vector(7 downto 0);
        ttc_cnt_limit_vio   : in  std_logic_vector(7 downto 0);
        ttc_bits_shift_vio  : in  std_logic_vector(2 downto 0);
        ttc_enable_vio      : in  std_logic;
        ----------------------------
        ----- TTC FSM Interface ----
        enable_ctrl         : in  std_logic;
        rst_cnt             : in  std_logic;
        l0l1_together       : in  std_logic;
        scan_enable         : in  std_logic;
        SrBcr_off           : in  std_logic_vector(7 downto 0);
        BcrTp_off           : in  std_logic_vector(7 downto 0);
        TpL0_off            : in  std_logic_vector(7 downto 0);
        L0L1_off            : in  std_logic_vector(7 downto 0);
        cktp_prio           : in  std_logic_vector(23 downto 0);
        cktp_limit          : in  std_logic_vector(11 downto 0);
        --
        sr_bitmask          : in  std_logic_vector(7 downto 0);
        bcr_bitmask         : in  std_logic_vector(7 downto 0);
        tp_bitmask          : in  std_logic_vector(7 downto 0);
        l0a_bitmask         : in  std_logic_vector(7 downto 0);
        l1a_bitmask         : in  std_logic_vector(7 downto 0);
        l1a_cnt_limit       : in  std_logic_vector(7 downto 0);
        ----------------------------
        --- Inhibitor Interface ----
        hdlc_out_valid      : in  std_logic; -- are we sending something to the SCA?
        scaReply_valid      : in  std_logic; -- are we receiving a reply from the SCA?
        rocReply_last       : in  std_logic; -- are we receiving a packet from the ROC?
        enable_inhib        : in  std_logic; -- use the inhibitor module
        inhibit_roc         : out std_logic; -- inhibit ROC packet reception
        inhibit_sca         : out std_logic; -- inhibit SCA packet reception
        sca_alarm           : out std_logic; -- missed a packet
        roc_alarm           : out std_logic; -- missed a packet
        ----------------------------
        ------- ROC Interface ------
        ttc_streamDbg       : out std_logic;
        ttc_stream          : out std_logic
    );
end component;

component elink2udp_manager
    generic(N : integer := 2); -- number of elink_TxRx instances
    port(
        ---------------------------------
        ----- General/VIO Interface -----
        clk_eth             : in  std_logic;
        rst                 : in  std_logic;
        ---------------------------------
        ---- elink2UDP Interface --------
        udp_tx_start_i      : in  std_logic_vector(N-1 downto 0);
        udp_txi_i           : in  udp_tx_array;
        udp_tx_dout_rdy_i   : out std_logic_vector(N-1 downto 0);
        ---------------------------------
        ----- VSB2UDP Interface ----
        udp_tx_dout_rdy     : in  std_logic;
        udp_tx_start        : out std_logic;
        udp_txi             : out udp_tx_type
    );
end component;

    -- some signals may need expansion if more elink_TxRx instances are added
    signal error_led_i          : std_logic_vector(1 downto 0) := (others => '0');  -- add more
    signal hdlc_dvalid_i        : std_logic_vector(3 downto 0) := (others => '0');
    signal udp_tx_start_i       : std_logic_vector(1 downto 0) := (others => '0');  -- add more
    signal udp_tx_dout_rdy_i    : std_logic_vector(1 downto 0) := (others => '0');  -- add more
    signal inhibit_roc          : std_logic_vector(1 downto 0) := (others => '0');  -- add more
    signal inhibit_sca          : std_logic_vector(1 downto 0) := (others => '0');  -- add more
    signal sca_alarm_i          : std_logic_vector(1 downto 0) := (others => '0');  -- add more
    signal roc_alarm_i          : std_logic_vector(1 downto 0) := (others => '0');  -- add more
    signal inhibitRX_i          : std_logic_vector(1 downto 0) := (others => '0');  -- add more
    signal udp_txi_manager      : udp_tx_type;
    signal udp_tx_start_manager : std_logic := '0';
    signal udp_txi_i            : udp_tx_array;
    
    type srcPort_conf_array is array (0 to 3) of std_logic_vector(15 downto 0);
    signal srcPort_conf_i : srcPort_conf_array;
    type srcPort_daq_array is array (0 to 3) of std_logic_vector(15 downto 0);
    signal srcPort_daq_i : srcPort_daq_array;
    
    -- if more signals are added, please add more entries in the udp_tx_array type at 
    -- the ipv4_types.vhd file. it looks like the commented-out entry below

    -- type udp_tx_array is array (0 to 1) of udp_tx_type;

-- debugging
-----
--component ila_overview
--    port(
--       clk     : in std_logic;
--       probe0  : in std_logic_vector(63 downto 0)
--    );
--end component;

--attribute mark_debug : string;
--attribute mark_debug of udp_txi_manager         : signal is "TRUE";
--attribute mark_debug of udp_tx_start_manager    : signal is "TRUE";
--attribute mark_debug of udp_tx_dout_rdy_i       : signal is "TRUE";
--attribute mark_debug of hdlc_dvalid_i           : signal is "TRUE";
--attribute mark_debug of hdlc_din                : signal is "TRUE";
--attribute mark_debug of udp_tx_start_i          : signal is "TRUE";
-----
    
begin

-- copy paste the commented-out templates if you want to add more modules. 
-- pay attention to the vector/array indices
-- every board has its own UDP source port (calculated at the end of this file)
-- every elink instance has its own inhibit bit that should be high if its elink line is unconnected
-- every elink instance has its own udp bus that drives the elink2udp_manager
-- don't forget to increment the N generic of the elink2udp_manager when adding new modules

-----------------------------
-- Board 0
-----------------------------
board0_SCA_inst: elink_TxRx
    generic map(DataRate    => 80,
                isDAQ       => '0')
    port map(
        ---------------------------------
        ----- General/VIO Interface -----
        clk_eth         => clk_eth,
        glbl_rst        => glbl_rst,
        rst_tx          => rst_tx,
        rst_rx          => rst_rx,
        swap_CONFtx     => swap_CONFtx,
        swap_CONFrx     => swap_CONFrx,
        swap_DAQrx      => swap_DAQrx,
        flush_elink2udp => flush_elink2udp,
        enable_roc2udp  => enable_roc2udp,
        udp_packet_size => udp_packet_size,
        inhibitRX       => inhibitRX_i(0),
        negateInput     => negateInput(0),
        bit_reverse     => bit_reverse(0),
        error_led       => error_led_i(0),
        data2bitOut     => open,
        ---------------------------------
        -------- E-link clocking --------
        clk_40          => clk_40,
        clk_80          => clk_80,
        clk_160         => clk_160,
        clk_320         => clk_320,
        elink_locked    => elink_locked,
        ---------------------------------
        ---- E-link Serial Interface ----
        elink_CONF_tx   => elink_CONF_tx(0),
        elink_CONF_rx   => elink_CONF_rx(0),
        elink_DAQ_rx    => '0',
        ------------------------------------
        ------ SCA Config Interface --------
        hdlc_busy       => open,
        hdlc_din        => hdlc_din,
        hdlc_dvalid     => hdlc_dvalid_i(0),
        ---------------------------------
        -------- UDP Interface ----------
        default_destIP  => default_destIP,
        dstPort_conf    => dstPort_conf,
        srcPort_conf    => srcPort_conf_i(0),
        dstPort_daq     => (others => '0'),
        srcPort_daq     => (others => '0'),
        udp_tx_dout_rdy => udp_tx_dout_rdy_i(0),
        udp_tx_start    => udp_tx_start_i(0),
        udp_txi         => udp_txi_i(0)
    );

-- first sROC
board0_ROC0_inst: elink_TxRx 
    generic map(DataRate    => 80,
                isDAQ       => '1')
    port map(
        ---------------------------------
        ----- General/VIO Interface -----
        clk_eth         => clk_eth,
        glbl_rst        => glbl_rst,
        rst_tx          => rst_tx,
        rst_rx          => rst_rx,
        swap_CONFtx     => swap_CONFtx,
        swap_CONFrx     => swap_CONFrx,
        swap_DAQrx      => swap_DAQrx,
        flush_elink2udp => flush_elink2udp,
        enable_roc2udp  => enable_roc2udp,
        udp_packet_size => udp_packet_size,
        inhibitRX       => inhibitRX_i(1),
        negateInput     => negateInput(1),
        bit_reverse     => bit_reverse(1),
        error_led       => error_led_i(1),
        data2bitOut     => data2bitOut,
        ---------------------------------
        -------- E-link clocking --------
        clk_40          => clk_40,
        clk_80          => clk_80,
        clk_160         => clk_160,
        clk_320         => clk_320,
        elink_locked    => elink_locked,
        ---------------------------------
        ---- E-link Serial Interface ----
        elink_CONF_tx   => open,
        elink_CONF_rx   => '0',
        elink_DAQ_rx    => elink_DAQ0_rx(0),
        ------------------------------------
        ------ SCA Config Interface --------
        hdlc_busy       => open,
        hdlc_din        => (others => '0'),
        hdlc_dvalid     => '0',
        ---------------------------------
        -------- UDP Interface ----------
        default_destIP  => default_destIP,
        dstPort_conf    => (others => '0'),
        srcPort_conf    => (others => '0'),
        dstPort_daq     => dstPort_daq,
        srcPort_daq     => srcPort_daq_i(0),
        udp_tx_dout_rdy => udp_tx_dout_rdy_i(1),
        udp_tx_start    => udp_tx_start_i(1),
        udp_txi         => udp_txi_i(1)
    );

-- second sROC
--board0_ROC1_inst: elink_TxRx 
    --generic map(DataRate    => 80,
    --            isDAQ       => '1')
--    port map(
--        ---------------------------------
--        ----- General/VIO Interface -----
--        clk_eth         => clk_eth,
--        glbl_rst        => glbl_rst,
--        rst_tx          => rst_tx,
--        rst_rx          => rst_rx,
--        swap_CONFtx     => swap_CONFtx,
--        swap_CONFrx     => swap_CONFrx,
--        swap_DAQrx      => swap_DAQrx,
--        flush_elink2udp => flush_elink2udp,
--        enable_roc2udp  => enable_roc2udp,
--        udp_packet_size => udp_packet_size,
--        inhibitRX       => inhibitRX(2),
--        negateInput     => negateInput(2),
--        bit_reverse     => bit_reverse(2),
--        error_led       => error_led_i(2),
--        ---------------------------------
--        -------- E-link clocking --------
--        clk_40          => clk_40,
--        clk_80          => clk_80,
--        clk_160         => clk_160,
--        clk_320         => clk_320,
--        elink_locked    => elink_locked,
--        ---------------------------------
--        ---- E-link Serial Interface ----
--        elink_CONF_tx   => open,
--        elink_CONF_rx   => '0',
--        elink_DAQ_rx    => elink_DAQ0_rx(1),
--        ------------------------------------
--        ------ SCA Config Interface --------
--        hdlc_busy       => open,
--        hdlc_din        => (others => '0'),
--        hdlc_dvalid     => '0',
--        ---------------------------------
--        -------- UDP Interface ----------
--        default_destIP  => default_destIP,
--        dstPort_conf    => (others => '0'),
--        srcPort_conf    => (others => '0'),
--        dstPort_daq     => dstPort_daq,
--        srcPort_daq     => srcPort_daq_i(0),
--        udp_tx_dout_rdy => udp_tx_dout_rdy_i(2),
--        udp_tx_start    => udp_tx_start_i(2),
--        udp_txi         => udp_txi_i(2)
--    );

-- ...
-- ... add more sROC here

-- one TTC module for each board
ttc_gen_wrapper_inst: ttc_gen_wrapper
    port map(
        ----------------------------
        ----- General Interface ----
        clk_40              => clk_40,
        clk_160             => clk_160,
        clk_320             => clk_320,
        clk_80              => clk_80,
        clk_eth             => clk_eth,
        mmcm_locked         => elink_locked,
        rst                 => glbl_rst,
        ----------------------------
        ----- TTC Gen Interface ----
        ttc_bits_vio        => ttc_bits_vio,
        ttc_cnt_limit_vio   => ttc_cnt_limit_vio,
        ttc_bits_shift_vio  => ttc_bits_shift_vio,
        ttc_enable_vio      => ttc_enable_vio,
        ----------------------------
        ----- TTC FSM Interface ----
        enable_ctrl         => enable_ctrl,
        rst_cnt             => rst_cnt,
        l0l1_together       => l0l1_together,
        scan_enable         => scan_enable,
        SrBcr_off           => SrBcr_off,
        BcrTp_off           => BcrTp_off,
        TpL0_off            => TpL0_off,
        L0L1_off            => L0L1_off,
        cktp_prio           => cktp_prio,
        cktp_limit          => cktp_limit,
        --
        sr_bitmask          => sr_bitmask,
        bcr_bitmask         => bcr_bitmask,
        tp_bitmask          => tp_bitmask,
        l0a_bitmask         => l0a_bitmask,
        l1a_bitmask         => l1a_bitmask,
        l1a_cnt_limit       => l1a_cnt_limit,
        ----------------------------
        --- Inhibitor Interface ----
        hdlc_out_valid      => hdlc_dvalid_i(0),
        scaReply_valid      => udp_txi_i(0).data.data_out_last, -- 0 is SCA reply
        rocReply_last       => udp_txi_i(1).data.data_out_last, -- 0 is sROC0 reply (you will need an OR gate here from other sROCs probably)
        enable_inhib        => enable_inhib,
        inhibit_roc         => inhibit_roc(0),
        inhibit_sca         => inhibit_sca(0),
        sca_alarm           => sca_alarm_i(0),
        roc_alarm           => roc_alarm_i(0),
        ----------------------------
        ----- VMM Interface --------
        ttc_streamDbg       => elink_TTC_tx(1),
        ttc_stream          => elink_TTC_tx(0)
    );

-----------------------------
-- Board 1
-----------------------------
--board1_SCA_inst: elink_TxRx
--    generic map(DataRate    => 80,
--                isDAQ       => '0')
--    port map(
--        ---------------------------------
--        ----- General/VIO Interface -----
--        clk_eth         => clk_eth,
--        glbl_rst        => glbl_rst,
--        rst_tx          => rst_tx,
--        rst_rx          => rst_rx,
--        swap_CONFtx     => swap_CONFtx,
--        swap_CONFrx     => swap_CONFrx,
--        swap_DAQrx      => swap_DAQrx,
--        flush_elink2udp => flush_elink2udp,
--        enable_roc2udp  => enable_roc2udp,
--        udp_packet_size => udp_packet_size,
--        inhibitRX       => inhibitRX(5),
--        negateInput     => negateInput(5),
--        bit_reverse     => bit_reverse(5),
--        error_led       => error_led_i(5),
--        ---------------------------------
--        -------- E-link clocking --------
--        clk_40          => clk_40,
--        clk_80          => clk_80,
--        clk_160         => clk_160,
--        clk_320         => clk_320,
--        elink_locked    => elink_locked,
--        ---------------------------------
--        ---- E-link Serial Interface ----
--        elink_CONF_tx   => elink_CONF_tx(1),
--        elink_CONF_rx   => elink_CONF_rx(1),
--        elink_DAQ_rx    => '0',
--        ------------------------------------
--        ------ SCA Config Interface --------
--        hdlc_busy       => open,
--        hdlc_din        => hdlc_din,
--        hdlc_dvalid     => hdlc_dvalid_i(1),
--        ---------------------------------
--        -------- UDP Interface ----------
--        default_destIP  => default_destIP,
--        dstPort_conf    => dstPort_conf,
--        srcPort_conf    => srcPort_conf_i(1),
--        dstPort_daq     => (others => '0'),
--        srcPort_daq     => (others => '0'),
--        udp_tx_dout_rdy => udp_tx_dout_rdy_i(5),
--        udp_tx_start    => udp_tx_start_i(5),
--        udp_txi         => udp_txi_i(5)
--    );

---- first sROC
--board1_ROC0_inst: elink_TxRx 
--    generic map(DataRate    => 80,
--                isDAQ       => '1')
--    port map(
--        ---------------------------------
--        ----- General/VIO Interface -----
--        clk_eth         => clk_eth,
--        glbl_rst        => glbl_rst,
--        rst_tx          => rst_tx,
--        rst_rx          => rst_rx,
--        swap_CONFtx     => swap_CONFtx,
--        swap_CONFrx     => swap_CONFrx,
--        swap_DAQrx      => swap_DAQrx,
--        flush_elink2udp => flush_elink2udp,
--        enable_roc2udp  => enable_roc2udp,
--        udp_packet_size => udp_packet_size,
--        inhibitRX       => inhibitRX(6),
--        negateInput     => negateInput(6),
--        bit_reverse     => bit_reverse(6),
--        error_led       => error_led_i(6),
--        ---------------------------------
--        -------- E-link clocking --------
--        clk_40          => clk_40,
--        clk_80          => clk_80,
--        clk_160         => clk_160,
--        clk_320         => clk_320,
--        elink_locked    => elink_locked,
--        ---------------------------------
--        ---- E-link Serial Interface ----
--        elink_CONF_tx   => open,
--        elink_CONF_rx   => '0',
--        elink_DAQ_rx    => elink_DAQ1_rx(0),
--        ------------------------------------
--        ------ SCA Config Interface --------
--        hdlc_busy       => open,
--        hdlc_din        => (others => '0'),
--        hdlc_dvalid     => '0',
--        ---------------------------------
--        -------- UDP Interface ----------
--        default_destIP  => default_destIP,
--        dstPort_conf    => (others => '0'),
--        srcPort_conf    => (others => '0'),
--        dstPort_daq     => dstPort_daq,
--        srcPort_daq     => srcPort_daq_i(1),
--        udp_tx_dout_rdy => udp_tx_dout_rdy_i(6),
--        udp_tx_start    => udp_tx_start_i(6),
--        udp_txi         => udp_txi_i(6)
--    );

-- ...
-- ... add more sROC here
-- ... add another TTC instance here

-- ... more boards?

--------------------------
-- elink2UDP manager
--------------------------
elink2udp_manager_inst: elink2udp_manager
    generic map(N => 2) -- number of elink_TxRx instances
    port map(
        ---------------------------------
        ----- General/VIO Interface -----
        clk_eth             => clk_eth,
        rst                 => glbl_rst,
        ---------------------------------
        ---- elink2UDP Interface --------
        udp_tx_start_i      => udp_tx_start_i,
        udp_txi_i           => udp_txi_i,
        udp_tx_dout_rdy_i   => udp_tx_dout_rdy_i,
        ---------------------------------
        ----- VSB2UDP Interface ----
        udp_tx_dout_rdy     => udp_tx_dout_rdy,
        udp_tx_start        => udp_tx_start_manager,
        udp_txi             => udp_txi_manager
    );
   
-- debugging
--------
--ila_udpManager: ila_overview
--    port map(
--        clk                  => clk_eth,
--        probe0(1 downto 0)   => udp_tx_start_i,
--        probe0(3 downto 2)   => udp_tx_dout_rdy_i,
--        probe0(4)            => udp_txi_i(0).data.data_out_valid,
--        probe0(5)            => udp_txi_i(0).data.data_out_last,
--        probe0(6)            => udp_txi_i(1).data.data_out_last,
--        probe0(7)            => udp_txi_i(1).data.data_out_valid,
--        probe0(15 downto 8)  => udp_txi_i(0).data.data_out,
--        probe0(23 downto 16) => udp_txi_i(1).data.data_out,
--        probe0(24)           => udp_txi_manager.data.data_out_last,
--        probe0(25)           => udp_txi_manager.data.data_out_valid,
--        probe0(33 downto 26) => udp_txi_manager.data.data_out,
--        probe0(34)           => udp_tx_start_manager,
--        probe0(35)           => udp_tx_dout_rdy,
--        probe0(36)           => hdlc_dvalid_i(0),
--        probe0(54 downto 37) => hdlc_din,
--        probe0(63 downto 55) => (others => '0')
--    );
--------

    udp_txi             <= udp_txi_manager;
    udp_tx_start        <= udp_tx_start_manager;

    hdlc_dvalid_i(0)    <= hdlc_dvalid and frontEnd_bmsk(0);
    hdlc_dvalid_i(1)    <= hdlc_dvalid and frontEnd_bmsk(1);
    hdlc_dvalid_i(2)    <= hdlc_dvalid and frontEnd_bmsk(2);
    hdlc_dvalid_i(3)    <= hdlc_dvalid and frontEnd_bmsk(3);

    error_led           <= error_led_i(0) or error_led_i(1);
    sca_alarm           <= sca_alarm_i(0) or sca_alarm_i(1);
    roc_alarm           <= roc_alarm_i(0) or roc_alarm_i(1);
    
    inhibitRX_i(0)      <= inhibitRX(0) or inhibit_sca(0); -- from top or from TTC inhibitor
    inhibitRX_i(1)      <= inhibitRX(1) or inhibit_roc(0); -- from top or from TTC inhibitor

    srcPort_conf_i(0)   <= srcPort_conf;
    srcPort_conf_i(1)   <= std_logic_vector(unsigned(srcPort_conf) + x"0001");
    srcPort_conf_i(2)   <= std_logic_vector(unsigned(srcPort_conf) + x"0002");
    srcPort_conf_i(3)   <= std_logic_vector(unsigned(srcPort_conf) + x"0003");

    srcPort_daq_i(0)    <= srcPort_daq;
    srcPort_daq_i(1)    <= std_logic_vector(unsigned(srcPort_daq) + x"0001");
    srcPort_daq_i(2)    <= std_logic_vector(unsigned(srcPort_daq) + x"0002");
    srcPort_daq_i(3)    <= std_logic_vector(unsigned(srcPort_daq) + x"0003");

end RTL;
