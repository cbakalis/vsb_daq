----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2017 Christos Bakalis
--
--    This file is part of NTUA-BNL_VMM_firmware.
--
--    NTUA-BNL_VMM_firmware is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    NTUA-BNL_VMM_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA-BNL_VMM_firmware.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 21.11.2017 16:08:28
-- Design Name: SCA Testing Block
-- Module Name: sca_tester - RTL
-- Project Name: MMFE8 - NTUA
-- Target Devices: Artix7 xc7a200t-2fbg484 and xc7a200t-3fbg484
-- Tool Versions: Vivado 2017.3
-- Description: Module that sends a packet to the SCA to read its ID
--
-- Dependencies: MMFE8 NTUA Project
-- 
-- Changelog:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.axi.all;
use work.ipv4_types.all;
use work.arp_types.all;

entity sca_tester is
    port(
    ------------------------------------
    ------- General Interface ----------
    clk_in          : in std_logic;
    ena_sca_test    : in std_logic;
    ------------------------------------
    ----- UDP Handler Interface --------
    hdlc_dout_udp   : in std_logic_vector(17 downto 0);
    hdlc_dvalid_udp : in std_logic;
    ------------------------------------
    ------ SCA Config Interface --------
    hdlc_buff_empty : in  std_logic;
    hdlc_dout       : out std_logic_vector(17 downto 0);
    hdlc_dvalid     : out std_logic
    );
end sca_tester;

architecture RTL of sca_tester is

--component CRC_0bytes
--    port(
--        clk     : in  std_logic;
--        rst     : in  std_logic;
--        crc_en  : in  std_logic;
--        din     : in  std_logic_vector(47 downto 0);
--        crc_out : out std_logic_vector(15 downto 0)
--    );
--end component;

    signal sel_mux          : std_logic := '0';
    signal hdlc_dout_tst    : std_logic_vector(17 downto 0) := (others => '0');
    signal hdlc_dvalid_tst  : std_logic := '0';
    signal wr_elink         : std_logic := '0';
    signal crc_en           : std_logic := '0';
    signal crc_rst          : std_logic := '0';
    signal cnt_data         : unsigned(2 downto 0) := (others => '0');
    signal wait_cnt         : unsigned(1 downto 0) := (others => '0');
    signal fcs_o            : std_logic_vector(15 downto 0) := (others => '0');
    signal din_crc          : std_logic_vector(47 downto 0) := (others => '0');

    type stateType is (ST_IDLE, ST_WRITE, ST_CNT, ST_WAIT, ST_DONE);
    signal state       : stateType := ST_IDLE;
    signal state_prv   : stateType := ST_IDLE;

    attribute FSM_ENCODING              : string;
    attribute FSM_ENCODING of state     : signal is "ONE_HOT";

begin

-- FSM that drives the data to the HDLC TX core
FSM_sca_tester_proc: process(clk_in)
begin
    if(rising_edge(clk_in))then
        if(ena_sca_test = '0')then
            sel_mux     <= '0';
            wr_elink    <= '0';
            crc_en      <= '0';
            crc_rst     <= '1';
            cnt_data    <= (others => '0');
            wait_cnt    <= (others => '0');
            state       <= ST_IDLE;
        else
            case state is
            when ST_IDLE =>
                crc_en      <= '1';
                crc_rst     <= '0';
                sel_mux     <= '1';
                state_prv   <= ST_IDLE;
                state       <= ST_WAIT; -- to ST_WRITE

            -- write the data to the HDLC core
            when ST_WRITE =>
                wr_elink    <= '1';
                state_prv   <= ST_WRITE;
                state       <= ST_WAIT; -- to ST_CNT

            -- drive the data to the MUX
            when ST_CNT =>
                cnt_data    <= cnt_data + 1;
                state_prv   <= ST_CNT;
                if(cnt_data = "011")then
                    state <= ST_DONE;
                else
                    state <= ST_WAIT; -- to ST_WRITE;
                end if;

            -- stay here until reset by VIO
            when ST_DONE =>
                state <= ST_DONE;

            -- generic state that waits
            when ST_WAIT =>
                wr_elink    <= '0';
                wait_cnt    <= wait_cnt + 1;

                if(wait_cnt = "11")then
                    case state_prv is
                    when ST_IDLE    => state <= ST_WRITE;
                    when ST_WRITE   => state <= ST_CNT;
                    when ST_CNT     => state <= ST_WRITE;
                    when others     => state <= ST_WAIT;
                    end case;
                else
                    state <= ST_WAIT;
                end if;


            when others => state <= ST_IDLE;
            end case;
        end if;
    end if;
end process;

-- data to be sent
-- NO 7E AT FIRST
--hdlc_dout_sel_proc: process(cnt_data, fcs_o)
--begin
--    case cnt_data is
--    when "000"  => hdlc_dout_tst <= "10" & x"00" & x"00"; -- SOP
--    when "001"  => hdlc_dout_tst <= "00" & x"7E" & x"00"; -- SOF & ADDR
--    when "010"  => hdlc_dout_tst <= "00" & x"2F" & x"01"; -- CTRL & TR_ID
--    when "011"  => hdlc_dout_tst <= "00" & x"14" & x"00"; -- CHAN & LEN
--    when "100"  => hdlc_dout_tst <= "00" & x"91" & fcs_o(15 downto 8); -- CMD & FCS(1/2) 
--    when "101"  => hdlc_dout_tst <= "00" & fcs_o(7 downto 0) & x"7E"; -- FCS (2/2) & EOF
--    when "110"  => hdlc_dout_tst <= "01" & x"00" & x"00"; -- EOP;
--    when "111"  => hdlc_dout_tst <= "10" & x"00" & x"00";
--    when others => hdlc_dout_tst <= (others => '0');
--    end case;
--end process;

hdlc_dout_sel_proc: process(cnt_data, fcs_o)
begin
    case cnt_data is
    when "000"  => hdlc_dout_tst <= "10" & x"00" & x"00"; -- SOP == SOF
    when "001"  => hdlc_dout_tst <= "00" & x"00" & x"8F"; -- ADDR & CTRL
    when "010"  => hdlc_dout_tst <= "00" & x"47" & x"8C"; -- TR_ID & CHAN
    when "011"  => hdlc_dout_tst <= "01" & x"00" & x"00"; -- LEN & CMD
--    when "100"  => hdlc_dout_tst <= "00" & fcs_o;         -- FCS
--    when "101"  => hdlc_dout_tst <= "01" & x"00" & x"00"; -- EOP;
--    when "110"  => hdlc_dout_tst <= "01" & x"00" & x"00"; -- EOP;
--    when "111"  => hdlc_dout_tst <= "10" & x"00" & x"00";
    when others => hdlc_dout_tst <= (others => '0');
    end case;
end process;


-- HDLC din MUX
hdlc_dout_mux_proc: process(sel_mux, hdlc_dout_udp, hdlc_dvalid_udp, hdlc_dout_tst, hdlc_dvalid_tst)
begin
    case sel_mux is
    when '0'    => hdlc_dout <= hdlc_dout_udp; hdlc_dvalid <= hdlc_dvalid_udp;
    when '1'    => hdlc_dout <= hdlc_dout_tst; hdlc_dvalid <= hdlc_dvalid_tst;
    when others => hdlc_dout <= hdlc_dout_udp; hdlc_dvalid <= hdlc_dvalid_udp;
    end case;
end process;

    hdlc_dvalid_tst <= wr_elink;
    din_crc         <= x"00" & x"2F" & x"01" & x"14" & x"00" & x"91";
    fcs_o           <= x"17C2";

--CRC_inst: CRC_0bytes
--    Port Map(
--        clk     => clk_in,
--        rst     => crc_rst,
--        crc_en  => crc_en,
--        din     => din_crc,
--        crc_out => fcs_o
--    );


end RTL;
