-----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2018 Christos Bakalis
--
--    This file is part of NTUA_BNL_VSB_firmware.
--
--    NTUA_BNL_VSB_firmware is free software: you can redistribute it 
--    and/or modify it under the terms of the GNU General Public License 
--    as published by the Free Software Foundation, either version 3 of
--    the License, or (at your option) any later version.
--
--    NTUA_BNL_VSB_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA_BNL_VSB_firmware. If not, see <http://www.gnu.org/licenses/>.
-- 
-- Create Date: 04.02.2018 19:36:25
-- Design Name: VSB Fine Trigger
-- Module Name: VSB_fineTrg - RTL
-- Project Name: VSB DAQ - NTUA/BNL
-- Target Devices: Virtex7 xc7vx690tffg1761-2
-- Tool Versions: Vivado 2017.3
-- Description: Module that asserts the trigger, either aligned with rising-edge
-- of CKBC (LHC-like) or not
--
-- Dependencies: NTUA_BNL_VSB_firmware
--
-- Changelog:
-- 
-----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity VSB_fineTrg is
    generic(fine_trigger : std_logic);
    port(
        ------------------------------------
        ------- General Interface ----------
        clk_ckbc        : in  std_logic;
        clk_art2        : in  std_logic;
        mmcm_locked     : in  std_logic;
        -------------------------------------
        ----------- CTF Interface -----------
        ctf_trigger     : in  std_logic;
        -------------------------------------
        ----------- TTC Interface -----------
        ext_trg         : out std_logic
    );
end VSB_fineTrg;

architecture RTL of VSB_fineTrg is

    signal ena_cnt      : std_logic := '0';
    signal ctf_trg_i0   : std_logic := '0';
    signal ctf_trg_i1   : std_logic := '0';
    signal ext_trg_i    : std_logic := '0';
    signal trg_sreg     : std_logic_vector(7 downto 0)  := (others => '0');
    signal align_cnt    : unsigned(2 downto 0)          := (others => '0');

begin

-- align with CKBC process
align_ckbc_proc: process(clk_ckbc)
begin
    if(rising_edge(clk_ckbc))then
        if(mmcm_locked = '1')then
            ena_cnt <= '1';
        else
            ena_cnt <= '0';
        end if;
    end if;
end process;

-- cnt ticks
tick_cnt_proc: process(clk_art2)
begin
    if(rising_edge(clk_art2))then
        if(ena_cnt = '1')then
            align_cnt <= align_cnt + "01";
        else
            align_cnt <= (others => '0');
        end if;
    end if;
end process;

-- mux
trg_mux_proc: process(ena_cnt, align_cnt, ctf_trg_i0, ctf_trg_i1, trg_sreg, ext_trg_i)
begin
    case fine_trigger is
    when '0' =>

        ext_trg_i <= ctf_trg_i0 or ctf_trg_i1;

        -- it is long enough...(check with oscilloscope if necessary)
        ext_trg <= ext_trg_i;

    when '1' =>
        if(ena_cnt = '1' and align_cnt = "000" and ctf_trg_i0 = '1' and ctf_trg_i1 = '0')then
            ext_trg_i <= '1';
        else
            ext_trg_i <= '0';
        end if;

        -- fail-safe for short trigger pulses
        ext_trg <= trg_sreg(7) or trg_sreg(6) or trg_sreg(5) or trg_sreg(4)
                or trg_sreg(3) or trg_sreg(2) or trg_sreg(1) or trg_sreg(0);

    when others =>
        ext_trg_i <= '0';

    end case;
end process;

-- trigger sreg
trg_sreg_proc: process(clk_art2)
begin
    if(rising_edge(clk_art2))then
        trg_sreg <= ext_trg_i & trg_sreg(7 downto 1);
    end if;
end process;

IDDR_TRG: IDDR
    generic map (
        DDR_CLK_EDGE => "SAME_EDGE",
        INIT_Q1      => '0',
        INIT_Q2      => '0',
        SRTYPE       => "SYNC")
    port map (
        Q1  => ctf_trg_i0,
        Q2  => ctf_trg_i1,
        C   => clk_art2,
        CE  => '1',
        D   => ctf_trigger,
        R   => '0',
        S   => '0'
    );

end RTL;
