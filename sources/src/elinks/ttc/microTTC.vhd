-----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2018 Christos Bakalis
--
--    This file is part of NTUA_BNL_VSB_firmware.
--
--    NTUA_BNL_VSB_firmware is free software: you can redistribute it 
--    and/or modify it under the terms of the GNU General Public License 
--    as published by the Free Software Foundation, either version 3 of
--    the License, or (at your option) any later version.
--
--    NTUA_BNL_VSB_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA_BNL_VSB_firmware. If not, see <http://www.gnu.org/licenses/>.
-- 
-- Create Date: 04.02.2018 19:36:25
-- Design Name: VSB TTC
-- Module Name: microTTC - RTL
-- Project Name: VSB DAQ - NTUA/BNL
-- Target Devices: Virtex7 xc7vx690tffg1761-2
-- Tool Versions: Vivado 2017.3
-- Description: Module that asserts L0 or soft-reset in 2-bit TTC
--
-- Dependencies: NTUA_BNL_VSB_firmware
--
-- Changelog:
-- 
-----------------------------------------------------------------------------------
library IEEE;
library UNISIM;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use UNISIM.VComponents.all;

entity microTTC is
    generic(fine_trigger    : std_logic;
            ena_perSR       : std_logic;
            sr_period       : std_logic_vector(15 downto 0));
    port(
        ------------------------------------
        ------- General Interface ----------
        clk_ckbc        : in  std_logic;
        clk_art         : in  std_logic;
        clk_art2        : in  std_logic;
        rst_oddr        : in  std_logic_vector(7 downto 0);
        acq_on          : in  std_logic;
        mmcm_locked     : in  std_logic;
        l0_latency      : in  std_logic_vector(15 downto 0);
        cktp_trg        : in  std_logic;
        ext_trg_enable  : in  std_logic;
        trigProc_enable : in  std_logic;
        trigProc_l0     : in  std_logic;
        trg_latched     : out std_logic;
        rst_latched     : out std_logic;
        trg_debug       : out std_logic;
        -------------------------------------
        ----------- CTF Interface -----------
        ctf_softReset   : in  std_logic;
        ctf_trigger     : in  std_logic;
        -------------------------------------
        ----------- FE Interface ------------
        fe_busy         : in  std_logic;
        fe_ttc          : out std_logic_vector(7 downto 0)
    );
end microTTC;

architecture RTL of microTTC is

component VSB_fineTrg
    generic(fine_trigger : std_logic);
    port(
        ------------------------------------
        ------- General Interface ----------
        clk_ckbc        : in  std_logic;
        clk_art2        : in  std_logic;
        mmcm_locked     : in  std_logic;
        -------------------------------------
        ----------- CTF Interface -----------
        ctf_trigger     : in  std_logic;
        -------------------------------------
        ----------- TTC Interface -----------
        ext_trg         : out std_logic
    );
end component;

component microTTC_perSR
    generic(ena_perSR : std_logic;
            sr_period : std_logic_vector(15 downto 0));
    port(
        ------------------------------------
        ------- General Interface ----------
        clk_ckbc        : in  std_logic;
        acq_on          : in  std_logic;
        fe_busy         : in  std_logic;
        trg_busy        : in  std_logic;
        trg_inhib       : out std_logic;
        periodic_sr     : out std_logic
    );
end component;

    signal ctf_rst_i0       : std_logic := '0';
    signal ctf_rst_i1       : std_logic := '0';
    signal ext_trg          : std_logic := '0';
    signal ext_trg_i        : std_logic := '0';
    signal trg_final        : std_logic := '0';
    signal oddr_vec         : std_logic_vector(1 downto 0) := (others => '0');
    signal fe_softReset     : std_logic := '0';
    signal fe_trigger       : std_logic := '0';
    signal trg_latched_i    : std_logic := '0';
    signal rst_latched_i    : std_logic := '0';
    signal trg_inhib        : std_logic := '0';
    signal periodic_sr      : std_logic := '0';
    signal wait_cnt_sr      : unsigned(25 downto 0) := (others => '0');
    signal wait_cnt_trg     : unsigned(15 downto 0) := (others => '0');
    signal sr_sreg          : std_logic_vector(31 downto 0) := (others => '0');

    type stateType_soft is (ST_IDLE, ST_ASSERT, ST_WAIT);
    type stateType_trig is (ST_IDLE, ST_LATENCY, ST_ASSERT, ST_WAIT_BUSY_HIGH, ST_WAIT_BUSY_LOW, ST_SLEEP, ST_WAIT_TRG);
    signal state_sr        : stateType_soft := ST_IDLE;
    signal state_trg       : stateType_trig := ST_IDLE;

    attribute FSM_ENCODING              : string;
    attribute FSM_ENCODING of state_sr  : signal is "ONE_HOT";
    attribute FSM_ENCODING of state_trg : signal is "ONE_HOT";

begin

-- soft-reset asserting process
srAssert_proc: process(clk_ckbc)
begin
    if(rising_edge(clk_ckbc))then
        if(acq_on = '1')then
            fe_softReset    <= '0';
            rst_latched_i   <= '0';
            wait_cnt_sr     <= (others => '0');
            state_sr        <= ST_IDLE;
        else
            case state_sr is

            -- wait for it...
            when ST_IDLE =>
                fe_softReset    <= '0';
                rst_latched_i   <= '0';
                wait_cnt_sr     <= (others => '0');

                if(ctf_rst_i0 = '1' or ctf_rst_i1 = '1')then
                    state_sr <= ST_ASSERT;
                else
                    state_sr <= ST_IDLE;
                end if;

            -- assert SR
            when ST_ASSERT =>
                fe_softReset    <= '1';
                rst_latched_i   <= '1';
                wait_cnt_sr     <= (others => '0');
                state_sr        <= ST_WAIT;

            -- inhibit for ~1.6s
            when ST_WAIT =>
                fe_softReset    <= '0';
                rst_latched_i   <= '0';
                wait_cnt_sr     <= wait_cnt_sr + 1;

                if(wait_cnt_sr = "11111111111111111111111111")then
                    state_sr   <= ST_IDLE;
                else
                    state_sr   <= ST_WAIT;
                end if;

            when others => state_sr <= ST_IDLE;
            end case;
        end if;
    end if;
end process;

-- l0/trigger asserting process
trgAssert_proc: process(clk_ckbc)
begin
    if(rising_edge(clk_ckbc))then
        if(acq_on = '0')then
            fe_trigger      <= '0';
            trg_latched_i   <= '0';
            wait_cnt_trg    <= (others => '0');
            state_trg       <= ST_IDLE;
        else
            case state_trg is

            -- wait for it...
            when ST_IDLE =>
                fe_trigger      <= '0';
                trg_latched_i   <= '0';
                wait_cnt_trg    <= (others => '0');

                if(trg_final = '1')then
                    state_trg <= ST_LATENCY;
                else
                    state_trg <= ST_IDLE;
                end if;

            -- start counting to the latency limit
            when ST_LATENCY =>
                fe_trigger      <= '0';
                trg_latched_i   <= '1';
                wait_cnt_trg    <= wait_cnt_trg + 1;

                if((wait_cnt_trg >= unsigned(l0_latency)) or (trigProc_enable = '1'))then
                    state_trg   <= ST_ASSERT;
                else                    
                    state_trg   <= ST_LATENCY;
                end if;

            -- assert TRG
            when ST_ASSERT =>
                fe_trigger      <= '1';
                trg_latched_i   <= '1';
                wait_cnt_trg    <= (others => '0');
                state_trg       <= ST_WAIT_BUSY_HIGH;

            -- wait for FEs to go BUSY
            when ST_WAIT_BUSY_HIGH =>
                fe_trigger      <= '0';
                trg_latched_i   <= '1';
                wait_cnt_trg    <= wait_cnt_trg + 1;

                if(fe_busy = '1')then
                    state_trg <= ST_WAIT_BUSY_LOW;
                elsif(wait_cnt_trg >= x"0032")then -- 50 in decimal = 1250 ns
                    state_trg <= ST_WAIT_TRG;
                else
                    state_trg <= ST_WAIT_BUSY_HIGH;
                end if;

            -- wait for FEs to go to IDLE
            when ST_WAIT_BUSY_LOW =>
                fe_trigger      <= '0';
                trg_latched_i   <= '1';
                wait_cnt_trg    <= (others => '0');

                if(fe_busy = '0')then
                    state_trg <= ST_SLEEP;
                else
                    state_trg <= ST_WAIT_BUSY_LOW;
                end if;

            -- sleep for a bit before accepting more triggers (let all boards go to idle,
            -- as there is a propagation delay between the busy_low here and when the FEs get it)
            when ST_SLEEP =>
                fe_trigger      <= '0';
                trg_latched_i   <= '1';
                wait_cnt_trg    <= wait_cnt_trg + 1;

                if(wait_cnt_trg = x"0005")then -- wait 125ns
                    state_trg   <= ST_WAIT_TRG;
                else
                    state_trg   <= ST_SLEEP;
                end if;

            -- make sure we always catch trigger at rising edge
            when ST_WAIT_TRG =>
                fe_trigger      <= '0';
                trg_latched_i   <= '0';
                wait_cnt_trg    <= (others => '0');

                if(trg_final = '0')then
                    state_trg <= ST_IDLE;
                else
                    state_trg <= ST_WAIT_TRG;
                end if;

            when others => state_trg <= ST_IDLE;
            end case;
        end if;
    end if;
end process;

-- pipeline & shift-register
pipeOut_proc: process(clk_ckbc)
begin
    if(rising_edge(clk_ckbc))then
        if(acq_on = '1')then
            oddr_vec <= fe_trigger & (periodic_sr and (not trigProc_enable));
        else
            oddr_vec <= '0' & fe_softReset;
        end if;
        sr_sreg  <= rst_latched_i & sr_sreg(31 downto 1);
    end if;
end process;

    trg_latched <= trg_latched_i;
    rst_latched <= sr_sreg(25);
    trg_debug   <= fe_trigger;
    trg_final   <= (ext_trg and ext_trg_enable and (not trigProc_enable) and (not trg_inhib)) 
                    or (cktp_trg and (not ext_trg_enable) and (not trigProc_enable) and (not trg_inhib))
                    or (trigProc_l0 and trigProc_enable);

VSB_fineTrg_inst: VSB_fineTrg
    generic map(fine_trigger => fine_trigger)
    port map(
        ------------------------------------
        ------- General Interface ----------
        clk_ckbc        => clk_ckbc,
        clk_art2        => clk_art2,
        mmcm_locked     => mmcm_locked,
        -------------------------------------
        ----------- CTF Interface -----------
        ctf_trigger     => ctf_trigger,
        -------------------------------------
        ----------- TTC Interface -----------
        ext_trg         => ext_trg_i
    );

microTTC_perSR_inst: microTTC_perSR
    generic map(ena_perSR => ena_perSR,
                sr_period => sr_period)
    port map(
        ------------------------------------
        ------- General Interface ----------
        clk_ckbc        => clk_ckbc,
        acq_on          => acq_on,
        fe_busy         => fe_busy,
        trg_busy        => trg_latched_i,
        trg_inhib       => trg_inhib,
        periodic_sr     => periodic_sr
    );
    
-- register the ext trigger signal
reg_trg_proc: process(clk_ckbc)
begin
    if(rising_edge(clk_ckbc))then
        ext_trg <= ext_trg_i;
    end if;
end process;

-- IDDR/ODDR instantiation
IDDR_RST: IDDR
    generic map (
        DDR_CLK_EDGE => "SAME_EDGE",
        INIT_Q1      => '0',
        INIT_Q2      => '0',
        SRTYPE       => "SYNC")
    port map (
        Q1  => ctf_rst_i0,
        Q2  => ctf_rst_i1,
        C   => clk_art,
        CE  => '1',
        D   => ctf_softReset,
        R   => '0',
        S   => '0'
    );
    
gen_oddr: for I in 0 to 7 generate

-- RST/TRG signal distribution
ODDR_RST_TRG: ODDR
    generic map(
        DDR_CLK_EDGE => "SAME_EDGE",
        INIT         => '0',
        SRTYPE       => "SYNC")
    port map(
        Q   => fe_ttc(I),
        C   => clk_ckbc,
        CE  => '1',
        D1  => oddr_vec(0),
        D2  => oddr_vec(1),
        R   => rst_oddr(I),
        S   => '0' 
    );

end generate gen_oddr;

end RTL;
