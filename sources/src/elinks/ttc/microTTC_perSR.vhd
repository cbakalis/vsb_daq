----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2017 Christos Bakalis
--
--    This file is part of NTUA-BNL_VMM_firmware.
--
--    NTUA-BNL_VMM_firmware is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    NTUA-BNL_VMM_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA-BNL_VMM_firmware.  If not, see <http://www.gnu.org/licenses/>.
-- 
-- Create Date: 18.07.2018 20:52:36
-- Design Name: TTC periodic soft-resetter
-- Module Name: microTTC_perSR - RTL
-- Project Name: VSB DAQ - NTUA/BNL
-- Target Devices: Virtex7 xc7vx690tffg1761-2
-- Tool Versions: Vivado 2017.3
-- Description: Module that asserts a periodic soft-reset
-- 
-- Dependencies: 
-- 
-- Changelog: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity microTTC_perSR is
    generic(ena_perSR : std_logic;
            sr_period : std_logic_vector(15 downto 0));
    port(
        ------------------------------------
        ------- General Interface ----------
        clk_ckbc        : in  std_logic;
        acq_on          : in  std_logic;
        fe_busy         : in  std_logic;
        trg_busy        : in  std_logic;
        trg_inhib       : out std_logic;
        periodic_sr     : out std_logic
    );
end microTTC_perSR;

architecture RTL of microTTC_perSR is

    type stateType_soft is (ST_COUNT, ST_ASSERT, ST_WAIT);
    signal state_sr        : stateType_soft := ST_COUNT;
    attribute FSM_ENCODING              : string;
    attribute FSM_ENCODING of state_sr  : signal is "ONE_HOT";

    signal sr_cnt : unsigned(15 downto 0) := (others => '0');

begin

-- periodic soft-reset FSM
peri_sr_FSM_proc: process(clk_ckbc)
begin
    if(rising_edge(clk_ckbc))then
        if(acq_on = '0' or ena_perSR = '0')then
            trg_inhib   <= '0';
            periodic_sr <= '0';
            sr_cnt      <= (others => '0');
            state_sr    <= ST_COUNT;
        else
            case state_sr is

            -- count till the limit
            when ST_COUNT =>
                periodic_sr     <= '0';
                trg_inhib       <= '0';
                if(sr_cnt = unsigned(sr_period))then
                    sr_cnt      <= (others => '0');
                    state_sr    <= ST_ASSERT;
                else
                    sr_cnt      <= sr_cnt + 1;
                    state_sr    <= ST_COUNT;
                end if;

            -- check the state
            when ST_ASSERT =>
                if(fe_busy = '0' and trg_busy = '0')then
                    periodic_sr <= '1';
                    trg_inhib   <= '1';
                    state_sr    <= ST_WAIT;
                else
                    periodic_sr <= '0';
                    trg_inhib   <= '0';
                    state_sr    <= ST_ASSERT;
                end if;

            -- wait a bit before releasing the trigger inhibit
            when ST_WAIT =>
                periodic_sr     <= '0'; 
                if(sr_cnt = x"000a")then -- wait 250ns
                    trg_inhib   <= '0';
                    sr_cnt      <= (others => '0');
                    state_sr    <= ST_COUNT;
                else
                    trg_inhib   <= '1';
                    sr_cnt      <= sr_cnt + 1;
                    state_sr    <= ST_WAIT;
                end if;

            when others => state_sr <= ST_COUNT;
            end case;
        end if;
    end if;
end process;


end RTL;
