----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch)
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2017 Christos Bakalis
--
--    This file is part of NTUA-BNL_VMM_firmware.
--
--    NTUA-BNL_VMM_firmware is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    NTUA-BNL_VMM_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA-BNL_VMM_firmware.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 19.02.2018 21:40:34
-- Design Name: TTC Controller
-- Module Name: ttc_controller - RTL
-- Project Name: 
-- Target Devices: Artix-7 xc7a200tfbg484-2 and xc7a200tfbg484-3
-- Tool Versions: Vivado 2017.3
-- Description: Module that manages the ttc_generator module, and asserts a sequence
-- of TTC bits.
-- 
-- Dependencies: 
-- 
-- Changelog: 
--
----------------------------------------------------------------------------------
library IEEE;
library UNISIM;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use UNISIM.VComponents.all;

entity ttc_controller is
    port(
        ----------------------------
        ----- General Interface ----
        CKBC            : in  std_logic;
        --
        l0l1_together   : in  std_logic;
        scan_enable     : in  std_logic;
        enable_ctrl     : in  std_logic;
        rst_cnt         : in  std_logic;
        SrBcr_off       : in  std_logic_vector(7 downto 0);
        BcrTp_off       : in  std_logic_vector(7 downto 0);
        TpL0_off        : in  std_logic_vector(7 downto 0);
        L0L1_off        : in  std_logic_vector(7 downto 0);
        cktp_prio       : in  std_logic_vector(23 downto 0);
        cktp_limit      : in  std_logic_vector(11 downto 0);
        --
        sr_bitmask      : in  std_logic_vector(7 downto 0);
        bcr_bitmask     : in  std_logic_vector(7 downto 0);
        tp_bitmask      : in  std_logic_vector(7 downto 0);
        l0a_bitmask     : in  std_logic_vector(7 downto 0);
        l1a_bitmask     : in  std_logic_vector(7 downto 0);
        l1a_cnt_limit   : in  std_logic_vector(7 downto 0);
        ----------------------------
        --- Generator Interface ----
        ttc_bits        : out std_logic_vector(7 downto 0);
        ttc_cnt_limit   : out std_logic_vector(7 downto 0);
        ttc_enable      : out std_logic
    );
end ttc_controller;

architecture RTL of ttc_controller is
    
    signal cnt_off              : unsigned(7 downto 0)  := (others => '0');
    signal cnt_prio             : unsigned(23 downto 0) := (others => '0');
    signal CKTPcnt_limit        : unsigned(11 downto 0) := (others => '0');
    signal cnt_TpL0_off_scan    : unsigned(7 downto 0)  := x"14"; -- is 20
    signal TpL0_off_i           : std_logic_vector(7 downto 0)  := (others => '0');
    signal ena_cktp_prio        : std_logic := '0';
    signal sel_inf              : std_logic := '0';

    type stateType is (ST_SR, ST_SR_BCR, ST_BCR, ST_BCR_TP,
                       ST_TP, ST_TP_L0A, ST_L0A, ST_L0A_L1A,
                       ST_L1A, ST_L0A_L1A_TOGETHER, ST_IDLE, ST_FINAL);
    signal state : stateType := ST_IDLE;

    attribute FSM_ENCODING : string;
    attribute FSM_ENCODING of state : signal is "ONE_HOT";

--  TTC mapping (old)
--  7    6   5   4   3   2   1   0
--      L0A BCR ECR TP  SR  L1A
--

--  TTC mapping (new)
--  7    6   5   4   3   2   1   0
--          L1A SR  TP  ECR BCR L0A
--
begin

TTC_FSM_proc: process(CKBC)
begin
    if(rising_edge(CKBC))then
        if(enable_ctrl = '0')then
            cnt_off         <= (others => '0');
            CKTPcnt_limit   <= (others => '0');
            ttc_bits        <= (others => '0');
            ttc_cnt_limit   <= (others => '0');
            ena_cktp_prio   <= '0';
            ttc_enable      <= '0';
            state           <= ST_IDLE;
        else
            case state is

            when ST_IDLE =>
                state <= ST_SR;

            -- assert soft reset
            when ST_SR =>
                ttc_bits        <= sr_bitmask; -- "0001" & "0000"
                ttc_cnt_limit   <= x"01";
                ttc_enable      <= '1';
                state           <= ST_SR_BCR;

            when ST_SR_BCR =>
                ttc_enable      <= '0';
                if(cnt_off >= unsigned(SrBcr_off))then
                    cnt_off     <= (others => '0');
                    state       <= ST_BCR;
                else
                    cnt_off     <= cnt_off + 1;
                    state       <= ST_SR_BCR;
                end if;

            -- assert bunch-crossing reset
            when ST_BCR =>
                ttc_bits        <= bcr_bitmask; -- "0000" & "0010"
                ttc_cnt_limit   <= x"02";
                ttc_enable      <= '1';
                state           <= ST_BCR_TP;    

            when ST_BCR_TP =>
                ttc_enable      <= '0';
                if(cnt_off >= unsigned(BcrTp_off))then
                    cnt_off     <= (others => '0');
                    state       <= ST_TP;
                else
                    cnt_off     <= cnt_off + 1;
                    state       <= ST_BCR_TP;
                end if;

            -- assert testPulse
            when ST_TP =>
                ena_cktp_prio   <= '0';
                ttc_bits        <= tp_bitmask; -- "0000" & "1000"
                ttc_cnt_limit   <= x"14"; -- 20 bunch crossings
                ttc_enable      <= '1';
                state           <= ST_TP_L0A;

                if(sel_inf = '0')then
                    CKTPcnt_limit   <= CKTPcnt_limit + 1; -- one TP more
                else
                    CKTPcnt_limit   <= (others => '0');
                end if;

            when ST_TP_L0A =>
                ttc_enable      <= '0';
                ena_cktp_prio   <= '1';
                if(cnt_off >= unsigned(TpL0_off_i) and l0l1_together = '0')then
                    cnt_off     <= (others => '0');
                    state       <= ST_L0A;
                elsif(cnt_off >= unsigned(TpL0_off_i) and l0l1_together = '1')then
                    cnt_off     <= (others => '0');
                    state       <= ST_L0A_L1A_TOGETHER;
                else
                    cnt_off     <= cnt_off + 1;
                    state       <= ST_TP_L0A;
                end if;

            -- assert L0A
            when ST_L0A =>
                ttc_bits        <= l0a_bitmask; -- "0000" & "0001"
                ttc_cnt_limit   <= x"01";
                ttc_enable      <= '1';
                state           <= ST_L0A_L1A;

            when ST_L0A_L1A =>
                ttc_enable      <= '0';
                if(cnt_off >= unsigned(L0L1_off))then
                    cnt_off     <= (others => '0');
                    state       <= ST_L1A;
                else
                    cnt_off     <= cnt_off + 1;
                    state       <= ST_L0A_L1A;
                end if;

            -- assert L1A
            when ST_L1A =>
                ttc_bits        <= l1a_bitmask; -- "0010" & "0000"
                ttc_cnt_limit   <= l1a_cnt_limit;
                ttc_enable      <= '1';
                state           <= ST_FINAL;

            -- assert them together
            when ST_L0A_L1A_TOGETHER =>
                ttc_bits        <= l0a_bitmask or l1a_bitmask; -- "0010" & "0001"
                ttc_cnt_limit   <= l1a_cnt_limit;
                ttc_enable      <= '1';
                state           <= ST_FINAL;

            -- what to do next?
            when ST_FINAL =>
                ttc_enable <= '0';
                if(CKTPcnt_limit >= unsigned(cktp_limit))then
                    state <= ST_FINAL;  -- stop here
                elsif(cnt_prio >= unsigned(cktp_prio))then
                    if(rst_cnt = '0')then
                        state <= ST_TP;    -- assert TP
                    else
                        state <= ST_SR;    -- we should reset first...
                    end if;
                elsif(cnt_prio < unsigned(cktp_prio))then
                    state <= ST_FINAL; -- keep counting
                else
                    state <= ST_FINAL;
                end if;

            when others => state <= ST_IDLE;
            end case;
        end if;
    end if;
end process;

-- process that selects if the internal limit if the scan is set to 1
scan_sel_proc: process(TpL0_off, cnt_TpL0_off_scan, scan_enable)
begin
    if(scan_enable = '1')then
        TpL0_off_i <= std_logic_vector(cnt_TpL0_off_scan);
    else
        TpL0_off_i <= TpL0_off; -- select the one from the VIO
    end if;
end process;

-- process that increments the scan counter by one
scan_cnt_proc: process(CKBC)
begin
    if(rising_edge(CKBC))then
        if(enable_ctrl = '0')then
            cnt_TpL0_off_scan <= x"14"; -- is 20
        else
             -- increment by one, once every assertion cycle
            if((state = ST_L1A or state = ST_L0A_L1A_TOGETHER) and cnt_TpL0_off_scan /= x"ff")then
                cnt_TpL0_off_scan <= cnt_TpL0_off_scan + 1;
            elsif((state = ST_L1A or state = ST_L0A_L1A_TOGETHER) and cnt_TpL0_off_scan = x"ff")then
                cnt_TpL0_off_scan <= x"14";
            else
                cnt_TpL0_off_scan <= cnt_TpL0_off_scan;
            end if;    
        end if;
    end if;
end process;

-- CKTP frequency counter
cktp_prio_proc: process(CKBC)
begin
    if(rising_edge(CKBC))then
        if(ena_cktp_prio = '0')then 
            cnt_prio <= (others => '0');
        else
            cnt_prio <= cnt_prio + 1;
        end if;
    end if;
end process;

-- process that decides if to send cktp indefinently 
sel_inf_proc: process(cktp_limit)
begin
    if(cktp_limit = x"fff")then
        sel_inf <= '1';
    else
        sel_inf <= '0';
    end if;
end process;

end RTL;
