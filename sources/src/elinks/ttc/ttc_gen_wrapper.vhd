----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch)
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2017 Christos Bakalis
--
--    This file is part of NTUA-BNL_VMM_firmware.
--
--    NTUA-BNL_VMM_firmware is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    NTUA-BNL_VMM_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA-BNL_VMM_firmware.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 26.01.2018 10:00:42
-- Design Name: TTC Generator Wrapper
-- Module Name: ttc_gen_wrapper - RTL
-- Project Name: 
-- Target Devices: Artix-7 xc7a200tfbg484-2 and xc7a200tfbg484-3
-- Tool Versions: Vivado 2017.3
-- Description: Wrapper for the TTC generator.
-- 
-- Dependencies: 
-- 
-- Changelog: 
-- 26.02.2018 Added elink_inhibitor module. (Christos Bakalis)
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ttc_gen_wrapper is
    port(
        ----------------------------
        ----- General Interface ----
        clk_40              : in  std_logic;
        clk_160             : in  std_logic;
        clk_320             : in  std_logic;
        clk_80              : in  std_logic;
        clk_eth             : in  std_logic;
        mmcm_locked         : in  std_logic;
        rst                 : in  std_logic;
        ----------------------------
        ----- TTC Gen Interface ----
        ttc_bits_vio        : in  std_logic_vector(7 downto 0);
        ttc_cnt_limit_vio   : in  std_logic_vector(7 downto 0);
        ttc_bits_shift_vio  : in  std_logic_vector(2 downto 0);
        ttc_enable_vio      : in  std_logic;
        ----------------------------
        ----- TTC FSM Interface ----
        enable_ctrl         : in  std_logic;
        rst_cnt             : in  std_logic;
        l0l1_together       : in  std_logic;
        scan_enable         : in  std_logic;
        SrBcr_off           : in  std_logic_vector(7 downto 0);
        BcrTp_off           : in  std_logic_vector(7 downto 0);
        TpL0_off            : in  std_logic_vector(7 downto 0);
        L0L1_off            : in  std_logic_vector(7 downto 0);
        cktp_prio           : in  std_logic_vector(23 downto 0);
        cktp_limit          : in  std_logic_vector(11 downto 0);
        --
        sr_bitmask          : in  std_logic_vector(7 downto 0);
        bcr_bitmask         : in  std_logic_vector(7 downto 0);
        tp_bitmask          : in  std_logic_vector(7 downto 0);
        l0a_bitmask         : in  std_logic_vector(7 downto 0);
        l1a_bitmask         : in  std_logic_vector(7 downto 0);
        l1a_cnt_limit       : in  std_logic_vector(7 downto 0);
        ----------------------------
        --- Inhibitor Interface ----
        hdlc_out_valid      : in  std_logic; -- are we sending something to the SCA?
        scaReply_valid      : in  std_logic; -- are we receiving a reply from the SCA?
        rocReply_last       : in  std_logic; -- are we receiving a packet from the ROC?
        enable_inhib        : in  std_logic; -- use the inhibitor module
        inhibit_roc         : out std_logic; -- inhibit ROC packet reception
        inhibit_sca         : out std_logic; -- inhibit SCA packet reception
        sca_alarm           : out std_logic; -- missed a packet
        roc_alarm           : out std_logic; -- missed a packet
        ----------------------------
        ------- ROC Interface ------
        ttc_streamDbg       : out std_logic;
        ttc_stream          : out std_logic  
    );
end ttc_gen_wrapper;

architecture RTL of ttc_gen_wrapper is

component ttc_generator
    port(
        ----------------------------
        ----- General Interface ----
        CKBC                : in  std_logic;
        clk_320             : in  std_logic;
        clk_160             : in  std_logic;
        clk_80              : in  std_logic;
        mmcm_locked         : in  std_logic;
        enable_ctrl         : in  std_logic;
        ttc_bits_shift_vio  : in  std_logic_vector(2 downto 0);
        ----------------------------
        --- Controller Interface ---
        ttc_bits_ctrl       : in  std_logic_vector(7 downto 0);
        ttc_cnt_limit_ctrl  : in  std_logic_vector(7 downto 0);
        ttc_enable_ctrl     : in  std_logic;
        ----------------------------
        ----- Config Interface -----
        ttc_bits_vio        : in  std_logic_vector(7 downto 0);
        ttc_cnt_limit_vio   : in  std_logic_vector(7 downto 0);
        ttc_enable_vio      : in  std_logic; 
        ----------------------------
        ----- VMM Interface --------
        ttc_streamDbg       : out std_logic;
        ttc_stream          : out std_logic
    );
end component;

component ttc_controller
    port(
        ----------------------------
        ----- General Interface ----
        CKBC            : in  std_logic;
        --
        l0l1_together   : in  std_logic;
        scan_enable     : in  std_logic;
        enable_ctrl     : in  std_logic;
        rst_cnt         : in  std_logic;
        SrBcr_off       : in  std_logic_vector(7 downto 0);
        BcrTp_off       : in  std_logic_vector(7 downto 0);
        TpL0_off        : in  std_logic_vector(7 downto 0);
        L0L1_off        : in  std_logic_vector(7 downto 0);
        cktp_prio       : in  std_logic_vector(23 downto 0);
        cktp_limit      : in  std_logic_vector(11 downto 0);
        --
        sr_bitmask      : in  std_logic_vector(7 downto 0);
        bcr_bitmask     : in  std_logic_vector(7 downto 0);
        tp_bitmask      : in  std_logic_vector(7 downto 0);
        l0a_bitmask     : in  std_logic_vector(7 downto 0);
        l1a_bitmask     : in  std_logic_vector(7 downto 0);
        l1a_cnt_limit   : in  std_logic_vector(7 downto 0);
        ----------------------------
        --- Generator Interface ----
        ttc_bits        : out std_logic_vector(7 downto 0);
        ttc_cnt_limit   : out std_logic_vector(7 downto 0);
        ttc_enable      : out std_logic
    );
end component;

component elink_inhibitor
    port(
        ----------------------------
        ----- General Interface ----
        clk_40          : in  std_logic; -- CKBC
        clk_160         : in  std_logic; -- CKBCx4
        clk_eth         : in  std_logic; -- ethernet 
        rst             : in  std_logic;
        ttc_bits        : in  std_logic_vector(7 downto 0); -- TTC broadcasted bits (with AND of ttc_enable)
        sca_alarm       : out std_logic; -- missed a packet
        roc_alarm       : out std_logic; -- missed a packet
        ttc_enable      : out std_logic; -- only allow TTC commands when ready
        ----------------------------
        ---- InhibFSM Interface ----
        l1a_bitmask     : in  std_logic_vector(7 downto 0); -- which is the L1A?
        hdlc_out_valid  : in  std_logic;    -- are we sending something to the SCA?
        scaReply_valid  : in  std_logic;    -- are we receiving a reply from the SCA?
        rocReply_last   : in  std_logic;    -- are we receiving a packet from the ROC?
        ----------------------------
        ----- E-link Interface -----
        inhibit_roc     : out std_logic;
        inhibit_sca     : out std_logic
    );
end component;

    signal ttc_enable_ctrl      : std_logic := '0';
    signal ttc_enable_ctrlGen   : std_logic := '0';
    signal ttc_enable_vioGen    : std_logic := '0';
    signal ttc_bits_ctrl        : std_logic_vector(7 downto 0) := (others => '0');
    signal ttc_cnt_limit_ctrl   : std_logic_vector(7 downto 0) := (others => '0');
    signal ttc_bits_inhib       : std_logic_vector(7 downto 0) := (others => '0');
    signal ttc_enable_inhib     : std_logic := '0';
    signal rst_inhib            : std_logic := '0';

begin

ttc_gen_inst: ttc_generator
    port map(
        ----------------------------
        ----- General Interface ----
        CKBC                => clk_40,
        clk_320             => clk_320,
        clk_160             => clk_160,
        clk_80              => clk_80,
        mmcm_locked         => mmcm_locked,
        enable_ctrl         => enable_ctrl,
        ttc_bits_shift_vio  => ttc_bits_shift_vio,
        ----------------------------
        --- Controller Interface ---
        ttc_bits_ctrl       => ttc_bits_ctrl,
        ttc_cnt_limit_ctrl  => ttc_cnt_limit_ctrl,
        ttc_enable_ctrl     => ttc_enable_ctrlGen,
        ----------------------------
        ----- Config Interface -----
        ttc_bits_vio        => ttc_bits_vio,
        ttc_cnt_limit_vio   => ttc_cnt_limit_vio,
        ttc_enable_vio      => ttc_enable_vioGen,
        ----------------------------
        ----- VMM Interface --------
        ttc_streamDbg       => ttc_streamDbg,
        ttc_stream          => ttc_stream
    );

ttc_ctrl_inst: ttc_controller
    port map(
        ----------------------------
        ----- General Interface ----
        CKBC            => clk_40,
        --
        l0l1_together   => l0l1_together,
        scan_enable     => scan_enable,
        enable_ctrl     => enable_ctrl,
        rst_cnt         => rst_cnt,
        SrBcr_off       => SrBcr_off,
        BcrTp_off       => BcrTp_off,
        TpL0_off        => TpL0_off,
        L0L1_off        => L0L1_off,
        cktp_prio       => cktp_prio,
        cktp_limit      => cktp_limit,
        --
        sr_bitmask      => sr_bitmask,
        bcr_bitmask     => bcr_bitmask,
        tp_bitmask      => tp_bitmask,
        l0a_bitmask     => l0a_bitmask,
        l1a_bitmask     => l1a_bitmask,
        l1a_cnt_limit   => l1a_cnt_limit,
        ----------------------------
        --- Generator Interface ----
        ttc_bits        => ttc_bits_ctrl,
        ttc_cnt_limit   => ttc_cnt_limit_ctrl,
        ttc_enable      => ttc_enable_ctrl
    );

elink_inhibitor_inst: elink_inhibitor
    port map(
        ----------------------------
        ----- General Interface ----
        clk_40          => clk_40,
        clk_160         => clk_160,
        clk_eth         => clk_eth,
        rst             => rst_inhib,
        ttc_bits        => ttc_bits_inhib,
        sca_alarm       => sca_alarm,
        roc_alarm       => roc_alarm,
        ttc_enable      => ttc_enable_inhib,
        ----------------------------
        ---- InhibFSM Interface ----
        l1a_bitmask     => l1a_bitmask,
        hdlc_out_valid  => hdlc_out_valid,
        scaReply_valid  => scaReply_valid,
        rocReply_last   => rocReply_last,
        ----------------------------
        ----- E-link Interface -----
        inhibit_roc     => inhibit_roc,
        inhibit_sca     => inhibit_sca
    );

    rst_inhib <= rst or (not enable_inhib);

-- mux for the ttc_bits of the inhibitor
muxTTCinhibitor_proc: process(clk_40)
begin
    if(rising_edge(clk_40))then
        case enable_ctrl is
        when '0' =>  
            if(ttc_enable_vio = '1' and ttc_cnt_limit_vio /= x"00")then 
                ttc_bits_inhib  <= ttc_bits_vio;
            else
                ttc_bits_inhib  <= (others => '0');
            end if;
        when '1' =>
            if(ttc_enable_ctrl = '1' and ttc_cnt_limit_ctrl /= x"00")then  
                ttc_bits_inhib  <= ttc_bits_ctrl;
            else
                ttc_bits_inhib  <= (others => '0');
            end if;
        when others => ttc_bits_inhib  <= (others => '0');
        end case;
    end if;
end process;

    ttc_enable_ctrlGen  <= ttc_enable_ctrl and ttc_enable_inhib;
    ttc_enable_vioGen   <= ttc_enable_vio  and ttc_enable_inhib;

end RTL;
