----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch)
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2017 Christos Bakalis
--
--    This file is part of NTUA-BNL_VMM_firmware.
--
--    NTUA-BNL_VMM_firmware is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    NTUA-BNL_VMM_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA-BNL_VMM_firmware.  If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 26.01.2018 10:00:42
-- Design Name: TTC Generator
-- Module Name: ttc_generator - RTL
-- Project Name: 
-- Target Devices: Artix-7 xc7a200tfbg484-2 and xc7a200tfbg484-3
-- Tool Versions: Vivado 2017.3
-- Description: Module that generates TTC bits, broadcasting them in sync with
-- the CKBC.
-- 
-- Dependencies: 
-- 
-- Changelog: 
-- 19.02.2018 Added controller interface and BUFG->ODDR. (Christos Bakalis)
-- 01.03.2018 Switched to ODDR. (Christos Bakalis)
--
----------------------------------------------------------------------------------
library UNISIM;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use UNISIM.VCOMPONENTS.ALL;

entity ttc_generator is
    port(
        ----------------------------
        ----- General Interface ----
        CKBC                : in  std_logic;
        clk_320             : in  std_logic;
        clk_160             : in  std_logic;
        clk_80              : in  std_logic;
        mmcm_locked         : in  std_logic;
        enable_ctrl         : in  std_logic;
        ttc_bits_shift_vio  : in  std_logic_vector(2 downto 0);
        ----------------------------
        --- Controller Interface ---
        ttc_bits_ctrl       : in  std_logic_vector(7 downto 0);
        ttc_cnt_limit_ctrl  : in  std_logic_vector(7 downto 0);
        ttc_enable_ctrl     : in  std_logic;
        ----------------------------
        ----- Config Interface -----
        ttc_bits_vio        : in  std_logic_vector(7 downto 0);
        ttc_cnt_limit_vio   : in  std_logic_vector(7 downto 0);
        ttc_enable_vio      : in  std_logic; 
        ----------------------------
        ----- VMM Interface --------
        ttc_streamDbg       : out std_logic;
        ttc_stream          : out std_logic
    );
end ttc_generator;

architecture RTL of ttc_generator is

    signal ttc_cnt          : unsigned(7 downto 0) := (others => '0');
    signal ttc_bits         : std_logic_vector(7 downto 0) := (others => '0');
    signal ttc_bits_out     : std_logic_vector(7 downto 0) := (others => '0');
    signal ttc_cnt_limit    : std_logic_vector(7 downto 0) := (others => '0');
    signal ttc_enable       : std_logic := '0';
    signal ttc_enable_i     : std_logic := '0';
    signal clear_latch      : std_logic := '0';

    signal cnt_align        : unsigned(1 downto 0) := (others => '0');
    signal ticks160_cnt     : unsigned(1 downto 0) := (others => '0');
    signal oddr_dout2bit    : std_logic_vector(1 downto 0) := (others => '0');
    signal dout2bit_i       : std_logic_vector(1 downto 0) := (others => '0');
    signal oddr_enable      : std_logic := '0';
    signal oddr_ena_i       : std_logic := '0';

    type stateType is (ST_IDLE, ST_DRIVE_ODDR, ST_KEEP, ST_DONE);
    signal state : stateType := ST_IDLE;

    attribute FSM_ENCODING : string;
    attribute FSM_ENCODING of state : signal is "ONE_HOT";

    --testbench
    --signal ttc_bits_shift_vio : std_logic_vector(2 downto 0) := "000";

begin

-------------------------
-- clk_160
-------------------------
-- what to select?
mux_ttc_proc: process(CKBC)
begin
    if(rising_edge(CKBC))then
        if(enable_ctrl = '1')then
            ttc_bits        <= ttc_bits_ctrl;
            ttc_cnt_limit   <= ttc_cnt_limit_ctrl;
            ttc_enable_i    <= ttc_enable_ctrl;    
        else
            ttc_bits        <= ttc_bits_vio;
            ttc_cnt_limit   <= ttc_cnt_limit_vio;
            ttc_enable_i    <= ttc_enable_vio;
        end if;
    end if;
end process;

-- latch for the ttc_enable
latch_ena_proc: process(CKBC)
begin
    if(rising_edge(CKBC))then
        if(ttc_enable = '1')then
            if(clear_latch = '1')then
                ttc_enable <= '0';
            else
                ttc_enable <= '1';
            end if;
        elsif(ttc_enable_i = '1')then
            ttc_enable <= '1';
        else 
            ttc_enable <= '0';
        end if;
    end if;
end process;

-- ODDR driver FSM
oddrDrv_FSM_proc: process(clk_160)
begin
    if(rising_edge(clk_160))then
        case state is

        -- wait for the enable signal from the CKBC domain
        when ST_IDLE =>
            oddr_ena_i  <= '0';
            --if(ttc_enable = '1' and cnt_align < "01")then
            --    dout2bit_i  <= (others => '0');
            --    cnt_align   <= cnt_align + 1;
            --    state       <= ST_IDLE;
            --elsif(ttc_enable = '1' and cnt_align = "01")then
            --    dout2bit_i  <= ttc_bits_out(7 downto 6);
            --    cnt_align   <= (others => '0');
            --    state       <= ST_DRIVE_ODDR;
            --else
            --    dout2bit_i  <= (others => '0');
            --    cnt_align   <= (others => '0');
            --    state       <= ST_IDLE;
            --end if;

            -- uncomment the above if re-alignment is necessary
            if(ttc_enable = '1')then
                dout2bit_i  <= ttc_bits_out(7 downto 6);
                state       <= ST_DRIVE_ODDR;
            else
                dout2bit_i  <= (others => '0');
                state       <= ST_IDLE;
            end if;

        -- drive the oddr with the appropriate part of the ttc_bits_out word
        when ST_DRIVE_ODDR =>
            oddr_ena_i      <= '1';
            ticks160_cnt    <= ticks160_cnt + 1;

            case ticks160_cnt is
            when "00"   => 
                dout2bit_i <= ttc_bits_out(7 downto 6); ttc_cnt <= ttc_cnt; state <= ST_DRIVE_ODDR;
            when "01"   => 
                dout2bit_i <= ttc_bits_out(5 downto 4); ttc_cnt <= ttc_cnt; state <= ST_DRIVE_ODDR;
            when "10"   => 
                dout2bit_i <= ttc_bits_out(3 downto 2); ttc_cnt <= ttc_cnt + 1; state <= ST_DRIVE_ODDR;
            when "11"   => 
                dout2bit_i <= ttc_bits_out(1 downto 0); ttc_cnt <= ttc_cnt;
                if(ttc_cnt_limit = x"FF")then -- infinite
                    state <= ST_DRIVE_ODDR;
                elsif(ttc_cnt = unsigned(ttc_cnt_limit))then
                    state <= ST_KEEP;
                else
                    state <= ST_DRIVE_ODDR;
                end if;
            when others =>  dout2bit_i <= (others => '0');
            end case;

        -- keep enable high for one more cycle
        when ST_KEEP =>
            oddr_ena_i  <= '1';
            dout2bit_i  <= (others => '0');
            state       <= ST_DONE;

        -- reset everything and back to idle
        when ST_DONE =>
            ticks160_cnt    <= (others => '0');
            ttc_cnt         <= (others => '0');
            dout2bit_i      <= (others => '0');
            oddr_ena_i      <= '0';
            
            if(ttc_enable = '1')then
                clear_latch     <= '1';
                state           <= ST_DONE;
            else
                clear_latch     <= '0';
                state           <= ST_IDLE;
            end if;

        when others => state <= ST_IDLE;
        end case;
    end if;
end process;

-- pipeline process
pipe_oddr_proc: process(clk_160)
begin
    if(rising_edge(clk_160))then
        oddr_dout2bit   <= dout2bit_i;
        oddr_enable     <= oddr_ena_i;
    end if;
end process;


ODDR_TTC_inst: ODDR
    generic map(
        DDR_CLK_EDGE => "SAME_EDGE", -- "OPPOSITE_EDGE" or "SAME_EDGE"
        INIT         => '0',         -- Initial value for Q port ('1' or '0')
         SRTYPE      => "SYNC")      -- Reset Type ("ASYNC" or "SYNC")
    port map (
        Q   => ttc_stream, -- 1-bit DDR output
        C   => clk_160, -- 1-bit clock input
        CE  => oddr_enable, -- 1-bit clock enable input
        D1  => oddr_dout2bit(1), -- 1-bit data input (positive edge)
        D2  => oddr_dout2bit(0), -- 1-bit data input (negative edge)
        R   => '0', -- 1-bit reset input
        S   => '0' -- 1-bit set input
    );

ODDR_dbg_inst: ODDR
    generic map(
        DDR_CLK_EDGE => "SAME_EDGE", -- "OPPOSITE_EDGE" or "SAME_EDGE"
        INIT         => '0',         -- Initial value for Q port ('1' or '0')
         SRTYPE      => "SYNC")      -- Reset Type ("ASYNC" or "SYNC")
    port map (
        Q   => ttc_streamDbg, -- 1-bit DDR output
        C   => clk_160, -- 1-bit clock input
        CE  => oddr_enable, -- 1-bit clock enable input
        D1  => oddr_dout2bit(1), -- 1-bit data input (positive edge)
        D2  => oddr_dout2bit(0), -- 1-bit data input (negative edge)
        R   => '0', -- 1-bit reset input
        S   => '0' -- 1-bit set input
    );


-- shift bits process
shift_proc: process(ttc_bits, ttc_bits_shift_vio)
begin
    case ttc_bits_shift_vio is
    when "000" => 
        ttc_bits_out(7) <= ttc_bits(7);
        ttc_bits_out(6) <= ttc_bits(6);
        ttc_bits_out(5) <= ttc_bits(5);
        ttc_bits_out(4) <= ttc_bits(4);
        ttc_bits_out(3) <= ttc_bits(3);
        ttc_bits_out(2) <= ttc_bits(2);
        ttc_bits_out(1) <= ttc_bits(1);
        ttc_bits_out(0) <= ttc_bits(0);
    when "001" =>
        ttc_bits_out(7) <= ttc_bits(0);
        ttc_bits_out(6) <= ttc_bits(7);
        ttc_bits_out(5) <= ttc_bits(6);
        ttc_bits_out(4) <= ttc_bits(5);
        ttc_bits_out(3) <= ttc_bits(4);
        ttc_bits_out(2) <= ttc_bits(3);
        ttc_bits_out(1) <= ttc_bits(2);
        ttc_bits_out(0) <= ttc_bits(1);
    when "010" =>
        ttc_bits_out(7) <= ttc_bits(1);
        ttc_bits_out(6) <= ttc_bits(0);
        ttc_bits_out(5) <= ttc_bits(7);
        ttc_bits_out(4) <= ttc_bits(6);
        ttc_bits_out(3) <= ttc_bits(5);
        ttc_bits_out(2) <= ttc_bits(4);
        ttc_bits_out(1) <= ttc_bits(3);
        ttc_bits_out(0) <= ttc_bits(2);
    when "011" =>
        ttc_bits_out(7) <= ttc_bits(2);
        ttc_bits_out(6) <= ttc_bits(1);
        ttc_bits_out(5) <= ttc_bits(0);
        ttc_bits_out(4) <= ttc_bits(7);
        ttc_bits_out(3) <= ttc_bits(6);
        ttc_bits_out(2) <= ttc_bits(5);
        ttc_bits_out(1) <= ttc_bits(4);
        ttc_bits_out(0) <= ttc_bits(3);
    when "100" =>
        ttc_bits_out(7) <= ttc_bits(3);
        ttc_bits_out(6) <= ttc_bits(2);
        ttc_bits_out(5) <= ttc_bits(1);
        ttc_bits_out(4) <= ttc_bits(0);
        ttc_bits_out(3) <= ttc_bits(7);
        ttc_bits_out(2) <= ttc_bits(6);
        ttc_bits_out(1) <= ttc_bits(5);
        ttc_bits_out(0) <= ttc_bits(4);
    when "101" =>
        ttc_bits_out(7) <= ttc_bits(4);
        ttc_bits_out(6) <= ttc_bits(3);
        ttc_bits_out(5) <= ttc_bits(2);
        ttc_bits_out(4) <= ttc_bits(1);
        ttc_bits_out(3) <= ttc_bits(0);
        ttc_bits_out(2) <= ttc_bits(7);
        ttc_bits_out(1) <= ttc_bits(6);
        ttc_bits_out(0) <= ttc_bits(5);
    when "110" =>
        ttc_bits_out(7) <= ttc_bits(5);
        ttc_bits_out(6) <= ttc_bits(4);
        ttc_bits_out(5) <= ttc_bits(3);
        ttc_bits_out(4) <= ttc_bits(2);
        ttc_bits_out(3) <= ttc_bits(1);
        ttc_bits_out(2) <= ttc_bits(0);
        ttc_bits_out(1) <= ttc_bits(7);
        ttc_bits_out(0) <= ttc_bits(6);
    when "111" =>
        ttc_bits_out(7) <= ttc_bits(6);
        ttc_bits_out(6) <= ttc_bits(5);
        ttc_bits_out(5) <= ttc_bits(4);
        ttc_bits_out(4) <= ttc_bits(3);
        ttc_bits_out(3) <= ttc_bits(2);
        ttc_bits_out(2) <= ttc_bits(1);
        ttc_bits_out(1) <= ttc_bits(0);
        ttc_bits_out(0) <= ttc_bits(7);
    when others => 
        ttc_bits_out(7) <= ttc_bits(7);
        ttc_bits_out(6) <= ttc_bits(6);
        ttc_bits_out(5) <= ttc_bits(5);
        ttc_bits_out(4) <= ttc_bits(4);
        ttc_bits_out(3) <= ttc_bits(3);
        ttc_bits_out(2) <= ttc_bits(2);
        ttc_bits_out(1) <= ttc_bits(1);
        ttc_bits_out(0) <= ttc_bits(0);
    end case;
end process;

end RTL;
