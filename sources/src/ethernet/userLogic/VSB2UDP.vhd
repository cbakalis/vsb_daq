----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2017 Christos Bakalis
--
--    This file is part of NTUA_BNL_VSB_firmware.
--
--    NTUA_BNL_VSB_firmware is free software: you can redistribute it 
--    and/or modify it under the terms of the GNU General Public License 
--    as published by the Free Software Foundation, either version 3 of
--    the License, or (at your option) any later version.
--
--    NTUA_BNL_VSB_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA_BNL_VSB_firmware. If not, see <http://www.gnu.org/licenses/>.
-- 
-- Create Date: 14.05.2018 16:25:09
-- Design Name: VSB2UDP
-- Module Name: VSB2UDP - RTL
-- Project Name: VSB DAQ - NTUA/BNL
-- Target Devices: Virtex7 xc7vx690tffg1761-2
-- Tool Versions: Vivado 2017.3
-- Description: Module that sends the bcid value of when the trigger was received
--
-- Dependencies: NTUA_BNL_VSB_firmware
-- 
-- Changelog:
-- 27.05.2018 Added ART address support. (Christos Bakalis)
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.axi.all;
use work.ipv4_types.all;
use work.arp_types.all;

entity VSB2UDP is
    port(
        ---------------------------
        ---- General Interface ----
        clk_ckbc        : in  std_logic;
        clk_eth         : in  std_logic;
        fifo_init       : in  std_logic;
        glbl_bcid       : in  std_logic_vector(11 downto 0);
        art_addr_udp    : in  std_logic_vector(17 downto 0);
        art_valid_udp   : in  std_logic;
        art_inhibit     : in  std_logic;
        ---------------------------
        ---- Trigger Interface ----
        trigger_latched : in  std_logic;
        fe_busy         : in  std_logic;
        ---------------------------
        ---- UDP Interface --------
        default_destIP  : in  std_logic_vector(31 downto 0);
        dstPort         : in  std_logic_vector(15 downto 0);
        srcPort         : in  std_logic_vector(15 downto 0);
        udp_tx_dout_rdy : in  std_logic;
        udp_tx_start    : out std_logic;
        udp_txi         : out udp_tx_type
    );
end VSB2UDP;

architecture RTL of VSB2UDP is

component elink2UDP_len
    port(
        rst         : in  std_logic;
        wr_clk      : in  std_logic;
        rd_clk      : in  std_logic;
        din         : in  std_logic_vector(31 downto 0);
        wr_en       : in  std_logic;
        rd_en       : in  std_logic;
        dout        : out std_logic_vector(31 downto 0);
        full        : out std_logic;
        empty       : out std_logic;
        wr_rst_busy : out std_logic;
        rd_rst_busy : out std_logic
    );
end component;

    signal word_latched : std_logic_vector(31 downto 0) := (others => '0');
    signal wr_en        : std_logic := '0';
    signal rd_en        : std_logic := '0';
    signal dout_udp     : std_logic_vector(31 downto 0) := (others => '0');
    signal fifo_empty   : std_logic := '0';
    signal fifo_full    : std_logic := '0';
    signal wr_rst_busy  : std_logic := '0';
    signal rd_rst_busy  : std_logic := '0';
    signal udp_valid    : std_logic := '0';
    signal udp_last     : std_logic := '0';
    signal udp_data     : std_logic_vector(7 downto 0)  := (others => '0');
    signal sel_chunk    : unsigned(1 downto 0)          := (others => '0');

    type stateType_wrFSM is (ST_IDLE, ST_LATCH_BCID, ST_LATCH_ART, ST_WAIT_BUSY, ST_WRITE, ST_WAIT_TRG);
    signal state_wr         : stateType_wrFSM := ST_IDLE;

    type stateType_rdFSM is (ST_IDLE, ST_READ, ST_SET_UDP, ST_CHK_UDP, ST_SEND_UDP);
    signal state_rd         : stateType_rdFSM := ST_IDLE;

    attribute FSM_ENCODING              : string;
    attribute FSM_ENCODING of state_wr : signal is "ONE_HOT";

begin

-- FSM that writes the BCID value to the FIFO
writeBCID_proc: process(clk_ckbc)
begin
    if(rising_edge(clk_ckbc))then
        case state_wr is

        -- wait to latch
        when ST_IDLE =>
            word_latched    <= (others => '0');
            wr_en           <= '0';

            if(trigger_latched = '1' and art_inhibit = '1')then
                state_wr <= ST_LATCH_BCID;
            elsif(art_valid_udp = '1' and art_inhibit = '0')then
                state_wr <= ST_LATCH_ART;
            else
                state_wr <= ST_IDLE;
            end if;

        -- latch the BCID
        when ST_LATCH_BCID =>
            word_latched(31 downto 12)  <= (others => '0');
            word_latched(11 downto 0)   <= glbl_bcid;
            wr_en                       <= '0';
            state_wr                    <= ST_WAIT_BUSY;

        -- latch the ART
        when ST_LATCH_ART =>
            word_latched(31 downto 18)  <= (others => '0');
            word_latched(17 downto 0)   <= art_addr_udp;
            wr_en                       <= '0';
            state_wr                    <= ST_WRITE;

        -- wait for the busy
        when ST_WAIT_BUSY =>
            word_latched    <= word_latched;
            wr_en           <= '0';
            if(fe_busy = '1' and trigger_latched = '1')then
                state_wr    <= ST_WRITE;
            elsif(trigger_latched = '0')then
                state_wr    <= ST_IDLE;
            else
                state_wr    <= ST_WAIT_BUSY;
            end if;

        -- write the word
        when ST_WRITE =>
            word_latched    <= word_latched;
            wr_en           <= '1';
            state_wr        <= ST_WAIT_TRG;

        -- wait for the trigger to go low
        when ST_WAIT_TRG =>
            word_latched    <= word_latched;
            wr_en           <= '0';
            if(trigger_latched = '0' and art_inhibit = '1')then
                state_wr    <= ST_IDLE;
            elsif(art_valid_udp = '0' and art_inhibit = '0')then
                state_wr    <= ST_IDLE;
            else
                state_wr    <= ST_WAIT_TRG;
            end if;

        when others => state_wr <= ST_IDLE;
        end case;
    end if;
end process;

-- FSM that reads the FIFO
sendBCID_proc: process(clk_eth)
begin
    if(rising_edge(clk_eth))then
        case state_rd is

        -- is it empty?
        when ST_IDLE =>
            rd_en           <= '0';
            udp_tx_start    <= '0';
            udp_valid       <= '0';
            udp_last        <= '0';
            sel_chunk       <= (others => '0');
            udp_data        <= (others => '0');

            if(fifo_empty = '0')then
                state_rd <= ST_READ;
            else
                state_rd <= ST_IDLE;
            end if;

        -- read the FIFO
        when ST_READ =>
            rd_en       <= '1';
            state_rd    <= ST_SET_UDP;

        -- set the UDP module start
        when ST_SET_UDP =>
            udp_tx_start    <= '1';
            rd_en           <= '0';
            state_rd        <= ST_CHK_UDP;

        -- are we ready to transmit?
        when ST_CHK_UDP =>
            if(udp_tx_dout_rdy = '1')then
                udp_tx_start    <= '0';
                state_rd        <= ST_SEND_UDP;
            else
                udp_tx_start    <= '1';
                state_rd        <= ST_CHK_UDP;
            end if;

        when ST_SEND_UDP =>
            udp_valid   <= '1';
            sel_chunk   <= sel_chunk + 1;

            if(sel_chunk = "11")then
                udp_last    <= '1';
                udp_data    <= dout_udp(7 downto 0); 
                state_rd    <= ST_IDLE;
            elsif(sel_chunk = "10")then
                udp_last    <= '0';
                udp_data    <= dout_udp(15 downto 8);
                state_rd    <= ST_SEND_UDP;
            elsif(sel_chunk = "01")then
                udp_last    <= '0';
                udp_data    <= dout_udp(23 downto 16);
                state_rd    <= ST_SEND_UDP;
            else
                udp_last    <= '0';
                udp_data    <= dout_udp(31 downto 24);
                state_rd    <= ST_SEND_UDP;
            end if;

        when others => state_rd <= ST_IDLE;
        end case;
    end if;
end process;

-- register the udp signals
reg_udp_proc: process(clk_eth)
begin
    if(rising_edge(clk_eth))then
        udp_txi.hdr.dst_ip_addr     <= default_destIP;
        udp_txi.hdr.src_port        <= srcPort;
        udp_txi.hdr.dst_port        <= dstPort;
        udp_txi.hdr.data_length     <= x"0004"; -- four bytes                         
        udp_txi.hdr.checksum        <= x"0000";
        udp_txi.data.data_out_last  <= udp_last;
        udp_txi.data.data_out_valid <= udp_valid;
        udp_txi.data.data_out       <= udp_data;
    end if;
end process;


FIFO_udp: elink2UDP_len
    port map(
        rst         => fifo_init,
        wr_clk      => clk_ckbc,
        rd_clk      => clk_eth,
        din         => word_latched,
        wr_en       => wr_en,
        rd_en       => rd_en,
        dout        => dout_udp,
        full        => fifo_full,
        empty       => fifo_empty,
        wr_rst_busy => wr_rst_busy,
        rd_rst_busy => rd_rst_busy
    );
   

end RTL;
