----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2017 Christos Bakalis
--
--    This file is part of NTUA_BNL_VSB_firmware.
--
--    NTUA_BNL_VSB_firmware is free software: you can redistribute it 
--    and/or modify it under the terms of the GNU General Public License 
--    as published by the Free Software Foundation, either version 3 of
--    the License, or (at your option) any later version.
--
--    NTUA_BNL_VSB_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA_BNL_VSB_firmware. If not, see <http://www.gnu.org/licenses/>.
-- 
-- Create Date: 14.05.2018 14:06:08
-- Design Name: VC709 IP switcher
-- Module Name: ip_switcher - RTL
-- Project Name: VSB DAQ - NTUA/BNL
-- Target Devices: Virtex7 xc7vx690tffg1761-2
-- Tool Versions: Vivado 2017.3
-- Description: Module that selects an IP for the VC709 based on the number of FEs
--
-- Dependencies: NTUA_BNL_VSB_firmware
-- 
-- Changelog:
-- 17.05.2018 New bypass option added. (Christos Bakalis)
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.all;

entity ip_switcher is
    generic(bypass_switcher : std_logic);
    port(
        ------------------------------------
        ------- General Interface ----------
        clk_eth         : in  std_logic;
        mmcm_locked     : in  std_logic;
        gpio_dip        : in  std_logic_vector(7 downto 0);
        default_IP      : in  std_logic_vector(31 downto 0);
        default_MAC     : in  std_logic_vector(47 downto 0);
        ------------------------------------
        --------- UDP Interface ------------
        vc709_IP        : out std_logic_vector(31 downto 0);
        vc709_MAC       : out std_logic_vector(47 downto 0)
    );
end ip_switcher;

architecture RTL of ip_switcher is

    signal gpio_dip_i   : std_logic_vector(7 downto 0)  := (others => '0');
    signal gpio_dip_s   : std_logic_vector(7 downto 0)  := (others => '0');
    signal gpio_dip_reg : std_logic_vector(7 downto 0)  := (others => '0');
    signal gpio_dip_prv : std_logic_vector(7 downto 0)  := (others => '0');
    signal wait_cnt     : unsigned(22 downto 0)         := (others => '0');
    signal vec_index    : unsigned(2 downto 0)          := (others => '0');
    signal cnt_ones     : unsigned(3 downto 0)          := (others => '0');
    signal sel_ip       : std_logic_vector(3 downto 0)  := (others => '0');

    attribute ASYNC_REG : string;
    attribute ASYNC_REG of gpio_dip_i   : signal is "TRUE";
    attribute ASYNC_REG of gpio_dip_s   : signal is "TRUE";

    type switcherFSM is (ST_IDLE, ST_SAMPLE, ST_CHECK, ST_CNT_ONES, ST_SET_SEL, ST_REG);
    signal state        : switcherFSM := ST_IDLE;

    attribute FSM_ENCODING          : string;
    attribute FSM_ENCODING of state : signal is "ONE_HOT";

begin

-- sync the dip
syncDip_proc: process(clk_eth)
begin
    if(rising_edge(clk_eth))then
        gpio_dip_i <= gpio_dip;
        gpio_dip_s <= gpio_dip_i;
    end if;
end process;

-- simple FSM that checks the gpio_dip state every 1.6 seconds
checkDip_FSM_proc: process(clk_eth)
begin
    if(rising_edge(clk_eth))then
        if(mmcm_locked = '0')then
            wait_cnt        <= (others => '0');
            gpio_dip_reg    <= (others => '0');
            gpio_dip_prv    <= (others => '0');
            vec_index       <= (others => '0');
            cnt_ones        <= (others => '0');
            sel_ip          <= (others => '0');
            state           <= ST_IDLE;
        else
            case state is

            -- wait before checking
            when ST_IDLE =>
                wait_cnt <= wait_cnt + 1;
                if(wait_cnt = "1111111111111111111111")then
                    state <= ST_SAMPLE;
                else
                    state <= ST_IDLE;
                end if;

            -- get the gpio dip state
            when ST_SAMPLE =>
                gpio_dip_reg    <= gpio_dip_s;
                state           <= ST_CHECK;

            -- is it different?
            when ST_CHECK =>
                if(gpio_dip_reg /= gpio_dip_prv)then
                    state <= ST_CNT_ONES;
                else
                    state <= ST_REG;
                end if;

            -- how many FEs?
            when ST_CNT_ONES =>
                vec_index <= vec_index + 1;

                if(gpio_dip_reg(to_integer(vec_index)) = '1')then
                    cnt_ones <= cnt_ones + 1;
                else
                    cnt_ones <= cnt_ones;
                end if;

                if(vec_index = "111")then
                    state <= ST_SET_SEL;
                else
                    state <= ST_CNT_ONES;
                end if;

            -- set the IP/MAC
            when ST_SET_SEL =>
                sel_ip  <= std_logic_vector(cnt_ones);
                state   <= ST_REG;

            -- register the value
            when ST_REG =>
                gpio_dip_prv <= gpio_dip_reg;
                cnt_ones     <= (others => '0');
                vec_index    <= (others => '0');
                wait_cnt     <= (others => '0');
                state        <= ST_IDLE;

            when others => state <= ST_IDLE;

            end case;
        end if;
    end if;
end process;

-- mux for the IP
IPmux_proc: process(clk_eth)
begin
    if(rising_edge(clk_eth))then
        if(bypass_switcher = '0')then
            case sel_ip is
            when "0000" => vc709_IP <= default_IP;  vc709_MAC <= default_MAC;
            when "0001" => vc709_IP <= x"c0a80003"; vc709_MAC <= x"c12657aad820";
            when "0010" => vc709_IP <= x"c0a80004"; vc709_MAC <= x"a11657a9d581";
            when "0011" => vc709_IP <= x"c0a80005"; vc709_MAC <= x"b12653a0d232";
            when "0100" => vc709_IP <= x"c0a80006"; vc709_MAC <= x"d1a613a1d823";
            when "0101" => vc709_IP <= x"c0a80007"; vc709_MAC <= x"a12417abd824";
            when "0110" => vc709_IP <= x"c0a80008"; vc709_MAC <= x"e12947add825";
            when "0111" => vc709_IP <= x"c0a80009"; vc709_MAC <= x"f121a7afd826";
            when "1000" => vc709_IP <= x"c0a8000a"; vc709_MAC <= x"f121b5afd826";
            when others => vc709_IP <= default_IP;  vc709_MAC <= default_MAC;
            end case;
        else
            vc709_IP <= default_IP;  vc709_MAC <= default_MAC;    
        end if;
    end if;
end process;


end RTL;
