----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2017 Christos Bakalis
--
--    This file is part of NTUA_BNL_VSB_firmware.
--
--    NTUA_BNL_VSB_firmware is free software: you can redistribute it 
--    and/or modify it under the terms of the GNU General Public License 
--    as published by the Free Software Foundation, either version 3 of
--    the License, or (at your option) any later version.
--
--    NTUA_BNL_VSB_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA_BNL_VSB_firmware. If not, see <http://www.gnu.org/licenses/>.
--  
-- Create Date: 13.11.2017 12:32:15
-- Design Name: SCA Configuration Block
-- Module Name: sca_config_block - RTL
-- Project Name: VSB DAQ - NTUA/BNL
-- Target Devices: Virtex7 xc7vx690tffg1761-2
-- Tool Versions: Vivado 2017.3
-- Description: Module that drives the UDP payload to the e-link wrapper, to be
-- transmitted to the SCA via HDLC encoding. The first 8 bytes of the UDP payload
-- are considered as a header and are not transmitted to the SCA.
--
-- Dependencies: NTUA_BNL_VSB_firmware
-- 
-- Changelog:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity sca_config_block is
    port(
    ---------------------------------
    ------- General Interface -------
    clk_eth         : in  std_logic;
    sca_conf        : in  std_logic;
    sca_buff_rst    : in  std_logic;
    sca_confDone    : out std_logic;
    cnt_bytes       : in  unsigned(7 downto 0);
    ---------------------------------
    --------- FIFO/UDP Interface ----
    user_din_udp    : in  std_logic_vector(7 downto 0);
    user_valid_udp  : in  std_logic;
    user_last_udp   : in  std_logic;
    ---------------------------------
    ---- HDLC/E-LINK Interface ------
    hdlc_dout       : out std_logic_vector(17 downto 0);
    hdlc_dvalid     : out std_logic
    );
end sca_config_block;

architecture RTL of sca_config_block is

    component sca_conf_buffer
    port (
        clk     : in  std_logic;
        srst    : in  std_logic;
        din     : in  std_logic_vector(7 downto 0);
        wr_en   : in  std_logic;
        rd_en   : in  std_logic;
        dout    : out std_logic_vector(15 downto 0);
        full    : out std_logic;
        empty   : out std_logic
    );
    end component;

    signal sca_packRdy      : std_logic := '0';
    signal sel_sca_data     : std_logic := '0';
    signal user_valid_fifo  : std_logic := '0';
    signal flag             : std_logic_vector(1 downto 0) := (others => '0');
    constant SOP            : std_logic_vector(1 downto 0) := "10";
    constant MOP            : std_logic_vector(1 downto 0) := "00";
    constant EOP            : std_logic_vector(1 downto 0) := "01";
    signal wr_elink         : std_logic := '0';
    signal rd_buff          : std_logic := '0';
    signal wait_cnt         : unsigned(1 downto 0) := (others => '0');
    
    signal dout_buff        : std_logic_vector(15 downto 0) := (others => '0');
    signal dinAdapt_data    : std_logic_vector(15 downto 0) := (others => '0');
    signal din_adapter      : std_logic_vector(17 downto 0) := (others => '0');
    
    signal buff_empty       : std_logic := '0';
    signal buff_full        : std_logic := '0';

    signal hdlc_dvalid_i    : std_logic := '0';
    
    type stateType is (ST_IDLE, ST_WR_SOP, ST_CHK_BUFF, ST_RD_BUFF, ST_WR_ADAPT, 
                       ST_WR_EOP_0, ST_WR_EOP_1, ST_ENA_ADAPT, ST_WAIT, ST_DONE);
    signal state       : stateType := ST_IDLE;
    signal state_prv   : stateType := ST_IDLE;

    attribute FSM_ENCODING              : string;
    attribute FSM_ENCODING of state     : signal is "ONE_HOT";

begin

-- sub-process that drives the data into the FIFO used for SCA configuration. 
-- it also detects the 'last' pulse sent from the UDP block to initialize the 
-- SCA config data sending
SCA_conf_proc: process(clk_eth)
begin
    if(rising_edge(clk_eth))then
        if(sca_buff_rst = '1')then
            sel_sca_data    <= '0';
            sca_packRdy     <= '0';
        else
            if(sca_conf = '1' and user_last_udp = '0')then
                case cnt_bytes is 
                when "00001000" => --8
                    sel_sca_data  <= '1'; -- select the correct data at the MUX
                when others => null;
                end case;
            elsif(sca_conf = '1' and user_last_udp = '1')then -- 'last' pulse detected, signal FSM
                sca_packRdy       <= '1';
            else
                sca_packRdy       <= '0';
                sel_sca_data      <= '0';
            end if;
        end if;
    end if;
end process;

-- MUX that drives the SCA configuration data into the FIFO
FIFO_valid_MUX: process(sel_sca_data, user_valid_udp)
begin
    case sel_sca_data is
    when '0'    =>  user_valid_fifo <= '0';
    when '1'    =>  user_valid_fifo <= user_valid_udp;
    when others =>  user_valid_fifo <= '0';
    end case;
end process;

-- FSM that sends the SCA data to the e-link wrapper
SCA_conf_FSM_proc: process(clk_eth)
begin
    if(rising_edge(clk_eth))then
        if(sca_conf = '0')then
            flag            <= SOP;
            wr_elink        <= '0';
            rd_buff         <= '0';
            sca_confDone    <= '0';
            wait_cnt        <= (others => '0');
            state           <= ST_IDLE;
        else
            case state is

            -- wait for the entire SCA packet to be written in the buffer
            when ST_IDLE =>
                if(sca_packRdy = '1')then
                    state <= ST_WR_SOP;
                else
                    state <= ST_IDLE;
                end if;

            -- write the SOP
            when ST_WR_SOP =>
                wr_elink    <= '1';
                state_prv   <= ST_WR_SOP;
                state       <= ST_WAIT; -- to ST_CHK_BUFF

            -- is the buffer empty?
            when ST_CHK_BUFF =>
                flag      <= MOP;
                if(buff_empty = '0')then
                    state <= ST_RD_BUFF;
                else
                    state <= ST_WR_EOP_0;
                end if;

            -- pass a word to the bus
            when ST_RD_BUFF =>
                rd_buff     <= '1';
                state_prv   <= ST_RD_BUFF;
                state       <= ST_WAIT; -- to ST_WR_ADAPT

            -- write an SCA config word to the elink FIFO
            when ST_WR_ADAPT =>
                wr_elink    <= '1';
                state_prv   <= ST_WR_ADAPT;
                state       <= ST_WAIT; -- to ST_CHK_BUFF

            -- write the EOP (1/2)
            when ST_WR_EOP_0 =>
                flag        <= EOP;
                state_prv   <= ST_WR_EOP_0;
                state       <= ST_WAIT; -- to ST_WR_EOP_1

            -- write the EOP (2/2)
            when ST_WR_EOP_1 =>
                wr_elink <= '1';
                state    <= ST_DONE;

            -- wait here till reset by master conf FSM
            when ST_DONE =>
                wr_elink        <= '0';
                sca_confDone    <= '1';
                state           <= ST_DONE;

            -- generic state that waits
            when ST_WAIT =>
                wr_elink    <= '0';
                rd_buff     <= '0';
                wait_cnt    <= wait_cnt + 1;

                if(wait_cnt = "11")then
                    case state_prv is
                    when ST_WR_SOP      => state <= ST_CHK_BUFF;
                    when ST_RD_BUFF     => state <= ST_WR_ADAPT;
                    when ST_WR_ADAPT    => state <= ST_CHK_BUFF;
                    when ST_WR_EOP_0    => state <= ST_WR_EOP_1;
                    when others         => state <= ST_WAIT;
                    end case;
                else
                    state <= ST_WAIT;
                end if;

            when others => state <= ST_IDLE;
            end case;
        end if;
    end if;
end process;

-- register the data and the valid signal
delay_valid_proc: process(clk_eth)
begin
    if(rising_edge(clk_eth))then
        hdlc_dvalid_i   <= wr_elink;
        hdlc_dvalid     <= hdlc_dvalid_i;
        hdlc_dout       <= din_adapter;
    end if;
end process;

-- what to send out to the HDLC E-link FIFO?
sel_dout_null: process(flag, dout_buff)
begin
    case flag is
    when SOP    => dinAdapt_data <= (others => '0');
    when MOP    => dinAdapt_data <= dout_buff;
    when EOP    => dinAdapt_data <= (others => '0');
    when others => dinAdapt_data <= (others => '0');
    end case;
end process;

scaBuffer: sca_conf_buffer
  port map (
    clk     => clk_eth,
    srst    => sca_buff_rst,
    din     => user_din_udp,
    wr_en   => user_valid_fifo,
    rd_en   => rd_buff,
    dout    => dout_buff,
    full    => buff_full,
    empty   => buff_empty
  );

    din_adapter     <= flag & dinAdapt_data;

end RTL;
