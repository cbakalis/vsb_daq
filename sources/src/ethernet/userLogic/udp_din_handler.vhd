-----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2017 Christos Bakalis
--
--    This file is part of NTUA_BNL_VSB_firmware.
--
--    NTUA_BNL_VSB_firmware is free software: you can redistribute it 
--    and/or modify it under the terms of the GNU General Public License 
--    as published by the Free Software Foundation, either version 3 of
--    the License, or (at your option) any later version.
--
--    NTUA_BNL_VSB_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA_BNL_VSB_firmware. If not, see <http://www.gnu.org/licenses/>.
-- 
-- Create Date: 09.02.2018 18:47:13
-- Design Name: UDP Data In Handler
-- Module Name: udp_din_handler - RTL
-- Project Name: VSB DAQ - NTUA/BNL
-- Target Devices: Virtex7 xc7vx690tffg1761-2
-- Tool Versions: Vivado 2017.3
-- Description: Logic that drives UDP packets to appropriate sub-modules/FIFOs
-- for ROC/SCA configuration, or FPGA configuration.
--
-- Dependencies: NTUA_BNL_VSB_firmware
-- 
-- Changelog:
-- 
-----------------------------------------------------------------------------------
library UNISIM;
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use UNISIM.VCOMPONENTS.all;

use work.axi.all;
use work.ipv4_types.all;
use work.arp_types.all;

entity udp_din_handler is
    port(
        ------------------------------------
        ------- General Interface ----------
        clk_eth         : in  std_logic; -- userclk2 from transceiver
        clk_ckbc        : in  std_logic; -- BC clock (40)
        clk_art         : in  std_logic; -- ART clock (160)
        clk_art2        : in  std_logic; -- ART clock x2 (320)
        rst             : in  std_logic; -- global reset
        fifo_init       : in  std_logic; -- initializing reset for fifos
        handler_busy    : out std_logic; -- busy signal
        serial_number   : out std_logic_vector(31 downto 0);
        frontEnd_bmsk   : out std_logic_vector(7 downto 0);
        ------------------------------------
        -------- UDP/IPv4 Interface --------
        udp_rx          : in  udp_rx_type;
        -------------------------------------
        -------- TPC RAM Interface ----------
        wrEn_LUTRAM      : out std_logic; -- wr_en to RAM-LUT
        din_LUTRAM       : out std_logic; -- din to RAM-LUT
        wrAddr_LUTRAM    : out std_logic_vector(17 downto 0); -- write-address
        -------------------------------------
        ------ SCA Config Interface ---------
        hdlc_dout       : out std_logic_vector(17 downto 0);
        hdlc_dvalid     : out std_logic;
        -------------------------------------
        ------ FPGA Config Interface --------
        fpga_rst        : out std_logic;
        latency         : out std_logic_vector(15 downto 0);
        acq_on          : out std_logic;
        extTrg_ena      : out std_logic;
        trigProc_ena    : out std_logic
    );
end udp_din_handler;

architecture RTL of udp_din_handler is

component fpga_config_block
    port(
        -----------------------------------
        ------- General Interface ----------
        clk_125             : in  std_logic;
        rst                 : in  std_logic;
        rst_fifo_init       : in  std_logic;
        cnt_bytes           : in  unsigned(7 downto 0);
        user_din_udp        : in  std_logic_vector(7 downto 0);
        ------------------------------------
        -------- UDP Interface -------------
        udp_rx              : in  udp_rx_type;
        ------------------------------------
        ---------- XADC Interface ----------
        xadc_conf           : in  std_logic;
        xadcPacket_rdy      : out std_logic;
        vmm_id_xadc         : out std_logic_vector(15 downto 0);
        xadc_sample_size    : out std_logic_vector(10 downto 0);
        xadc_delay          : out std_logic_vector(17 downto 0);
        ------------------------------------
        ---------- AXI4SPI Interface -------
        flash_conf          : in  std_logic;
        flashPacket_rdy     : out std_logic;
        myIP_set            : out std_logic_vector(31 downto 0);
        myMAC_set           : out std_logic_vector(47 downto 0);
        destIP_set          : out std_logic_vector(31 downto 0);
        ------------------------------------
        -------- CKTP/CKBC Interface -------
        ckbc_freq           : out std_logic_vector(7 downto 0);
        cktk_max_num        : out std_logic_vector(7 downto 0);
        cktp_max_num        : out std_logic_vector(15 downto 0);
        cktp_skew           : out std_logic_vector(7 downto 0);
        cktp_period         : out std_logic_vector(15 downto 0);
        cktp_width          : out std_logic_vector(7 downto 0);
        ckbc_max_num        : out std_logic_vector(7 downto 0);
        ------------------------------------
        -------- FPGA Config Interface -----
        fpga_conf           : in  std_logic;
        fpga_rst            : out std_logic;
        fpgaPacket_rdy      : out std_logic;
        analog_on           : out std_logic;
        latency             : out std_logic_vector(15 downto 0);
        latency_extra       : out std_logic_vector(15 downto 0);
        tr_delay_limit      : out std_logic_vector(15 downto 0);
        artTimeout          : out std_logic_vector(7 downto 0);
        daq_on              : out std_logic;
        ext_trigger         : out std_logic;
        ckbcMode            : out std_logic
    );
end component;

component sca_config_block
    port(
        ---------------------------------
        ------- General Interface -------
        clk_eth         : in  std_logic;
        sca_conf        : in  std_logic;
        sca_buff_rst    : in  std_logic;
        sca_confDone    : out std_logic;
        cnt_bytes       : in  unsigned(7 downto 0);
        ---------------------------------
        --------- FIFO/UDP Interface ----
        user_din_udp    : in  std_logic_vector(7 downto 0);
        user_valid_udp  : in  std_logic;
        user_last_udp   : in  std_logic;
        ---------------------------------
        ---- HDLC/E-LINK Interface ------
        hdlc_dout       : out std_logic_vector(17 downto 0);
        hdlc_dvalid     : out std_logic
    );
end component;

component CDCC
    generic(
        NUMBER_OF_BITS : integer := 8); -- number of signals to be synced
    port(
        clk_src     : in  std_logic;                                        -- input clk (source clock)
        clk_dst     : in  std_logic;                                        -- input clk (dest clock)
        data_in     : in  std_logic_vector(NUMBER_OF_BITS - 1 downto 0);    -- data to be synced
        data_out_s  : out std_logic_vector(NUMBER_OF_BITS - 1 downto 0)     -- synced data to clk_dst
    );
end component;


    signal user_data_prv    : std_logic_vector(7 downto 0)  := (others => '0');
    signal user_valid_prv   : std_logic := '0';
    signal user_last_prv    : std_logic := '0';
    signal command          : std_logic_vector(15 downto 0) := (others => '0');
    signal cnt_bytes        : unsigned(7 downto 0)          := (others => '0');
    signal sample_hdr       : std_logic := '0';
    signal fpga_conf        : std_logic := '0';
    signal sca_conf         : std_logic := '0';
    signal tpc_conf         : std_logic := '0';
    signal sca_confDone     : std_logic := '0';
    signal fpgaConf_done    : std_logic := '0';
    signal tpcConf_done     : std_logic := '0';
    signal fpga_rst_i       : std_logic := '0';
    signal daq_on_i         : std_logic := '0';
    signal fpga_rst_s40     : std_logic := '0';
    signal latency_i        : std_logic_vector(15 downto 0) := (others => '0');
    signal extTrg_ena_i     : std_logic := '0';
    signal trigProc_ena_i   : std_logic := '0';

    type masterFSM is       (ST_IDLE, ST_CHK_PORT, ST_COUNT, ST_CHK_UDP_LOW, ST_ERROR);
    signal st_master        : masterFSM := ST_IDLE;

    attribute FSM_ENCODING                  : string;
    attribute FSM_ENCODING of st_master     : signal is "ONE_HOT";

begin

-- delay the input data for correct sampling by the sub-processes
delay_din: process(clk_eth)
begin
    if(rising_edge(clk_eth))then
        user_data_prv   <= udp_rx.data.data_in;
        user_valid_prv  <= udp_rx.data.data_in_valid;
        user_last_prv   <= udp_rx.data.data_in_last;
    end if;
end process;

-- central configuarion FSM that checks for the first valid pulse
-- and for the UDP port, in order to initialize the byte counter 
-- that the sub-processes will use to sample the configuration data
master_handling_FSM: process(clk_eth)
begin
    if(rising_edge(clk_eth))then
        if(rst = '1')then
            cnt_bytes       <= (others => '0');
            sample_hdr      <= '0';
            sca_conf        <= '0';
            fpga_conf       <= '0';
            tpc_conf        <= '0';
            handler_busy    <= '0';
            st_master       <= ST_IDLE;
        else
            case st_master is

            -- wait for valid signal to initialize counter
            when ST_IDLE =>
                if(udp_rx.data.data_in_valid = '1')then
                    cnt_bytes       <= cnt_bytes + 1;
                    sample_hdr      <= '1';
                    handler_busy    <= '1';
                    st_master       <= ST_CHK_PORT;
                else
                    cnt_bytes       <= (others => '0');
                    sample_hdr      <= '0';
                    handler_busy    <= '0';
                    st_master       <= ST_IDLE;
                end if;

            -- check the port and activate the corresponding sub-process
            when ST_CHK_PORT =>
                cnt_bytes   <= cnt_bytes + 1;

                case udp_rx.hdr.dst_port is
                when x"1234" => -- SCA CONF
                    sca_conf    <= '1';
                    st_master   <= ST_COUNT;
                when x"19C8" | x"1777"  => -- FPGA CONF
                    fpga_conf   <= '1';
                    st_master   <= ST_COUNT;
                when x"c01a" => -- TPC RAM CONF
                    tpc_conf    <= '1';
                    st_master   <= ST_COUNT;
                when others =>  -- Unknown Port
                    st_master   <= ST_ERROR;
                end case;

            -- keep counting and wait for configuration packets to be formed
            when ST_COUNT =>
                cnt_bytes   <= cnt_bytes + 1;

                if(sca_confDone = '1' or fpgaConf_done = '1' or 
                   tpcConf_done = '1')then
                    st_master <= ST_CHK_UDP_LOW;
                else
                    st_master <= ST_COUNT;
                end if;

            -- back to IDLE if no UDP packet is being sampled
            when ST_CHK_UDP_LOW =>
                sca_conf        <= '0';
                fpga_conf       <= '0';
                tpc_conf        <= '0';
                handler_busy    <= '0';
                cnt_bytes       <= (others => '0');
                if(udp_rx.data.data_in_valid = '0')then
                    st_master   <= ST_IDLE;
                else
                    st_master   <= ST_CHK_UDP_LOW;
                end if;

            -- stay here until the UDP packet passes    
            when ST_ERROR =>
                cnt_bytes <= (others => '0');
                if(udp_rx.data.data_in_valid = '0')then
                    st_master   <= ST_IDLE;
                else
                    st_master   <= ST_ERROR;
                end if;

            when others => st_master <= ST_IDLE;
            end case;
        end if;
    end if;
end process;

-- process that samples the header data from the UDP packet
sample_header_proc: process(clk_eth)
begin
    if(rising_edge(clk_eth))then
        if(rst = '1')then
            serial_number <= (others => '0');
            frontEnd_bmsk <= (others => '0');
            command       <= (others => '0');
        else
            if(sample_hdr = '1')then
                case cnt_bytes is
                ---- sample s/n
                when "00000001" => --1
                    serial_number(31 downto 24) <= user_data_prv;
                when "00000010" => --2
                    serial_number(23 downto 16) <= user_data_prv;
                when "00000011" => --3
                    serial_number(15 downto 8)  <= user_data_prv;
                when "00000100" => --4
                    serial_number(7 downto 0)   <= user_data_prv;

                -- sample bitmask
                when "00000110" => --6
                    frontEnd_bmsk               <= user_data_prv;

                -- sample command
                when "00000111" => --7
                    command(15 downto 8)        <= user_data_prv;
                when "00001000" => --8
                    command(7 downto 0)         <= user_data_prv;
                when others => null;
                end case;
            else null;
            end if;   
        end if;
    end if;
end process;

sca_config_logic: sca_config_block
    port map(
        ---------------------------------
        ------- General Interface -------
        clk_eth         => clk_eth,
        sca_conf        => sca_conf,
        sca_buff_rst    => fifo_init,
        sca_confDone    => sca_confDone,
        cnt_bytes       => cnt_bytes,
        ---------------------------------
        --------- FIFO/UDP Interface ----
        user_din_udp    => user_data_prv,
        user_valid_udp  => user_valid_prv,
        user_last_udp   => user_last_prv,
        ---------------------------------
        ---- HDLC/E-LINK Interface ------
        hdlc_dout       => hdlc_dout,
        hdlc_dvalid     => hdlc_dvalid
    );

fpga_config_logic: fpga_config_block
    port map(
        -----------------------------------
        ------- General Interface ----------
        clk_125             => clk_eth,
        rst                 => rst,
        rst_fifo_init       => fifo_init,
        cnt_bytes           => cnt_bytes,
        user_din_udp        => user_data_prv,
        ------------------------------------
        -------- UDP Interface -------------
        udp_rx              => udp_rx,
        ------------------------------------
        ---------- XADC Interface ----------
        xadc_conf           => '0',
        xadcPacket_rdy      => open,
        vmm_id_xadc         => open,
        xadc_sample_size    => open,
        xadc_delay          => open,
        ------------------------------------
        ---------- AXI4SPI Interface -------
        flash_conf          => '0',
        flashPacket_rdy     => open,
        myIP_set            => open,
        myMAC_set           => open,
        destIP_set          => open,
        ------------------------------------
        -------- CKTP/CKBC Interface -------
        ckbc_freq           => open,
        cktk_max_num        => open,
        cktp_max_num        => open,
        cktp_skew           => open,
        cktp_period         => open,
        cktp_width          => open,
        ckbc_max_num        => open,
        ------------------------------------
        -------- FPGA Config Interface -----
        fpga_conf           => fpga_conf,
        fpga_rst            => fpga_rst_i,
        fpgaPacket_rdy      => fpgaConf_done,
        analog_on           => open,
        latency             => latency_i,
        latency_extra       => open,
        artTimeout          => open,
        tr_delay_limit      => open,
        daq_on              => daq_on_i,
        ext_trigger         => extTrg_ena_i,
        ckbcMode            => trigProc_ena_i
    );

CDCC_125to40: CDCC
    generic map(NUMBER_OF_BITS => 20)
    port map(
        clk_src                 => clk_eth,
        clk_dst                 => clk_ckbc,
  
        data_in(0)              => fpga_rst_i,
        data_in(1)              => daq_on_i,
        data_in(17 downto 2)    => latency_i,
        data_in(18)             => extTrg_ena_i,
        data_in(19)             => trigProc_ena_i,

        data_out_s(0)           => fpga_rst_s40,
        data_out_s(1)           => acq_on,
        data_out_s(17 downto 2) => latency,
        data_out_s(18)          => extTrg_ena,
        data_out_s(19)          => trigProc_ena
    );

glbl_rst_buf: BUFG port map (O => fpga_rst, I => fpga_rst_i); -- was fpga_rst_s40

end RTL;
