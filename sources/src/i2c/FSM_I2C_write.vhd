library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity FSM_I2C_write is
    Port ( 
        CLK_50_MHz                   : in  std_logic;
        destIP                       : in  std_logic_vector(31 downto 0);
        srcIP                        : in  std_logic_vector(31 downto 0);
        mac                          : in  std_logic_vector(47 downto 0);
        destIP_retrieved             : in  std_logic_vector(31 downto 0);
        srcIP_retrieved              : in  std_logic_vector(31 downto 0);
        mac_retrieved                : in  std_logic_vector(47 downto 0);
        start                        : in  std_logic;
        done                         : out std_logic;
        destIP_address               : in  std_logic_vector(7 downto 0);
        srcIP_address                : in  std_logic_vector(7 downto 0);
        mac_address                  : in  std_logic_vector(7 downto 0);
        i2c_address_to_write         : out std_logic_vector(7 downto 0);
        i2c_byte_to_write            : out std_logic_vector(7 downto 0);
        i2c_start_sig                : out std_logic;
        i2c_done_sig                 : in  std_logic;
        i2c_read_writeN_ctrl_flag    : out std_logic;
        i2c_control_request          : out std_logic;
        read_fsm_done                : in  std_logic;
        read_fsm_start               : out std_logic);
end FSM_I2C_write;

architecture Behavioral of FSM_I2C_write is

    type stateType                          is (ST_IDLE, ST_WRITE, ST_SEND_CMD, ST_I2C_WAIT, ST_DEVICE_WAIT, ST_READ, ST_WAIT_READ, ST_VALIDATE, ST_ERROR);
    signal state                            : stateType := ST_IDLE;
    attribute FSM_ENCODING                  : string;
    attribute FSM_ENCODING of state         : signal is "ONE_HOT";
    signal write_counter                    : unsigned(3 downto 0);
    signal finished                         : std_logic := '0';
    signal i2c_device_delay_counter         : unsigned(18 downto 0); --for ~5ms delay on 50MHz clock
    signal control_request                  : std_logic := '0';
    signal i2c_address_to_write_unsg        : unsigned(7 downto 0) := (others => '0');
    signal read_fsm_start_internal          : std_logic := '0';
    
begin

    i2c_read_writeN_ctrl_flag   <= '0';
    done                        <= finished;
    i2c_control_request         <= control_request;
    i2c_address_to_write        <= std_logic_vector(i2c_address_to_write_unsg);
    read_fsm_start              <= read_fsm_start_internal;
    
    output: process(CLK_50_MHz)
    begin
    
        if(rising_edge(CLK_50_MHz)) then
            
            case state is
            
                when ST_IDLE =>
                    write_counter               <= (others => '0');
                    i2c_address_to_write_unsg   <= (others => '0');
                    i2c_start_sig               <= '0';
                    i2c_device_delay_counter    <= (others => '0');
                    if(start = '1') then
                        finished                <= '0';
                        control_request         <= '1';
                        state                   <= ST_WRITE; --begin sending commands to i2c module
                    end if;
                
                when ST_WRITE =>
                    case write_counter is
                    
                        when "0000" =>
                            i2c_byte_to_write           <= destIP(7 downto 0);
                            i2c_address_to_write_unsg   <= unsigned(destIP_address);
                            
                        when "0001" =>
                            i2c_byte_to_write           <= destIP(15 downto 8);
                            i2c_address_to_write_unsg   <= unsigned(destIP_address) + 1;
                            
                        when "0010" =>
                            i2c_byte_to_write           <= destIP(23 downto 16);
                            i2c_address_to_write_unsg   <= unsigned(destIP_address) + 2;
                            
                        when "0011" =>
                            i2c_byte_to_write           <= destIP(31 downto 24);
                            i2c_address_to_write_unsg   <= unsigned(destIP_address) + 3;
                        
                        when "0100" =>
                            i2c_byte_to_write           <= srcIP(7 downto 0);
                            i2c_address_to_write_unsg   <= unsigned(srcIP_address);
                            
                        when "0101" =>
                            i2c_byte_to_write           <= srcIP(15 downto 8);
                            i2c_address_to_write_unsg   <= unsigned(srcIP_address) + 1;
                            
                        when "0110" =>
                            i2c_byte_to_write           <= srcIP(23 downto 16);
                            i2c_address_to_write_unsg   <= unsigned(srcIP_address) + 2;
                            
                        when "0111" =>
                            i2c_byte_to_write           <= srcIP(31 downto 24);
                            i2c_address_to_write_unsg   <= unsigned(srcIP_address) + 3;
                        
                        when "1000" =>
                            i2c_byte_to_write           <= mac(7 downto 0);
                            i2c_address_to_write_unsg   <= unsigned(mac_address);
                            
                        when "1001" =>
                            i2c_byte_to_write           <= mac(15 downto 8);
                            i2c_address_to_write_unsg   <= unsigned(mac_address) + 1;
                            
                        when "1010" =>
                            i2c_byte_to_write           <= mac(23 downto 16);
                            i2c_address_to_write_unsg   <= unsigned(mac_address) + 2;
                            
                        when "1011" =>
                            i2c_byte_to_write           <= mac(31 downto 24);
                            i2c_address_to_write_unsg   <= unsigned(mac_address) + 3;
                            
                        when "1100" =>
                            i2c_byte_to_write           <= mac(39 downto 32);
                            i2c_address_to_write_unsg   <= unsigned(mac_address) + 4;
                            
                        when "1101" =>
                            i2c_byte_to_write           <= mac(47 downto 40);
                            i2c_address_to_write_unsg   <= unsigned(mac_address) + 5;        
                            
                        WHEN OTHERS => null;
                        
                    end case;
                    i2c_start_sig   <= '1';
                    state           <= ST_SEND_CMD;
                
                when ST_SEND_CMD =>
                    i2c_start_sig   <= '0';
                    write_counter   <= write_counter + 1;
                    state           <= ST_I2C_WAIT;
                
                when ST_I2C_WAIT =>
                    if(i2c_done_sig = '1') then
                        state <= ST_DEVICE_WAIT;
                    end if;
                
                when ST_DEVICE_WAIT =>
                    i2c_device_delay_counter <= i2c_device_delay_counter + 1;
                    if(i2c_device_delay_counter(18) = '1') then
                        if(write_counter = "1110") then
                            state                   <= ST_READ;
                            read_fsm_start_internal <= '1';
                            control_request         <= '0';
                        else
                            state                       <= ST_WRITE;
                            i2c_device_delay_counter    <= (others => '0');
                        end if;
                    end if;
                
                when ST_READ =>
                    read_fsm_start_internal <= '0';
                    state                   <= ST_WAIT_READ;
                
                when ST_WAIT_READ =>
                    if(read_fsm_done = '1') then
                        state <= ST_VALIDATE;
                    else
                        state <= ST_WAIT_READ;
                    end if;
                    
                when ST_VALIDATE =>
                    if(destIP = destIP_retrieved and srcIP = srcIP_retrieved and mac = mac_retrieved) then
                        state       <= ST_IDLE;
                        finished    <= '1';
                    else
                        state       <= ST_ERROR;
                    end if;

                when others => null;
                
            end case;
            
        end if;
    
    end process;

end Behavioral;
