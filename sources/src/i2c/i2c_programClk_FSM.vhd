----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2017 Christos Bakalis
--
--    This file is part of NTUA_BNL_VSB_firmware.
--
--    NTUA_BNL_VSB_firmware is free software: you can redistribute it 
--    and/or modify it under the terms of the GNU General Public License 
--    as published by the Free Software Foundation, either version 3 of
--    the License, or (at your option) any later version.
--
--    NTUA_BNL_VSB_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA_BNL_VSB_firmware. If not, see <http://www.gnu.org/licenses/>.
-- 
-- Create Date: 24.11.2017 17:17:47
-- Design Name: I2C Program Clk FSM
-- Module Name: i2c_programClk_FSM - RTL
-- Project Name: MMFE8 - NTUA
-- Target Devices: Virtex7 xc7vx690tffg1761-2
-- Tool Versions: Vivado 2017.3
-- Description: Test module to read and write the programmable clock values
--
-- Dependencies: NTUA_BNL_VSB_firmware
-- 
-- Changelog:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity i2c_programClk_FSM is
    Port(
        --------------------------
        ---- General Interface ---
        clk_50      : in  std_logic;
        clk_200khz  : in  std_logic;
        SDA         : in  std_logic;
        SCL         : in  std_logic;
        fifo_init   : in  std_logic;
        resetB      : out std_logic;
        --------------------------
        --- I2C Ctrl Interface ---
        Read_WriteN : out std_logic;
        DataOut     : out std_logic_vector(7 downto 0);
        SlaveOut    : out std_logic_vector(6 downto 0);
        AddressOut  : out std_logic_vector(7 downto 0);
        startI2C    : out std_logic;
        resetNI2C   : out std_logic;
        doneI2C     : in  std_logic;
        state_i2c   : in  std_logic_vector(7 downto 0);
        DataIn      : in  std_logic_vector(7 downto 0)
    );
end i2c_programClk_FSM;

architecture RTL of i2c_programClk_FSM is

component CDCC is
generic(
    NUMBER_OF_BITS : integer := 8); -- number of signals to be synced
port(
    clk_src     : in  std_logic;                                        -- input clk (source clock)
    clk_dst     : in  std_logic;                                        -- input clk (dest clock)
    data_in     : in  std_logic_vector(NUMBER_OF_BITS - 1 downto 0);    -- data to be synced
    data_out_s  : out std_logic_vector(NUMBER_OF_BITS - 1 downto 0)     -- synced data to clk_dst
    );
end component;

    signal cnt_send         : unsigned(1 downto 0) := (others => '0');
    signal cnt_wait         : unsigned(2 downto 0) := (others => '0');
    signal fifo_init_200k   : std_logic := '0';

    type stateTypeTX                 is (ST_IDLE, ST_WAIT_FOR_MASTER, ST_CNT, ST_DONE);
    signal  state                    : stateTypeTx := ST_IDLE;
    attribute FSM_ENCODING           : string;
    attribute FSM_ENCODING of state  : signal is "ONE_HOT";

begin

-- FSM that sequentially programs the Si5324 to provide a 125Mhz clock
Si5324_program_FSM: process(clk_200khz)
begin
    if(rising_edge(clk_200khz))then
        if(fifo_init_200k = '1')then
            cnt_send    <= (others => '0');
            cnt_wait    <= (others => '0');
            Read_WriteN <= '0';
            resetNI2C   <= '0';
            startI2C    <= '0';
            state       <= ST_IDLE;
        else
            case state is
            
            when ST_IDLE =>
                Read_WriteN <= '0';
                resetNI2C   <= '1';
                cnt_wait    <= cnt_wait + 1;
                if(cnt_wait = "111")then
                    state <= ST_WAIT_FOR_MASTER;
                else
                    state <= ST_IDLE;
                end if;

            -- is it done yet?
            when ST_WAIT_FOR_MASTER =>
                startI2C    <= '1';
                if(doneI2C = '1')then
                    state <= ST_CNT;
                else
                    state <= ST_WAIT_FOR_MASTER;
                end if;

            -- is everything out yet?
            when ST_CNT =>
                startI2C    <= '0';
                resetNI2C   <= '0';
                cnt_send    <= cnt_send + 1;
                if(cnt_send = "11")then
                    state <= ST_DONE;
                else
                    state <= ST_IDLE;
                end if;

            -- done, sent all. stay here (and rot).
            when ST_DONE => 
                state <= ST_DONE;

            when others => state <= ST_WAIT_FOR_MASTER;
            end case;

        end if;
    end if;
end process;

-- dout synchronous multiplexer
dout_mux: process(clk_200khz)
begin
    if(rising_edge(clk_200khz))then
        if(fifo_init_200k = '1')then
            SlaveOut    <= (others => '0');
            AddressOut  <= (others => '0');
            DataOut     <= (others => '0');
        else
            case cnt_send is
            when "00"   => SlaveOut <= "1110100"; AddressOut <= "10000000"; DataOut <= "10000000"; 
            when "01"   => SlaveOut <= "1101000"; AddressOut <= "00110111"; DataOut <= "00000011"; 
            when "10"   => SlaveOut <= "1101000"; AddressOut <= "00000000"; DataOut <= "00000010"; 
            when "11"   => SlaveOut <= "1101000"; AddressOut <= "10001000"; DataOut <= "01000000"; 
            when others => SlaveOut <= (others => '0'); AddressOut <= (others => '0'); DataOut <= (others => '0');
            end case;
        end if;
    end if;
end process;

sync_50to200k: CDCC
generic map(
    NUMBER_OF_BITS => 1) -- number of signals to be synced
port map(
    clk_src         => clk_50,
    clk_dst         => clk_200khz,
    data_in(0)      => fifo_init,
    data_out_s(0)   => fifo_init_200k
    );

    resetB <= '1';


end RTL;
