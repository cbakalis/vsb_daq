library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity i2c_wrapper is
    Port ( 
        clk_50  : in    std_logic;
        resetB  : out   std_logic;
        sda     : inout std_logic;
        scl     : out   std_logic
     );
end i2c_wrapper;

architecture Logic of i2c_wrapper is

component I2C_controller
	port(
		CLK_50_MHz    : in    std_logic;
		ResetN        : in    std_logic;
		Read_WriteN   : in    std_logic;
		DataIn        : in    std_logic_vector(7 downto 0);
		AddressIn     : in    std_logic_vector(7 downto 0);    
		SDA           : inout std_logic;      
		SCL           : out   std_logic;
		DataOut       : out   std_logic_vector(7 downto 0);
		SlaveAddr     : in    std_logic_vector(6 DOWNTO 0);
		Done          : out   std_logic;
		State_o       : out   std_logic_vector(7 downto 0);
        Start         : in    std_logic;
        i2cClockOut   : out   std_logic
    );
end component;

component i2c_programClk_FSM
    port(
        --------------------------
        ---- General Interface ---
        clk_50      : in  std_logic;
        clk_200khz  : in  std_logic;
        SDA         : in  std_logic;
        SCL         : in  std_logic;
        resetB      : out std_logic;
        --------------------------
        --- I2C Ctrl Interface ---
        Read_WriteN : out std_logic;
        DataOut     : out std_logic_vector(7 downto 0);
        SlaveOut    : out std_logic_vector(6 downto 0);
        AddressOut  : out std_logic_vector(7 downto 0);
        startI2C    : out std_logic;
        resetNI2C   : out std_logic;
        doneI2C     : in  std_logic;
        state_i2c   : in  std_logic_vector(7 downto 0);
        DataIn      : in  std_logic_vector(7 downto 0)
    );
end component;

    signal clk_200khz           : std_logic := '0';
    signal i2cReadWriteNflag    : std_logic := '0';
    signal i2cSlaveAddr         : std_logic_vector(6 downto 0) := (others => '0');
    signal i2cStart             : std_logic := '0';
    signal i2cDone              : std_logic := '0';
    signal i2cResetN            : std_logic := '0';
    signal i2cDataIn            : std_logic_vector(7 downto 0) := (others => '0');
    signal i2cAddressIn         : std_logic_vector(7 downto 0) := (others => '0');
    signal i2cDataOut           : std_logic_vector(7 downto 0) := (others => '0');
    signal i2cState             : std_logic_vector(7 downto 0) := (others => '0');
    signal sda_i                : std_logic := '0';
    signal scl_i                : std_logic := '0';
	   
begin
 
I2C_module: I2C_controller
    port map(
        CLK_50_MHz  => clk_50,
        ResetN      => i2cResetN,
        SCL         => scl_i,
        SDA         => sda_i,
        Read_WriteN => i2cReadWriteNflag,
        SlaveAddr   => i2cSlaveAddr,
        Start       => i2cStart,
        Done        => i2cDone,
        DataIn      => i2cDataIn,
        AddressIn   => i2cAddressIn,
        DataOut     => i2cDataOut,
        State_o     => i2cState,
        i2cClockOut => clk_200khz
    );
    
I2C_CTRL_FSM: i2c_programClk_FSM
    port map(
        --------------------------
        ---- General Interface ---
        clk_50      => clk_50,
        clk_200khz  => clk_200khz,
        SCL         => scl_i,
        SDA         => sda_i,
        resetB      => resetB,
        --------------------------
        --- I2C Ctrl Interface ---
        Read_WriteN => i2cReadWriteNflag,
        DataOut     => i2cDataIn,
        SlaveOut    => i2cSlaveAddr,
        AddressOut  => i2cAddressIn,
        startI2C    => i2cStart,
        resetNI2C   => i2cResetN,
        doneI2C     => i2cDone,
        state_i2c   => i2cState,
        DataIn      => i2cDataOut
    );

    sda <= sda_i;
    scl <= scl_i;

end Logic;
