-----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2017 Christos Bakalis
--
--    This file is part of NTUA_BNL_VSB_firmware.
--
--    NTUA_BNL_VSB_firmware is free software: you can redistribute it 
--    and/or modify it under the terms of the GNU General Public License 
--    as published by the Free Software Foundation, either version 3 of
--    the License, or (at your option) any later version.
--
--    NTUA_BNL_VSB_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA_BNL_VSB_firmware. If not, see <http://www.gnu.org/licenses/>.
-- 
-- Create Date: 21.05.2018 00:51:33
-- Design Name: VSB CKTP
-- Module Name: VSB_cktp - RTL
-- Project Name: VSB DAQ - NTUA/BNL
-- Target Devices: Virtex7 xc7vx690tffg1761-2
-- Tool Versions: Vivado 2017.3
-- Description: Module that asserts CKTP pulses to FEs
--
-- Dependencies: NTUA_BNL_VSB_firmware
-- 
-- Changelog:
-- 
-----------------------------------------------------------------------------------
library IEEE;
library UNISIM;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use UNISIM.VComponents.all;

entity VSB_cktp is
    port(
        ------------------------------------
        ------- General Interface ----------
        clk_ckbc    : in  std_logic;
        enable      : in  std_logic;
        rst_oddr    : in  std_logic_vector(7 downto 0);
        cktp_trg    : out std_logic;
        ------------------------------------
        --------- FE Interface -------------
        fe_busy     : in  std_logic;
        fe_cktp     : out std_logic_vector(7 downto 0)
     );
end VSB_cktp;

architecture RTL of VSB_cktp is

    signal cktp_cnt     : unsigned(21 downto 0) := (others => '0');
    signal cktp_per     : unsigned(21 downto 0) := "1111111111111111111111";
    signal fe_cktp_i    : std_logic := '0';
    signal fe_cktpVec_i : std_logic_vector(7 downto 0) := (others => '0');
    signal cktp_sreg    : std_logic_vector(7 downto 0) := (others => '0');

begin

-- process that asserts CKTP
cktpAssert_proc: process(clk_ckbc)
begin
    if(rising_edge(clk_ckbc))then
        if(enable = '1')then

            cktp_trg    <= fe_cktp_i;
            cktp_sreg   <= fe_cktp_i & cktp_sreg(7 downto 1);

            if(cktp_cnt <= "0000000000000111111111")then
                fe_cktp_i   <= '1';
                cktp_cnt    <= cktp_cnt + 1;
            elsif(cktp_cnt > "0000000000000111111111" and
                  cktp_cnt < cktp_per)then
                fe_cktp_i   <= '0';
                cktp_cnt    <= cktp_cnt + 1;
            else
                fe_cktp_i   <= '0';
                cktp_cnt    <= (others => '0');
            end if;

        else
            fe_cktp_i   <= '0';
            cktp_trg    <= '0';
            cktp_cnt    <= (others => '0');
        end if;
    end if;
end process;

-- process that propagates the CKTP to the FEs
cktp_distr_proc: process(clk_ckbc)
begin
    if(rising_edge(clk_ckbc))then
        if(fe_busy = '0' and enable = '1')then
            fe_cktpVec_i(7) <= cktp_sreg(7);
            fe_cktpVec_i(6) <= cktp_sreg(7);
            fe_cktpVec_i(5) <= cktp_sreg(7);
            fe_cktpVec_i(4) <= cktp_sreg(7);
            fe_cktpVec_i(3) <= cktp_sreg(7);
            fe_cktpVec_i(2) <= cktp_sreg(7);
            fe_cktpVec_i(1) <= cktp_sreg(7);
            fe_cktpVec_i(0) <= cktp_sreg(7);
        else
            fe_cktpVec_i <= (others => '0');
        end if;
    end if;
end process;

genOddr_cktp: for I in 0 to 7 generate

ODDR_CKTP: ODDR
    generic map(
        DDR_CLK_EDGE => "SAME_EDGE",
        INIT         => '0',
        SRTYPE       => "SYNC")
    port map(
        Q   => fe_cktp(I),
        C   => clk_ckbc,
        CE  => '1',
        D1  => fe_cktpVec_i(I),
        D2  => fe_cktpVec_i(I),
        R   => rst_oddr(I),
        S   => '0' 
    );

end generate genOddr_cktp;

end RTL;
