----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch)
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2017 Christos Bakalis
--
--    This file is part of NTUA_BNL_VSB_firmware.
--
--    NTUA_BNL_VSB_firmware is free software: you can redistribute it 
--    and/or modify it under the terms of the GNU General Public License 
--    as published by the Free Software Foundation, either version 3 of
--    the License, or (at your option) any later version.
--
--    NTUA_BNL_VSB_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA_BNL_VSB_firmware. If not, see <http://www.gnu.org/licenses/>.
-- 
-- Create Date: 27.12.2017 17:51:44
-- Design Name: CLK ODDR Wrapper
-- Module Name: clk_oddr_wrapper - RTL
-- Project Name: VSB - NTUA/BNL
-- Target Devices: Virtex7 xc7vx690tffg1761-2
-- Tool Versions: Vivado 2017.3
-- Description: Wrapper that contains the ODDR instantiations necessary for
-- clock forwarding.
-- 
-- Dependencies: 
-- 
-- Changelog: 
-- 
----------------------------------------------------------------------------------
library IEEE;
library UNISIM;
use IEEE.STD_LOGIC_1164.ALL;
use UNISIM.VComponents.all;

entity clk_oddr_wrapper is
    Port(
        -----------------------------
        ---- Clock Input Interface --
        clk_40          : in  std_logic;
        clk_50          : in  std_logic;
        clk_80          : in  std_logic;
        clk_125_dirty   : in  std_logic;
        clk_125_clean   : in  std_logic;
        clk_160         : in  std_logic;
        clk_200         : in  std_logic;
        clk_320         : in  std_logic;
        rst_oddr        : in  std_logic_vector(7 downto 0);
        -----------------------------
        --- Clock Output Interface --
        clk_40_oddr     : out  std_logic_vector(7 downto 0);
        clk_50_oddr     : out  std_logic_vector(0 downto 0);
        clk_80_oddr     : out  std_logic_vector(0 downto 0);
        clk_125cl_oddr  : out  std_logic_vector(0 downto 0);
        clk_125dir_oddr : out  std_logic_vector(1 downto 0);
        clk_160_oddr    : out  std_logic_vector(0 downto 0);
        clk_200_oddr    : out  std_logic_vector(0 downto 0);
        clk_320_oddr    : out  std_logic_vector(0 downto 0)
    );
end clk_oddr_wrapper;

architecture RTL of clk_oddr_wrapper is

begin

----------------------------
--------- CKDT/ODDR --------
----------------------------
gen_oddr40: for I in 0 to 7 generate

ODDR_40: ODDR
        generic map(
            DDR_CLK_EDGE => "OPPOSITE_EDGE",
            INIT         => '0',
            SRTYPE       => "SYNC")
        port map(
            Q   => clk_40_oddr(I),
            C   => clk_40,
            CE  => '1',
            D1  => '1',
            D2  => '0',
            R   => rst_oddr(I),
            S   => '0' 
        );

end generate gen_oddr40;


ODDR_50_0: ODDR
    generic map(
        DDR_CLK_EDGE => "OPPOSITE_EDGE",
        INIT         => '0',
        SRTYPE       => "SYNC")
    port map(
        Q   => clk_50_oddr(0),
        C   => clk_50,
        CE  => '1',
        D1  => '1',
        D2  => '0',
        R   => '0',
        S   => '0' 
    );
    
ODDR_125_0: ODDR
        generic map(
            DDR_CLK_EDGE => "OPPOSITE_EDGE",
            INIT         => '0',
            SRTYPE       => "SYNC")
        port map(
            Q   => clk_125dir_oddr(0),
            C   => clk_125_dirty,
            CE  => '1',
            D1  => '1',
            D2  => '0',
            R   => '0',
            S   => '0' 
        );
        
ODDR_125_1: ODDR
        generic map(
            DDR_CLK_EDGE => "OPPOSITE_EDGE",
            INIT         => '0',
            SRTYPE       => "SYNC")
        port map(
            Q   => clk_125dir_oddr(1),
            C   => clk_125_dirty,
            CE  => '1',
            D1  => '1',
            D2  => '0',
            R   => '0',
            S   => '0' 
        );
        
ODDR_125_2: ODDR
        generic map(
            DDR_CLK_EDGE => "OPPOSITE_EDGE",
            INIT         => '0',
            SRTYPE       => "SYNC")
        port map(
            Q   => clk_125cl_oddr(0),
            C   => clk_125_clean,
            CE  => '1',
            D1  => '1',
            D2  => '0',
            R   => '0',
            S   => '0' 
        );

end RTL;