library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity mux_2to1_Nbit is
    generic (N : integer);
    Port (
            sel : in  std_logic;
            A   : in  std_logic_vector(N-1 downto 0);
            B   : in  std_logic_vector(N-1 downto 0);
            X   : out std_logic_vector(N-1 downto 0)
    );
end mux_2to1_Nbit;

architecture Logic of mux_2to1_Nbit is

begin
    X <= A when (sel = '1') else B;
end Logic;
