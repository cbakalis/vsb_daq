----------------------------------------------------------------------------------
-- Company: NTU ATHNENS - BNL
-- Engineer: Panagiotis Gkountoumis
-- 
-- Create Date: 18.04.2016 13:00:21
-- Design Name: 
-- Module Name: config_logic - Behavioral
-- Project Name: MMFE8 
-- Target Devices: Arix7 xc7a200t-2fbg484 and xc7a200t-3fbg484 
-- Tool Versions: Vivado 2017.3
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
library UNISIM;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use UNISIM.VComponents.all;

entity startup_logic is
    
        generic(cnt_1ms : natural := 40_000;  -- 25ns*40_000 = 1ms
                 cnt_10ms : natural := 400_000); --25ns*400_000 = 10ms 
    
    port(
        clk_in       : in  std_logic; -- clk40
        mmcm_locked  : in  std_logic_vector(1 downto 0);
        fifo_init    : out std_logic;
        phy_rstn_out : out std_logic
        );
    
end startup_logic;

architecture rtl of startup_logic is

    
    signal phy_resetn       : std_logic := '0';
    signal fifo_init_i      : std_logic := '0';

    

begin


INIT_BUFG: BUFG port map(O => fifo_init, I => fifo_init_i);

    phy_rstn_out     <= phy_resetn;
    fifo_init_i      <= not phy_resetn;

                
    phy_resetn_process : process(clk_in) is
        
            variable cnt : natural range 0 to cnt_10ms := 0; --1ms
            
                begin
                    if (rising_edge(clk_in)) then
                        --if(mmcm_locked(0) = '1' and mmcm_locked(1) = '1')then
                            if phy_resetn = '0' then --resetn
                                if(cnt < cnt_10ms)then --cnt
                                    cnt := cnt + 1;
                                elsif(cnt = cnt_10ms)then
                                    cnt := 0;
                                    phy_resetn <= '1';
                                else null;
                                end if; --cnt
                            else null;
                            end if; --resetn check
                        --else
                           -- phy_resetn <= '0';
                            --cnt := 0;
                        --end if;
                    end if; --clk               
                end process;

    
end rtl;
