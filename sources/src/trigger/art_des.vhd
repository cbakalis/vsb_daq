----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2017 Christos Bakalis
--
--    This file is part of NTUA_BNL_VSB_firmware.
--
--    NTUA_BNL_VSB_firmware is free software: you can redistribute it 
--    and/or modify it under the terms of the GNU General Public License 
--    as published by the Free Software Foundation, either version 3 of
--    the License, or (at your option) any later version.
--
--    NTUA_BNL_VSB_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA_BNL_VSB_firmware. If not, see <http://www.gnu.org/licenses/>.
-- 
-- Create Date: 24.01.2018 11:20:03
-- Design Name: ART Deserializer
-- Module Name: art_des - RTL
-- Project Name: VSB DAQ - NTUA/BNL
-- Target Devices: Virtex7 xc7vx690tffg1761-2
-- Tool Versions: Vivado 2017.3
-- Description: Module that deserializes ART data and interfaces with the ART shift
-- registers.
--
-- Dependencies: NTUA_BNL_VSB_firmware
-- 
-- Changelog:
-- 21.05.2018: Upgraded module to align with art stream. (Christos Bakalis)
--
----------------------------------------------------------------------------------
library IEEE;
library UNISIM;
use IEEE.STD_LOGIC_1164.ALL;
use UNISIM.VComponents.all;
use WORK.ART_PACKAGE.all;

entity art_des is
    port(
        ------------------------------------
        ------- General Interface ----------
        clk_art         : in  std_logic; -- ART clock
        rst             : in  std_logic;
        inhibit_des     : in  std_logic;
        sreg_dbg        : out std_logic_vector(11 downto 0);
        ------------------------------------
        ---------- VMM Interface -----------
        art_in          : in  std_logic; -- ART line
        ------------------------------------
        -------- ART Sreg Interface --------
        art_word_out    : out std_logic_vector(6 downto 0); -- to shift register
        art_word_sreg   : in  std_logic_vector(6 downto 0)  -- value of first position of sreg
    );
end art_des;

architecture RTL of art_des is

    signal data_pos         : std_logic := '0';
    signal data_neg         : std_logic := '0';
    signal rst_iddr         : std_logic := '0';
    signal fsm_ena          : std_logic := '0';
    signal art_channel      : std_logic_vector(5 downto 0)  := (others => '0');
    signal art_flag         : std_logic := '0';
    signal sample_chan      : std_logic := '0';
    signal art_word_out_i   : std_logic_vector(6 downto 0)  := (others => '0');
    signal sel_ch           : std_logic_vector(1 downto 0)  := (others => '0');
    signal ch_mux           : std_logic_vector(1 downto 0)  := (others => '0');
    signal artDes_sreg      : std_logic_vector(11 downto 0) := (others => '0');

    type stateType is (ST_IDLE, ST_REG_CHAN, ST_REG_WORD, ST_SYNC, ST_END);
    signal state    : stateType := ST_IDLE;

    attribute FSM_ENCODING              : string;
    attribute FSM_ENCODING of state     : signal is "ONE_HOT";

begin

-- deserialization monitoring process (add an extra zero at the right side?)
monitorDes_proc: process(clk_art)
begin
    if(rising_edge(clk_art))then
        if(artDes_sreg(10 downto 8) = "011")then -- flag = 011
            fsm_ena <= '1';
            ch_mux  <= "00";
        elsif(artDes_sreg(11 downto 9) = "011")then
            fsm_ena <= '1';
            ch_mux  <= "01";
        elsif(artDes_sreg(10 downto 7) = "0111")then -- flag = 0111
            fsm_ena <= '1';
            ch_mux  <= "10";
        elsif(artDes_sreg(11 downto 8) = "0111")then
            fsm_ena <= '1';
            ch_mux  <= "11";
        else
            fsm_ena <= '0';
            ch_mux  <= "00";
        end if;
    end if;
end process;

-- counter/channer selector FSM
chMuxFSM_proc: process(clk_art)
begin
    if(rising_edge(clk_art))then
        if(rst = '1')then
            sel_ch          <= "00";
            art_flag        <= '0';
            sample_chan     <= '0';
            art_channel     <= (others => '0');
            art_word_out_i  <= (others => '0');
            state           <= ST_IDLE;
        else
            case state is

            -- do we have an event?
            when ST_IDLE =>
                art_flag        <= '0';
                sample_chan     <= '0';
                art_channel     <= (others => '0');
                art_word_out_i  <= (others => '0');

                if(fsm_ena = '1')then
                    sel_ch  <= ch_mux;
                    state   <= ST_REG_CHAN;
                else
                    sel_ch  <= "00";
                    state   <= ST_IDLE;
                end if;

            -- get the channel and flag
            when ST_REG_CHAN =>
                sample_chan <= not sample_chan;
                
                if(sample_chan = '1')then
                    case sel_ch is
                    when "00"   => art_channel <= artDes_sreg(10 downto 5); art_flag <= artDes_sreg(2);
                    when "01"   => art_channel <= artDes_sreg(11 downto 6); art_flag <= artDes_sreg(3);
                    when "10"   => art_channel <= artDes_sreg(10 downto 5); art_flag <= artDes_sreg(1);
                    when "11"   => art_channel <= artDes_sreg(11 downto 6); art_flag <= artDes_sreg(2);
                    when others => art_channel <= (others => '0');          art_flag <= '0';
                    end case;
                    state <= ST_REG_WORD;
                else
                    state <= ST_REG_CHAN;
                end if;

            -- pass it to the sregs
            when ST_REG_WORD =>
                art_word_out_i(6)           <= art_flag;
                art_word_out_i(5 downto 0)  <= art_channel;
                state                       <= ST_SYNC;

            -- did the sregs get the message?
            when ST_SYNC =>
                if(art_word_out_i /= art_word_sreg)then
                    art_word_out_i(6)           <= art_flag;
                    art_word_out_i(5 downto 0)  <= art_channel;
                    state                       <= ST_SYNC;
                else
                    art_word_out_i              <= (others => '0');
                    state                       <= ST_END;
                end if;

            -- is the line quiet?
            when ST_END =>
                if(artDes_sreg = x"000" and fsm_ena = '0')then
                    state <= ST_IDLE;
                else
                    state <= ST_END;
                end if;

            when others => state <= ST_IDLE;

            end case;
        end if;
    end if;
end process;

IDDR_ART: IDDR
    generic map (
        DDR_CLK_EDGE => "SAME_EDGE",
        INIT_Q1      => '0',
        INIT_Q2      => '0',
        SRTYPE       => "SYNC")
    port map (
        Q1  => data_pos,
        Q2  => data_neg,
        C   => clk_art,
        CE  => '1',
        D   => art_in,
        R   => rst_iddr,
        S   => '0'
);

-- sregs
artSregs_proc: process(clk_art)
begin
    if(rising_edge(clk_art))then
        artDes_sreg <= data_pos & data_neg & artDes_sreg(11 downto 2);
    end if;
end process;

    rst_iddr        <= inhibit_des;
    art_word_out    <= art_word_out_i;
    sreg_dbg        <= artDes_sreg;

end RTL;
