----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2017 Christos Bakalis
--
--    This file is part of NTUA_BNL_VSB_firmware.
--
--    NTUA_BNL_VSB_firmware is free software: you can redistribute it 
--    and/or modify it under the terms of the GNU General Public License 
--    as published by the Free Software Foundation, either version 3 of
--    the License, or (at your option) any later version.
--
--    NTUA_BNL_VSB_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA_BNL_VSB_firmware. If not, see <http://www.gnu.org/licenses/>.
-- 
-- Create Date: 30.01.2018 17:32:13
-- Design Name: ART FIFO Network
-- Module Name: art_fifos - RTL
-- Project Name: VSB DAQ - NTUA/BNL
-- Target Devices: Virtex7 xc7vx690tffg1761-2
-- Tool Versions: Vivado 2017.3
-- Description: Wrapper for the ART FIFOs. Contains the arbiter FSM which drives
-- the din of the final, candidate-picking FIFO
--
-- Dependencies: NTUA_BNL_VSB_firmware, art_package
-- 
-- Changelog:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use WORK.ART_PACKAGE.all;
use IEEE.NUMERIC_STD.ALL;

entity art_fifos is
    port(
        ------------------------------------
        ------- General Interface ----------
        clk_ckbc    : in  std_logic; -- CKBC
        clk_art     : in  std_logic; -- ART clock
        clk_art2    : in  std_logic; -- ART clockx2
        rst         : in  std_logic;
        fifo_init   : in  std_logic;
        full_prior  : out std_logic;
        full_main0  : out std_logic_vector(4 downto 0);
        full_main1  : out std_logic_vector(4 downto 0);
        full_main2  : out std_logic_vector(4 downto 0);
        -------------------------------------
        -------- ART Sregs Interface --------
        wrEn_fifo0  : in  std_logic_vector(4 downto 0);
        wrEn_fifo1  : in  std_logic_vector(4 downto 0);
        wrEn_fifo2  : in  std_logic_vector(4 downto 0);
        --
        din_fifo0   : in  fifoData_main;
        din_fifo1   : in  fifoData_main;
        din_fifo2   : in  fifoData_main;        
        -------------------------------------
        --------- FIFO Interface ---------
        wrEn_FIFO : out std_logic;
        dout_FIFO : out std_logic_vector(53 downto 0)
    );

end art_fifos;

architecture RTL of art_fifos is

-- FIFO that stores information for the arbiter
component fifo_prioritizer
    port(
        rst         : in  std_logic;
        wr_clk      : in  std_logic;
        rd_clk      : in  std_logic;
        din         : in  std_logic_vector(14 downto 0);
        wr_en       : in  std_logic;
        rd_en       : in  std_logic;
        dout        : out std_logic_vector(14 downto 0);
        full        : out std_logic;
        empty       : out std_logic;
        wr_rst_busy : out std_logic;
        rd_rst_busy : out std_logic
    );
end component;

-- main ART data storing FIFO
component fifo_main
    port (
        rst         : in  std_logic;
        wr_clk      : in  std_logic;
        rd_clk      : in  std_logic;
        din         : in  std_logic_vector(58 downto 0);
        wr_en       : in  std_logic;
        rd_en       : in  std_logic;
        dout        : out std_logic_vector(58 downto 0);
        full        : out std_logic;
        empty       : out std_logic;
        wr_rst_busy : out std_logic;
        rd_rst_busy : out std_logic
  );
end component;

    signal rst_i                : std_logic := '0';

    -- prioritizing FIFO signals
    signal din_fifoPrior_i      : std_logic_vector(14 downto 0) := (others => '0');
    signal din_fifoPrior        : std_logic_vector(14 downto 0) := (others => '0');
    signal dout_fifoPrior       : std_logic_vector(14 downto 0) := (others => '0');
    signal wrEn_fifoPrior_i     : std_logic := '0';
    signal wrEn_fifoPrior       : std_logic := '0';
    signal rdEn_fifoPrior       : std_logic := '0';
    signal full_prior_i         : std_logic := '0';
    signal empty_prior          : std_logic := '0';
    signal wr_busy_prior        : std_logic := '0';
    signal rd_busy_prior        : std_logic := '0';

    -- main FIFO signals
    signal rd_en_main0          : std_logic_vector(4 downto 0)  := (others => '0');
    signal rd_en_main1          : std_logic_vector(4 downto 0)  := (others => '0');
    signal rd_en_main2          : std_logic_vector(4 downto 0)  := (others => '0');
    signal empty_main0          : std_logic_vector(4 downto 0)  := (others => '0');
    signal empty_main1          : std_logic_vector(4 downto 0)  := (others => '0');
    signal empty_main2          : std_logic_vector(4 downto 0)  := (others => '0');
    signal wr_busy_main0        : std_logic_vector(4 downto 0)  := (others => '0');
    signal wr_busy_main1        : std_logic_vector(4 downto 0)  := (others => '0');
    signal wr_busy_main2        : std_logic_vector(4 downto 0)  := (others => '0');
    signal rd_busy_main0        : std_logic_vector(4 downto 0)  := (others => '0');
    signal rd_busy_main1        : std_logic_vector(4 downto 0)  := (others => '0');
    signal rd_busy_main2        : std_logic_vector(4 downto 0)  := (others => '0');
    signal dout_fifoPrior_i     : std_logic_vector(15 downto 0) := (others => '0');
    signal dout_main0           : fifoData_main;
    signal dout_main1           : fifoData_main;
    signal dout_main2           : fifoData_main;

    -- master FSM signals
    signal wait_cnt_mst         : unsigned(1 downto 0) := (others => '0');
    signal ena_arb              : std_logic := '0';
    type stateType_master is    (ST_IDLE, ST_WAIT_0, ST_WAIT_1, ST_RD_FIFO_MAIN, ST_ENA_ARB);
    signal state_mst            : stateType_master := ST_IDLE;

    -- arbiter FSM signals
    signal arb_done             : std_logic := '0';
    signal wrEn_FIFO_i        : std_logic := '0';
    signal bcid_a               : unsigned(11 downto 0)         := (others => '0');
    signal bcid_b               : unsigned(11 downto 0)         := (others => '0');
    signal bcid_c               : unsigned(11 downto 0)         := (others => '0');
    signal dout_buff            : std_logic_vector(58 downto 0) := (others => '0');
    signal art_a                : std_logic_vector(5 downto 0)  := (others => '0');
    signal art_b                : std_logic_vector(5 downto 0)  := (others => '0');
    signal art_c                : std_logic_vector(5 downto 0)  := (others => '0');
    signal scan_prior_cnt       : unsigned(3 downto 0)          := (others => '0');
    signal scan_buff_cnt        : unsigned(5 downto 0)          := to_unsigned(6, 6);
    type stateType_arbiter is   (ST_SCAN_PRIOR, ST_GET_BCID_ART, ST_SCAN_BUFF, ST_WR_BUFF, ST_DONE);
    signal state_arb            : stateType_arbiter := ST_SCAN_PRIOR;

    -- misc
    attribute FSM_ENCODING              : string;
    attribute FSM_ENCODING of state_mst : signal is "ONE_HOT";
    attribute FSM_ENCODING of state_arb : signal is "ONE_HOT";

begin

-- one pipeline level before the prioritizer
pipe_prior_proc: process(clk_ckbc)
begin
    if(rising_edge(clk_ckbc))then
        wrEn_fifoPrior  <= wrEn_fifoPrior_i;
        din_fifoPrior   <= din_fifoPrior_i;   
    end if;
end process;

-- master FSM. Reads prioritizing FIFO, end enables the arbiter FSM
master_FSM_proc: process(clk_art)
begin
    if(rising_edge(clk_art))then
        if(rst = '1')then
            rdEn_fifoPrior  <= '0';
            wait_cnt_mst    <= (others => '0');
            rd_en_main0     <= (others => '0');
            rd_en_main1     <= (others => '0');
            rd_en_main2     <= (others => '0');
            ena_arb         <= '0';
            state_mst       <= ST_IDLE;
        else
            case state_mst is

            -- is prior empty?
            when ST_IDLE =>
                if(empty_prior = '0')then
                    rdEn_fifoPrior  <= '1';
                    state_mst       <= ST_WAIT_0;
                else
                    rdEn_fifoPrior  <= '0';
                    state_mst       <= ST_IDLE;
                end if;

            -- state that waits for the dout to stabilize
            when ST_WAIT_0 =>
                rdEn_fifoPrior <= '0';
                if(wait_cnt_mst = "10")then
                    wait_cnt_mst    <= (others => '0');
                    state_mst       <= ST_RD_FIFO_MAIN;
                else
                    wait_cnt_mst    <= wait_cnt_mst + 1;
                    state_mst       <= ST_WAIT_0;
                end if;

            -- read the appropriate main FIFOs
            when ST_RD_FIFO_MAIN =>
                rd_en_main2     <= dout_fifoPrior_i(14 downto 10);
                rd_en_main1     <= dout_fifoPrior_i(9 downto 5);
                rd_en_main0     <= dout_fifoPrior_i(4 downto 0);
                state_mst       <= ST_WAIT_1;

            -- state that waits for the dout to stabilize
            when ST_WAIT_1 =>
                rd_en_main0     <= (others => '0');
                rd_en_main1     <= (others => '0');
                rd_en_main2     <= (others => '0');
                if(wait_cnt_mst = "10")then
                    wait_cnt_mst    <= (others => '0');
                    state_mst       <= ST_ENA_ARB;
                else
                    wait_cnt_mst    <= wait_cnt_mst + 1;
                    state_mst       <= ST_WAIT_1;
                end if;

            -- activate the arbiter FSM
            when ST_ENA_ARB =>
                if(arb_done = '1')then
                    ena_arb     <= '0';
                    state_mst   <= ST_IDLE;
                else
                    ena_arb     <= '1';
                    state_mst   <= ST_ENA_ARB;
                end if;

            when others => state_mst <= ST_IDLE;
            end case;
        end if;
    end if;
end process;

-- arbiter FSM. decides what to write in the FIFO
arbiter_FSM_proc: process(clk_art)
begin
    if(rising_edge(clk_art))then
        if(ena_arb = '0')then
             arb_done             <= '0';
             wrEn_FIFO_i        <= '0';
             bcid_a               <= (others => '0');
             bcid_b               <= (others => '0');
             bcid_c               <= (others => '0');
             dout_buff            <= (others => '0');
             art_a                <= (others => '0');
             art_b                <= (others => '0');
             art_c                <= (others => '0');
             scan_prior_cnt       <= (others => '0');
             scan_buff_cnt        <= to_unsigned(6, 6);
             state_arb            <= ST_SCAN_PRIOR;
        else
            case state_arb is

            -- scan the dout of the prioritizer 
            when ST_SCAN_PRIOR =>
                wrEn_FIFO_i <= '0';
                if(dout_fifoPrior_i(to_integer(scan_prior_cnt)) = '1')then
                    scan_prior_cnt  <= scan_prior_cnt;
                    case to_integer(scan_prior_cnt) is
                    when 0      => dout_buff <= dout_main0(0); bcid_a <= unsigned(dout_buff(58 downto 47)); art_a <= dout_buff(46 downto 41); state_arb <= ST_GET_BCID_ART;
                    when 1      => dout_buff <= dout_main0(1); bcid_a <= unsigned(dout_buff(58 downto 47)); art_a <= dout_buff(46 downto 41); state_arb <= ST_GET_BCID_ART;
                    when 2      => dout_buff <= dout_main0(2); bcid_a <= unsigned(dout_buff(58 downto 47)); art_a <= dout_buff(46 downto 41); state_arb <= ST_GET_BCID_ART;
                    when 3      => dout_buff <= dout_main0(3); bcid_a <= unsigned(dout_buff(58 downto 47)); art_a <= dout_buff(46 downto 41); state_arb <= ST_GET_BCID_ART;
                    when 4      => dout_buff <= dout_main0(4); bcid_a <= unsigned(dout_buff(58 downto 47)); art_a <= dout_buff(46 downto 41); state_arb <= ST_GET_BCID_ART;
                    when 5      => dout_buff <= dout_main1(0); bcid_b <= unsigned(dout_buff(58 downto 47)); art_b <= dout_buff(46 downto 41); state_arb <= ST_GET_BCID_ART;
                    when 6      => dout_buff <= dout_main1(1); bcid_b <= unsigned(dout_buff(58 downto 47)); art_b <= dout_buff(46 downto 41); state_arb <= ST_GET_BCID_ART;
                    when 7      => dout_buff <= dout_main1(2); bcid_b <= unsigned(dout_buff(58 downto 47)); art_b <= dout_buff(46 downto 41); state_arb <= ST_GET_BCID_ART;
                    when 8      => dout_buff <= dout_main1(3); bcid_b <= unsigned(dout_buff(58 downto 47)); art_b <= dout_buff(46 downto 41); state_arb <= ST_GET_BCID_ART;
                    when 9      => dout_buff <= dout_main1(4); bcid_b <= unsigned(dout_buff(58 downto 47)); art_b <= dout_buff(46 downto 41); state_arb <= ST_GET_BCID_ART;
                    when 10     => dout_buff <= dout_main2(0); bcid_c <= unsigned(dout_buff(58 downto 47)); art_c <= dout_buff(46 downto 41); state_arb <= ST_GET_BCID_ART;
                    when 11     => dout_buff <= dout_main2(1); bcid_c <= unsigned(dout_buff(58 downto 47)); art_c <= dout_buff(46 downto 41); state_arb <= ST_GET_BCID_ART;
                    when 12     => dout_buff <= dout_main2(2); bcid_c <= unsigned(dout_buff(58 downto 47)); art_c <= dout_buff(46 downto 41); state_arb <= ST_GET_BCID_ART;
                    when 13     => dout_buff <= dout_main2(3); bcid_c <= unsigned(dout_buff(58 downto 47)); art_c <= dout_buff(46 downto 41); state_arb <= ST_GET_BCID_ART;
                    when 14     => dout_buff <= dout_main2(4); bcid_c <= unsigned(dout_buff(58 downto 47)); art_c <= dout_buff(46 downto 41); state_arb <= ST_GET_BCID_ART;
                    when 15     => dout_buff <= (others =>'0'); state_arb <= ST_DONE; -- done, scanned all
                    when others => dout_buff <= (others =>'0'); state_arb <= ST_SCAN_PRIOR; -- ?
                    end case;
                else
                    scan_prior_cnt  <= scan_prior_cnt + 1;
                    state_arb       <= ST_SCAN_PRIOR;
                end if;

            -- get first data
            when ST_GET_BCID_ART =>
                    case to_integer(scan_prior_cnt) is
                    when 0      => bcid_a <= unsigned(dout_buff(58 downto 47)); art_a <= dout_buff(46 downto 41); state_arb <= ST_SCAN_BUFF;
                    when 1      => bcid_a <= unsigned(dout_buff(58 downto 47)); art_a <= dout_buff(46 downto 41); state_arb <= ST_SCAN_BUFF;
                    when 2      => bcid_a <= unsigned(dout_buff(58 downto 47)); art_a <= dout_buff(46 downto 41); state_arb <= ST_SCAN_BUFF;
                    when 3      => bcid_a <= unsigned(dout_buff(58 downto 47)); art_a <= dout_buff(46 downto 41); state_arb <= ST_SCAN_BUFF;
                    when 4      => bcid_a <= unsigned(dout_buff(58 downto 47)); art_a <= dout_buff(46 downto 41); state_arb <= ST_SCAN_BUFF;
                    when 5      => bcid_b <= unsigned(dout_buff(58 downto 47)); art_b <= dout_buff(46 downto 41); state_arb <= ST_SCAN_BUFF;
                    when 6      => bcid_b <= unsigned(dout_buff(58 downto 47)); art_b <= dout_buff(46 downto 41); state_arb <= ST_SCAN_BUFF;
                    when 7      => bcid_b <= unsigned(dout_buff(58 downto 47)); art_b <= dout_buff(46 downto 41); state_arb <= ST_SCAN_BUFF;
                    when 8      => bcid_b <= unsigned(dout_buff(58 downto 47)); art_b <= dout_buff(46 downto 41); state_arb <= ST_SCAN_BUFF;
                    when 9      => bcid_b <= unsigned(dout_buff(58 downto 47)); art_b <= dout_buff(46 downto 41); state_arb <= ST_SCAN_BUFF;
                    when 10     => bcid_c <= unsigned(dout_buff(58 downto 47)); art_c <= dout_buff(46 downto 41); state_arb <= ST_SCAN_BUFF;
                    when 11     => bcid_c <= unsigned(dout_buff(58 downto 47)); art_c <= dout_buff(46 downto 41); state_arb <= ST_SCAN_BUFF;
                    when 12     => bcid_c <= unsigned(dout_buff(58 downto 47)); art_c <= dout_buff(46 downto 41); state_arb <= ST_SCAN_BUFF;
                    when 13     => bcid_c <= unsigned(dout_buff(58 downto 47)); art_c <= dout_buff(46 downto 41); state_arb <= ST_SCAN_BUFF;
                    when 14     => bcid_c <= unsigned(dout_buff(58 downto 47)); art_c <= dout_buff(46 downto 41); state_arb <= ST_SCAN_BUFF;
                    when 15     => state_arb <= ST_DONE; -- done, scanned all (shouldnt jump there from here though...)
                    when others => state_arb <= ST_SCAN_PRIOR; -- ?
                    end case;

            -- scan the dout of the main FIFO and write if a flag is detected.
            -- also manipulate the BCIDs
            when ST_SCAN_BUFF =>
                wrEn_FIFO_i   <= '0';

                if(dout_buff(to_integer(scan_buff_cnt)) = '1')then
                    state_arb   <= ST_WR_BUFF;

                    case to_integer(scan_prior_cnt) is
                    when 0      =>
                        bcid_b  <= bcid_a;
                        art_b   <= dout_buff(40 downto 35);
                        case to_integer(scan_buff_cnt) is
                        when 6      => bcid_c <= bcid_a;     art_c <= dout_buff(5 downto 0);
                        when 13     => bcid_c <= bcid_a + 1; art_c <= dout_buff(12 downto 7);
                        when 20     => bcid_c <= bcid_a + 2; art_c <= dout_buff(19 downto 14);
                        when 27     => bcid_c <= bcid_a + 3; art_c <= dout_buff(26 downto 21);
                        when 34     => bcid_c <= bcid_a + 4; art_c <= dout_buff(33 downto 28);
                        when others => null;
                        end case;
                    when 1      =>
                        bcid_b  <= bcid_a + 1;
                        art_b   <= dout_buff(40 downto 35);
                        case to_integer(scan_buff_cnt) is
                        when 6      => bcid_c <= bcid_a;     art_c <= dout_buff(5 downto 0);
                        when 13     => bcid_c <= bcid_a + 1; art_c <= dout_buff(12 downto 7);
                        when 20     => bcid_c <= bcid_a + 2; art_c <= dout_buff(19 downto 14);
                        when 27     => bcid_c <= bcid_a + 3; art_c <= dout_buff(26 downto 21);
                        when 34     => bcid_c <= bcid_a + 4; art_c <= dout_buff(33 downto 28);
                        when others => null;
                        end case;
                    when 2      =>
                        bcid_b  <= bcid_a + 2;
                        art_b   <= dout_buff(40 downto 35);
                        case to_integer(scan_buff_cnt) is
                        when 6      => bcid_c <= bcid_a;     art_c <= dout_buff(5 downto 0);
                        when 13     => bcid_c <= bcid_a + 1; art_c <= dout_buff(12 downto 7);
                        when 20     => bcid_c <= bcid_a + 2; art_c <= dout_buff(19 downto 14);
                        when 27     => bcid_c <= bcid_a + 3; art_c <= dout_buff(26 downto 21);
                        when 34     => bcid_c <= bcid_a + 4; art_c <= dout_buff(33 downto 28);
                        when others => null;
                        end case;
                    when 3      =>
                        bcid_b  <= bcid_a + 3;
                        art_b   <= dout_buff(40 downto 35);
                        case to_integer(scan_buff_cnt) is
                        when 6      => bcid_c <= bcid_a;     art_c <= dout_buff(5 downto 0);
                        when 13     => bcid_c <= bcid_a + 1; art_c <= dout_buff(12 downto 7);
                        when 20     => bcid_c <= bcid_a + 2; art_c <= dout_buff(19 downto 14);
                        when 27     => bcid_c <= bcid_a + 3; art_c <= dout_buff(26 downto 21);
                        when 34     => bcid_c <= bcid_a + 4; art_c <= dout_buff(33 downto 28);
                        when others => null;
                        end case;
                    when 4      =>
                        bcid_b  <= bcid_a + 4;
                        art_b   <= dout_buff(40 downto 35);
                        case to_integer(scan_buff_cnt) is
                        when 6      => bcid_c <= bcid_a;     art_c <= dout_buff(5 downto 0);
                        when 13     => bcid_c <= bcid_a + 1; art_c <= dout_buff(12 downto 7);
                        when 20     => bcid_c <= bcid_a + 2; art_c <= dout_buff(19 downto 14);
                        when 27     => bcid_c <= bcid_a + 3; art_c <= dout_buff(26 downto 21);
                        when 34     => bcid_c <= bcid_a + 4; art_c <= dout_buff(33 downto 28);
                        when others => null;
                        end case;
                    when 5      =>
                        bcid_c  <= bcid_b;
                        art_c   <= dout_buff(40 downto 35);
                        case to_integer(scan_buff_cnt) is
                        when 6      => bcid_a <= bcid_b;     art_a <= dout_buff(5 downto 0);
                        when 13     => bcid_a <= bcid_b + 1; art_a <= dout_buff(12 downto 7);
                        when 20     => bcid_a <= bcid_b + 2; art_a <= dout_buff(19 downto 14);
                        when 27     => bcid_a <= bcid_b + 3; art_a <= dout_buff(26 downto 21);
                        when 34     => bcid_a <= bcid_b + 4; art_a <= dout_buff(33 downto 28);
                        when others => null;
                        end case;
                    when 6      =>
                        bcid_c  <= bcid_b + 1;
                        art_c   <= dout_buff(40 downto 35);
                        case to_integer(scan_buff_cnt) is
                        when 6      => bcid_a <= bcid_b;     art_a <= dout_buff(5 downto 0);
                        when 13     => bcid_a <= bcid_b + 1; art_a <= dout_buff(12 downto 7);
                        when 20     => bcid_a <= bcid_b + 2; art_a <= dout_buff(19 downto 14);
                        when 27     => bcid_a <= bcid_b + 3; art_a <= dout_buff(26 downto 21);
                        when 34     => bcid_a <= bcid_b + 4; art_a <= dout_buff(33 downto 28);
                        when others => null;
                        end case;
                    when 7      =>
                        bcid_c  <= bcid_b + 2;
                        art_c   <= dout_buff(40 downto 35);
                        case to_integer(scan_buff_cnt) is
                        when 6      => bcid_a <= bcid_b;     art_a <= dout_buff(5 downto 0);
                        when 13     => bcid_a <= bcid_b + 1; art_a <= dout_buff(12 downto 7);
                        when 20     => bcid_a <= bcid_b + 2; art_a <= dout_buff(19 downto 14);
                        when 27     => bcid_a <= bcid_b + 3; art_a <= dout_buff(26 downto 21);
                        when 34     => bcid_a <= bcid_b + 4; art_a <= dout_buff(33 downto 28);
                        when others => null;
                        end case;
                    when 8      =>
                        bcid_c  <= bcid_b + 3;
                        art_c   <= dout_buff(40 downto 35);
                        case to_integer(scan_buff_cnt) is
                        when 6      => bcid_a <= bcid_b;     art_a <= dout_buff(5 downto 0);
                        when 13     => bcid_a <= bcid_b + 1; art_a <= dout_buff(12 downto 7);
                        when 20     => bcid_a <= bcid_b + 2; art_a <= dout_buff(19 downto 14);
                        when 27     => bcid_a <= bcid_b + 3; art_a <= dout_buff(26 downto 21);
                        when 34     => bcid_a <= bcid_b + 4; art_a <= dout_buff(33 downto 28);
                        when others => null;
                        end case;
                    when 9      =>
                        bcid_c  <= bcid_b + 4;
                        art_c   <= dout_buff(40 downto 35);
                        case to_integer(scan_buff_cnt) is
                        when 6      => bcid_a <= bcid_b;     art_a <= dout_buff(5 downto 0);
                        when 13     => bcid_a <= bcid_b + 1; art_a <= dout_buff(12 downto 7);
                        when 20     => bcid_a <= bcid_b + 2; art_a <= dout_buff(19 downto 14);
                        when 27     => bcid_a <= bcid_b + 3; art_a <= dout_buff(26 downto 21);
                        when 34     => bcid_a <= bcid_b + 4; art_a <= dout_buff(33 downto 28);
                        when others => null;
                        end case;
                    when 10     =>
                        bcid_a  <= bcid_c;
                        art_a   <= dout_buff(40 downto 35);
                        case to_integer(scan_buff_cnt) is
                        when 6      => bcid_b <= bcid_c;     art_b <= dout_buff(5 downto 0);
                        when 13     => bcid_b <= bcid_c + 1; art_b <= dout_buff(12 downto 7);
                        when 20     => bcid_b <= bcid_c + 2; art_b <= dout_buff(19 downto 14);
                        when 27     => bcid_b <= bcid_c + 3; art_b <= dout_buff(26 downto 21);
                        when 34     => bcid_b <= bcid_c + 4; art_b <= dout_buff(33 downto 28);
                        when others => null;
                        end case;
                    when 11     =>
                        bcid_a  <= bcid_c + 1;
                        art_a   <= dout_buff(40 downto 35);
                        case to_integer(scan_buff_cnt) is
                        when 6      => bcid_b <= bcid_c;     art_b <= dout_buff(5 downto 0);
                        when 13     => bcid_b <= bcid_c + 1; art_b <= dout_buff(12 downto 7);
                        when 20     => bcid_b <= bcid_c + 2; art_b <= dout_buff(19 downto 14);
                        when 27     => bcid_b <= bcid_c + 3; art_b <= dout_buff(26 downto 21);
                        when 34     => bcid_b <= bcid_c + 4; art_b <= dout_buff(33 downto 28);
                        when others => null;
                        end case;
                    when 12     =>
                        bcid_a  <= bcid_c + 2;
                        art_a   <= dout_buff(40 downto 35);
                        case to_integer(scan_buff_cnt) is
                        when 6      => bcid_b <= bcid_c;     art_b <= dout_buff(5 downto 0);
                        when 13     => bcid_b <= bcid_c + 1; art_b <= dout_buff(12 downto 7);
                        when 20     => bcid_b <= bcid_c + 2; art_b <= dout_buff(19 downto 14);
                        when 27     => bcid_b <= bcid_c + 3; art_b <= dout_buff(26 downto 21);
                        when 34     => bcid_b <= bcid_c + 4; art_b <= dout_buff(33 downto 28);
                        when others => null;
                        end case;
                    when 13     =>
                        bcid_a  <= bcid_c + 3;
                        art_a   <= dout_buff(40 downto 35);
                        case to_integer(scan_buff_cnt) is
                        when 6      => bcid_b <= bcid_c;     art_b <= dout_buff(5 downto 0);
                        when 13     => bcid_b <= bcid_c + 1; art_b <= dout_buff(12 downto 7);
                        when 20     => bcid_b <= bcid_c + 2; art_b <= dout_buff(19 downto 14);
                        when 27     => bcid_b <= bcid_c + 3; art_b <= dout_buff(26 downto 21);
                        when 34     => bcid_b <= bcid_c + 4; art_b <= dout_buff(33 downto 28);
                        when others => null;
                        end case;
                    when 14     =>
                        bcid_a  <= bcid_c + 4;
                        art_a   <= dout_buff(40 downto 35);
                        case to_integer(scan_buff_cnt) is
                        when 6      => bcid_b <= bcid_c;     art_b <= dout_buff(5 downto 0);
                        when 13     => bcid_b <= bcid_c + 1; art_b <= dout_buff(12 downto 7);
                        when 20     => bcid_b <= bcid_c + 2; art_b <= dout_buff(19 downto 14);
                        when 27     => bcid_b <= bcid_c + 3; art_b <= dout_buff(26 downto 21);
                        when 34     => bcid_b <= bcid_c + 4; art_b <= dout_buff(33 downto 28);
                        when others => null;
                        end case;
                    when 15     => state_arb <= ST_DONE; -- ? error
                    when others =>
                    end case;
                else
                    case to_integer(scan_buff_cnt) is
                    when 6      => scan_buff_cnt <= to_unsigned(13, 6); state_arb <= ST_SCAN_BUFF;
                    when 13     => scan_buff_cnt <= to_unsigned(20, 6); state_arb <= ST_SCAN_BUFF; 
                    when 20     => scan_buff_cnt <= to_unsigned(27, 6); state_arb <= ST_SCAN_BUFF;
                    when 27     => scan_buff_cnt <= to_unsigned(34, 6); state_arb <= ST_SCAN_BUFF;
                    when 34     => scan_buff_cnt <= to_unsigned(6, 6);  state_arb <= ST_SCAN_PRIOR; scan_prior_cnt <= scan_prior_cnt + 1; -- done, other dout
                    when others => scan_buff_cnt <= to_unsigned(6, 6);  state_arb <= ST_SCAN_PRIOR;
                    end case;
                end if;

            -- write to FIFO, and resume scanning
            when ST_WR_BUFF =>
                wrEn_FIFO_i <= '1';
                case to_integer(scan_buff_cnt) is
                when 6      => scan_buff_cnt <= to_unsigned(13, 6); state_arb <= ST_SCAN_BUFF;
                when 13     => scan_buff_cnt <= to_unsigned(20, 6); state_arb <= ST_SCAN_BUFF; 
                when 20     => scan_buff_cnt <= to_unsigned(27, 6); state_arb <= ST_SCAN_BUFF;
                when 27     => scan_buff_cnt <= to_unsigned(34, 6); state_arb <= ST_SCAN_BUFF;
                when 34     => scan_buff_cnt <= to_unsigned(6, 6);  state_arb <= ST_SCAN_PRIOR; scan_prior_cnt <= scan_prior_cnt + 1; -- done, other dout
                when others => scan_buff_cnt <= to_unsigned(6, 6);  state_arb <= ST_SCAN_PRIOR;
                end case;

            -- done reading event
            when ST_DONE =>
                arb_done <= '1';

            when others => state_arb <= ST_SCAN_PRIOR;
            end case;
        end if;
    end if;
end process;



    -- if a wr_en is asserted, then a word is being written in the prioritizer
    wrEn_fifoPrior_i <= wrEn_fifo2(4) or wrEn_fifo2(3) or wrEn_fifo2(2) or wrEn_fifo2(1) or wrEn_fifo2(0) or
                        wrEn_fifo1(4) or wrEn_fifo1(3) or wrEn_fifo1(2) or wrEn_fifo1(1) or wrEn_fifo1(0) or
                        wrEn_fifo0(4) or wrEn_fifo0(3) or wrEn_fifo0(2) or wrEn_fifo0(1) or wrEn_fifo0(0);
      
    din_fifoPrior_i <= wrEn_fifo2 & wrEn_fifo1 & wrEn_fifo0;

    -- one pipeline stage
pipe_proc: process(clk_art)
begin
    if(rising_edge(clk_art))then
        dout_FIFO     <= std_logic_vector(bcid_a) & art_a & std_logic_vector(bcid_b) & art_b & std_logic_vector(bcid_c) & art_c;
        wrEn_FIFO     <= wrEn_FIFO_i;
    end if;
end process;
    

fifo_prior_inst: fifo_prioritizer
    port map(
        rst         => rst_i,
        wr_clk      => clk_ckbc,
        rd_clk      => clk_art,
        din         => din_fifoPrior,
        wr_en       => wrEn_fifoPrior,
        rd_en       => rdEn_fifoPrior,
        dout        => dout_fifoPrior,
        full        => full_prior_i,
        empty       => empty_prior,
        wr_rst_busy => wr_busy_prior,
        rd_rst_busy => rd_busy_prior
    );

-- generate main FIFOs
gen_FIFOmain: for I in 0 to 4 generate

fifo_main0_inst: fifo_main
    port map(
        rst         => rst_i,
        wr_clk      => clk_ckbc,
        rd_clk      => clk_art,
        din         => din_fifo0(I),
        wr_en       => wrEn_fifo0(I),
        rd_en       => rd_en_main0(I),
        dout        => dout_main0(I),
        full        => full_main0(I),
        empty       => empty_main0(I),
        wr_rst_busy => wr_busy_main0(I),
        rd_rst_busy => rd_busy_main0(I)
    );

fifo_main1_inst: fifo_main
    port map(
        rst         => rst_i,
        wr_clk      => clk_ckbc,
        rd_clk      => clk_art,
        din         => din_fifo1(I),
        wr_en       => wrEn_fifo1(I),
        rd_en       => rd_en_main1(I),
        dout        => dout_main1(I),
        full        => full_main1(I),
        empty       => empty_main1(I),
        wr_rst_busy => wr_busy_main1(I),
        rd_rst_busy => rd_busy_main1(I)
    );

fifo_main2_inst: fifo_main
    port map(
        rst         => rst_i,
        wr_clk      => clk_ckbc,
        rd_clk      => clk_art,
        din         => din_fifo2(I),
        wr_en       => wrEn_fifo2(I),
        rd_en       => rd_en_main2(I),
        dout        => dout_main2(I),
        full        => full_main2(I),
        empty       => empty_main2(I),
        wr_rst_busy => wr_busy_main2(I),
        rd_rst_busy => rd_busy_main2(I)
    );

end generate gen_FIFOmain;

    rst_i               <= rst or fifo_init;
    dout_fifoPrior_i    <= '1' & dout_fifoPrior;
    full_prior          <= full_prior_i;

end RTL;
