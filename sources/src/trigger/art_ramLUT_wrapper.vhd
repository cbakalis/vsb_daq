-----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2017 Christos Bakalis
--
--    This file is part of NTUA_BNL_VSB_firmware.
--
--    NTUA_BNL_VSB_firmware is free software: you can redistribute it 
--    and/or modify it under the terms of the GNU General Public License 
--    as published by the Free Software Foundation, either version 3 of
--    the License, or (at your option) any later version.
--
--    NTUA_BNL_VSB_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA_BNL_VSB_firmware. If not, see <http://www.gnu.org/licenses/>.
-- 
-- Create Date: 04.02.2018 19:36:25
-- Design Name: ART RAM-LUT FIFO Wrapper
-- Module Name: art_ramLUT_wrapper - RTL
-- Project Name: VSB DAQ - NTUA/BNL
-- Target Devices: Virtex7 xc7vx690tffg1761-2
-- Tool Versions: Vivado 2017.3
-- Description: Wrapper for the candidate FIFO and RAM.
--
-- Dependencies: NTUA_BNL_VSB_firmware, art_package
-- 
-- Changelog:
-- 
-----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use WORK.ART_PACKAGE.all;
use IEEE.NUMERIC_STD.ALL;

entity art_ramLUT_wrapper is
    port(
        ------------------------------------
        ------- General Interface ----------
        clk_ckbc        : in  std_logic; -- CKBC
        clk_art         : in  std_logic; -- ART clock
        clk_art2        : in  std_logic; -- ART clockx2
        clk_eth         : in  std_logic; -- Ethernet I/F clock
        rst             : in  std_logic;
        fifo_init       : in  std_logic;
        full_FIFO     : out std_logic;
        art_addr_udp    : out std_logic_vector(17 downto 0);
        -------------------------------------
        --------- ART-FIFOs Interface -------
        wrEn_FIFO     : in  std_logic;
        din_FIFO      : in  std_logic_vector(53 downto 0);
        -------------------------------------
        ---------- UDP Interface -----------
        wrEn_LUTRAM      : in  std_logic;
        din_LUTRAM       : in  std_logic;
        wrAddr_LUTRAM    : in  std_logic_vector(17 downto 0);
        -------------------------------------
        -------- L1A Buff Interface ---------
        l1a_buff_dout   : out l1aBuff_data;
        l1a_buff_wrEn   : out std_logic_vector(3 downto 0)
    );
end art_ramLUT_wrapper;

architecture RTL of art_ramLUT_wrapper is

-- FIFO that stores art address candidates and their bcids
component fifo_ramLUT
    port(
        clk     : in  std_logic;
        srst    : in  std_logic;
        din     : in  std_logic_vector(53 downto 0);
        wr_en   : in  std_logic;
        rd_en   : in  std_logic;
        dout    : out std_logic_vector(53 downto 0);
        full    : out std_logic;
        empty   : out std_logic
    );
end component;

-- RAM that stores event candidates as addresses
component ramLUT
    port(
        clka        : in  std_logic;
        ena         : in  std_logic;
        wea         : in  std_logic_vector(0 downto 0);
        addra       : in  std_logic_vector(17 downto 0);
        dina        : in  std_logic_vector(0 downto 0);
        clkb        : in  std_logic;
        rstb        : in  std_logic;
        enb         : in  std_logic;
        addrb       : in  std_logic_vector(17 downto 0);
        doutb       : out std_logic_vector(0 downto 0);
        rsta_busy   : out std_logic;
        rstb_busy   : out std_logic
    );
end component;

    -- FIFO signals
    signal rst_i            : std_logic := '0';
    signal rdEn_FIFO      : std_logic := '0';
    signal dout_FIFO      : std_logic_vector(53 downto 0) := (others => '0');
    signal empty_FIFO     : std_logic := '0';

    -- RAM signals
    signal rstN_i           : std_logic := '0';
    signal rsta_busy        : std_logic := '0';
    signal rstb_busy        : std_logic := '0';
    signal din_LUTRAM_vec    : std_logic_vector(0 downto 0)  := (others => '0');
    signal dout_LUTRAM_vec   : std_logic_vector(0 downto 0)  := (others => '0');
    signal wrEn_LUTRAM_vec   : std_logic_vector(0 downto 0)  := (others => '0');
    signal rd_addr_LUTRAM    : std_logic_vector(17 downto 0) := (others => '0');
    signal rdEn_LUTRAM       : std_logic := '0';
    signal l1a_bit          : std_logic := '0';

    -- candidate evaulator FSM signals
    signal eval_wait_cnt    : unsigned(1 downto 0) := (others => '0');
    type stateType_ramLUT is (ST_IDLE, ST_RD_RAM, ST_CHK_RAM, ST_WAIT);
    signal state            : stateType_ramLUT := ST_IDLE;
    signal state_prv        : stateType_ramLUT := ST_IDLE;

    -- misc
    attribute FSM_ENCODING              : string;
    attribute FSM_ENCODING of state     : signal is "ONE_HOT";

    -- bcid evaluator process
    signal bcid_a           : unsigned(11 downto 0) := (others => '0');
    signal bcid_b           : unsigned(11 downto 0) := (others => '0');
    signal bcid_c           : unsigned(11 downto 0) := (others => '0');
    signal bcid_d           : unsigned(11 downto 0) := (others => '0');

begin

-- FSM that reads the FIFO, forwards the art addresses 
-- as RAM read addreses, and writes l1a if a coincidence is detected
FSM_candidate_eval_proc: process(clk_art)
begin
    if(rising_edge(clk_art))then
        if(rst = '1')then
            l1a_buff_wrEn   <= (others => '0');
            eval_wait_cnt   <= (others => '0');
            rdEn_LUTRAM      <= '0';
            rdEn_FIFO     <= '0';
            state           <= ST_IDLE;
        else
            case state is

            -- is the FIFO empty?
            when ST_IDLE =>
                state_prv       <= ST_IDLE;
                l1a_buff_wrEn   <= (others => '0');
                if(empty_FIFO = '0')then
                    rdEn_FIFO <= '1';
                    state       <= ST_WAIT; -- to ST_RD_RAM 
                else
                    rdEn_FIFO <= '0';
                    state       <= ST_IDLE;
                end if;

            -- read the RAM
            when ST_RD_RAM =>
                state_prv       <= ST_RD_RAM;
                eval_wait_cnt   <= (others => '0');
                rdEn_LUTRAM      <= '1';
                state           <= ST_WAIT; -- to ST_CHK_RAM
                
            -- is it a real track?
            when ST_CHK_RAM =>
                eval_wait_cnt   <= (others => '0');
                if(l1a_bit = '1')then
                    l1a_buff_wrEn <= "1111";    
                else
                    l1a_buff_wrEn <= (others => '0');
                end if;
                state <= ST_IDLE;

            -- generic state that waits
            when ST_WAIT =>
                eval_wait_cnt   <= eval_wait_cnt + 1;
                rdEn_LUTRAM      <= '0';
                rdEn_FIFO     <= '0';
                if(eval_wait_cnt = "11")then
                    case state_prv is
                    when ST_IDLE    => state <= ST_RD_RAM;
                    when ST_RD_RAM  => state <= ST_CHK_RAM;
                    when others     => state <= ST_WAIT;
                    end case;
                else
                    state <= ST_WAIT;
                end if;

            when others => state <= ST_IDLE;
            end case;
        end if;
    end if;
end process;

-- process that approximates a bcid for the 4th board
bcid_eval_proc: process(bcid_a, bcid_b, bcid_c)
begin
    if(bcid_a >= bcid_b and bcid_b >= bcid_c)then
        bcid_d <= bcid_b;

    elsif(bcid_a >= bcid_c and bcid_c >= bcid_b)then
        bcid_d <= bcid_c;

    elsif(bcid_b >= bcid_a and bcid_a >= bcid_c)then
        bcid_d <= bcid_a;

    elsif(bcid_b >= bcid_c and bcid_c >= bcid_a)then
        bcid_d <= bcid_c;

    elsif(bcid_c >= bcid_a and bcid_a >= bcid_b)then
        bcid_d <= bcid_a;

    elsif(bcid_c >= bcid_b and bcid_b >= bcid_a)then
        bcid_d <= bcid_b;

    else
        bcid_d <= bcid_a;
    end if;
end process;

-- one pipeline stage
pipe_ramLUT_out: process(clk_art)
begin
    if(rising_edge(clk_art))then
        --l1a_buff_dout(0)    <= dout_FIFO(53 downto 42);
        --l1a_buff_dout(1)    <= dout_FIFO(35 downto 24);
        --l1a_buff_dout(2)    <= dout_FIFO(17 downto 6);
        l1a_buff_dout(3)    <= std_logic_vector(bcid_d);
    end if;
end process;


fifo_ramLUT_inst: fifo_ramLUT
    port map(
        clk     => clk_art,
        srst    => rst_i,
        din     => din_FIFO,
        wr_en   => wrEn_FIFO,
        rd_en   => rdEn_FIFO,
        dout    => dout_FIFO,
        full    => full_FIFO,
        empty   => empty_FIFO
    );

ramLUT_inst: ramLUT
    port map(
        clka        => clk_eth,
        ena         => rstN_i,
        wea         => wrEn_LUTRAM_vec,
        addra       => wrAddr_LUTRAM,
        dina        => din_LUTRAM_vec,
        clkb        => clk_art,
        rstb        => rst_i,
        enb         => rdEn_LUTRAM,
        addrb       => rd_addr_LUTRAM,
        doutb       => dout_LUTRAM_vec,
        rsta_busy   => rsta_busy,
        rstb_busy   => rstb_busy
    );

    rd_addr_LUTRAM       <= dout_FIFO(41 downto 36) & dout_FIFO(23 downto 18) & dout_FIFO(5 downto 0);
                        --          art_a                   art_b                       art_c

    bcid_a              <= unsigned(dout_FIFO(53 downto 42));
    bcid_b              <= unsigned(dout_FIFO(35 downto 24));
    bcid_c              <= unsigned(dout_FIFO(17 downto 6));

    l1a_buff_dout(0)    <= dout_FIFO(53 downto 42);
    l1a_buff_dout(1)    <= dout_FIFO(35 downto 24);
    l1a_buff_dout(2)    <= dout_FIFO(17 downto 6);

    din_LUTRAM_vec(0)    <= din_LUTRAM;
    wrEn_LUTRAM_vec(0)   <= wrEn_LUTRAM;
    l1a_bit             <= dout_LUTRAM_vec(0);
    rst_i               <= rst or fifo_init;
    rstN_i              <= not fifo_init; -- RAM only gets ini
    art_addr_udp        <= rd_addr_LUTRAM;

end RTL;
