----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2017 Christos Bakalis
--
--    This file is part of NTUA_BNL_VSB_firmware.
--
--    NTUA_BNL_VSB_firmware is free software: you can redistribute it 
--    and/or modify it under the terms of the GNU General Public License 
--    as published by the Free Software Foundation, either version 3 of
--    the License, or (at your option) any later version.
--
--    NTUA_BNL_VSB_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA_BNL_VSB_firmware. If not, see <http://www.gnu.org/licenses/>.
-- 
-- Create Date: 24.01.2018 15:39:38
-- Design Name: ART Shift Registers
-- Module Name: art_sregs - RTL
-- ProIect Name: VSB - NTUA/BNL
-- Target Devices: Virtex7 xc7vx690tffg1761-2
-- Tool Versions: Vivado 2017.3
-- Description: Module that feeds the ART addresses into three shift registers
-- and asserts the wr_en signal to FIFOs that buffer potential event candidates.
--
-- Dependencies: NTUA_BNL_VSB_firmware
--
-- Changelog:
-- 
----------------------------------------------------------------------------------
library IEEE;
library UNISIM;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use UNISIM.VComponents.all;
use WORK.ART_PACKAGE.all;

entity art_sregs is
    port(
        ------------------------------------
        ------- General Interface ----------
        clk_ckbc        : in  std_logic; -- CKBC
        clk_art         : in  std_logic; -- ART clock
        bcid_glbl       : in  std_logic_vector(11 downto 0); -- BCID counter
        rst             : in  std_logic;
        ------------------------------------
        -------- ART Des Interface ---------
        art_word_in0    : in  std_logic_vector(6 downto 0);  -- to shift register
        art_word_out0   : out std_logic_vector(6 downto 0);  -- from shift register
        
        art_word_in1    : in  std_logic_vector(6 downto 0);  -- to shift register
        art_word_out1   : out std_logic_vector(6 downto 0);  -- from shift register
        
        art_word_in2    : in  std_logic_vector(6 downto 0);  -- to shift register
        art_word_out2   : out std_logic_vector(6 downto 0);  -- from shift register
        -------------------------------------
        -------- FIFO Net Interface ---------
        wrEn_fifo0      : out std_logic_vector(4 downto 0);
        wrEn_fifo1      : out std_logic_vector(4 downto 0);
        wrEn_fifo2      : out std_logic_vector(4 downto 0);
        --
        din_fifo0       : out fifoData_main;
        din_fifo1       : out fifoData_main;
        din_fifo2       : out fifoData_main
    );
end art_sregs;

architecture RTL of art_sregs is

    -- shift registers
    signal bcid_sreg    : std_logic_vector(59 downto 0)  := (others => '0');

    signal art_sreg0    : std_logic_vector(34 downto 0)  := (others => '0');
    signal art_sreg1    : std_logic_vector(34 downto 0)  := (others => '0');
    signal art_sreg2    : std_logic_vector(34 downto 0)  := (others => '0');

    -- valid flags from shift registers
    signal flag_sreg0   : std_logic_vector(4 downto 0) := (others => '0');
    signal flag_sreg1   : std_logic_vector(4 downto 0) := (others => '0');
    signal flag_sreg2   : std_logic_vector(4 downto 0) := (others => '0');

    signal flag_valid0  : std_logic := '0';
    signal flag_valid1  : std_logic := '0';
    signal flag_valid2  : std_logic := '0';

begin

-- bcid shift register
bcid_sreg_proc: process(clk_ckbc)
begin
    if(rising_edge(clk_ckbc))then
        bcid_sreg <= bcid_glbl & bcid_sreg(59 downto 12);
    end if;
end process;

-- art+flag shift register for front-end boards
art_sreg_proc: process(clk_ckbc)
begin
    if(rising_edge(clk_ckbc))then
        art_sreg0 <= art_word_in0 & art_sreg0(34 downto 7);
        art_sreg1 <= art_word_in1 & art_sreg1(34 downto 7);
        art_sreg2 <= art_word_in2 & art_sreg2(34 downto 7);
    end if;
end process;

-- process that checks for valid flags in the sregs and asserts the wr_en signal to the FIFOs
wrEn_asserter_proc: process(clk_ckbc)
begin
    if(rising_edge(clk_ckbc))then
        if(art_sreg0(6) = '1' and flag_valid2 = '1')then -- flag detected, last time we see this word.
            wrEn_fifo0 <= flag_sreg1; -- from 0 to 1 (check 2)
        else
            wrEn_fifo0 <= (others => '0');
        end if;

        if(art_sreg1(6) = '1' and flag_valid0 = '1')then -- flag detected, last time we see this word.
           -- wrEn_fifo1 <= flag_sreg2; -- from 1 to 2 (check 0 afterwards)
            wrEn_fifo1(4) <= flag_sreg2(4);
            wrEn_fifo1(3) <= flag_sreg2(3);
            wrEn_fifo1(2) <= flag_sreg2(2);
            wrEn_fifo1(1) <= flag_sreg2(1);
            wrEn_fifo1(0) <= '0';
        else
            wrEn_fifo1 <= (others => '0');
        end if;

        if(art_sreg2(6) = '1' and flag_valid1 = '1')then -- flag detected, last time we see this word.
            --wrEn_fifo2 <= flag_sreg0; -- from 2 to 0 (check 1 afterwards)
            wrEn_fifo2(4) <= flag_sreg0(4);
            wrEn_fifo2(3) <= flag_sreg0(3);
            wrEn_fifo2(2) <= flag_sreg0(2);
            wrEn_fifo2(1) <= flag_sreg0(1);
            wrEn_fifo2(0) <= '0';
        else
            wrEn_fifo2 <= (others => '0');
        end if;
    end if;
end process;

    art_word_out0    <= art_sreg0(34 downto 28); -- grab the 7 most significant bits
    art_word_out1    <= art_sreg1(34 downto 28);
    art_word_out2    <= art_sreg2(34 downto 28);

    flag_sreg0       <= art_sreg0(34) & art_sreg0(27) & art_sreg0(20) & art_sreg0(13) & art_sreg0(6);
    flag_sreg1       <= art_sreg1(34) & art_sreg1(27) & art_sreg1(20) & art_sreg1(13) & art_sreg1(6);
    flag_sreg2       <= art_sreg2(34) & art_sreg2(27) & art_sreg2(20) & art_sreg2(13) & art_sreg2(6);

    flag_valid0 <= flag_sreg0(4) or flag_sreg0(3) or flag_sreg0(2) or flag_sreg0(1) or flag_sreg0(0);
    flag_valid1 <= flag_sreg1(4) or flag_sreg1(3) or flag_sreg1(2) or flag_sreg1(1) or flag_sreg1(0);
    flag_valid2 <= flag_sreg2(4) or flag_sreg2(3) or flag_sreg2(2) or flag_sreg2(1) or flag_sreg2(0);


gen_dinFIFO: for I in 0 to 4 generate

-- pipeline the din to the FIFOs
din_pipe_proc: process(clk_ckbc)
begin
    if(rising_edge(clk_ckbc))then

        din_fifo0(I)     <= bcid_sreg(11 downto 0) & 
                            art_sreg0(5 downto 0) & 
                            art_sreg1((I*7 + 5) downto (I*7)) &
                            art_sreg2;

        din_fifo1(I)     <= bcid_sreg(11 downto 0) & 
                            art_sreg1(5 downto 0) & 
                            art_sreg2((I*7 + 5) downto (I*7)) &
                            art_sreg0;

        din_fifo2(I)     <= bcid_sreg(11 downto 0) & 
                            art_sreg2(5 downto 0) & 
                            art_sreg0((I*7 + 5) downto (I*7)) &
                            art_sreg1;
    end if;
end process;

end generate gen_dinFIFO;

end RTL;
