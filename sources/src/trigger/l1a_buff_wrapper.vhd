-----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2017 Christos Bakalis
--
--    This file is part of NTUA_BNL_VSB_firmware.
--
--    NTUA_BNL_VSB_firmware is free software: you can redistribute it 
--    and/or modify it under the terms of the GNU General Public License 
--    as published by the Free Software Foundation, either version 3 of
--    the License, or (at your option) any later version.
--
--    NTUA_BNL_VSB_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA_BNL_VSB_firmware. If not, see <http://www.gnu.org/licenses/>.
-- 
-- Create Date: 05.02.2018 19:27:42
-- Design Name: L1A Buffer Wrapper
-- Module Name: l1a_buff_wrapper - RTL
-- Project Name: VSB DAQ - NTUA/BNL
-- Target Devices: Virtex7 xc7vx690tffg1761-2
-- Tool Versions: Vivado 2017.3
-- Description: Wrapper for the L1A FIFOs. Contains logic that inhibits L1As,
-- and compares the FIFO outputs with the global bcid counter, taking into account
-- the bcid offset, which is the difference between the rollover and l0offset regs
-- of the VMM.
--
-- Dependencies: NTUA_BNL_VSB_firmware, art_package
-- 
-- Changelog:
-- 
-----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use WORK.ART_PACKAGE.all;

entity l1a_buff_wrapper is
    port(
        ------------------------------------
        ------- General Interface ----------
        clk_ckbc        : in  std_logic; -- CKBC
        clk_art         : in  std_logic; -- ART clock
        clk_art2        : in  std_logic; -- ART clockx2
        bcid_glbl       : in  std_logic_vector(11 downto 0); -- BCID counter
        bcid_offset     : in  std_logic_vector(11 downto 0); -- when to send L1A?
        rst             : in  std_logic;
        fifo_init       : in  std_logic;
        alarm           : out std_logic; -- if we are too slow, raise an alarm
        l1a_buff_full   : out std_logic_vector(3 downto 0);
        -------------------------------------
        ------ RAM/LUT Wrapper Interface ----
        l1a_buff_din    : in  l1aBuff_data;
        l1a_buff_wrEn   : in  std_logic_vector(3 downto 0);
        -------------------------------------
        l1a_vmm         : out std_logic_vector(3 downto 0)
    );
end l1a_buff_wrapper;

architecture RTL of l1a_buff_wrapper is

component fifo_l1a
    port(
        rst         : in  std_logic;
        wr_clk      : in  std_logic;
        rd_clk      : in  std_logic;
        din         : in  std_logic_vector(11 downto 0);
        wr_en       : in  std_logic;
        rd_en       : in  std_logic;
        dout        : out std_logic_vector(11 downto 0);
        full        : out std_logic;
        empty       : out std_logic;
        wr_rst_busy : out std_logic;
        rd_rst_busy : out std_logic
    );
end component;

    -- FIFO signals
    signal rst_i            : std_logic := '0';
    signal l1a_buff_empty   : std_logic_vector(3 downto 0) := (others => '0');
    signal l1a_buff_wrRst   : std_logic_vector(3 downto 0) := (others => '0');
    signal l1a_buff_rdRst   : std_logic_vector(3 downto 0) := (others => '0');
    signal l1a_buff_rdEn    : std_logic_vector(3 downto 0) := (others => '0');
    signal l1a_buff_dout    : l1aBuff_data;

    -- L1A asserter FSM
    signal alarm_vec        : std_logic_vector(3 downto 0)  := (others => '0');

    type wait_cnt_array     is array (0 to 3) of unsigned(1 downto 0);
    signal wait_cnt         : wait_cnt_array;

    type stateType_l1a is   (ST_IDLE, ST_WAIT_ASSERT, ST_ASSERT, ST_INHIBIT);
    type stateType_array    is array (0 to 3) of stateType_l1a;
    signal state            : stateType_array;

    -- misc
    attribute FSM_ENCODING          : string;
    attribute FSM_ENCODING of state : signal is "ONE_HOT";

begin

gen_l1a_FIFOs_FSMs: for I in 0 to 3 generate

-- four FSMs that read their respective FIFOs and assert the L1A
FSM_L1A_proc: process(clk_ckbc)
begin
    if(rising_edge(clk_ckbc))then
        if(rst_i = '1')then -- initialize everything. fifo init always comes first
            l1a_buff_rdEn(I)    <= '0';
            wait_cnt(I)         <= (others => '0');
            l1a_vmm(I)          <= '0';
            alarm_vec(I)        <= '0';
            state(I)            <= ST_IDLE;
        else
            case state(I) is

            -- is there any data in the FIFO?
            when ST_IDLE =>
                if(l1a_buff_empty(I) = '0')then
                    l1a_buff_rdEn(I)    <= '1';
                    state(I)            <= ST_WAIT_ASSERT;
                else
                    l1a_buff_rdEn(I)    <= '0';
                    state(I)            <= ST_IDLE;
                end if;

            -- wait a bit (output regs)
            when ST_WAIT_ASSERT =>
                l1a_buff_rdEn(I)    <= '0';
                if(wait_cnt(I) = "01")then
                    wait_cnt(I)     <= (others => '0');
                    state(I)        <= ST_ASSERT;
                else
                    wait_cnt(I)     <= wait_cnt(I) + 1;
                    state(I)        <= ST_WAIT_ASSERT;
                end if;

            -- assert the L1A
            when ST_ASSERT =>
                --     current BCID                  event bcid                 VMM conf offset (contains adjustment for L1A latency)
                if  (unsigned(bcid_glbl) + 1  = unsigned(l1a_buff_dout(I)) + unsigned(bcid_offset))then -- + 1 was added later given the simulation results
                    alarm_vec(I)    <= '0';
                    l1a_vmm(I)      <= '1';
                    state(I)        <= ST_INHIBIT;
                elsif(unsigned(bcid_glbl)     = unsigned(l1a_buff_dout(I)) + unsigned(bcid_offset) + 2)then -- a bit slow but still ok
                    alarm_vec(I)    <= '0';
                    l1a_vmm(I)      <= '1';
                    state(I)        <= ST_INHIBIT;
                elsif(unsigned(bcid_glbl)     = unsigned(l1a_buff_dout(I)) + unsigned(bcid_offset) + 1)then -- a bit slow but still ok
                    alarm_vec(I)    <= '0';
                    l1a_vmm(I)      <= '1';
                    state(I)        <= ST_INHIBIT;
                elsif(unsigned(bcid_glbl)     = unsigned(l1a_buff_dout(I)) + unsigned(bcid_offset))then  -- a bit slow but still ok
                    alarm_vec(I)    <= '0';
                    l1a_vmm(I)      <= '1';
                    state(I)        <= ST_INHIBIT;
                elsif(unsigned(bcid_glbl) + 1 > unsigned(l1a_buff_dout(I)) + unsigned(bcid_offset))then -- too slow! raise alarm
                    alarm_vec(I)    <= '1';
                    l1a_vmm(I)      <= '0';
                    state(I)        <= ST_INHIBIT;
                else
                    alarm_vec(I)    <= '0'; -- wait...we are ok
                    l1a_vmm(I)      <= '0';
                    state(I)        <= ST_ASSERT;
                end if;

            -- no successive L1As
            when ST_INHIBIT =>
                l1a_vmm(I)      <= '0';
                alarm_vec(I)    <= '0';
                wait_cnt(I)     <= wait_cnt(I) + 1;
                if(wait_cnt(I) = "11")then
                    state(I)    <= ST_IDLE;
                else     
                    state(I)    <= ST_INHIBIT;
                end if;

            when others => state(I) <= ST_IDLE;

            end case;
        end if;
    end if;
end process;

-- four FIFOs that contain the BCID that the event occured
fifo_l1a_inst: fifo_l1a
    port map(
        rst         => rst_i,
        wr_clk      => clk_art,
        rd_clk      => clk_ckbc,
        din         => l1a_buff_din(I),
        wr_en       => l1a_buff_wrEn(I),
        rd_en       => l1a_buff_rdEn(I),
        dout        => l1a_buff_dout(I),
        full        => l1a_buff_full(I),
        empty       => l1a_buff_empty(I),
        wr_rst_busy => l1a_buff_wrRst(I),
        rd_rst_busy => l1a_buff_rdRst(I)
    );

end generate gen_l1a_FIFOs_FSMs;

    rst_i           <= rst or fifo_init;
    alarm           <= alarm_vec(0) or alarm_vec(1) or alarm_vec(2) or alarm_vec(3);

end RTL;
