----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2017 Christos Bakalis
--
--    This file is part of NTUA_BNL_VSB_firmware.
--
--    NTUA_BNL_VSB_firmware is free software: you can redistribute it 
--    and/or modify it under the terms of the GNU General Public License 
--    as published by the Free Software Foundation, either version 3 of
--    the License, or (at your option) any later version.
--
--    NTUA_BNL_VSB_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA_BNL_VSB_firmware. If not, see <http://www.gnu.org/licenses/>.
-- 
-- Create Date: 01.06.2018 14:40:18
-- Design Name: Trigger Processor RAM filler
-- Module Name: tp_ramFiller - RTL
-- Project Name: VSB DAQ - NTUA/BNL
-- Target Devices: Virtex7 xc7vx690tffg1761-2
-- Tool Versions: Vivado 2017.3
-- Description: FSM that fills the TP's RAM with valid address combinations.
-- only vertical trajectories supported.
--
-- Dependencies: NTUA_BNL_VSB_firmware
-- 
-- Changelog:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity tp_ramFiller is
    generic(addr_offset : std_logic_vector(2 downto 0));
    port(
        ------------------------------------
        ------- General Interface ----------
        clk_eth         : in  std_logic;
        fifo_init       : in  std_logic;
        ------------------------------------
        ------- TP-RAM Interface -----------
        wrEn_LUTRAM      : out std_logic; -- wr_en to RAM-LUT
        din_LUTRAM       : out std_logic; -- din to RAM-LUT
        wrAddr_LUTRAM    : out std_logic_vector(17 downto 0) -- write-address
    );
end tp_ramFiller;

architecture RTL of tp_ramFiller is

    signal wait_cnt     : unsigned(5 downto 0)  := (others => '0');
    signal ch_cnt0      : unsigned(5 downto 0)  := (others => '0');
    signal ch_cnt1      : unsigned(5 downto 0)  := (others => '0');
    signal ch_cnt2      : unsigned(5 downto 0)  := (others => '0');
    signal idx_main     : unsigned(5 downto 0)  := (others => '0');
    signal cnt_zeros    : unsigned(17 downto 0) := (others => '0');
    signal sel_addr     : std_logic := '0';

    constant offset     : unsigned(5 downto 0) := "000" & unsigned(addr_offset); 


    type stateType is (ST_IDLE, ST_FILL_ZEROS, ST_SCAN_WINDOW, ST_INCR_MAIN, ST_REG_MAIN, ST_DONE);
    signal state : stateType := ST_IDLE;

    attribute FSM_ENCODING          : string;
    attribute FSM_ENCODING of state : signal is "ONE_HOT";

begin

fillerFSM_proc: process(clk_eth)
begin
    if(rising_edge(clk_eth))then
        if(fifo_init = '1')then
            wait_cnt    <= (others => '0');
            ch_cnt0     <= (others => '0');
            ch_cnt1     <= (others => '0');
            ch_cnt2     <= (others => '0');
            idx_main    <= (others => '0');
            sel_addr    <= '0';
            wrEn_LUTRAM  <= '0';
            din_LUTRAM   <= '0';
            state       <= ST_IDLE;
        else
            case state is

            -- wait a bit before filling
            when ST_IDLE =>
                wait_cnt    <= wait_cnt + 1;
                ch_cnt0     <= idx_main;
                ch_cnt1     <= idx_main;
                ch_cnt2     <= idx_main;
                if(wait_cnt = "111111")then
                    din_LUTRAM   <= '0';
                    wrEn_LUTRAM  <= '1';
                    state       <= ST_FILL_ZEROS;
                else
                    din_LUTRAM   <= '0';
                    wrEn_LUTRAM  <= '0';
                    state       <= ST_IDLE;
                end if;

            -- fill with zeros first (is it really needed?)
            when ST_FILL_ZEROS =>
                cnt_zeros <= cnt_zeros + 1;

                if(cnt_zeros = "111111" & "111111" & "111111")then
                    din_LUTRAM   <= '1';
                    wrEn_LUTRAM  <= '1';
                    sel_addr    <= '1';
                    state       <= ST_SCAN_WINDOW;
                else
                    din_LUTRAM   <= '0';
                    wrEn_LUTRAM  <= '1';
                    sel_addr    <= '0';
                    state       <= ST_FILL_ZEROS;
                end if;


            -- start scanning the window (a n x m matrix)
            when ST_SCAN_WINDOW =>
                din_LUTRAM   <= '1';
                wrEn_LUTRAM  <= '1';

                if(ch_cnt2 < idx_main + offset)then
                    ch_cnt0 <= ch_cnt0;
                    ch_cnt1 <= ch_cnt1;
                    ch_cnt2 <= ch_cnt2 + 1;
                    state   <= ST_SCAN_WINDOW;
                elsif(ch_cnt1 < idx_main + offset)then
                    ch_cnt0 <= ch_cnt0;
                    ch_cnt1 <= ch_cnt1 + 1;
                    ch_cnt2 <= idx_main;
                    state   <= ST_SCAN_WINDOW;
                elsif(ch_cnt0 < idx_main + offset)then
                    ch_cnt0 <= ch_cnt0 + 1;
                    ch_cnt1 <= idx_main;
                    ch_cnt2 <= idx_main;
                    state   <= ST_SCAN_WINDOW;
                else -- done scanning the window
                    state   <= ST_INCR_MAIN;
                end if;

            -- increment the main counter and scan a new window
            when ST_INCR_MAIN =>
                din_LUTRAM   <= '0';
                wrEn_LUTRAM  <= '0';
                idx_main    <= idx_main + 1;

                if(idx_main >= "111111" - offset)then
                    state <= ST_DONE;
                else
                    state <= ST_REG_MAIN;
                end if;

            -- pass the init value to the counters
            when ST_REG_MAIN =>
                ch_cnt0     <= idx_main;
                ch_cnt1     <= idx_main;
                ch_cnt2     <= idx_main;
                din_LUTRAM   <= '1';
                wrEn_LUTRAM  <= '1';
                state       <= ST_SCAN_WINDOW;

            -- done. stay here
            when ST_DONE =>
                ch_cnt0     <= (others => '0');
                ch_cnt1     <= (others => '0');
                ch_cnt2     <= (others => '0');
                din_LUTRAM   <= '0';
                wrEn_LUTRAM  <= '0';

            when others => state <= ST_IDLE;
               
            end case;
        end if;
    end if;
end process;

sel_addr_proc: process(sel_addr, cnt_zeros, ch_cnt0, ch_cnt1, ch_cnt2)
begin
    case sel_addr is
    when '0'    => wrAddr_LUTRAM <= std_logic_vector(cnt_zeros);
    when '1'    => wrAddr_LUTRAM <= std_logic_vector(ch_cnt0) & std_logic_vector(ch_cnt1) & std_logic_vector(ch_cnt2);
    when others => wrAddr_LUTRAM <= (others => '0');
    end case;
end process;
    
end RTL;
