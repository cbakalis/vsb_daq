-----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2017 Christos Bakalis
--
--    This file is part of NTUA_BNL_VSB_firmware.
--
--    NTUA_BNL_VSB_firmware is free software: you can redistribute it 
--    and/or modify it under the terms of the GNU General Public License 
--    as published by the Free Software Foundation, either version 3 of
--    the License, or (at your option) any later version.
--
--    NTUA_BNL_VSB_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA_BNL_VSB_firmware. If not, see <http://www.gnu.org/licenses/>.
-- 
-- Create Date: 06.02.2018 16:02:52
-- Design Name: VSB Trigger Processing Core Supervisor
-- Module Name: tp_supervisor - RTL
-- Project Name: VSB DAQ - NTUA/BNL
-- Target Devices: Virtex7 xc7vx690tffg1761-2
-- Tool Versions: Vivado 2017.3
-- Description: Supervising module of the trigger processing core.
--
-- Dependencies: NTUA_BNL_VSB_firmware, art_package
-- 
-- Changelog:
-- 
-----------------------------------------------------------------------------------
library UNISIM;
library IEEE;
use UNISIM.VComponents.all;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity tp_supervisor is
    port(
        ------------------------------------
        ------- General Interface ----------
        clk_ckbc        : in  std_logic; -- BC clock (40)
        clk_art         : in  std_logic; -- ART clock (160)
        clk_art2        : in  std_logic; -- ART clock x2 (320)
        rst             : in  std_logic; -- global reset
        ------------------------------------
        ----------- TP Interface -----------
        alarm           : in  std_logic;
        full_prior      : in  std_logic;
        full_main0      : in  std_logic_vector(4 downto 0);
        full_main1      : in  std_logic_vector(4 downto 0);
        full_main2      : in  std_logic_vector(4 downto 0);
        l1a_buff_full   : in  std_logic_vector(3 downto 0);
        full_FIFO     : in  std_logic;
        --
        alarm_o         : out std_logic;
        rst_tp          : out std_logic;
        buff_overflow   : out std_logic
    );
end tp_supervisor;

architecture RTL of tp_supervisor is

    signal cnt_0d8s_buf     : unsigned(24 downto 0) := (others => '0');
    signal flag_overflow    : std_logic := '0';
    signal buff_overflow_i  : std_logic := '0';

    signal cnt_0d8s_alarm   : unsigned(24 downto 0) := (others => '0');
    signal flag_alarm       : std_logic := '0';

    signal cnt_rst          : unsigned(3 downto 0) := (others => '0');
    signal flag_rst         : std_logic := '0';
    signal rst_i            : std_logic := '0';

begin

-- warning LED process for overflow
LED_overflow_proc: process(clk_art)
begin
    if(rising_edge(clk_art))then
        if(rst = '1')then
            flag_overflow   <= '0';
            cnt_0d8s_buf    <= (others => '0');
        else
            if(flag_overflow = '1')then
                cnt_0d8s_buf <= cnt_0d8s_buf + 1;
                if(cnt_0d8s_buf = "1111111111111111111111111")then
                    flag_overflow <= '0';
                else
                    flag_overflow <= '1';
                end if;
            elsif(buff_overflow_i = '1')then
                flag_overflow <= '1';
            else
                flag_overflow <= '0';
            end if;
        end if;
    end if;
end process;

-- warning LED process for alarm
LED_alarm_proc: process(clk_art)
begin
    if(rising_edge(clk_art))then
        if(rst = '1')then
            flag_alarm      <= '0';
            cnt_0d8s_alarm  <= (others => '0');
        else
            if(flag_alarm = '1')then
                cnt_0d8s_alarm <= cnt_0d8s_alarm + 1;
                if(cnt_0d8s_alarm = "1111111111111111111111111")then
                    flag_alarm <= '0';
                else
                    flag_alarm <= '1';
                end if;
            elsif(alarm = '1')then
                flag_alarm <= '1';
            else
                flag_alarm <= '0';
            end if;
        end if;
    end if;
end process;

-- reset process
rst_all_proc: process(clk_art)
begin
    if(rising_edge(clk_art))then
        if(flag_rst = '1')then
            cnt_rst <= cnt_rst + 1;
            if(cnt_rst = "1111")then
                flag_rst <= '0';
            else
                flag_rst <= '1';
            end if;    
        elsif(alarm = '1' or buff_overflow_i = '1')then
            flag_rst    <= '1';
        else
            flag_rst    <= '0';
            cnt_rst     <= (others => '0');
        end if;
    end if;
end process;


    buff_overflow_i <= full_FIFO   or l1a_buff_full(3) or l1a_buff_full(2) or l1a_buff_full(1) or l1a_buff_full(0) or
                       full_main0(4) or full_main0(3)    or full_main0(2)    or full_main0(1)    or full_main0(0) or
                       full_main1(4) or full_main1(3)    or full_main1(2)    or full_main1(1)    or full_main1(0) or
                       full_main2(4) or full_main2(3)    or full_main2(2)    or full_main2(1)    or full_main2(0) or
                       full_prior;
                       

   buff_overflow    <= flag_overflow;
   alarm_o          <= flag_alarm;
   rst_i            <= flag_rst or rst;

BUFG_rst_TP: BUFG port map(O => rst_tp, I => rst_i);

end RTL;
