-----------------------------------------------------------------------------------
-- Company: NTU Athens - BNL
-- Engineer: Christos Bakalis (christos.bakalis@cern.ch) 
-- 
-- Copyright Notice/Copying Permission:
--    Copyright 2017 Christos Bakalis
--
--    This file is part of NTUA_BNL_VSB_firmware.
--
--    NTUA_BNL_VSB_firmware is free software: you can redistribute it 
--    and/or modify it under the terms of the GNU General Public License 
--    as published by the Free Software Foundation, either version 3 of
--    the License, or (at your option) any later version.
--
--    NTUA_BNL_VSB_firmware is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with NTUA_BNL_VSB_firmware. If not, see <http://www.gnu.org/licenses/>.
-- 
-- Create Date: 06.02.2018 14:43:11
-- Design Name: VSB Trigger Processing Core
-- Module Name: trigger_processing_core - RTL
-- Project Name: VSB DAQ - NTUA/BNL
-- Target Devices: Virtex7 xc7vx690tffg1761-2
-- Tool Versions: Vivado 2017.3
-- Description: Trigger processing core of the VSB firmware. Deserializes the
-- ART data from the VMMs, combines the ART addresses into FIFOs, checks if an event
-- is valid and asserts L1A to the front-ends if so.
--
-- Dependencies: NTUA_BNL_VSB_firmware, art_package
-- 
-- Changelog:
-- 
-----------------------------------------------------------------------------------
library IEEE;
library UNISIM;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use UNISIM.VComponents.all;
use WORK.ART_PACKAGE.all;

entity trigger_processing_core is
    generic(debug_trigProc : std_logic := '0');
    port(
        ------------------------------------
        ------- General Interface ----------
        clk_ckbc        : in  std_logic; -- BC clock (40)
        clk_art         : in  std_logic; -- ART clock (160)
        clk_art2        : in  std_logic; -- ART clock x2 (320)
        clk_eth         : in  std_logic; -- userclk2 from transceiver
        rst             : in  std_logic; -- global reset
        fifo_init       : in  std_logic; -- initializing reset for fifos
        bcid_glbl       : in  std_logic_vector(11 downto 0); -- global BCID (must be synced with front-ends)
        bcid_offset     : in  std_logic_vector(11 downto 0); -- when to send L1A?
        inhibit_des     : in  std_logic; -- inhibit deserialization
        trg_cktp        : in  std_logic; -- debugging
        alarm           : out std_logic; -- processor too slow
        buff_overflow   : out std_logic; -- throughput too big
        art_addr_udp    : out std_logic_vector(17 downto 0); -- to be sent through UDP
        art_valid_udp   : out std_logic; -- found valid addresses
        -------------------------------------
        ---------- UDP Interface ------------
        wrEn_LUTRAM      : in  std_logic; -- wr_en to RAM-LUT
        din_LUTRAM       : in  std_logic; -- din to RAM-LUT
        wrAddr_LUTRAM    : in  std_logic_vector(17 downto 0); -- write-address
        ------------------------------------
        ---------- VMM Interface -----------
        art_in0         : in  std_logic_vector(7 downto 0); -- ART board 0
        art_in1         : in  std_logic_vector(7 downto 0); -- ART board 1
        art_in2         : in  std_logic_vector(7 downto 0); -- ART board 2
        l1a_vmm_vec     : out std_logic_vector(3 downto 0); -- L1A signal
        l1a_vmm_all     : out std_logic                     -- L1A signal
    );
end trigger_processing_core;

architecture RTL of trigger_processing_core is

-- ART data deserializer
component art_des
    port(
        ------------------------------------
        ------- General Interface ----------
        clk_art         : in  std_logic; -- ART clock
        rst             : in  std_logic;
        inhibit_des     : in  std_logic;
        sreg_dbg        : out std_logic_vector(11 downto 0);
        ------------------------------------
        ---------- VMM Interface -----------
        art_in          : in  std_logic; -- ART line
        ------------------------------------
        -------- ART Sreg Interface --------
        art_word_out    : out std_logic_vector(6 downto 0); -- to shift register
        art_word_sreg   : in  std_logic_vector(6 downto 0)  -- value of first position of sreg
    );
end component;

-- shift-registers for the ART data
component art_sregs
    port(
        ------------------------------------
        ------- General Interface ----------
        clk_ckbc        : in  std_logic; -- CKBC
        clk_art         : in  std_logic; -- ART clock
        bcid_glbl       : in  std_logic_vector(11 downto 0); -- BCID counter
        rst             : in  std_logic;
        ------------------------------------
        -------- ART Des Interface ---------
        art_word_in0    : in  std_logic_vector(6 downto 0);  -- to shift register
        art_word_out0   : out std_logic_vector(6 downto 0);  -- from shift register
        
        art_word_in1    : in  std_logic_vector(6 downto 0);  -- to shift register
        art_word_out1   : out std_logic_vector(6 downto 0);  -- from shift register
        
        art_word_in2    : in  std_logic_vector(6 downto 0);  -- to shift register
        art_word_out2   : out std_logic_vector(6 downto 0);  -- from shift register
        -------------------------------------
        -------- FIFO Net Interface ---------
        wrEn_fifo0      : out std_logic_vector(4 downto 0);
        wrEn_fifo1      : out std_logic_vector(4 downto 0);
        wrEn_fifo2      : out std_logic_vector(4 downto 0);
        --
        din_fifo0       : out fifoData_main;
        din_fifo1       : out fifoData_main;
        din_fifo2       : out fifoData_main
    );
end component;

-- buffers of ART+BCIDs
component art_fifos
    port(
        ------------------------------------
        ------- General Interface ----------
        clk_ckbc        : in  std_logic; -- CKBC
        clk_art         : in  std_logic; -- ART clock
        clk_art2        : in  std_logic; -- ART clockx2
        rst             : in  std_logic;
        fifo_init       : in  std_logic;
        full_prior      : out std_logic;
        full_main0      : out std_logic_vector(4 downto 0);
        full_main1      : out std_logic_vector(4 downto 0);
        full_main2      : out std_logic_vector(4 downto 0);
        -------------------------------------
        -------- ART Sregs Interface --------
        wrEn_fifo0      : in  std_logic_vector(4 downto 0);
        wrEn_fifo1      : in  std_logic_vector(4 downto 0);
        wrEn_fifo2      : in  std_logic_vector(4 downto 0);
        --
        din_fifo0       : in  fifoData_main;
        din_fifo1       : in  fifoData_main;
        din_fifo2       : in  fifoData_main;        
        -------------------------------------
        --------- FIFO Interface ---------
        wrEn_FIFO     : out std_logic;
        dout_FIFO     : out std_logic_vector(53 downto 0)
    );
end component;

-- wrapper for the ramLUT FIFO and RAM
component art_ramLUT_wrapper
    port(
        ------------------------------------
        ------- General Interface ----------
        clk_ckbc        : in  std_logic; -- CKBC
        clk_art         : in  std_logic; -- ART clock
        clk_art2        : in  std_logic; -- ART clockx2
        clk_eth         : in  std_logic; -- Ethernet I/F clock
        rst             : in  std_logic;
        fifo_init       : in  std_logic;
        full_FIFO     : out std_logic;
        art_addr_udp    : out std_logic_vector(17 downto 0);
        -------------------------------------
        --------- ART-FIFOs Interface -------
        wrEn_FIFO     : in  std_logic;
        din_FIFO      : in  std_logic_vector(53 downto 0);
        -------------------------------------
        ---------- UDP Interface -----------
        wrEn_LUTRAM      : in  std_logic;
        din_LUTRAM       : in  std_logic;
        wrAddr_LUTRAM    : in  std_logic_vector(17 downto 0);
        -------------------------------------
        -------- L1A Buff Interface ---------
        l1a_buff_dout   : out l1aBuff_data;
        l1a_buff_wrEn   : out std_logic_vector(3 downto 0)
    );
end component;

-- L1A asserter
component l1a_buff_wrapper is
    port(
        ------------------------------------
        ------- General Interface ----------
        clk_ckbc        : in  std_logic; -- CKBC
        clk_art         : in  std_logic; -- ART clock
        clk_art2        : in  std_logic; -- ART clockx2
        bcid_glbl       : in  std_logic_vector(11 downto 0); -- BCID counter
        bcid_offset     : in  std_logic_vector(11 downto 0); -- when to send L1A?
        rst             : in  std_logic;
        fifo_init       : in  std_logic;
        alarm           : out std_logic; -- if we are too slow, raise an alarm
        l1a_buff_full   : out std_logic_vector(3 downto 0);
        -------------------------------------
        ------ RAM/LUT Wrapper Interface ----
        l1a_buff_din    : in  l1aBuff_data;
        l1a_buff_wrEn   : in  std_logic_vector(3 downto 0);
        -------------------------------------
        l1a_vmm         : out std_logic_vector(3 downto 0)
    );
end component;

-- supervisory module
component tp_supervisor
    port(
        ------------------------------------
        ------- General Interface ----------
        clk_ckbc        : in  std_logic; -- BC clock (40)
        clk_art         : in  std_logic; -- ART clock (160)
        clk_art2        : in  std_logic; -- ART clock x2 (320)
        rst             : in  std_logic; -- global reset
        ------------------------------------
        ----------- TP Interface -----------
        alarm           : in  std_logic;
        full_prior      : in  std_logic;
        full_main0      : in  std_logic_vector(4 downto 0);
        full_main1      : in  std_logic_vector(4 downto 0);
        full_main2      : in  std_logic_vector(4 downto 0);
        l1a_buff_full   : in  std_logic_vector(3 downto 0);
        full_FIFO     : in  std_logic;
        --
        alarm_o         : out std_logic;
        rst_tp          : out std_logic;
        buff_overflow   : out std_logic
    );
end component;

    -- interconnecting signals
    signal art_word_des0    : std_logic_vector(6 downto 0)  := (others => '0');
    signal art_word_sreg0   : std_logic_vector(6 downto 0)  := (others => '0');
    signal art_word_des1    : std_logic_vector(6 downto 0)  := (others => '0');
    signal art_word_sreg1   : std_logic_vector(6 downto 0)  := (others => '0');
    signal art_word_des2    : std_logic_vector(6 downto 0)  := (others => '0');
    signal art_word_sreg2   : std_logic_vector(6 downto 0)  := (others => '0');
    signal wrEn_fifo0       : std_logic_vector(4 downto 0)  := (others => '0');
    signal wrEn_fifo1       : std_logic_vector(4 downto 0)  := (others => '0');
    signal wrEn_fifo2       : std_logic_vector(4 downto 0)  := (others => '0');
    signal data_FIFO      : std_logic_vector(53 downto 0) := (others => '0');
    signal l1a_buff_wrEn    : std_logic_vector(3 downto 0)  := (others => '0');
    signal l1a_vmm_vec_i    : std_logic_vector(3 downto 0)  := (others => '0');
    signal l1a_vmm_all_i    : std_logic := '0';
    signal wrEn_FIFO      : std_logic := '0';
    signal wrEn_debug       : std_logic := '0';
    signal buff_overflow_i  : std_logic := '0';
    signal art_sreg         : artSregs_dbg;
    signal fifoData_0       : fifoData_main;
    signal fifoData_1       : fifoData_main;
    signal fifoData_2       : fifoData_main;
    signal l1a_buff_data    : l1aBuff_data;

    -- internal signals for supervisory module
    signal rst_i            : std_logic := '0';
    signal alarm_i          : std_logic := '0';
    signal full_prior       : std_logic := '0';
    signal full_main0       : std_logic_vector(4 downto 0);
    signal full_main1       : std_logic_vector(4 downto 0);
    signal full_main2       : std_logic_vector(4 downto 0);
    signal l1a_buff_full    : std_logic_vector(3 downto 0)  := (others => '0');
    signal full_FIFO      : std_logic := '0';

    signal art0_flag        : std_logic := '0';
    signal art1_flag        : std_logic := '0';
    signal art2_flag        : std_logic := '0';
    signal art0_cnt         : unsigned(2 downto 0) := (others => '0');
    signal art1_cnt         : unsigned(2 downto 0) := (others => '0');
    signal art2_cnt         : unsigned(2 downto 0) := (others => '0');
    signal art_flag_dbg     : std_logic_vector(4 downto 0) := (others => '0');
    
    -- debugging
component ila_tp
    port(
        clk     : in std_logic;
        probe0  : in std_logic_vector(70 downto 0)
    );
end component;

    --attribute mark_debug                    : string;
    --attribute mark_debug of art_word_sreg0  : signal is "TRUE";
    --attribute mark_debug of art_word_sreg1  : signal is "TRUE";
    --attribute mark_debug of art_word_sreg2  : signal is "TRUE";
    --attribute mark_debug of wrEn_fifo0      : signal is "TRUE";
    --attribute mark_debug of wrEn_fifo1      : signal is "TRUE";
    --attribute mark_debug of wrEn_fifo2      : signal is "TRUE";
    --attribute mark_debug of data_FIFO     : signal is "TRUE";
    --attribute mark_debug of rst             : signal is "TRUE";
    --attribute mark_debug of fifo_init       : signal is "TRUE";
    --attribute mark_debug of art_sreg        : signal is "TRUE";
    --attribute mark_debug of trg_cktp        : signal is "TRUE";
    --attribute mark_debug of art_word_des0   : signal is "TRUE";
    --attribute mark_debug of art_word_des1   : signal is "TRUE";
    --attribute mark_debug of art_word_des2   : signal is "TRUE";
    --attribute mark_debug of l1a_vmm_all_i   : signal is "TRUE";
    --attribute mark_debug of l1a_buff_wrEn   : signal is "TRUE";
    --attribute mark_debug of wrEn_debug      : signal is "TRUE";
    --attribute mark_debug of buff_overflow_i : signal is "TRUE";
    --attribute mark_debug of full_FIFO     : signal is "TRUE";
    --attribute mark_debug of full_prior      : signal is "TRUE";
    --attribute mark_debug of l1a_buff_full   : signal is "TRUE";
    --attribute mark_debug of full_main0      : signal is "TRUE";
    --attribute mark_debug of full_main1      : signal is "TRUE";
    --attribute mark_debug of full_main2      : signal is "TRUE";
    --attribute mark_debug of art_flag_dbg    : signal is "TRUE";

begin

-- ART data deserializer (board 0)
art_des0_inst: art_des
    port map(
        ------------------------------------
        ------- General Interface ----------
        clk_art         => clk_art,
        rst             => rst_i,
        inhibit_des     => inhibit_des,
        sreg_dbg        => art_sreg(0),
        ------------------------------------
        ---------- VMM Interface -----------
        art_in          => art_in0(0),
        ------------------------------------
        -------- ART Sreg Interface --------
        art_word_out    => art_word_des0,
        art_word_sreg   => art_word_sreg0
    );

-- ART data deserializer (board 1)
art_des1_inst: art_des
    port map(
        ------------------------------------
        ------- General Interface ----------
        clk_art         => clk_art,
        rst             => rst_i,
        inhibit_des     => inhibit_des,
        sreg_dbg        => art_sreg(1),
        ------------------------------------
        ---------- VMM Interface -----------
        art_in          => art_in1(0),
        ------------------------------------
        -------- ART Sreg Interface --------
        art_word_out    => art_word_des1,
        art_word_sreg   => art_word_sreg1
    );

-- ART data deserializer (board 2)
art_des2_inst: art_des
    port map(
        ------------------------------------
        ------- General Interface ----------
        clk_art         => clk_art,
        rst             => rst_i,
        inhibit_des     => inhibit_des,
        sreg_dbg        => art_sreg(2),
        ------------------------------------
        ---------- VMM Interface -----------
        art_in          => art_in2(0),
        ------------------------------------
        -------- ART Sreg Interface --------
        art_word_out    => art_word_des2,
        art_word_sreg   => art_word_sreg2
    );

-- shift-registers for the ART data
art_sregs_inst: art_sregs
    port map(
        ------------------------------------
        ------- General Interface ----------
        clk_ckbc        => clk_ckbc,
        clk_art         => clk_art,
        bcid_glbl       => bcid_glbl,
        rst             => rst_i,
        ------------------------------------
        -------- ART Des Interface ---------
        art_word_in0    => art_word_des0,
        art_word_out0   => art_word_sreg0,
        
        art_word_in1    => art_word_des1,
        art_word_out1   => art_word_sreg1,
        
        art_word_in2    => art_word_des2,
        art_word_out2   => art_word_sreg2,
        -------------------------------------
        -------- FIFO Net Interface ---------
        wrEn_fifo0      => wrEn_fifo0,
        wrEn_fifo1      => wrEn_fifo1,
        wrEn_fifo2      => wrEn_fifo2,
        --
        din_fifo0       => fifoData_0,
        din_fifo1       => fifoData_1,
        din_fifo2       => fifoData_2
    );

-- buffers of ART+BCIDs
art_fifos_inst: art_fifos
    port map(
        ------------------------------------
        ------- General Interface ----------
        clk_ckbc        => clk_ckbc,
        clk_art         => clk_art,
        clk_art2        => clk_art2,
        rst             => rst_i,
        fifo_init       => fifo_init,
        full_prior      => full_prior,
        full_main0      => full_main0,
        full_main1      => full_main1,
        full_main2      => full_main2,
        -------------------------------------
        -------- ART Sregs Interface --------
        wrEn_fifo0      => wrEn_fifo0,
        wrEn_fifo1      => wrEn_fifo1,
        wrEn_fifo2      => wrEn_fifo2,
        --
        din_fifo0       => fifoData_0,
        din_fifo1       => fifoData_1,
        din_fifo2       => fifoData_2,      
        -------------------------------------
        --------- FIFO Interface ---------
        wrEn_FIFO     => wrEn_FIFO,
        dout_FIFO     => data_FIFO
    );

-- wrapper for the ramLUT FIFO and RAM
art_ramLUT_wrapper_inst: art_ramLUT_wrapper
    port map(
        ------------------------------------
        ------- General Interface ----------
        clk_ckbc        => clk_ckbc,
        clk_art         => clk_art,
        clk_art2        => clk_art2,
        clk_eth         => clk_eth,
        rst             => rst_i,
        fifo_init       => fifo_init,
        full_FIFO     => full_FIFO,
        art_addr_udp    => art_addr_udp,
        -------------------------------------
        --------- ART-FIFOs Interface -------
        wrEn_FIFO     => wrEn_FIFO,
        din_FIFO      => data_FIFO,
        -------------------------------------
        ---------- UDP Interface -----------
        wrEn_LUTRAM      => wrEn_LUTRAM,
        din_LUTRAM       => din_LUTRAM,
        wrAddr_LUTRAM    => wrAddr_LUTRAM,
        -------------------------------------
        -------- L1A Buff Interface ---------
        l1a_buff_dout   => l1a_buff_data,
        l1a_buff_wrEn   => l1a_buff_wrEn
    );

-- L1A asserter
l1a_buff_wrapper_inst: l1a_buff_wrapper
    port map(
        ------------------------------------
        ------- General Interface ----------
        clk_ckbc        => clk_ckbc,
        clk_art         => clk_art,
        clk_art2        => clk_art2,
        bcid_glbl       => bcid_glbl,
        bcid_offset     => bcid_offset,
        rst             => rst_i,
        fifo_init       => fifo_init,
        alarm           => alarm_i,
        l1a_buff_full   => l1a_buff_full,
        -------------------------------------
        ------ RAM/LUT Wrapper Interface ----
        l1a_buff_din    => l1a_buff_data,
        l1a_buff_wrEn   => l1a_buff_wrEn,
        -------------------------------------
        l1a_vmm         => l1a_vmm_vec_i
    );

-- supervisory module and reset asserter
tp_supervisor_inst: tp_supervisor
    port map(
        ------------------------------------
        ------- General Interface ----------
        clk_ckbc        => clk_ckbc,
        clk_art         => clk_art,
        clk_art2        => clk_art2,
        rst             => rst,
        ------------------------------------
        ----------- TP Interface -----------
        alarm           => alarm_i,
        full_prior      => full_prior,
        full_main0      => full_main0,
        full_main1      => full_main1,
        full_main2      => full_main2,
        l1a_buff_full   => l1a_buff_full,
        full_FIFO     => full_FIFO,
        --
        alarm_o         => alarm,
        rst_tp          => rst_i,
        buff_overflow   => buff_overflow_i
    );

-- async signals
    l1a_vmm_vec     <= l1a_vmm_vec_i;
    l1a_vmm_all_i   <= l1a_vmm_vec_i(3) or l1a_vmm_vec_i(2) or l1a_vmm_vec_i(1) or l1a_vmm_vec_i(0);
    l1a_vmm_all     <= l1a_vmm_all_i;
    buff_overflow   <= buff_overflow_i;
    art_valid_udp   <= l1a_buff_wrEn(3) or l1a_buff_wrEn(2) or l1a_buff_wrEn(1) or l1a_buff_wrEn(0);
    
-- debugging
debug_trigProc_generate: if (debug_trigProc = '1') generate

ila_art: ila_tp
    port map(
        clk                     => clk_art,
        probe0(11 downto 0)     => art_sreg(0),
        probe0(23 downto 12)    => art_sreg(1),
        probe0(35 downto 24)    => art_sreg(2),
        probe0(36)              => trg_cktp,
        probe0(43 downto 37)    => art_word_des0,
        probe0(50 downto 44)    => art_word_des1,
        probe0(57 downto 51)    => art_word_des2,
        probe0(58)              => wrEn_debug,
        probe0(70 downto 59)    => (others => '0')
    );

ila_tp40: ila_tp
    port map(
        clk                     => clk_ckbc,
        probe0(6 downto 0)      => art_word_sreg0,
        probe0(13 downto 7)     => art_word_sreg1,
        probe0(20 downto 14)    => art_word_sreg2,
        probe0(25 downto 21)    => wrEn_fifo0,
        probe0(30 downto 26)    => wrEn_fifo1,
        probe0(35 downto 31)    => wrEn_fifo2,
        probe0(36)              => rst,
        probe0(37)              => fifo_init,
        probe0(38)              => trg_cktp,
        probe0(39)              => l1a_vmm_all_i,
        probe0(40)              => wrEn_debug,
        probe0(41)              => alarm_i,
        probe0(42)              => buff_overflow_i,
        probe0(43)              => full_FIFO,
        probe0(44)              => full_prior,
        probe0(48 downto 45)    => l1a_buff_full,
        probe0(53 downto 49)    => full_main0,
        probe0(58 downto 54)    => full_main1,
        probe0(63 downto 59)    => full_main2,
        probe0(68 downto 64)    => art_flag_dbg,
        probe0(70 downto 69)    => (others => '0')
    );

ila_tp160: ila_tp
    port map(
        clk                     => clk_art,
        probe0(53 downto 0)     => data_FIFO,
        probe0(57 downto 54)    => l1a_buff_wrEn,
        probe0(58)              => trg_cktp,
        probe0(59)              => wrEn_debug,
        probe0(70 downto 60)   => (others => '0')
    );

-- auxilliary monitoring process
aux_art_proc: process(clk_ckbc)
begin
    if(rising_edge(clk_ckbc))then
    
        -- art0
        if(art0_flag = '1')then
            art0_cnt <= art0_cnt + 1;
            if(art0_cnt = "111")then
                art0_flag <= '0';
            else
                art0_flag <= art0_flag;
            end if;
        elsif(art_word_sreg0 /= "0000000")then
            art0_flag <= '1';
            art0_cnt  <= (others => '0');
        else
            art0_flag <= '0';
            art0_cnt  <= (others => '0');
        end if;

        -- art1
        if(art1_flag = '1')then
            art1_cnt <= art1_cnt + 1;
            if(art1_cnt = "111")then
                art1_flag <= '0';
            else
                art1_flag <= art1_flag;
            end if;
        elsif(art_word_sreg1 /= "0000000")then
            art1_flag <= '1';
            art1_cnt  <= (others => '0');
        else
            art1_flag <= '0';
            art1_cnt  <= (others => '0');
        end if;

        -- art2
        if(art2_flag = '1')then
            art2_cnt <= art2_cnt + 1;
            if(art2_cnt = "111")then
                art2_flag <= '0';
            else
                art2_flag <= art2_flag;
            end if;
        elsif(art_word_sreg2 /= "0000000")then
            art2_flag <= '1';
            art2_cnt  <= (others => '0');
        else
            art2_flag <= '0';
            art2_cnt  <= (others => '0');
        end if;

    end if;
end process;

    art_flag_dbg(0) <= art0_flag and art1_flag;
    art_flag_dbg(1) <= art0_flag and art2_flag;
    art_flag_dbg(2) <= art1_flag and art2_flag;
    art_flag_dbg(3) <= art_flag_dbg(0) or art_flag_dbg(1) or art_flag_dbg(2);
    art_flag_dbg(4) <= art_flag_dbg(0) and art_flag_dbg(1) and art_flag_dbg(2);

    wrEn_debug <=   wrEn_fifo0(4) or wrEn_fifo0(3) or wrEn_fifo0(2) or wrEn_fifo0(1) or wrEn_fifo0(0) or
                    wrEn_fifo1(4) or wrEn_fifo1(3) or wrEn_fifo1(2) or wrEn_fifo1(1) or wrEn_fifo1(0) or
                    wrEn_fifo2(4) or wrEn_fifo2(3) or wrEn_fifo2(2) or wrEn_fifo2(1) or wrEn_fifo2(0);
                    
end generate debug_trigProc_generate;

end RTL;
