#======================= TIMING ASSERTIONS SECTION ====================
#============================= Primary Clocks =========================
#create_clock -period 5.000 -name sysclk_p   -waveform {0.000 2.500}  [get_ports SYSCLK_P]
create_clock -period 8.000 -name gtrefclk_p -waveform {0.000 4.000}  [get_ports GTREFCLK_P]

#============================= Virtual Clocks =========================
#============================= Generated Clocks =======================

# CLK_200_KHZ (from 50Mhz)
create_generated_clock -name clk_200_khz -source [get_pins mmcm_master_inst/inst/mmcm_adv_inst/CLKOUT0] -divide_by 4000000 [get_pins i2c_inst/I2C_module/CLK_200k_Hz_ub_reg/Q]

#======================= TIMING EXCEPTIONS SECTION ====================

#============================== Clock Groups ==========================


#=============================== False Paths ==========================
# resets
set_false_path -reset_path -from [get_cells startup_inst/phy_resetn_reg]
set_false_path -reset_path -from [get_cells trigProc_generate.trigger_processing_core_inst/tp_supervisor_inst/flag_rst_reg]
set_false_path -reset_path -from [get_cells udp_din_handler_inst/fpga_config_logic/fpga_rst_reg]

set_false_path -from [get_cells i2c_inst/I2C_CTRL_FSM/sync_50to200k/data_in_reg_reg[*]] -to [get_cells i2c_inst/I2C_CTRL_FSM/sync_50to200k/data_sync_stage_0_reg[*]]
set_false_path -from [get_cells vc709_eth_wrapper_inst/sync_50to125/data_in_reg_reg[*]] -to [get_cells vc709_eth_wrapper_inst/sync_50to125/data_sync_stage_0_reg[*]]

set_false_path -from [get_cells udp_din_handler_inst/CDCC_125to40/data_in_reg_reg[*]] -to [get_cells udp_din_handler_inst/CDCC_125to40/data_sync_stage_0_reg[*]]

set_false_path -from [get_cells udp_din_handler_inst/CDCC_125to40/data_out_s_int_reg[*]] -to [get_cells trigProc_generate.trigger_processing_core_inst/art_des0_inst/IDDR_ART]
set_false_path -from [get_cells udp_din_handler_inst/CDCC_125to40/data_out_s_int_reg[*]] -to [get_cells trigProc_generate.trigger_processing_core_inst/art_des1_inst/IDDR_ART]
set_false_path -from [get_cells udp_din_handler_inst/CDCC_125to40/data_out_s_int_reg[*]] -to [get_cells trigProc_generate.trigger_processing_core_inst/art_des2_inst/IDDR_ART]

#================================= Other ==============================
