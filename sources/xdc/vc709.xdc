#VC709 pinout mapping
#======================================================================
#====================== I/O Placement - IOSTANDARDS ===================

#--------- General ---------
set_property PACKAGE_PIN H19                [get_ports SYSCLK_P]
set_property IOSTANDARD DIFF_SSTL15         [get_ports SYSCLK_P]
set_property PACKAGE_PIN G18                [get_ports SYSCLK_N]
set_property IOSTANDARD DIFF_SSTL15         [get_ports SYSCLK_N]

set_property PACKAGE_PIN AK34               [get_ports USER_CLK_P]
set_property IOSTANDARD LVDS                [get_ports USER_CLK_P]
set_property PACKAGE_PIN AL34               [get_ports USER_CLK_N]
set_property IOSTANDARD LVDS                [get_ports USER_CLK_N]

#set_property PACKAGE_PIN AY18              [get_ports SYSCLK_233_P]
#set_property IOSTANDARD DIFF_SSTL15_DCI    [get_ports SYSCLK_233_P]
#set_property PACKAGE_PIN AY17              [get_ports SYSCLK_233_N]
#set_property IOSTANDARD DIFF_SSTL15_DCI    [get_ports SYSCLK_233_N]

# DS2.2
set_property PACKAGE_PIN AM39       [get_ports GPIO_LED[0]]
set_property IOSTANDARD LVCMOS18    [get_ports GPIO_LED[0]]
# DS3.2
set_property PACKAGE_PIN AN39       [get_ports GPIO_LED[1]]
set_property IOSTANDARD LVCMOS18    [get_ports GPIO_LED[1]]
# DS4.2
set_property PACKAGE_PIN AR37       [get_ports GPIO_LED[2]]
set_property IOSTANDARD LVCMOS18    [get_ports GPIO_LED[2]]
# DS5.2
set_property PACKAGE_PIN AT37       [get_ports GPIO_LED[3]]
set_property IOSTANDARD LVCMOS18    [get_ports GPIO_LED[3]]
# DS6.2
set_property PACKAGE_PIN AR35       [get_ports GPIO_LED[4]]
set_property IOSTANDARD LVCMOS18    [get_ports GPIO_LED[4]]
# DS7.2
set_property PACKAGE_PIN AP41       [get_ports GPIO_LED[5]]
set_property IOSTANDARD LVCMOS18    [get_ports GPIO_LED[5]]
# DS8.2
set_property PACKAGE_PIN AP42       [get_ports GPIO_LED[6]]
set_property IOSTANDARD LVCMOS18    [get_ports GPIO_LED[6]]
# DS9.2
set_property PACKAGE_PIN AU39       [get_ports GPIO_LED[7]]
set_property IOSTANDARD LVCMOS18    [get_ports GPIO_LED[7]]

#GPIO P.B. SW
# SW3
set_property PACKAGE_PIN AR40       [get_ports GPIO_SW[0]]
set_property IOSTANDARD LVCMOS18    [get_ports GPIO_SW[0]]
# SW4
set_property PACKAGE_PIN AU38       [get_ports GPIO_SW[1]]
set_property IOSTANDARD LVCMOS18    [get_ports GPIO_SW[1]]
# SW5
set_property PACKAGE_PIN AP40       [get_ports GPIO_SW[2]]
set_property IOSTANDARD LVCMOS18    [get_ports GPIO_SW[2]]
# SW6
set_property PACKAGE_PIN AV39       [get_ports GPIO_SW[3]]
set_property IOSTANDARD LVCMOS18    [get_ports GPIO_SW[3]]
# SW7
set_property PACKAGE_PIN AW40       [get_ports GPIO_SW[4]]
set_property IOSTANDARD LVCMOS18    [get_ports GPIO_SW[4]]

#GPIO DIP SW
set_property PACKAGE_PIN AV30       [get_ports GPIO_DIP[0]]
set_property IOSTANDARD LVCMOS18    [get_ports GPIO_DIP[0]]
set_property PACKAGE_PIN AY33       [get_ports GPIO_DIP[1]]
set_property IOSTANDARD LVCMOS18    [get_ports GPIO_DIP[1]]
set_property PACKAGE_PIN BA31       [get_ports GPIO_DIP[2]]
set_property IOSTANDARD LVCMOS18    [get_ports GPIO_DIP[2]]
set_property PACKAGE_PIN BA32       [get_ports GPIO_DIP[3]]
set_property IOSTANDARD LVCMOS18    [get_ports GPIO_DIP[3]]
set_property PACKAGE_PIN AW30       [get_ports GPIO_DIP[4]]
set_property IOSTANDARD LVCMOS18    [get_ports GPIO_DIP[4]]
set_property PACKAGE_PIN AY30       [get_ports GPIO_DIP[5]]
set_property IOSTANDARD LVCMOS18    [get_ports GPIO_DIP[5]]
set_property PACKAGE_PIN BA30       [get_ports GPIO_DIP[6]]
set_property IOSTANDARD LVCMOS18    [get_ports GPIO_DIP[6]]
set_property PACKAGE_PIN BB31       [get_ports GPIO_DIP[7]]
set_property IOSTANDARD LVCMOS18    [get_ports GPIO_DIP[7]]



# --------- CTF ---------
# J1 (CTF)
set_property PACKAGE_PIN M41   [get_ports CTF_CLK_P]
set_property IOSTANDARD LVDS   [get_ports CTF_CLK_P]
set_property PACKAGE_PIN L41   [get_ports CTF_CLK_N]
set_property IOSTANDARD LVDS   [get_ports CTF_CLK_N]
set_property DIFF_TERM TRUE    [get_ports CTF_CLK_P]

set_property PACKAGE_PIN N38   [get_ports CTF_RST_P]
set_property IOSTANDARD LVDS   [get_ports CTF_RST_P]
set_property PACKAGE_PIN M39   [get_ports CTF_RST_N]
set_property IOSTANDARD LVDS   [get_ports CTF_RST_N]
set_property DIFF_TERM TRUE    [get_ports CTF_RST_P]

set_property PACKAGE_PIN M42   [get_ports CTF_TRG_P]
set_property IOSTANDARD LVDS   [get_ports CTF_TRG_P]
set_property PACKAGE_PIN L42   [get_ports CTF_TRG_N]
set_property IOSTANDARD LVDS   [get_ports CTF_TRG_N]
set_property DIFF_TERM TRUE    [get_ports CTF_TRG_P]



# ------------ FE ------------
# J2 (FE0)
set_property PACKAGE_PIN J30   [get_ports FE_REFCLK_P[0]]
set_property IOSTANDARD LVDS   [get_ports FE_REFCLK_P[0]]
set_property PACKAGE_PIN H30   [get_ports FE_REFCLK_N[0]]
set_property IOSTANDARD LVDS   [get_ports FE_REFCLK_N[0]]

set_property PACKAGE_PIN J25   [get_ports FE_INHIBIT_P[0]]
set_property IOSTANDARD LVDS   [get_ports FE_INHIBIT_P[0]]
set_property PACKAGE_PIN J26   [get_ports FE_INHIBIT_N[0]]
set_property IOSTANDARD LVDS   [get_ports FE_INHIBIT_N[0]]

set_property PACKAGE_PIN C33   [get_ports FE_BUSY_P[0]]
set_property IOSTANDARD LVDS   [get_ports FE_BUSY_P[0]]
set_property PACKAGE_PIN C34   [get_ports FE_BUSY_N[0]]
set_property IOSTANDARD LVDS   [get_ports FE_BUSY_N[0]]

set_property PACKAGE_PIN P30   [get_ports FE_TTC_P[0]]
set_property IOSTANDARD LVDS   [get_ports FE_TTC_P[0]]
set_property PACKAGE_PIN N31   [get_ports FE_TTC_N[0]]
set_property IOSTANDARD LVDS   [get_ports FE_TTC_N[0]]

set_property PACKAGE_PIN R28   [get_ports FE_CKTP_P[0]]
set_property IOSTANDARD LVDS   [get_ports FE_CKTP_P[0]]
set_property PACKAGE_PIN P28   [get_ports FE_CKTP_N[0]]
set_property IOSTANDARD LVDS   [get_ports FE_CKTP_N[0]]

# J3 (FE1)
set_property PACKAGE_PIN U31   [get_ports FE_REFCLK_P[1]]
set_property IOSTANDARD LVDS   [get_ports FE_REFCLK_P[1]]
set_property PACKAGE_PIN T31   [get_ports FE_REFCLK_N[1]]
set_property IOSTANDARD LVDS   [get_ports FE_REFCLK_N[1]]

set_property PACKAGE_PIN M24   [get_ports FE_INHIBIT_P[1]]
set_property IOSTANDARD LVDS   [get_ports FE_INHIBIT_P[1]]
set_property PACKAGE_PIN L24   [get_ports FE_INHIBIT_N[1]]
set_property IOSTANDARD LVDS   [get_ports FE_INHIBIT_N[1]]

set_property PACKAGE_PIN R30   [get_ports FE_BUSY_P[1]]
set_property IOSTANDARD LVDS   [get_ports FE_BUSY_P[1]]
set_property PACKAGE_PIN P31   [get_ports FE_BUSY_N[1]]
set_property IOSTANDARD LVDS   [get_ports FE_BUSY_N[1]]

set_property PACKAGE_PIN L29   [get_ports FE_TTC_P[1]]
set_property IOSTANDARD LVDS   [get_ports FE_TTC_P[1]]
set_property PACKAGE_PIN L30   [get_ports FE_TTC_N[1]]
set_property IOSTANDARD LVDS   [get_ports FE_TTC_N[1]]

set_property PACKAGE_PIN M28   [get_ports FE_CKTP_P[1]]
set_property IOSTANDARD LVDS   [get_ports FE_CKTP_P[1]]
set_property PACKAGE_PIN M29   [get_ports FE_CKTP_N[1]]
set_property IOSTANDARD LVDS   [get_ports FE_CKTP_N[1]]

# J4 (FE2)
set_property PACKAGE_PIN M36   [get_ports FE_REFCLK_P[2]]
set_property IOSTANDARD LVDS   [get_ports FE_REFCLK_P[2]]
set_property PACKAGE_PIN L37   [get_ports FE_REFCLK_N[2]]
set_property IOSTANDARD LVDS   [get_ports FE_REFCLK_N[2]]

set_property PACKAGE_PIN N30   [get_ports FE_INHIBIT_P[2]]
set_property IOSTANDARD LVDS   [get_ports FE_INHIBIT_P[2]]
set_property PACKAGE_PIN M31   [get_ports FE_INHIBIT_N[2]]
set_property IOSTANDARD LVDS   [get_ports FE_INHIBIT_N[2]]

set_property PACKAGE_PIN R40   [get_ports FE_BUSY_P[2]]
set_property IOSTANDARD LVDS   [get_ports FE_BUSY_P[2]]
set_property PACKAGE_PIN P40   [get_ports FE_BUSY_N[2]]
set_property IOSTANDARD LVDS   [get_ports FE_BUSY_N[2]]

set_property PACKAGE_PIN H39   [get_ports FE_TTC_P[2]]
set_property IOSTANDARD LVDS   [get_ports FE_TTC_P[2]]
set_property PACKAGE_PIN G39   [get_ports FE_TTC_N[2]]
set_property IOSTANDARD LVDS   [get_ports FE_TTC_N[2]]

set_property PACKAGE_PIN F40   [get_ports FE_CKTP_P[2]]
set_property IOSTANDARD LVDS   [get_ports FE_CKTP_P[2]]
set_property PACKAGE_PIN F41   [get_ports FE_CKTP_N[2]]
set_property IOSTANDARD LVDS   [get_ports FE_CKTP_N[2]]

# J5 (FE3)
set_property PACKAGE_PIN F36   [get_ports FE_REFCLK_P[3]]
set_property IOSTANDARD LVDS   [get_ports FE_REFCLK_P[3]]
set_property PACKAGE_PIN F37   [get_ports FE_REFCLK_N[3]]
set_property IOSTANDARD LVDS   [get_ports FE_REFCLK_N[3]]

set_property PACKAGE_PIN C35   [get_ports FE_INHIBIT_P[3]]
set_property IOSTANDARD LVDS   [get_ports FE_INHIBIT_P[3]]
set_property PACKAGE_PIN C36   [get_ports FE_INHIBIT_N[3]]
set_property IOSTANDARD LVDS   [get_ports FE_INHIBIT_N[3]]

set_property PACKAGE_PIN F39   [get_ports FE_BUSY_P[3]]
set_property IOSTANDARD LVDS   [get_ports FE_BUSY_P[3]]
set_property PACKAGE_PIN E39   [get_ports FE_BUSY_N[3]]
set_property IOSTANDARD LVDS   [get_ports FE_BUSY_N[3]]

set_property PACKAGE_PIN G28   [get_ports FE_TTC_P[3]]
set_property IOSTANDARD LVDS   [get_ports FE_TTC_P[3]]
set_property PACKAGE_PIN G29   [get_ports FE_TTC_N[3]]
set_property IOSTANDARD LVDS   [get_ports FE_TTC_N[3]]

set_property PACKAGE_PIN B39   [get_ports FE_CKTP_P[3]]
set_property IOSTANDARD LVDS   [get_ports FE_CKTP_P[3]]
set_property PACKAGE_PIN A39   [get_ports FE_CKTP_N[3]]
set_property IOSTANDARD LVDS   [get_ports FE_CKTP_N[3]]

# J6 (FE4)
set_property PACKAGE_PIN P21   [get_ports FE_REFCLK_P[4]]
set_property IOSTANDARD LVDS   [get_ports FE_REFCLK_P[4]]
set_property PACKAGE_PIN N21   [get_ports FE_REFCLK_N[4]]
set_property IOSTANDARD LVDS   [get_ports FE_REFCLK_N[4]]

set_property PACKAGE_PIN K24   [get_ports FE_INHIBIT_P[4]]
set_property IOSTANDARD LVDS   [get_ports FE_INHIBIT_P[4]]
set_property PACKAGE_PIN K25   [get_ports FE_INHIBIT_N[4]]
set_property IOSTANDARD LVDS   [get_ports FE_INHIBIT_N[4]]

set_property PACKAGE_PIN M22   [get_ports FE_BUSY_P[4]]
set_property IOSTANDARD LVDS   [get_ports FE_BUSY_P[4]]
set_property PACKAGE_PIN L22   [get_ports FE_BUSY_N[4]]
set_property IOSTANDARD LVDS   [get_ports FE_BUSY_N[4]]

set_property PACKAGE_PIN P22   [get_ports FE_TTC_P[4]]
set_property IOSTANDARD LVDS   [get_ports FE_TTC_P[4]]
set_property PACKAGE_PIN P23   [get_ports FE_TTC_N[4]]
set_property IOSTANDARD LVDS   [get_ports FE_TTC_N[4]]

set_property PACKAGE_PIN H23   [get_ports FE_CKTP_P[4]]
set_property IOSTANDARD LVDS   [get_ports FE_CKTP_P[4]]
set_property PACKAGE_PIN G23   [get_ports FE_CKTP_N[4]]
set_property IOSTANDARD LVDS   [get_ports FE_CKTP_N[4]]

# J7 (FE5)
set_property PACKAGE_PIN J36   [get_ports FE_REFCLK_P[5]]
set_property IOSTANDARD LVDS   [get_ports FE_REFCLK_P[5]]
set_property PACKAGE_PIN H36   [get_ports FE_REFCLK_N[5]]
set_property IOSTANDARD LVDS   [get_ports FE_REFCLK_N[5]]

set_property PACKAGE_PIN L39   [get_ports FE_INHIBIT_P[5]]
set_property IOSTANDARD LVDS   [get_ports FE_INHIBIT_P[5]]
set_property PACKAGE_PIN L40   [get_ports FE_INHIBIT_N[5]]
set_property IOSTANDARD LVDS   [get_ports FE_INHIBIT_N[5]]

set_property PACKAGE_PIN E33   [get_ports FE_BUSY_P[5]]
set_property IOSTANDARD LVDS   [get_ports FE_BUSY_P[5]]
set_property PACKAGE_PIN D33   [get_ports FE_BUSY_N[5]]
set_property IOSTANDARD LVDS   [get_ports FE_BUSY_N[5]]

set_property PACKAGE_PIN E34   [get_ports FE_TTC_P[5]]
set_property IOSTANDARD LVDS   [get_ports FE_TTC_P[5]]
set_property PACKAGE_PIN E35   [get_ports FE_TTC_N[5]]
set_property IOSTANDARD LVDS   [get_ports FE_TTC_N[5]]

set_property PACKAGE_PIN F34   [get_ports FE_CKTP_P[5]]
set_property IOSTANDARD LVDS   [get_ports FE_CKTP_P[5]]
set_property PACKAGE_PIN F35   [get_ports FE_CKTP_N[5]]
set_property IOSTANDARD LVDS   [get_ports FE_CKTP_N[5]]

# --- art --- from RX0 (VMM4)
set_property PACKAGE_PIN D35   [get_ports FE_ART_P[0]]
set_property IOSTANDARD LVDS   [get_ports FE_ART_P[0]]
set_property PACKAGE_PIN D36   [get_ports FE_ART_N[0]]
set_property IOSTANDARD LVDS   [get_ports FE_ART_N[0]]

# J8 (FE6)
set_property PACKAGE_PIN E37   [get_ports FE_REFCLK_P[6]]
set_property IOSTANDARD LVDS   [get_ports FE_REFCLK_P[6]]
set_property PACKAGE_PIN E38   [get_ports FE_REFCLK_N[6]]
set_property IOSTANDARD LVDS   [get_ports FE_REFCLK_N[6]]

set_property PACKAGE_PIN D37   [get_ports FE_INHIBIT_P[6]] 
set_property IOSTANDARD LVDS   [get_ports FE_INHIBIT_P[6]]
set_property PACKAGE_PIN D38   [get_ports FE_INHIBIT_N[6]] 
set_property IOSTANDARD LVDS   [get_ports FE_INHIBIT_N[6]]

set_property PACKAGE_PIN G36   [get_ports FE_BUSY_P[6]]
set_property IOSTANDARD LVDS   [get_ports FE_BUSY_P[6]]
set_property PACKAGE_PIN G37   [get_ports FE_BUSY_N[6]]
set_property IOSTANDARD LVDS   [get_ports FE_BUSY_N[6]]

set_property PACKAGE_PIN K28   [get_ports FE_TTC_P[6]]
set_property IOSTANDARD LVDS   [get_ports FE_TTC_P[6]]
set_property PACKAGE_PIN J28   [get_ports FE_TTC_N[6]]
set_property IOSTANDARD LVDS   [get_ports FE_TTC_N[6]]

set_property PACKAGE_PIN J37   [get_ports FE_CKTP_P[6]]
set_property IOSTANDARD LVDS   [get_ports FE_CKTP_P[6]]
set_property PACKAGE_PIN J38   [get_ports FE_CKTP_N[6]]
set_property IOSTANDARD LVDS   [get_ports FE_CKTP_N[6]]

# --- art --- from RX0 (VMM4)
set_property PACKAGE_PIN M32   [get_ports FE_ART_P[1]]
set_property IOSTANDARD LVDS   [get_ports FE_ART_P[1]]
set_property PACKAGE_PIN L32   [get_ports FE_ART_N[1]]
set_property IOSTANDARD LVDS   [get_ports FE_ART_N[1]]

# J9 (FE7)
set_property PACKAGE_PIN G26   [get_ports FE_REFCLK_P[7]]
set_property IOSTANDARD LVDS   [get_ports FE_REFCLK_P[7]]
set_property PACKAGE_PIN G27   [get_ports FE_REFCLK_N[7]]
set_property IOSTANDARD LVDS   [get_ports FE_REFCLK_N[7]]

set_property PACKAGE_PIN K23   [get_ports FE_INHIBIT_P[7]] 
set_property IOSTANDARD LVDS   [get_ports FE_INHIBIT_P[7]]
set_property PACKAGE_PIN J23   [get_ports FE_INHIBIT_N[7]] 
set_property IOSTANDARD LVDS   [get_ports FE_INHIBIT_N[7]]

set_property PACKAGE_PIN T29   [get_ports FE_BUSY_P[7]]
set_property IOSTANDARD LVDS   [get_ports FE_BUSY_P[7]]
set_property PACKAGE_PIN T30   [get_ports FE_BUSY_N[7]]
set_property IOSTANDARD LVDS   [get_ports FE_BUSY_N[7]]

set_property PACKAGE_PIN N25   [get_ports FE_TTC_P[7]]
set_property IOSTANDARD LVDS   [get_ports FE_TTC_P[7]]
set_property PACKAGE_PIN N26   [get_ports FE_TTC_N[7]]
set_property IOSTANDARD LVDS   [get_ports FE_TTC_N[7]]

set_property PACKAGE_PIN H28   [get_ports FE_CKTP_P[7]]
set_property IOSTANDARD LVDS   [get_ports FE_CKTP_P[7]]
set_property PACKAGE_PIN H29   [get_ports FE_CKTP_N[7]]
set_property IOSTANDARD LVDS   [get_ports FE_CKTP_N[7]]

# --- art --- from RX0 (VMM4)
set_property PACKAGE_PIN K27   [get_ports FE_ART_P[2]]
set_property IOSTANDARD LVDS   [get_ports FE_ART_P[2]]
set_property PACKAGE_PIN J27   [get_ports FE_ART_N[2]]
set_property IOSTANDARD LVDS   [get_ports FE_ART_N[2]]


# --------- Ethernet -----------
# Quad 113
# MGTREFCLK0 (from Si5324)
# (SI5324)
set_property PACKAGE_PIN AH8        [get_ports GTREFCLK_P]
set_property PACKAGE_PIN AH7        [get_ports GTREFCLK_N]

# GTHE2_CHANNEL_X1Y12
# SFP+4 (cage P5)
set_property PACKAGE_PIN AL2        [get_ports TXP]
set_property PACKAGE_PIN AL1        [get_ports TXN]
set_property PACKAGE_PIN AJ6        [get_ports RXP]
set_property PACKAGE_PIN AJ5        [get_ports RXN]

set_property PACKAGE_PIN AW32       [get_ports SI5324_REFCLK_P]
set_property IOSTANDARD LVDS        [get_ports SI5324_REFCLK_P]
set_property PACKAGE_PIN AW33       [get_ports SI5324_REFCLK_N]
set_property IOSTANDARD LVDS        [get_ports SI5324_REFCLK_N]

set_property PACKAGE_PIN AU34       [get_ports SI5324_INTRPT]
set_property IOSTANDARD LVCMOS18    [get_ports SI5324_INTRPT]

set_property PACKAGE_PIN AT36       [get_ports SI5324_RESETB]
set_property IOSTANDARD LVCMOS18    [get_ports SI5324_RESETB]

set_property PACKAGE_PIN AC40       [get_ports SFP_TX_DISABLE]
set_property IOSTANDARD LVCMOS18    [get_ports SFP_TX_DISABLE]



# ---------- I2C -------------
set_property PACKAGE_PIN AT35       [get_ports SCL]
set_property IOSTANDARD LVCMOS18    [get_ports SCL]

set_property PACKAGE_PIN AU32       [get_ports SDA]
set_property IOSTANDARD LVCMOS18    [get_ports SDA]

set_property PACKAGE_PIN AY42       [get_ports I2C_MUX_RESETB]
set_property IOSTANDARD LVCMOS18    [get_ports I2C_MUX_RESETB]

set_property PULLUP TRUE            [get_ports SDA]
set_property PULLUP TRUE            [get_ports SCL]

# ---------- bitstream-related ----

set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]
set_property CONFIG_MODE BPI16 [current_design]
